<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Response;
use GuzzleHttp\Client;
use Excel;

class PredictController extends Controller
{
    public function createPredict( Request $request )
    {
        $predict_id = time().rand( 10,99 );
        $request->merge( array( 'predict_id' => $predict_id ) );
        
    	User::where( '_id' , Auth::user()->id )->push( 'predict' , [ 
			'predict_id'	        => $predict_id,
            'date_created'          => date('m-d-Y h:i A'),             
            'product_name'          => $request->get( 'product_name' ),         
			'campaign_name'	        => $request->get( 'campaign_name' ),			
            'product_category'      => $request->get( 'product_category' ), 
            'gender'                => $request->get( 'gender' ),
            'mediaplatform'         => $request->get( 'mediaplatform' ),
            'objective'             => $request->get( 'objective' ), 
            'hashtags'              => $request->get( 'hashtags' ), 
            'country'               => $request->get( 'country' ),   
            'status'                => 'In Progress'
    	] );
        echo json_encode( $request->all() );
    }
    public function adminDashbordPredict()
    {
    	echo json_encode( User::all() );
    }
    public function getClientExperiments( $id )
    {
        // LIST DOWN ALL EXPERIMENTS FOR ALL USERS - *done
        // PUT IT ON THE SAME ARRAY - *done

        $data = User::where( 'company' , $id )->get();
        $dataArr = array();
        foreach ($data as $key) {
            if ( $key['predict'] ) {
                foreach ( $key['predict'] as $predictkey) {
                    $predictkey['client_id'] = $key['_id'];
                    $dataArr[] = $predictkey;
                }
            }
        }
        echo json_encode( $dataArr );
    }
    public function addImageToExperiment( Request $request )
    {
        $result = DB::collection('users')->where( '_id', $request->get( 'client_id' ) )->pull('predict', [ 'predict_id' => $request->get( 'predict_id' ) ] );
        User::where( '_id' , $request->get( 'client_id' ) )->push( 'predict' , [ 
            'predict_id'            => $request->get( 'predict_id' ),    
            'date_created'          => $request->get( 'date_created' ),  
            'product_name'          => $request->get( 'product_name' ),         
            'product_category'      => $request->get( 'product_category' ), 
            'gender'                => $request->get( 'gender' ),
            'mediaplatform'         => $request->get( 'mediaplatform' ),
            'objective'             => $request->get( 'objective' ), 
            'hashtags'              => $request->get( 'hashtags' ), 
            'country'               => $request->get( 'country' ),
            'filename'              => $request->get( 'filename' ),
            'image_lib'             => $request->get( 'image_lib' ),
            'status'                => 'Completed'
        ] ); 
        echo json_encode( $request->all() );
    }
    public function removeExperiment( Request $request )
    {
        $response = DB::collection('users')->where( '_id', $request->get( 'client_id' ) )->pull('predict', [ 'predict_id' => $request->get( 'predict_id' ) ] );
        echo json_encode( $response );
    }
    public function downloadBrief($num)
    {
        if( $num == 0 ){
            $download_path = ( public_path() . '/file_downloads/brief_report.pdf' );   
            return( Response::download( $download_path ) ); 
        }else if( $num == 1 ){
            $download_path = ( public_path() . '/file_downloads/brief_report2.pdf' );
            return( Response::download( $download_path ) );
        }else if( $num == 2 ){
            $download_path = ( public_path() . '/file_downloads/brief_report3.pdf' );
            return( Response::download( $download_path ) );
        }
        // return response()->download( asset( '/file_downloads/brief_report.pptx' ) );
    }
    public function updateValue()
    {
        $arr_data = array(
            'client_id'     => '56c30501317c3d76bf8b4569',
            'predict_id'    => '145871526987',
            'date_created'  => '03-20-2016 02:39 PM' //data you wanted to insert
        );

        DB::collection('users')->where( '_id', $arr_data['client_id'] )->pull('predict', [ 'predict_id' => $arr_data['predict_id'] ] );
        User::where( '_id' , $arr_data['client_id'] )->push( 'predict' , [ 
            'predict_id'            => $arr_data['predict_id'],    
            'date_created'          => $arr_data['date_created'],  
            'product_name'          => 'Mobile Plan',         
            'product_category'      => null, 
            'gender'                => 'Both',
            'mediaplatform'         => 'facebook',
            'objective'             => null, 
            'hashtags'              => '#Sports Car Photoshot, #Sports, #Entertainment', 
            'country'               => 'Australia',
            'status'                => 'In Progress'
        ] ); 


        // Second Batch
        $arr_data2 = array(
            'client_id'     => '56c30501317c3d76bf8b4569',
            'predict_id'    => '145871626469',
            'date_created'  => '12-03-2016 04:12 PM' //data you wanted to insert
        );

        DB::collection('users')->where( '_id', $arr_data2['client_id'] )->pull('predict', [ 'predict_id' => $arr_data2['predict_id'] ] );
        User::where( '_id' , $arr_data2['client_id'] )->push( 'predict' , [ 
            'predict_id'            => $arr_data2['predict_id'],    
            'date_created'          => $arr_data2['date_created'],  
            'product_name'          => 'Yogurt drinks',         
            'product_category'      => null, 
            'gender'                => 'Female',
            'mediaplatform'         => 'facebook',
            'objective'             => null, 
            'hashtags'              => '#Health, #Foods, #Summer', 
            'country'               => 'Australia',
            'status'                => 'In Progress'
        ] ); 

    }

    public function extractCSV() 
    {
        // $csv = public_path() . '/csv/b1.csv';
        $csv = public_path() . '/csv/c_serie_out.csv';

        $arr = array_map('str_getcsv', file($csv));

        $container = array();

        $api_key = '8204f65235dacdfe68d2bedd443b1b7e';
        
        $api_url = 'http://api.graymatics.com/grayit/process/image/ondemand';

        foreach ($arr as $key => $data) {
            $post_args = array( 'API_KEY'   =>  $api_key,
                                'URL'       =>  base64_encode($data[1]),
                            );
            $curl = curl_init($api_url); 

            curl_setopt($curl, CURLOPT_POST, true); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_args); 

            // this is needed
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $result = json_decode(curl_exec($curl), true);

            curl_close($curl);

            $container[] = $result;

            // $container[$key] = array(
            //     'url' => $result['url'],
            // );

            // if (isset( $result['results'][0] ) && $result['results'][0]['aesthetics']) {
            //     foreach ( $result['results'][0]['aesthetics'] as $key2 => $res ) {
            //         $container[$key][$key2] = $key2. ' :  ' . (string) $res;
            //     }
            // }
            // if (isset( $result['results'][0] ) && $result['results'][0]['attributes']) {
            //     foreach ( $result['results'][0]['attributes'] as $key2 => $res ) {
            //         foreach ($res as $key3 => $res3) {
            //             $container[$key][$key3] = $res3;
            //             if ( $key3 == 'genre' ) {
            //                 foreach ($res3 as $key4 => $res4) {
            //                     $container[$key][$key3] = $key3 . ' :  ' . $res4;
            //                 }
            //             }else {
            //                 $container[$key][$key3] = '';
            //             } if( $key3 == 'style' ) {
            //                 foreach ($res3 as $key4 => $res4) {
            //                     $container[$key][$key3] = $key3 . ' :  ' . $res4;
            //                 }
            //             }else {
            //                 $container[$key][$key3] = '';
            //             }
            //         }
            //     }
            // }
            // if (isset( $result['results'][0] ) && $result['results'][0]['color']) {
            //     if ($result['results'][0]['color']['colorpalette']) {
            //         foreach ( $result['results'][0]['color']['colorpalette']['colors'] as $key2 => $res ) {
            //             $container[$key]['color' . ($key2+1)] = 'color' . ($key2+1) . ' :  ' .$res[1];
            //         }
            //     }
            // }
            // if (isset($result['categorization']) && $result['categorization'] ) {
            //     foreach ( $result['categorization']['IAB'] as $key2 => $res ) {
            //         foreach ($res as $key3 => $res3) {
            //             $container[$key]['categorization '. ($key3 + 1 )] = 'categorization '. ($key3 + 1 ) . ' :  ' . $res3;
            //         }
            //     }
            // }

        }

        // Excel::create('Graymatic Result', function($excel) use($container) {

        //     $excel->sheet('Sheetname', function($sheet) use($container) {

        //         $sheet->fromArray( $container );

        //     });

        // })->export('csv');

        // We'll be outputting a text file
        header('Content-type: text/plain');

        // It will be called report.txt
        header('Content-Disposition: attachment; filename="c_serie_out.json"');


        echo stripslashes(json_encode( $container , JSON_PRETTY_PRINT ));

    }
}

