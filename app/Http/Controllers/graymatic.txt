{
  "url": "http://scontent.cdninstagram.com/t51.2885-15/s750x750/sh0.08/e35/13652235_481909215340709_1427675190_n.jpg",
  "status": "true",
  "error_code": 0,
  "transaction_id": "i:da52a757dbc5905de28f62d14062a2339d178a14",
  "iprofile_id": "default",
  "message": "Already Analysed",
  "analysis_duration": 9,
  "timestamp": "2016-07-21 01-19-50",
  "results": [
    {
      "status": "success",
      "attributes": [
        {
          "genre": [
            "cloudscene"
          ],
          "object": "image_genre"
        },
        {
          "confidence_index": 0.427,
          "style": [
            " style_Serene ",
            " style_Sunny ",
            " style_Romantic "
          ],
          "object": "imagestyle"
        }
      ],
      "context_connect_data": {
        "category_marginal_theme": [
          "autumnwinter",
          "cloudy",
          "snow"
        ],
        "category_dominant_theme": [
          "sky_blue",
          "daytime"
        ],
        "category": {
          "outdoors": [
            {
              "confidence_index": 0.437,
              "object": "outdoor",
              "type": "bridge"
            }
          ]
        },
        "text": [
          {
            "text": {
              "text": []
            },
            "confidence_index": "high"
          }
        ]
      },
      "aesthetics": {
        "hue": 4.19,
        "aesthetic_value": 0.6,
        "object": "aesthetics",
        "animation_index": 0,
        "arousal": -1,
        "valence": -1,
        "image_quality": 0.8
      },
      "color": {
        "colorpalette": {
          "colors": [
            [
              [
                24.94756821
              ],
              "Light gray"
            ],
            [
              [
                22.26642942
              ],
              "Dark gray"
            ],
            [
              [
                15.95041518
              ],
              "Blue"
            ],
            [
              [
                15.00616845
              ],
              "Gray"
            ]
          ],
          "object": "colorpalette"
        }
      }
    }
  ],
  "categorization": {
    "IAB": {
      "tier1": [
        "places"
      ],
      "tier2": [
        "weather"
      ]
    }
  },
  "updated_at": "2016-07-21 18:48:15",
  "created_at": "2016-07-21 18:48:15",
  "_id": "5790a86f6cbbc65b0d8b4567"
}