<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Upload;

use App;

use Excel;

class DashboardController extends Controller
{
    public function index()
    {
    	return view('dashboard.admin');
    }
    public function getClarifaiData()
    {
        // echo "<pre>";
    	// echo var_dump(Upload::where( 'category' , 'api_upload' )->get());
        // echo "</pre>";
        $data_array = array();

        // $data = Upload::where( 'category' , 'api_upload' )->select( 'filename' , 'clarifai' )->get();
        // foreach ($data as $key => $img) {
        //     // echo "Filename:" .$img['filename']. "<br>";
        //     array_push( $data_array , $img['filename']);
        //     foreach ($img['clarifai'] as $key2 => $clfy) {
        //         array_push( $data_array , $clfy);
        //     }
        // }
        // $xls_arr = array();
        // $xls_arr['filename'] = $data_array[0];
        // $xls_arr['clarifai'] = $data_array[1];
        echo "<pre>";
        var_dump( $data );
        echo "</pre>";
    }
    public function exportCSV()
    {
        $data = Upload::where( 'category' , 'api_upload' )->select( 'filename' , 'clarifai' )->get();

        $data_array = array();

        foreach ($data as $key => $value) {
            // echo "<pre>";
            // var_dump( $value['clarifai']['data'] );
            // echo "</pre>";
            foreach ($value['clarifai']['data'] as $key => $value2) {
                // echo "<pre>";
                // var_dump( $value2 );
                // echo "</pre>";
                array_push( $data_array , 
                    array( 
                        'FILENAME'      => $value['filename'],
                        'CLASS'         => $value2['class'],                
                        'PROBABILITY'   => $value2['probability']                
                    )
                );
            }
        }




        Excel::create('clarfiy', function( $excel ) use( $data_array ) {
            $excel->sheet('Clarifai', function($sheet) use ( $data_array ) {
                $sheet->fromArray($data_array);
            });
        })->export('xls');
       
    }
}
