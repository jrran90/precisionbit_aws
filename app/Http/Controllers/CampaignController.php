<?php
namespace App\Http\Controllers;

use Mail;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Campaign;

class CampaignController extends Controller
{
    protected $model;
    
    public function __construct()
    {
        $this->model = new Campaign();
    }
    public function createBriefCampaign( Request $request )
    {
        $response = $this->model->saveBriefCampaign( $request->all() );

        Mail::send('emails.create_campaign', [], function ($message) {
            // $message->from(env('MAIL_FROM'), 'Test Sender');
            // $message->to('billy.ranario@must.edu.ph')->subject('testing SES');
            $message->to( session('user.email')[0] )->subject('AdHero');
            // $message->to('jhon@youradhero.com')->subject('testing SES');
            // $message->to('jr.ran90@gmail.com')->subject('testing SES');
        });

        return $response;
    }
    public function updateCampaign( Request $request )
    {
        $response = $this->model->updateCampaign( $request->all() );

        if ( $request->status == 'in-progress' ) {
            Mail::send('emails.paid', [], function ($message) {
                // $message->from(env('MAIL_FROM'), 'Test Sender');
                // $message->to('billy.ranario@must.edu.ph')->subject('testing SES');
                $message->to( session('user.email')[0] )->subject('AdHero');
                // $message->to('jhon@youradhero.com')->subject('testing SES');
                // $message->to('jr.ran90@gmail.com')->subject('testing SES');
            });
        }

        return $response;
    }
    public function deleteCampaign( $id )
    {
        $response = $this->model->deleteCampaign( $id );
        return $response;
    }
    public function countCampaigns( $id )
    {
        return $this->model->checkForCampaign( $id );
    }
    public function fetchCampaignBStatus()
    {   
        $campaign_arr = array(
            'active'        =>  $this->model->retrieveUserCampaignByStatus( 'active' ),
            'in-progress'   =>  $this->model->retrieveUserCampaignByStatus( 'in-progress' ),
            'pending'       =>  $this->model->retrieveUserCampaignByStatus( 'pending' ),
        ); 
        return $campaign_arr;
    }
    public function store( Request $request )
    {
        //
    }
    public function show($id)
    {
        $response = $this->model->getUserCampaign( $id );
        return $response;
    }
    public function edit($id)
    {
        //
    }
    public function update( Request $request, $id )
    {
        //;
    }
    public function destroy($id)
    {
        // 
    }
    
    /*
    *   Email Notifications
    */
    public function sendLaunchCampaignEmail()
    {
        Mail::send('emails.launch_campaign', [], function ($message) {
            // $message->from(env('MAIL_FROM'), 'Test Sender');
            // $message->to('billy.ranario@must.edu.ph')->subject('testing SES');
            $message->to( session('user.email')[0] )->subject('AdHero');
            // $message->to('jhon@youradhero.com')->subject('testing SES');
            // $message->to('jr.ran90@gmail.com')->subject('testing SES');
        });

        return 'success';
    }
}
