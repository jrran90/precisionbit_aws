<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Excel;
use App\Instaprofile;


class ReportController extends Controller
{

    public function index(Request $request)
    {
        $criteria = [
            ['$unwind' => '$tags'],
            ['$project' => [
                    'field' => '$tags.details.field',
                    'value' => '$tags.details.value',
                    'type'  => '$tags.type',
                    'unique_ctr' => 1,
                    'ad_id' => 1,
                    'ad_number' => 1,
                    'target_country' => 1,
                    'target_gender'  => 1,
                    'campaign_name'  => 1,
                    'start'          => 1,
                    'end'            => 1,
                    'impression'     => 1,
                    'reach'          => 1,
                    'cpc'            => 1,
                    'product_filter' => 1
                ]
            ]
        ];

        if ($request->get('filter') != 'all') {
            array_push($criteria, ['$match'  => [
                    'field'             => $request->get('type'),
                    'product_filter'    => $request->get('filter')
                ]
            ]);
        } else {
            array_push($criteria, ['$match'  => [
                    'field'     => $request->get('type'),
                ]
            ]);
        }

        return \DB::collection('dummy_reports')->raw()->aggregate($criteria);

    }

    public function tags(Request $request)
    {
        $criteria = [
            ['$project' => [
                    'value' => '$tags.details.value',
                    'type'  => '$tags.type',
                    'ad_id' => 1
                ]
            ],
            ['$unwind' => '$type'],
            ['$match'  => [
                            'type'  => $request->get('type'),
                            'ad_id' => $request->get('ad_id')
                ]
            ],
        ];

        return \DB::collection('dummy_reports')->raw()->aggregate($criteria);       
    }


    public function generateExcel()
    {
        $criteria = [
            ['$project' => [
                    '_id'       => 0,
                    'image'     => '$clarifai.image',
                    'tags'      => '$clarifai.data.class',
                    'scores'    => '$clarifai.data.probability'
                ]
            ]
        ];

        return \DB::collection('uploads')->raw()->aggregate($criteria);
    }


    // Billy - Start
    public function importFileData()
    {
        ini_set('max_execution_time', 360);
        // Delete the contents first exists in database
        \DB::collection('insta_profile')->delete();

        $xls = public_path() . '/csv/all_user_info.xlsx';

        $data = Excel::load( $xls , function($reader) {
        })->get();

        // var_dump( $data );
        $insert_data = [];

        if(!empty($data) && $data->count()) {
            foreach ($data as $key => $d) {
                $insert_data[] = array(
                    'profile_name'      =>  $d->profile_name,
                    'gender'            =>  $d->gender,
                    'total_posts'       =>  $d->total_posts,
                    'total_followers'   =>  $d->total_followers,
                    'total_following'   =>  $d->total_following,
                    'location'          =>  $d->location,
                    'profile_link'      =>  $d->profile_link
                );
            }
        }

        \DB::collection('insta_profile')->insert($insert_data);
        // return $insert_data;
        
    }
    public function findByLocation( Request $request )
    {
        $data = array(
            'data' => \DB::collection('insta_profile')->where( $request->get( 'type' ) , $request->get( 'location' ) )->get(),
            'directory' => $request->get( 'location' )

        );
        return $data;
    }
    public function getUserDataFile( Request $request )
    {
        $csv_content = array();

        // $csv = public_path() . '/csv/starhub/' . $request->get( 'folder' ) . '/' . $request->get( 'location' ) . '.csv';
        $csv = public_path() . '/csv/all_users/' . $request->get( 'location' ) . '.csv';

        if ( !file_exists($csv) ) { // If Excel File
            $csv = public_path() . '/csv/all_users/' . $request->get( 'location' ) . '.csv';
            $arr = array_map('str_getcsv', file($csv));
            // $csv = public_path() . '/csv/starhub/' . $request->get( 'folder' ) . '/' . $request->get( 'location' ) . '.xlsx';
            // $arr = Excel::load( $csv , function($reader) {} )->get();
        } else { // If CSV file
            $arr = array_map('str_getcsv', file($csv));
        }
        
        $current_image_route = '';
        $index = 0;
        // For Image link

        // return $arr;
        // $unique_ids = array();

        // $fd = fopen($csv, 'r');

        // // return array_map($fd); 

        // while ($row = fgetcsv($fd)) {
        //     // change the 3 to the column you want
        //     // using the keys of arrays to make final values unique since php
        //     // arrays cant contain duplicate keys
        //     $unique_ids[$row[0]] = true;
        //     // $unique_ids[$row[1]] = true;
        // }

        // return array_keys($unique_ids);

        foreach ($arr as $key => $data) {
            if ( $data[0] == null || $data[0] == 'url' ) {
                $data[0] = $current_image_route;
            } else {
                $current_image_route = $data[0];
                $index += 1;
            }

            if ( (isset( $data[3] ) == null ) || (isset( $data[4] ) == null ) ) {
                $data[3] = '';
                $data[4] = '';
            }

            $csv_content[] = array(
                'url'   => isset( $data[0] ) ? $data[0] : '',
                'class' => isset( $data[1] ) ? $data[1] : '',
                'score' => isset( $data[2] ) ? $data[2] : '',

                // 'primary_passion_point_a'   => isset( $data[3] ) ? $data[3] : null,
                // 'secondary_passion_point_a' => isset( $data[4] ) ? $data[4] : null,
                // 'primary_passion_point_b'   => isset( $data[5] ) ? $data[5] : null,
                // 'secondary_passion_point_b' => isset( $data[6] ) ? $data[6] : null,

                'primary_passion_point_1'   => isset( $data[3] ) ? $data[3] : null,
                'primary_passion_point_2'   => isset( $data[4] ) ? $data[4] : null,
                'primary_passion_point_3'   => isset( $data[5] ) ? $data[5] : null,
                'primary_passion_point_4'   => isset( $data[6] ) ? $data[6] : null,
                
                'secondary_passion_point_1'   => isset( $data[7] ) ? $data[7] : null,
                'secondary_passion_point_2'   => isset( $data[8] ) ? $data[8] : null,
                'secondary_passion_point_3'   => isset( $data[9] ) ? $data[9] : null,
                'secondary_passion_point_4'   => isset( $data[10] ) ? $data[10] : null,


                'interest' => isset($data[11]) ? $data[11] : null,
                'index' => $index
            );
        }
        $result = array();
        foreach ( $csv_content as $key => $data ) {
            if ( $data[ 'index' ] != null ) {
                $result[ $data[ 'index' ] ][] =  $data;
            }
        }

        if ( $request->get( 'folder' ) == 'rod' ) {
            $db_data = \DB::collection('starhub_data')->where( $request->get( 'type' ) , $request->get( 'location' ) )->get();
        } else {
            $db_data = \DB::collection('insta_profile')->where( $request->get( 'type' ) , $request->get( 'location' ) )->get();
        }

        $data = array(
            'data'          => $db_data,
            'directory'     => $request->get( 'location' ), 
            'csv_content'   => $result, //Removing the first index of the array,
            'csv_count'     => count( $result )
        );

        return $data;
    }

    public function getAllPassionData( Request $request )
    {

        $user_list = [];
        
        foreach ( $request->get( 'users' ) as $key => $user) {
            if ( \DB::collection('passion_points')->where( 'user' , $user )->where( 'passion' , 'like' , '%'.$request->get( 'passion' ).'%' )->count() > 0 ) {
               $user_list[] = \DB::collection('passion_points')->where( 'user' , $user )->where( 'passion' , 'like' , '%'.$request->get( 'passion' ).'%' )->first();
            }
        }

        return $user_list;
    }

    public function getPassionData( $passion )
    {
        // return \DB::collection('passion_points')->where( 'passion' , 'like' ,  '%'.$passion.'%' )->get();
        return \DB::collection('passion_points')->where( 'passion' , $passion )->get();
    }
    public function locationUpdate( Request $request )
    {
        // Update location in CSV
        $csv = public_path() . '/csv/starhub/rod/' . $request->get( 'user' ) . '.csv';

        $csv_content_old = array();

        $arr = array_map('str_getcsv', file($csv));

        foreach ($arr as $key => $data) {
            if ( !$data[0] ) {
                $data[0] = "";
            }
            if ( (isset( $data[3] ) == null ) || (isset( $data[4] ) == null ) || (isset( $data[5] ) == null ) || (isset( $data[6] ) == null ) ) {
                $data[3] = '';
                $data[4] = '';
                $data[5] = '';
                $data[6] = '';
            }
            $csv_content_old[$key] = array(
                'url'       => $data[0],
                'class'     => $data[1],
                'score'     => $data[2],
                'passion'   => $data[3],
                'interest'  => $data[4],
                'location'  => $data[5],
                'interest_area'  => $data[6],
            );

            if ( $request->get( 'url' ) == $data[0] ) {
                $csv_content_old[$key]['location'] = $request->get( 'location' );
            }
        }

        $csv_content_old[0] = array(
            'URL' , 'CLASS' , 'SCORE' , 'PASSION' , 'INTEREST' , 'LOCATION' , 'INTEREST AREA'
        );

        // return $csv_content_old;

        // Open CSV file for update
        $fp = fopen( $csv , 'w');
        foreach ( $csv_content_old as $fields ) {
            fputcsv( $fp , $fields );
        }
        fclose($fp);

        // Update location in database
        return \DB::collection('passion_points')->where( 'permalink' , $request->get('data')['permalink'] )->update( [
                'location' => $request->get( 'location' )
            ] );
        
        // return $request->get('data')['permalink'];
    }
    public function interestUpdate( Request $request )
    {
        // Update location in CSV
        $csv = public_path() . '/csv/starhub/rod/' . $request->get( 'user' ) . '.csv';

        $csv_content_old = array();

        $arr = array_map('str_getcsv', file($csv));

        foreach ($arr as $key => $data) {
            if ( !$data[0] ) {
                $data[0] = "";
            }
            if ( (isset( $data[3] ) == null ) || (isset( $data[4] ) == null ) || (isset( $data[5] ) == null ) ) {
                $data[3] = '';
                $data[4] = '';
                $data[5] = '';
            }
            if ( (isset( $data[6] ) == null ) ) {
                $data[6] = '';
            }
            $csv_content_old[$key] = array(
                'url'       => $data[0],
                'class'     => $data[1],
                'score'     => $data[2],
                'passion'   => $data[3],
                'interest'  => $data[4],
                'location'  => $data[5],
                'interest_area' => $data[6],
            );

            if ( $request->get('permalink') == $data[0] ) {
                $csv_content_old[$key]['interest'] = $request->get( 'interest' );
            }
        }

        $csv_content_old[0] = array(
            'URL' , 'CLASS' , 'SCORE' , 'PASSION' , 'INTEREST' , 'LOCATION' , 'INTEREST AREA'
        );


        // Open CSV file for update
        $fp = fopen( $csv , 'w');
        foreach ( $csv_content_old as $fields ) {
            fputcsv( $fp , $fields );
        }
        fclose($fp);

        // Update location in database
        return \DB::collection('passion_points')->where( 'permalink' , $request->get('permalink') )->update( [
                'interest' => $request->get( 'interest' )
            ] );
        
        // return $csv_content_old;
    }
    public function countPassion( Request $request )
    {
        $counter = 0;
        
        foreach ( $request->get( 'users' ) as $key => $user) {
            if ( \DB::collection('passion_points')->where( 'user' , $user )->where( 'passion' , 'like' , '%'.$request->get( 'data' ).'%' )->count() > 0 ) {
               $counter++;
            }
        }

        return $counter;
    }
    public function updateDataFile( Request $request )
    {
        // $csv = public_path() . '/csv/starhub/' . $request->get( 'folder' ) . '/' . $request->get( 'user' ) . '.csv';
        $csv = public_path() . '/csv/all_users/' . $request->get( 'user' ) . '.csv';
        \DB::collection('passion_points')->where( 'user' , $request->get( 'user' ) )->delete( );
        
        // Get current data of the file
        $csv_content_old = array();
        $db_insert = array();

        $arr = array_map('str_getcsv', file($csv));

        // Set CSV header title
        $csv_content_old[0] = array(
            'URL' , 
            'CLASS' , 
            'SCORE' , 

            'primary_passion_point_1' , 
            'primary_passion_point_2' , 
            'primary_passion_point_3' , 
            'primary_passion_point_4' , 

            'secondary_passion_point_1' , 
            'secondary_passion_point_2' , 
            'secondary_passion_point_3' , 
            'secondary_passion_point_4' , 

            // 'INTEREST'
        );

        $current_index = '';
        foreach ($arr as $key => $data) {
            if ( $data[0] !== 'URL' ) {
                $csv_content_old[$key] = array(
                    'URL'   => $data[0],
                    'CLASS' => isset($data[1]) ? $data[1] : '',
                    'SCORE' => isset($data[2]) ? $data[2] : '',

                    'primary_passion_point_1'   => isset( $data[3] ) ? $data[3] : '',
                    'primary_passion_point_2'   => isset( $data[4] ) ? $data[4] : '',
                    'primary_passion_point_3'   => isset( $data[5] ) ? $data[5] : '',
                    'primary_passion_point_4'   => isset( $data[6] ) ? $data[6] : '',

                    'secondary_passion_point_1' => isset( $data[7] ) ? $data[7] : '', 
                    'secondary_passion_point_2' => isset( $data[8] ) ? $data[8] : '', 
                    'secondary_passion_point_3' => isset( $data[9] ) ? $data[9] : '', 
                    'secondary_passion_point_4' => isset( $data[10] ) ? $data[10] : '', 
                );
                

                if ( $request->get( 'url' ) === $csv_content_old[$key]['URL'] ) {
                    $csv_content_old[$key]['primary_passion_point_1'] = isset( $request->get('data')['primary_affinity'][0] ) ? $request->get('data')['primary_affinity'][0] : ''; 
                    $csv_content_old[$key]['primary_passion_point_2'] = isset( $request->get('data')['primary_affinity'][1] ) ? $request->get('data')['primary_affinity'][1] : '';
                    $csv_content_old[$key]['primary_passion_point_3'] = isset( $request->get('data')['primary_affinity'][2] ) ? $request->get('data')['primary_affinity'][2] : '';
                    $csv_content_old[$key]['primary_passion_point_4'] = isset( $request->get('data')['primary_affinity'][3] ) ? $request->get('data')['primary_affinity'][3] : '';


                    $csv_content_old[$key]['secondary_passion_point_1'] = isset( $request->get('data')['secondary_affinity'][0] ) ? $request->get('data')['secondary_affinity'][0] : ''; 
                    $csv_content_old[$key]['secondary_passion_point_2'] = isset( $request->get('data')['secondary_affinity'][1] ) ? $request->get('data')['secondary_affinity'][1] : '';
                    $csv_content_old[$key]['secondary_passion_point_3'] = isset( $request->get('data')['secondary_affinity'][2] ) ? $request->get('data')['secondary_affinity'][2] : '';
                    $csv_content_old[$key]['secondary_passion_point_4'] = isset( $request->get('data')['secondary_affinity'][3] ) ? $request->get('data')['secondary_affinity'][3] : '';
                }

                $db_insert[$key] = array(
                    'permalink' => $data[0],
                    'interest'  => '',
                    'user'      => $request->get( 'user' ),

                    'primary_passion_point_1' => $csv_content_old[$key]['primary_passion_point_1'],
                    'primary_passion_point_2' => $csv_content_old[$key]['primary_passion_point_2'],
                    'primary_passion_point_3' => $csv_content_old[$key]['primary_passion_point_3'],
                    'primary_passion_point_4' => $csv_content_old[$key]['primary_passion_point_4'],

                    'secondary_passion_point_1' => $csv_content_old[$key]['secondary_passion_point_1'],
                    'secondary_passion_point_2' => $csv_content_old[$key]['secondary_passion_point_2'],
                    'secondary_passion_point_3' => $csv_content_old[$key]['secondary_passion_point_3'],
                    'secondary_passion_point_4' => $csv_content_old[$key]['secondary_passion_point_4'],

                );
            }



        }

        // Insert to table ( permalink, passion, interest )
        \DB::collection('passion_points')->insert( $db_insert );


        // Open CSV file for update
        $fp = fopen( $csv , 'w');
        foreach ( $csv_content_old as $fields ) {
            fputcsv( $fp , $fields );
        }
        fclose($fp);


        // return $request->all();
        return $csv_content_old;
    }

    public function getFileName()
    {
        $dir    = base_path() . '/public/csv/starhub/rod/';
        $files  = array_diff(scandir($dir), array('..', '.'));
        
        $file_list = array();

        foreach ($files as $key => $file) {
            $file_list[] = basename( $file , ".csv");
        }
        return $file_list;
    }

    public function importFileDataByType()
    {
        // Delete the contents first exists in database
        \DB::collection('starhub_data')->delete();

        $csv = public_path() . '/csv/starhub-followers.csv';

        $data = array_map('str_getcsv', file($csv));

        // var_dump( $data );
        $insert_data = [];

        if(!empty($data)) {
            foreach ($data as $key => $d) {
                if ( $d[0] != 'Profile Name' ) {
                    $insert_data[] = array(
                        'profile_name'          => $d[0],
                        'profile_stats'         => $d[1],
                        'ExtraData'             => $d[2],
                        'gender'                => $d[3],
                        'age'                   => $d[4],
                        'total_post'            => $d[5],
                        'total_followers'       => $d[6],
                        'total_following'       => $d[7],
                        'total_follower_fng'    => $d[8],
                        'profile_link'          => $d[10],
                    );
                }
            }
        }
        \DB::collection('starhub_data')->insert($insert_data);  
    }

    public function getTags( Request $request )
    {

        $csv = public_path() . '/csv/starhub/rod/' . $request->get( 'user' ) . '.csv';

        $csv_content_tags = array();

        $arr = array_map('str_getcsv', file($csv));

        if ( \DB::collection('permalink_tags')->where( 'permalink' , $request->get( 'permalink' ) )->count() > 0 ) {
            \DB::collection('permalink_tags')->where( 'permalink' , $request->get( 'permalink' ) )->delete();
        }

// \DB::collection('permalink_tags')->insert($csv_content_tags); 

        $current_permalink = '';
        $current_pp = '';
        $current_interest = '';
        $current_score = '';

        $csv_content_tags['permalink'] = $request->get( 'permalink' );

        foreach ($arr as $key => $data) {

            if ( $data[0] != '' || $data[0] == '' ) {
                $current_score = $data[2];
                $current_pp = $data[3];
                $current_interest = $data[4];
            }

            if ( $data[0] != 'URL' ) {
                
                if ( $data[0] != null ) {
                    $current_permalink = $data[0];
                } else {
                    $data[0] = $current_permalink;
                }

                if ( $current_permalink == $request->get( 'permalink' ) ) {
                    $csv_content_tags['tags'][] = $data[1];
                }
            }
        }

        $csv_content_tags['score'] = $current_score;
        $csv_content_tags['passion'] = $current_pp;
        $csv_content_tags['interest'] = $current_interest;

        \DB::collection('permalink_tags')->insert($csv_content_tags); 

        return $csv_content_tags;
    }

    public function getGraymatic( Request $request )
    {
        $api_key = '8204f65235dacdfe68d2bedd443b1b7e';
        
        $api_url = 'http://api.graymatics.com/grayit/process/image/ondemand';

        $arr = [];


        foreach ( $request->all() as $key => $data ) {
            $post_args = array( 
                'API_KEY'   =>  $api_key,
                'URL'       =>  base64_encode( $data[ 'url' ] ),
            );

            $curl = curl_init($api_url); 

            curl_setopt($curl, CURLOPT_POST, true); 
            curl_setopt($curl, CURLOPT_POSTFIELDS, $post_args); 

            // this is needed
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            $result = json_decode(curl_exec($curl), true);
            curl_close($curl);

            $container[$key] = array(
                'url' => $result['url'],
            );

            if ($result['results'][0]['aesthetics']) {
                foreach ( $result['results'][0]['aesthetics'] as $key2 => $res ) {
                    $container[$key][$key2] = (string) $res;
                }
            }

            if ($result['results'][0]['attributes']) {
                foreach ( $result['results'][0]['attributes'] as $key2 => $res ) {
                    foreach ($res as $key3 => $res3) {
                        $container[$key][$key3] = $res3;
                        if ( $key3 == 'genre' ) {
                            foreach ($res3 as $key4 => $res4) {
                                $container[$key][$key3] = $res4;
                            }
                        }else {
                            $container[$key][$key3] = '';
                        } if( $key3 == 'style' ) {
                            foreach ($res3 as $key4 => $res4) {
                                $container[$key][$key3] = $res4;
                            }
                        }else {
                            $container[$key][$key3] = '';
                        }
                    }
                }
            }
            if ($result['results'][0]['color']) {
                if ($result['results'][0]['color']['colorpalette']) {
                    foreach ( $result['results'][0]['color']['colorpalette']['colors'] as $key2 => $res ) {
                        $container[$key]['color' . ($key2+1)] = $res[1];
                    }
                }
            }
            if ($result['categorization']) {
                foreach ( $result['categorization']['IAB'] as $key2 => $res ) {
                    foreach ($res as $key3 => $res3) {
                        $container[$key]['categorization '. ($key3 + 1 )] = $res3;
                    }
                }
            }

        }
        Excel::create('Graymatic Result', function($excel) use($container) {

            $excel->sheet('Sheetname', function($sheet) use($container) {

                $sheet->fromArray( $container );

            });

        })->export('csv');
    }

    public function exportToCSV()
    {
        $data = \DB::collection('passion_points')->get();

        $container = array();

        foreach ($data as $key => $d) {
            if ( $d['permalink'] != 'URL' ) {
                $container[] = array(
                    'permalink' => $d['permalink'],
                    'passion' => $d['passion'],
                    'interest' => $d['interest'],
                    'user' => $d['user'],
                    'location' => isset($d['location']) ? $d['location'] : '' ,
                );    
            }
        }

        Excel::create('Passion Points', function($excel) use($container) {

            $excel->sheet('Sheetname', function($sheet) use($container) {

                $sheet->fromArray( $container );

            });

        })->export('csv');        
    }


    public function getUserPassionData()
    {
        $data = \DB::collection('passion_points')->groupBy('user')->get();


        // Get the users first
        $container = array();

        $passions = array();
        foreach ( $data as $key => $d ) {
            $passions[$key] = \DB::collection('passion_points')
                ->select( 'passion' , 'user' )
                ->where( 'user' , $d['user'] )
                ->where( 'passion' , '!=' , '' )
                ->get();
        }

        $current_passion = null;
        $counter = 0;
        $arr_passion = [];
        $arr_passion_ctr = [];
        $current_user = null;

        foreach ( $passions as $key => $val ) {
            foreach ($val as $key2 => $value) {

                if ( $current_user != $value['user'] ) {
                    $arr_passion = [];
                    $arr_passion_ctr = [];
                }

                if ( in_array( $value['passion'] , $arr_passion ) ) {
                    $index = array_search( $value['passion'] , $arr_passion );
                    $arr_passion_ctr[$index] = $arr_passion_ctr[$index] +  1 ;
                } else {
                    array_push( $arr_passion , $value['passion'] );
                    array_push( $arr_passion_ctr , 1 );
                }



                if ( !isset( $result[$value['user']] ) ) {
                    $result[$value['user']] = array(
                        'user' => $value['user'],
                        'passion' => $value['passion'],
                        'count' => 0,
                    );
                }

                $result[$value['user']]['count'] = $arr_passion_ctr;
                // $result[$value['user']]['count'] = $counter;
                $result[$value['user']]['user'] = $value['user'];

                $current_user = $value['user'];
            }
        }

        foreach ($result as $key => $v) {
            $container[] = array(
                'USER'  => $v['user'],
                'TOP PASSION'  => $v['passion'],
                'PASSION COUNT'  => max( $v['count'] ),
            );
        }
        // echo '<pre>';
        // echo json_encode( $container , JSON_PRETTY_PRINT );
        // echo '</pre>';
        Excel::create('User Top Passion', function($excel) use($container) {

            $excel->sheet('Sheetname', function($sheet) use($container) {

                $sheet->fromArray( $container );

            });

        })->export('csv');  

    }
    public function getUserDatabase( $page  = 1)
    {
        $arr = [];
        $starhub = \DB::collection('starhub_data')->get();
        $locations = \DB::collection('insta_profile')->get();
        foreach ($starhub as $key => $values) {
            if ( $values['profile_name'] != null ) {
                array_push($arr, 
                    ['profile_name' => $values['profile_name'],
                    'data_src'  => 'Starhub followers',
                    'profile_stats' => $values['profile_stats'],
                    'ExtraData' => $values['ExtraData'],
                    'gender' => $values['gender'],
                    'age' => $values['age'],
                    'total_posts' => $values['total_post'],
                    'total_followers' => $values['total_followers'],
                    'total_following' => $values['total_following'],
                    'total_follower_fng' => $values['total_follower_fng'],
                    'location' => '',
                    'profile_link' => $values['profile_link'],]
                );
            }
        }



        foreach ($locations as $key => $values) {
            if ( $values['profile_name'] != null ) {
                array_push($arr, 
                    ['profile_name' => $values['profile_name'],
                    'data_src'  => '15 location',
                    'profile_stats' => '',
                    'ExtraData' => '',
                    'gender' => $values['gender'],
                    'age' => '',
                    'total_posts' => $values['total_posts'],
                    'total_followers' => $values['total_followers'],
                    'total_following' => $values['total_following'],
                    'total_follower_fng' => '',
                    'location' => $values['location'],
                    'profile_link' => $values['profile_link'],]
                );
            }
        }

        array_multisort($arr, SORT_ASC);

        $total = count( $arr ); //total items in array    
        $limit = 150; //per page    
        $totalPages = ceil( $total/ $limit ); //calculate total pages
        $page = max($page, 1); //get 1 page when $_GET['page'] <= 0
        $page = min($page, $totalPages); //get last page when $_GET['page'] > $totalPages
        $offset = ($page - 1) * $limit;
        if( $offset < 0 ) $offset = 0;

        $arr = array_slice( $arr, $offset, $limit );


        return [ 'data' => $arr, 'total_pages' => $totalPages ];
    }

    public function jsonCSV($filename)
    {
        $filename = $filename;

        $str = file_get_contents( public_path() . '/file_downloads/poke/' . $filename  );
        
        $json = json_decode( $str );


        $arr = array();

        foreach ($json as $key => $value) {
            
            $arr[$key] = array(
                // 'caption_is_edited' => (string)$value->media->caption_is_edited,
                // 'code'  => (string)$value->media->caption_is_edited,
                // 'width' => (string)$value->media->dimensions->width,
                // 'height' => (string)$value->media->dimensions->height,
                // // 'usertags' => (string)$value->media->usertags,
                // 'comments_disabled' => $value->media->comments_disabled,
                // 'comments_count' => $value->media->comments->count,
                // 'has_previous_page' => $value->media->comments->page_info->has_previous_page,
                // 'start_cursor' => $value->media->comments->page_info->start_cursor,
                // 'end_cursor' => $value->media->comments->page_info->end_cursor,
                // 'has_next_page' => $value->media->comments->page_info->has_next_page,
                // 'id'   => $value->media->id,
                'Post-Text'   => isset( $value->media->caption ) ? $value->media->caption : null,
                'date'  => gmdate("F j, Y,   g:i a", $value->media->date),
                'likes_count'   => $value->media->likes->count,
                'viewer_has_liked'   => $value->media->likes->viewer_has_liked,
                'owner_username'   => $value->media->owner->username,
                'owner_fullname'   => $value->media->owner->full_name,
                // 'is_unpublished'   => $value->media->owner->is_unpublished,
                // 'requested_by_viewer'   => $value->media->owner->requested_by_viewer,
                // 'followed_by_viewer'   => $value->media->owner->followed_by_viewer,
                // 'blocked_by_viewer'   => $value->media->owner->blocked_by_viewer,
                // 'owner_profile_pic_url'   => $value->media->owner->profile_pic_url,
                // 'has_blocked_viewer'   => $value->media->owner->has_blocked_viewer,
                // 'owner_id'   => $value->media->owner->id,
                // 'is_private'   => $value->media->owner->is_private,
                // 'is_video'   => $value->media->is_video,
                // 'is_ad'   => $value->media->is_ad,
                'display_src'   => strstr( $value->media->display_src, '?', true),
                'location_id'   => isset($value->media->location->id) ? $value->media->location->id : NULL,
                'location_has_public_page'   => isset($value->media->location->has_public_page) ? $value->media->location->has_public_page : NULL,
                'location_name'   => isset($value->media->location->name) ? $value->media->location->name : NULL,
            );

            // For Comments
            // foreach ($value->media->comments->nodes as $node_index => $node) {
            //     $arr[$key][ 'text_' . (integer)($node_index + 1) ] = $node->text;
            //     $arr[$key][ 'created_at_' . (integer)($node_index + 1) ] = $node->created_at;
            //     $arr[$key][ 'id_' . (integer)($node_index + 1) ] = $node->id;
            //     $arr[$key][ 'username_' . (integer)($node_index + 1) ] = $node->user->username;
            //     $arr[$key][ 'profile_pic_url_' . (integer)($node_index + 1) ] = $node->user->profile_pic_url;
            //     $arr[$key][ 'user_id_' . (integer)($node_index + 1) ] = $node->user->id;

            // }
            
            // foreach ($value->media->likes->nodes as $node_index => $like) {
            //     $arr[$key][ 'liked_by_username_' . (integer)($node_index + 1) ] = $like->user->username;
            //     $arr[$key][ 'liked_profile_pic_' . (integer)($node_index + 1) ] = $like->user->profile_pic_url;
            //     $arr[$key][ 'liked_user_id_' . (integer)($node_index + 1) ] = $like->user->id;
            // }
            // $arr[] = $value->media;

        }

        // echo "<pre>";
        // var_dump( $arr );
        // echo "</pre>";

        Excel::create(  $filename , function($excel) use($arr) {

            $excel->sheet('Sheetname', function($sheet) use($arr) {

                $sheet->fromArray( $arr );

            });

        })->export('csv');  
    }
    public function multipleJSONConversion()
    {
        // $dir    = base_path() . '/public/file_downloads/json-file/';
        $dir    = base_path() . '/public/file_downloads/poke/';
        $files  = array_diff(scandir($dir), array('..', '.'));
        
        $file_list = array();
        $arr = array(); 
        $count = 1;

        echo "<style> .straight { text-decoration: line-through; color:green } .selected_link:visited {color:red}</style>";

        foreach ($files as $key => $file) {
            echo "[" . $count++ ."] == <a class='selected_link' href='/user/json/csv/".$file."' target='__blank'>".$file."</a><br>";

        }

        echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>";
        echo "<script>
                $( '.selected_link' ).on( 'click' , function() {
                    $( this ).addClass( 'straight');
                })
              </script>";
    }
    public function getSingaporeLocals()
    {
        $csv = public_path() . '/csv/singapore-locals.csv';
        $arr = array_map('str_getcsv', file($csv));
        $data_arr = array();
        foreach ($arr as $key => $value) {
            $user = \DB::collection('insta_profile')->where( 'profile_name' , $value[0] )->first();
            if ( $user ) {
                $data_arr[] = array(
                    'user' => $user
                );
            }
        }
        return $data_arr;
    }
    public function deletePassionUser( Request $request )
    {
        $fNum = $request->get( 'field' );

        \DB::collection('passion_points')
            ->where( 'permalink' , $request->get( 'url' ) )
            ->update( [ 'primary_passion_point_'.$fNum => '' ,  'secondary_passion_point_'.$fNum => '' ] );


        // Update CSV
        $csv = public_path() . '/csv/all_users/' . $request->get( 'user' ) . '.csv';

        // Get current data of the file
        $csv_content_old = array();

        $arr = array_map('str_getcsv', file($csv));

        // Set headers
        $csv_content_old[0] = array(
            'URL',
            'CLASS',
            'SCORE',

            'primary_passion_point_1',
            'primary_passion_point_2',
            'primary_passion_point_3',
            'primary_passion_point_4',

            'secondary_passion_point_1',
            'secondary_passion_point_2',
            'secondary_passion_point_3',
            'secondary_passion_point_4',
        );

        foreach ($arr as $key => $old) {
            if ( (string) $old[0] !== 'URL' ) {
                $csv_content_old[$key] = array(
                    'URL' => isset( $old[0] ) ? $old[0] : '',
                    'CLASS' => isset( $old[1] ) ? $old[1] : '',
                    'SCORE' => isset( $old[2] ) ? $old[2] : '',
                    'primary_passion_point_1'   =>  isset( $old[3] ) ? $old[3] : $old[3],
                    'primary_passion_point_2'   =>  isset( $old[4] ) ? $old[4] : $old[4],
                    'primary_passion_point_3'   =>  isset( $old[5] ) ? $old[5] : $old[5],
                    'primary_passion_point_4'   =>  isset( $old[6] ) ? $old[6] : $old[6],
                    'secondary_passion_point_1' =>  isset( $old[7] ) ? $old[7] : $old[7],
                    'secondary_passion_point_2' =>  isset( $old[8] ) ? $old[8] : $old[8],
                    'secondary_passion_point_3' =>  isset( $old[9] ) ? $old[9] : $old[9],
                    'secondary_passion_point_4' =>  isset( $old[10] ) ? $old[10] : $old[10], 
                );

                if ( $request->get('url') === $old[0] ) {
                    $csv_content_old[$key]['primary_passion_point_'.$fNum ] = '';
                    $csv_content_old[$key]['secondary_passion_point_'.$fNum ] = '';
                }

            }
        }

        // Open CSV file for update
        $fp = fopen( $csv , 'w');
        foreach ( $csv_content_old as $fields ) {
            fputcsv( $fp , $fields );
        }
        fclose($fp);

        return $csv_content_old;
    }

    public function removeJSONtext()
    {
        $dir    = base_path() . '/public/csv/all_users/';
        $files  = array_diff(scandir($dir), array('..', '.'));
        
        $file_list = array();

        foreach ($files as $key => $file) {
            $newname = str_replace( ".json", "", $file );
            rename( $dir . $file , $dir . $newname );
        }
        return $file_list;
    } 

    public function getUserPercentage()
    {
        // $csv = public_path() . '/csv/all_users/';

        // Get all Existing users
        $all_users = $this->getSingaporeLocals();

        // Get All user CSV
        // $dir    = base_path() . '/public/csv/all_users/';
        // $files  = array_diff(scandir($dir), array('..', '.'));
        
        $file_list = array();

        foreach ($all_users as $key => $filename) {
            $file_list[] = $filename['user']['profile_name'] . '.csv';
        }

        // foreach ($files as $key => $filename) {
        //     $file_list[] = $filename;
        // }

        // Check each files if have affinity tags
        $content = array();
        $content['count_tagged_users'] = 0;
        $content['count_untagged_users'] = 0;

        $content['count_img'] = 0;
        $content['count_tagged_img'] = 0;
        $content['count_untagged_img'] = 0;

        foreach ($file_list as $key => $filename) {
            $csv = public_path() . '/csv/all_users/' . $filename;
            if ( file_exists( $csv ) ) {
                
                $arr = array_map('str_getcsv', file($csv));

                $count = 0;
                $count_img_aff = 0;
                $primary_affinity = array();

                foreach ($arr as $key => $csv_content) {
                    // USERS
                    if ( isset($csv_content[3]) && $csv_content[3] == '' || isset($csv_content[4]) && $csv_content[4] == '' || isset($csv_content[5]) && $csv_content[5] == '' || isset($csv_content[6]) && $csv_content[6] == '' ) {
                        $count += 1;
                    }

                    // Total IMAGES
                    if ( isset( $csv_content[0] ) && $csv_content[0] !== 'url' && $csv_content[0] !== ''  ) {
                       $content['count_img'] += 1;

                       if ( isset($csv_content[3]) && $csv_content[3] !== '' || isset($csv_content[4]) && $csv_content[4] !== '' || isset($csv_content[5]) && $csv_content[5] !== '' || isset($csv_content[6]) && $csv_content[6] !== '' ) {
                            $content['count_untagged_img'] += 1;
                       } else {
                            $content['count_tagged_img'] += 1;
                       }
                    }

                    // Untagged Images 
                    // if ( isset( $csv_content[0] ) && $csv_content[0] !== 'url' && $csv_content[0] !== '' ) {
                    //     if ( isset($csv_content[3]) && $csv_content[3] == '' ) {
                    //         // $content['count_untagged_img'] += 1;
                    //     }
                    // }

                    // Primary Affinitiisset($csv_content[3]) && $csv_content[3] !== '' ) {
                        # code...
                }
                // USERS
                if ( $count == 0 ) {
                    $content['count_untagged_users'] += 1;
                } else {
                    $content['count_tagged_users'] += 1;
                }
            }
            // Get All Primary Affinities
        }

        echo "<pre>";
        dd( $content );
        echo "</pre>";
    }

    public function searchTag(Request $request)
    {
        $tags = $request->get('tags');

        // Get All user CSV
        $dir    = base_path() . '/public/csv/all_users/';
        $files  = array_diff(scandir($dir), array('..', '.'));
        
        $file_list = array();

        foreach ($files as $key => $filename) {
            $file_list[] = $filename;
        }


        $csv_content = [];
        $current_image = null;

        $index_img = 0;

        foreach ($file_list as $key => $filename) {
            $csv = public_path() . '/csv/all_users/' . $filename;
            $arr = array_map('str_getcsv', file($csv));

            // Find All Images having specific clarifai tag value
            foreach ($arr as $csv_key => $csv_val) {
                // Don't include CSV row[0]
                if ( $csv_val[0] !== 'url' ) { 
                    $csv_content[$csv_val[0]] = array( 
                        'url'   =>  $csv_val[0],
                        'username'   =>  str_replace( ".csv", "", $filename ),
                        'primary_passion_point_1'   => isset( $csv_val[3] ) ? $csv_val[3] : null,
                        'primary_passion_point_2'   => isset( $csv_val[4] ) ? $csv_val[4] : null,
                        'primary_passion_point_3'   => isset( $csv_val[5] ) ? $csv_val[5] : null,
                        'primary_passion_point_4'   => isset( $csv_val[6] ) ? $csv_val[6] : null,
                        'secondary_passion_point_1'   => isset( $csv_val[7] ) ? $csv_val[7] : null,
                        'secondary_passion_point_2'   => isset( $csv_val[8] ) ? $csv_val[8] : null,
                        'secondary_passion_point_3'   => isset( $csv_val[9] ) ? $csv_val[9] : null,
                        'secondary_passion_point_4'   => isset( $csv_val[10] ) ? $csv_val[10] : null,
                    );
                    if ( $csv_val[0] == null ) {
                        $csv_val[0] = $current_image;
                    } else {
                        $current_image = $csv_val[0];
                        $index_img += 1;
                    }
                    $csv_content[$csv_val[0]]["tags"][] = isset( $csv_val[1] ) ? $csv_val[1] : null;

                }
            }
        }

        $response = array();
        foreach ($csv_content as $key => $csv) {
            if ( !array_diff( $tags, isset( $csv['tags'] ) ? $csv['tags'] : array() ) ) {
                $response[$key] = $csv;
            }
        }


        // echo "<pre>";
        return $response;
        // echo "</pre>";

    }
    public function taggedUserLists()
    {
        $users = \DB::collection( 'passion_points' )
            ->whereNotNull( 'primary_passion_point_1' )
            ->whereNotNull( 'primary_passion_point_2' )
            ->whereNotNull( 'primary_passion_point_3' )
            ->whereNotNull( 'primary_passion_point_4' )
            ->groupBy( 'user' )
            ->get();

        $count = 1;

        echo "<style> .straight { text-decoration: line-through; color:green } .selected_link:visited {color:red}</style>";

        foreach ($users as $key => $file) {
            echo "[" . $count++ ."] == <a class='selected_link' href='/user/affinity/tagged/".$file['user']."' target='__blank'>".$file['user']."</a><br>";

        }

        echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>";
        echo "<script>
                $( '.selected_link' ).on( 'click' , function() {
                    $( this ).addClass( 'straight');
                })
              </script>";
    }

    public function taggedUsers($user)
    {
        $users = \DB::collection( 'passion_points' )
            ->whereNotNull( 'primary_passion_point_1' )
            ->whereNotNull( 'primary_passion_point_2' )
            ->whereNotNull( 'primary_passion_point_3' )
            ->whereNotNull( 'primary_passion_point_4' )
            ->where( 'user' , $user )
            ->get();

        $csv_content = array();

        foreach ($users as $key => $u) {
            if ($u['permalink'] != '') {
                $csv_content[] = array(
                    'user'       => (string)$user,
                    'permalink'  => (string)$u['permalink'],
                    'primary_passion_point_1' => (string)$u['primary_passion_point_1'],
                    'primary_passion_point_2' => (string)$u['primary_passion_point_2'],
                    'primary_passion_point_3' => (string)$u['primary_passion_point_3'],
                    'primary_passion_point_4' => (string)$u['primary_passion_point_4'],
                    'secondary_passion_point_1' => (string)$u['secondary_passion_point_1'],
                    'secondary_passion_point_2' => (string)$u['secondary_passion_point_2'],
                    'secondary_passion_point_3' => (string)$u['secondary_passion_point_3'],
                    'secondary_passion_point_4' => (string)$u['secondary_passion_point_4'],
                ); 
           }           
        }

        echo  $this->array2csv( $csv_content , $user );

    }
    function array2csv(array &$array , $user)
    {
       if (count($array) == 0) {
         return null;
       }
       ob_start();
       $df = fopen("php://output", 'w');
       fputcsv($df, array_keys(reset($array)));
       foreach ($array as $row) {
          fputcsv($df, $row);
       }
       header('Content-Type: text/csv; charset=utf-8');
       header('Content-Disposition: attachment; filename="'.$user.'.csv";');
       fclose($df);
       return ob_get_clean();
    }

    public function jsonCSVconverter( )
    {
        return view( 'json-file-upload' );
    }
    public function getJSONdata( Request $request )
    {
        $file_content = file_get_contents($request->file('file_data'));
        $json = json_decode( $file_content );



        $arr = array();

        foreach ($json as $key => $value) {
            
            $arr[$key] = array(
                // 'caption_is_edited' => (string)$value->media->caption_is_edited,
                // 'code'  => (string)$value->media->caption_is_edited,
                // 'width' => (string)$value->media->dimensions->width,
                // 'height' => (string)$value->media->dimensions->height,
                // // 'usertags' => (string)$value->media->usertags,
                // 'comments_disabled' => $value->media->comments_disabled,
                // 'comments_count' => $value->media->comments->count,
                // 'has_previous_page' => $value->media->comments->page_info->has_previous_page,
                // 'start_cursor' => $value->media->comments->page_info->start_cursor,
                // 'end_cursor' => $value->media->comments->page_info->end_cursor,
                // 'has_next_page' => $value->media->comments->page_info->has_next_page,
                // 'id'   => $value->media->id,
                'Post-Text'   => isset( $value->media->caption ) ? $value->media->caption : null,
                'date'  => gmdate("F j, Y,   g:i a", $value->media->date),
                'likes_count'   => $value->media->likes->count,
                'viewer_has_liked'   => $value->media->likes->viewer_has_liked,
                'owner_username'   => $value->media->owner->username,
                'owner_fullname'   => $value->media->owner->full_name,
                // 'is_unpublished'   => $value->media->owner->is_unpublished,
                // 'requested_by_viewer'   => $value->media->owner->requested_by_viewer,
                // 'followed_by_viewer'   => $value->media->owner->followed_by_viewer,
                // 'blocked_by_viewer'   => $value->media->owner->blocked_by_viewer,
                // 'owner_profile_pic_url'   => $value->media->owner->profile_pic_url,
                // 'has_blocked_viewer'   => $value->media->owner->has_blocked_viewer,
                // 'owner_id'   => $value->media->owner->id,
                // 'is_private'   => $value->media->owner->is_private,
                // 'is_video'   => $value->media->is_video,
                // 'is_ad'   => $value->media->is_ad,
                'display_src'   => strstr( $value->media->display_src, '?', true),
                'location_id'   => isset($value->media->location->id) ? $value->media->location->id : NULL,
                'location_has_public_page'   => isset($value->media->location->has_public_page) ? $value->media->location->has_public_page : NULL,
                'location_name'   => isset($value->media->location->name) ? $value->media->location->name : NULL,
            );

            // For Comments
            // foreach ($value->media->comments->nodes as $node_index => $node) {
            //     $arr[$key][ 'text_' . (integer)($node_index + 1) ] = $node->text;
            //     $arr[$key][ 'created_at_' . (integer)($node_index + 1) ] = $node->created_at;
            //     $arr[$key][ 'id_' . (integer)($node_index + 1) ] = $node->id;
            //     $arr[$key][ 'username_' . (integer)($node_index + 1) ] = $node->user->username;
            //     $arr[$key][ 'profile_pic_url_' . (integer)($node_index + 1) ] = $node->user->profile_pic_url;
            //     $arr[$key][ 'user_id_' . (integer)($node_index + 1) ] = $node->user->id;

            // }
            
            // foreach ($value->media->likes->nodes as $node_index => $like) {
            //     $arr[$key][ 'liked_by_username_' . (integer)($node_index + 1) ] = $like->user->username;
            //     $arr[$key][ 'liked_profile_pic_' . (integer)($node_index + 1) ] = $like->user->profile_pic_url;
            //     $arr[$key][ 'liked_user_id_' . (integer)($node_index + 1) ] = $like->user->id;
            // }
            // $arr[] = $value->media;

        }

        // echo "<pre>";
        // var_dump( $arr );
        // echo "</pre>";

        $filename = pathinfo($request->file( 'file_data' )->getClientOriginalName() , PATHINFO_FILENAME );

        Excel::create( $filename , function($excel) use($arr) {

            $excel->sheet('Sheetname', function($sheet) use($arr) {

                $sheet->fromArray( $arr );

            });

        })->export('csv');

        // echo "<pre>";
        // var_dump(  );
        // echo "</pre>";
    }

    public function importFileName()
    {
        ini_set('max_execution_time', 360);

        $dir    = base_path() . '/public/file_downloads/pokemon_user/';
        $files  = array_diff(scandir($dir), array('..', '.'));
        
        $file_list = array();
        $arr = array(); 
        $count = 1;


        foreach ($files as $key => $file) {
            $newname = str_replace( ".csv", "", $file );
            $insert_data[] = array(
                'profile_name'      =>  $newname,
                'gender'            =>  '',
                'total_posts'       =>  '',
                'total_followers'   =>  '',
                'total_following'   =>  '',
                'location'          =>  '',
                'profile_link'      =>  ''
            );
        }

        // \DB::collection('insta_profile')->insert($insert_data);
        return $insert_data;
    }

    public function downloadUserClarifai()
    {
        $users = \DB::collection( 'passion_points' )
            ->groupBy( 'user' )
            ->orderBy( 'user' , 'asc' )
            ->get();

        $count = 1;

        echo "<style> .straight { text-decoration: line-through; color:green } .selected_link:visited {color:red}</style>";

        foreach ($users as $key => $file) {
            echo "[" . $count++ ."] == <a class='selected_link' href='/download/force/".$file['user']."' target='__blank'>".$file['user']."</a><br>";     
        }

        echo "<script src='https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js'></script>";
        echo "<script>
                $( '.selected_link' ).on( 'click' , function() {
                    $( this ).addClass( 'straight');
                })
              </script>";
  
    }
    public function forceDownload($user)
    {
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename='.$user.'.csv');
        header('Pragma: no-cache');
        readfile( public_path() . '/csv/all_users/' .$user. '.csv' );   
    }

}
 
