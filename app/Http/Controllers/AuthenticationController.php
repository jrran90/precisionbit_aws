<?php

namespace App\Http\Controllers;

use Session;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Authentication;
use App\User;
use DB;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk; 
use Facebook\Exceptions\FacebookSDKException;


class AuthenticationController extends Controller
{

    protected $model;

    protected $redirectPath = '/home';

    /**
     * Redirects any other unregistered routes back to the main
     * angular template so angular can deal with them
     *
     * @Get( "{path?}", as="catch.all" )
     * @Where({"path": ".+"})
     *
     * @return Response
     */
    public function catchOtherRoutes()
    {
        return view("index");
    }
    public function __construct()
    {
        $this->model = new Authentication();
    }
    public function index()
    {
        //        
    }
    public function authenticate( $request )
    {
        $action = array();
        // Fetch first if user's Facebook UID is already exists
        
        $status = $this->model->checkFbUID( $request['id'] ); // 1 or 0

        if ( $status == 1 ) {
            $userInfo = $this->getUserInfo( $request );
            $action = array( 'uniqid' => '' , 'uid' => $request['id'] , 'status' => 'success' , 'usertype' => '' , 'msg' => 'Account already in use', 'date_joined' => '' );
        } else if( $status == 0 ) {
            $this->model->createFBuserData( $request ); // will insert facebook basic account info 
            $userInfo = $this->getUserInfo( $request );
            $action = array( 'uniqid' => $userInfo['uniqid'] , 'uid' => $request['id'] , 'status' => 'success' , 'usertype' => $userInfo['usertype'] , 'msg' => 'Facebook info successfully saved.', 'date_joined' => $userInfo['date_joined'] );
        } else {
            $action = array( 'uniqid' => 'Error uniqid' , 'uid' => 'Error UID' , 'status' => 'error' , 'usertype' => 'Error' , 'msg' => 'You have encountered some problem during your login.' , 'date_joined' => null );
        }
        
        return $action;
    }
    public function create()
    {
        
    }
    public function store(Request $request)
    {
        //
    }
    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        //
    }
    public function update(Request $request, $id)
    {
        //
    }
    public function destroy($id)
    {
        //
    }
    public function getUserInfo( $data )
    {
        $info = array();
        
        $query = DB::table( 'users' )
            ->where( 'uid' , '=' , $data['id'] )
            ->get();

        // if ( $query ) {
        //     // $info['usertype']       = $query[0]->usertype;
        //     $info['date_joined']    = $query[0]->created_at;
        //     $info['uniqid']         = $query[0]->uniqid;
        // }else {
        //     $info['usertype']       = $data['usertype'];
        //     $info['date_joined']    = null;            
        //     $info['uniqid']         = null;
        // }

        return $info;
    }

    /* 
    * Facebook Authentication Process 
    */

    // Get Session Facebook Access Token
    public function getFacebookAccessToken( Request $request )
    {
        return $request->session()->get('fb_user_access_token');
    }
    public function FBlogin( LaravelFacebookSdk $fb , $usertype )
    {
        Session::put('usertype', $usertype);
        return redirect( $fb->getLoginUrl( [ 'email' ] ) );
    }
    public function FBcallback( LaravelFacebookSdk $fb )
    {
        // Obtain an access token.
        try {
            $token = $fb->getAccessTokenFromRedirect();
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }
        // Access token will be null if the user denied the request
        // or if someone just hit this URL outside of the OAuth flow.
        if (! $token) {
            // Get the redirect helper
            $helper = $fb->getRedirectLoginHelper();

            if (! $helper->getError()) {
                abort(403, 'Unauthorized action.');
            }
            // User denied the request
            dd(
                $helper->getError(),
                $helper->getErrorCode(),
                $helper->getErrorReason(),
                $helper->getErrorDescription()
            );
        }
        if (! $token->isLongLived()) {
            // OAuth 2.0 client handler
            $oauth_client = $fb->getOAuth2Client();
            // Extend the access token.
            try {
                $token = $oauth_client->getLongLivedAccessToken($token);
            } catch (Facebook\Exceptions\FacebookSDKException $e) {
                dd($e->getMessage());
            }
        }
        $fb->setDefaultAccessToken($token);
        // Save for later
        Session::put('fb_user_access_token', (string) $token);
        
        // Get basic info on the user from Facebook.
        try {
            $response = $fb->get('/me?fields=id,name,email');
        } catch (Facebook\Exceptions\FacebookSDKException $e) {
            dd($e->getMessage());
        }
        // Convert the response to a `Facebook/GraphNodes/GraphUser` collection
        $facebook_user = $response->getGraphUser();
        $graphNode = $response->getGraphNode();


        // Authentication process for existing/non-existing users
        $auth_response = $this->authenticate( array( 
            'id'        =>  $graphNode->getField('id'),
            'name'      =>  $graphNode->getField('name'),
            'email'     =>  $graphNode->getField('email'),
            'usertype'  =>  session('usertype'),
        ) );


        if ( $auth_response['status'] == 'success' ) {
            Session::push( 'user.id' ,    $graphNode->getField('id') );
            Session::put( 'uniqid' , $auth_response['uniqid'] );
            Session::push( 'user.name' ,  $graphNode->getField('name') );
            Session::push( 'user.email' , $graphNode->getField('email') );
            return redirect('/dashboard' ); 
        }else {
            return redirect()->back()->with('error', 'Something went wrong.');
        }
    }
    public function login_test( LaravelFacebookSdk $fb )
    {
        $helper = $fb->getRedirectLoginHelper();
        
        if ( session('fb_user_access_token') ) {
            echo "You are logged in!";
        } else {
            $permissions = ['ads_management'];
            $loginUrl = $helper->getLoginUrl('http://localhost:3000/', $permissions);
            echo '<a href="' . $loginUrl . '">Log in with Facebook</a>';
        } 
    }



}
