<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;


class AuthController extends Controller
{
    public function getLogin()
    {
    	return view('login');
    }
    public function postLogin(Request $request)
    {
    	// Validate inputs from user
    	$this->validate($request,[
    			'email'		=> 'required|email',
    			'password'	=> 'required'
    		]);

    	$credentials = [
    		'email'		=> $request->get('email'),
    		'password'	=> $request->get('password')
    	];
        session( $credentials );
    	if (Auth::attempt($credentials)) {
    		return redirect()->intended('dashboard');
    	}

    	return redirect('/login')->withMsg('<div class="alert alert-danger">Invalid Email/Password</div>');

    }
    public function getLogout()
    {
    	Auth::logout();

        return redirect('login')->withMsg('<div class="alert alert-success">Successfully logged out!</div>');
    }


    public function getRegister()
    {
    	return view('register');
    }
    public function postRegister(Request $request)
    {
    	// validate field
    	$this->validate($request, [
                'firstname' => 'required',
                'lastname'  => 'required',
    			// 'company'	=> 'required',
    			'email'		=> 'required|unique:users|email',
    			'password'	=> 'required|confirmed'
    		]);

    	$credentials = [
            'firstname'       => $request->get('firstname'),
            'lastname'        => $request->get('lastname'),
            'company'         => 'Telstra',
            // 'is_first_time'   => true,
    		'email'		      => $request->get('email'),
    		'password'	      => bcrypt($request->get('password')),
    	];

    	User::create($credentials);

        // return redirect('login')->withMsg('<div class="alert alert-success">Confirmation was sent to your email address.</div>');
    	return redirect('login')->withMsg('<div class="alert alert-success">You can now login.</div>');
    }
    public function getUserData()
    {
        $user =User::where( 'email' , session('email') )->get();
        echo json_encode($user);
    }
}
