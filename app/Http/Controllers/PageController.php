<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Mail;


class PageController extends Controller
{
    public function sendEmailContact(Request $request)
    {

    	$user = [
    		'firstname'		=> $request->get('firstname'),
    		'lastname'		=> $request->get('lastname'),
    		'company_name'	=> $request->get('company_name'),
    		'email'			=> $request->get('email'),
    		'message'		=> $request->get('message'),
    	];


    	Mail::send('emails.contact', ['user'=>$user], function ($m) use ($user) {

    		$m->from( $user["email"], 'PrecisionBit');

    		$m->to( 'hadi@precisionbit.com' , $user["firstname"])->subject('Contact!!!');

    	});
    }
}
