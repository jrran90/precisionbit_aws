<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Jleagle\Imagga\Imagga;
use DB;

class ImaggaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return DB::collection('image_score_imagga')->paginate(5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $image = $request->get('image');

        // $url = 'http://52.77.17.243/upload_get_api/' . $image;
        $url = $image;

        $apiKey     = "acc_a86099052dc6b27";
        $apiSecret  = "2bee0283f94898ff0539e70cbc563fa0";

        $imagga = new Imagga($apiKey, $apiSecret);

        $tags = $imagga->tags($url);

        $colors = $imagga->colors($url);

        $data = [
                    'image_filename'    => $image,
                    'result'            => ['tags'=>$tags, 'colors'=>$colors]
                ];

        DB::collection('image_score_imagga')->insert($data);

        return $data;

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $int
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        DB::collection('image_score_imagga')->where('_id', $id)->delete();

        return 'true';
    }
}
