<?php



Route::get('test-wew',function(){
	return 'this is just a test';
});


Route::get( 'token' , function() {
	return csrf_token();
} );


/**
* By Billy
* 
*/ 

Route::get( '/extract/csv/' , 'PredictController@extractCSV' );

// Route::get( '/extract/csv/' , function() {
// 	$csv = public_path() . '/csv/Instagram-Weekend-Pic-Link-Compiled.csv';

// 	$arr = array_map('str_getcsv', file($csv));

// 	$container = array();

// 	foreach ($arr as $key => $data) {
// 		$container[] = $data[0];
// 	}

// } );

/**
 * From Jhon
 * import from csv to mongo
 */
Route::get('import/csv', function () {

	$csv = public_path() . '/csv/report.csv';

	$arr = array_map('str_getcsv', file($csv));

	for ($i=0; $i < count($arr); $i++) {

		$d = [
			'unique_ctr'		=> $arr[$i][0],
			'ad_id'				=> $arr[$i][1],
			'ad_number'			=> $arr[$i][2],
			'target_country'	=> $arr[$i][3],
			'target_gender' 	=> $arr[$i][4],
			'campaign_name'		=> $arr[$i][19],
			'start'				=> $arr[$i][20],
			'end'				=> $arr[$i][21],
			'impression'		=> $arr[$i][22],
			'reach'				=> $arr[$i][23],
			'cpc'				=> $arr[$i][24]
		];

		// insert initial data
		\DB::collection('dummy_reports')->insert($d);

		$type 	= explode(",", $arr[$i][5]);
		$field 	= explode(",", $arr[$i][6]);

		foreach ($type as $key => $value_type) {

			$d['tags'][] = [
				'type' => $value_type,
			];

			for ($j=0; $j < count($field); $j++) { 

				if ($value_type=="emotions" && $field[$j]=="emotions") {
					$d['tags'][$key]['details'][] = [
						'field' => $field[$j],
						'value'	=> $arr[$i][7]
					];
				} elseif ($value_type=="theme") {
					if ($field[$j]=="scenario") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][8]
						];
					} elseif ($field[$j]=="indoor/outdoor") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][9]
						];
					} elseif ($field[$j]=="layout") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][10]
						];
					} elseif ($field[$j]=="weather") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][11]
						];
					}
				} elseif ($value_type=="people") {
					if ($field[$j]=="gender") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][12]
						];
					} elseif ($field[$j]=="activities") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][13]
						];
					} elseif ($field[$j]=="age") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][14]
						];
					} elseif ($field[$j]=="clothing") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][15]
						];
					} elseif ($field[$j]=="hair colour") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][16]
						];
					}
				} elseif ($value_type=="focus") {
					if ($field[$j]=="salient object") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][17]
						];
					}
				} elseif ($value_type=="dominant colours") {
					if ($field[$j]=="dominant colour") {
						$d['tags'][$key]['details'][] = [
							'field' => $field[$j],
							'value'	=> $arr[$i][18]
						];
					}
				}

			}

		}

		\DB::collection('dummy_reports')->raw()->update(["ad_id"=>$arr[$i][1]],$d);

	}

});

Route::get('import/csv/filter', function () {

	$csv = public_path() . '/csv/filter.csv';

	$arr = array_map('str_getcsv', file($csv));

	for ($i=0; $i < count($arr); $i++) { 

		$query = [

			'ad_id' 	=> $arr[$i][0],
			'ad_number'	=> $arr[$i][1]
		];

		$data = [
			'$push' => [
				'product_filter' => $arr[$i][2]
			]
		];

		\DB::collection('dummy_reports')->raw()->update($query,$data);
	}

});

Route::get('import/csv/all_user', 'ReportController@importFileData' );
Route::get('import/csv/starhub_follower', 'ReportController@importFileDataByType' );
Route::post('user/location/', 'ReportController@findByLocation' );
Route::post('user/location/info/', 'ReportController@getUserDataFile' );
Route::post('user/location/update/', 'ReportController@updateDataFile' );
Route::get('user/get/filename/', 'ReportController@getFileName' );
Route::get('user/get/passion/{passion}', 'ReportController@getPassionData' );
Route::post('user/get/passion', 'ReportController@getAllPassionData' );
Route::post('user/get/location/update', 'ReportController@locationUpdate' );
Route::post('user/get/interest/update', 'ReportController@interestUpdate' );
Route::post('user/get/count/', 'ReportController@countPassion' );
Route::post('user/get/tags/', 'ReportController@getTags' );
Route::post('graymatic/post/analize/', 'ReportController@getGraymatic' );
Route::get('import/csv/passion-points', 'ReportController@exportToCSV' );
Route::get('csv/passion-points', 'ReportController@getUserPassionData' );
Route::get('user/database/{page}', 'ReportController@getUserDatabase' );
Route::get('user/database/{category}/{elem}', 'ReportController@getUserDatabaseby' );
Route::get('user/json/csv/{filename}', 'ReportController@jsonCSV' );
Route::get('user/multiple/json/csv/', 'ReportController@multipleJSONConversion' );
Route::get('user/locals/singapore', 'ReportController@getSingaporeLocals' );
Route::post( 'user/delete/passion' , 'ReportController@deletePassionUser' );
Route::get( 'user/delete/json/text' , 'ReportController@removeJSONtext' );
Route::get( 'user/get/percentage' , 'ReportController@getUserPercentage' );
Route::post( 'user/search/image' , 'ReportController@searchTag' );
Route::get( 'user/import/filename' , 'ReportController@importFileName' );

Route::get( 'user/affinity/tagged' , 'ReportController@taggedUserLists' );
Route::get( 'user/affinity/tagged/{user}' , 'ReportController@taggedUsers' );

Route::get( 'user/clarifai/get' , 'ReportController@downloadUserClarifai' );
Route::get( 'download/force/{user}' , 'ReportController@forceDownload' );

Route::get( 'json/csv/convert' , 'ReportController@jsonCSVconverter' );
Route::post( 'json/csv/convert' , 'ReportController@getJSONdata' );


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('cache:clear');
    // return what you want
});


Route::get( '/view/clarifai/test' , 'Admin\DashboardController@getClarifaiData' );

// Route::get( '/pdf/download' , 'Admin\DashboardController@exportPDF' );

Route::get( '/export/csv/clarify' , 'Admin\DashboardController@exportCSV' );

Route::get( '/export/csv/json/{user}' , function( $user ) {
	$csv = public_path() . '/csv/audience_data.csv';

	$arr = array_map('str_getcsv', file($csv));

	$container = array();

	foreach ($arr as $key => $data) {
		if ($data[4] == $user) {
			$container[] = array(
				'id' 		=> $data[0],
				'permalink' => $data[1],
				'profile_pic' => $data[2],
				'profile' => $data[3],
				'username' => $data[4],
				'image_url' => $data[5],
				'likes' => $data[6],
				'comments' => $data[7],
				'location' => $data[8],
				'video' => $data[9],
				'image' => $data[10],
				'themes' => $data[11]
			);
		}
	}

	echo json_encode( $container );
	// echo "</pre>";

	// echo "<pre>";
	// var_dump( $container );
	// echo "</pre>";
} );
Route::get( '/export/csv/affinities/{user}' , function( $user ) {
	$csv = public_path() . '/csv/AFINITIES.csv';

	$arr = array_map('str_getcsv', file($csv));

	$container = array();

	foreach ($arr as $key => $data) {
		if ($data[2] == $user ) {
			$container[] = array(
				'profile_id' 	=> time().rand( 10,99 ),
				'profile_pic' 		=> $data[0],
				'profile_link' => $data[1],
				'profile_image' => $data[11],
				'username' => $data[2],
				'posts' => $data[3],
				'followers' => $data[4],
				'following' => $data[5],
				'affinities' => array(
					$data[6],
					$data[7],
					$data[8],
					$data[9],
					$data[10]
				)
			);
		}
		// echo "<pre>";
		// var_dump( $arr );
		// echo "</pre>";
	}

	echo json_encode( $container );
	// echo "</pre>";

} );


// {
//     $html = view( 'tpl.tpl-pdf-exports' )->render();
//     // return PDF::load($html)->download();
// });

/**
 * Authentication
 *
 */

// Route::get( '/' , 'AuthController@getLogin' );

Route::group(['middleware'=>'guest'], function() {
	Route::get('login', 	'AuthController@getLogin');
	Route::post('login',	'AuthController@postLogin');
	Route::post('sendEmail', 'EmailController@sendEmail');
});

Route::get('logout',	'AuthController@getLogout');

Route::get('register',	'AuthController@getRegister');
Route::post('register',	'AuthController@postRegister');

Route::group(['middleware'=>'auth'], function() {

	// Client
	Route::get('dashboard',	'Client\DashboardController@index');

	Route::group(['prefix'=>'api'], function() {
		Route::resource('user/client', 'Client\UserController',['only'=>['index']]);
	});

	// Admin
	// Route::get('admin',	'Admin\DashboardController@index');

});

// Note: jhon
// isulod dayon ni sa auth nga middleware after, for testing purpose
Route::group(['prefix'=>'api'], function() {

	Route::get('graymatic', 					'PredictAPIController@getGraymatic');
	Route::post('graymatic', 					'PredictAPIController@postGraymatic');
	// Graymatic API
	Route::get('graymatic-clarifai/{image}',	'PredictAPIController@removeGraymaticAndClarifai');

	Route::get('uploads',		'Admin\UploadController@index');
	Route::resource('imagga', 	'ImaggaController', ['only'=>['index','store','destroy']]);		// Imagga API

	// Dummy Report
	Route::get('report',		'ReportController@index');
	// Route::get('report/excel', 	'ReportController@generateExcel');
	// this should be consolidated to the upper part, use aggregation for this
	Route::get('report-tags', 'ReportController@tags');

	Route::group(['prefix'=>'admin'], function() {
		Route::get('report/clarifai', 'Admin\ReportController@getClarifai');
	});

});

// Route::post('send-email-contact', 'PageController@sendEmailContact');


/**
 * Test IG API
 *
 * test IG api like websta
 * pull out themes and trends
 */
Route::get('ig/auth', 'InstagramAPIController@auth');


Route::get( 'getuserdata' , 'AuthController@getUserData' );


/* Page Routings */ 
// Route::get( '/' , 'HomeController@home' );

Route::get( '/' , 'HomeController@home' );


Route::get( '/pricing' , 'HomeController@pricing' );

Route::get( '/testimonials' , 'HomeController@testimonials' );

Route::get( '/sample-ad' , 'HomeController@sample_ad' );

Route::get( '/login-customer' , 'HomeController@login_customer' );

Route::get( '/creative' , 'HomeController@creative' );

Route::get( '/login-creative' , 'HomeController@creative_login' );

Route::get( '/privacy' , 'HomeController@privacy' );

Route::get( '/terms' , 'HomeController@terms' );

Route::get( '/creative-terms' , 'HomeController@creative_terms' );

Route::get( '/about' , 'HomeController@about' );

Route::get( '/platform' , 'HomeController@platform' );
Route::get( '/graymatic' , function( ){
	return view('graymatic');
} );

// Route::get( '/dashboard' , ['middleware'=>'auth','uses'=>'DashboardController@index'] );

// Route::get( '/admin' , ['middleware' => 'auth', 'uses' => 'DashboardController@admin'] );


Route::group(['middleware' => 'guest'], function() {
	Route::get('login-admin', 'DashboardController@loginAdmin');
	Route::post('auth-admin', 'DashboardController@authAdmin');
});

Route::get('logout-admin', 'DashboardController@logoutAdmin');



Route::get( '/admin' , ['middlewarex' => 'auth', 'uses' => 'DashboardController@admin']);

Route::get( '/location/get' , 'LocationController@getAllLocations' );
Route::post( '/location/create' , 'LocationController@createLocation' );
Route::post( '/location/update' , 'LocationController@update' );
Route::post( '/location/delete' , 'LocationController@removeLocation' );

Route::get( '/get/loaded_js/{page_number}' , function( $page )
{
	$csv = public_path() . '/csv/AFINITIES.csv';
	$arr = array_map('str_getcsv', file($csv));
	$container = array();
	$perPage = 12;
    $x = ($page - 1) * $perPage;
   	$z = $page * $perPage;
   	$y = ($z > count($arr)) ? (count($arr)-1) : $z;

   	array_splice($arr, 0, 1);

   	for(; $x < $y; $x++) {
    	$container[$x] = array(
    		'profile_id' 			=> time().rand( 10,99 ),
    		'profile_pic'	=> $arr[$x][0],
    		'profile_link'	=> $arr[$x][1],
    		'profile_image' => $arr[$x][11],
    		'username'	=> $arr[$x][2],
    		'posts'	=> $arr[$x][3],
    		'followers'	=> $arr[$x][4],
    		'following'	=> $arr[$x][5],
    		'affinities' => array(
					$arr[$x][6],
					$arr[$x][7],
					$arr[$x][8],
					$arr[$x][9],
					$arr[$x][10],
				)
    	);
   	}
   	// echo "<pre>";
   	// var_dump($container);
   	// echo "</pre>";
    echo json_encode($container);
} );
Route::get( '/get/audience/{page_number}' , function( $page )
{
	$csv = public_path() . '/csv/AUTDIENCE-AFINITIES.csv';
	$arr = array_map('str_getcsv', file($csv));
	$container = array();
	$perPage = 3;

    $x = ($page - 1) * $perPage;
   	$z = $page * $perPage;
   	$y = ($z > count($arr)) ? (count($arr)-1) : $z;

   	array_splice($arr, 0, 1);

   	for(; $x < $y; $x++) {
	    if ( isset( $arr[$x] ) ) {
	    	$container[$x] = array(
	    		'profile_id' 			=> time().rand( 10,99 ),
	    		'profile_pic'	=> $arr[$x][0],
	    		'profile_link'	=> $arr[$x][1],
	    		'username'	=> $arr[$x][2],
	    		'posts'	=> $arr[$x][3],
	    		'followers'	=> $arr[$x][4],
	    		'following'	=> $arr[$x][5],
	    		'recent'	=> $arr[$x][6],
	    		'affinities' => array(
						$arr[$x][7],
						$arr[$x][8],
						$arr[$x][9]
					)
	    	);
	    }
   	}
   	// echo "<pre>";
   	// var_dump($container);
   	// echo "</pre>";
    echo json_encode($container);
} );

/* Predic Routes */ 
Route::post( 'createPrediction' , 'PredictController@createPredict' );
Route::get( 'getClientLists' , 'PredictController@adminDashbordPredict' );
Route::get( 'getClientExperiments/{id}' , 'PredictController@getClientExperiments' );
Route::get( 'getSingleExperiment/{id}' , 'PredictController@getSingleExperiments' );

// UPLOAD FILE IMAGES
Route::post( 'upload-experiments' , 'Admin\UploadController@storeDataImage' );
Route::get( 'getCategoryImages/{category}' , 'Admin\UploadController@getCategoryImages' );
Route::get( 'getSingleImage/{filename}' , 'Admin\UploadController@getSingleImagebyFilename' );
Route::post( 'downloadImage' , 'Admin\UploadController@downloadImage' );

// Client Image Experiment
Route::post( 'addimageexperimentattribute' , 'Admin\UploadController@addClientImageAttributes' );
Route::post( 'updateimageexperimentattribute' , 'Admin\UploadController@updateClientImageAttributes' );
Route::post( 'addclientexperimentimage' , 'PredictController@addImageToExperiment' );
Route::post( 'storeImageTagValue' , 'Admin\UploadController@storeImageTagValue' );
Route::post( 'deleteExperiment' , 'PredictController@removeExperiment' );
Route::post( 'deleteImageUpload' , 'Admin\UploadController@removeImageUpload' );
Route::get( 'deleteAPIImage/{id}' , 'Admin\UploadController@removeImageAPI' );
Route::post( 'userImageUploads' ,  'Admin\UploadController@userImageUploads' );
Route::post( 'deleteUserImage' ,  'Admin\UploadController@deleteUserImage' );
Route::post( 'deleteUserGroup' ,  'Admin\UploadController@deleteUserGroup' );

/* Facebook Login */
Route::get('/auth/fbloginurl/{usertype}', 'AuthenticationController@FBlogin' ); // Generate a login URL

Route::get('/facebook/callback', 'AuthenticationController@FBcallback' );
// End Facebook Login


/* For AngularJS RESTFul API */
/*Route::post( 'accountStatus' , 'UserController@accountStatus' );

Route::post( 'createUserInfo' , 'UserController@createUserInfo' );*/

Route::post( 'createBriefCampaign' , 'CampaignController@createBriefCampaign' );

Route::get( 'campaigns/deleteCampaign/{id}', 'CampaignController@deleteCampaign' );

Route::get( 'campaigns/count/{uniqid}' , 'CampaignController@countCampaigns' );

Route::get( 'campaigns/bystatus' , 'CampaignController@fetchCampaignBStatus' ); // Fetch Campaign separted by status

Route::resource( 'campaigns', 'CampaignController');

Route::post( 'campaignsUpdate', 'CampaignController@updateCampaign' );

// Route::get( 'sessionUserInfo', 'UserController@getFBUserInfo' ); // Fetch Facebook UID

// Route::get( 'logout' , 'UserController@sessionDestroy' );
// Route::get( 'access_token/{token_id}', 'MarketingAPIController@test' );

Route::post( 'upload', 'DashboardController@upload' );

Route::get( 'downloadBrief/{num}', 'PredictController@downloadBrief' );

// Email API
Route::get( 'sendLaunchCampaignEmail' , 'CampaignController@sendLaunchCampaignEmail' );

// Tests
Route::get( 'api-test' , function () {
	return view( 'welcome' );
} );

Route::get( 'showuser' , 'MarketingAPIController@getAdUser' );

Route::get( 'checkifloggedin' , 'AuthenticationController@login_test' );

Route::get( 'createaduser' , 'MarketingAPIController@getAdsImage' );

Route::get( 'session' , 'MarketingAPIController@getSession' );

Route::get( 'test-rani' , 'MarketingAPIController@testGraphAPI' );

Route::get( 'updateValue' , 'PredictController@updateValue' );

Route::get( 'test' , function() {
	return DB::table('users')->get();
} );

Route::get( 'mockup' , function(){
	return view("mockup");
} );

/* For AngularJS Path */ 
Route::any('{path?}', function()
{	
    return view("errors.503");
})->where("path", ".+");

