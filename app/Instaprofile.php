<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instaprofile extends Model
{
    protected $collection = "insta_profile";
	protected $guarded  = [ 'id' ];
}
