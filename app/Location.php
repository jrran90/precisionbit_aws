<?php

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class Location extends Eloquent
{
	protected $collection = "location";
	protected $guarded  = [ 'id' ];
}
