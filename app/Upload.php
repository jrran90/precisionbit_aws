<?php

namespace App;

use Jenssegers\Mongodb\Model as Eloquent;
use Illuminate\Database\Eloquent\Model;

class Upload extends Eloquent
{
	protected $collection = "uploads";
	protected $guarded  = [ 'filename' , 'category' , 'id' ];
}
