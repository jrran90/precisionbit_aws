<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>

<link href="{{ asset('/css/index-mockup.css') }}" rel="stylesheet">
<link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('/css/font-awesome.min.css') }}" rel="stylesheet">
<body>
	<header id="header" class="fixed-top">
		<div class="nav-container">
			<nav class="navbar">
				<div class="container-fluid">
					<div class="navbar-header">
					  <button id="open_navbar" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				        <span class="sr-only">Toggle navigation</span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				        <span class="icon-bar"></span>
				      </button>
				      <a class="navbar-brand" href="#">
				        <!-- <img id="black" src="../img/logo/PBit logo black transparent.png" class="img-10p" style="position: absolute;"> -->
				        <!-- <img id="white" src="../img/logo/PBit logo white transparent.png" class="img-10p" style="position: absolute;display: none"> -->
				        <h4 id="black" style="font-size: 40px;color: #333;font-family: Raleway-Bold;display: none">PrecisionBit</h4>	
				        <h4 id="white" style="font-size: 40px;color: #FFF;font-family: Raleway-Bold">PrecisionBit</h4>	
				      </a>
				    </div>

				    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" >
				      <ul class="nav navbar-nav navbar-right" >
						<li><a href="" >Solutions</a></li>
						<!-- <li><a href="" >Services</a></li> -->
						<!-- <li><a href="" >Resources</a></li> -->
						<!-- <li><a href="" >Blog</a></li> -->
						<li><a href="" >About</a></li>
						<!-- <li><a href="" >Login</a></li> -->
						<li><a href="#content-six" ><span class="btn btn-yellow">Request a Demo</span></a></li>
					  </ul>
				    </div><!-- /.navbar-collapse -->
				
				</div>
			</nav>

		</div>
	</header>

	<section id="content-one" class="block-section">

		<div class="overlay"></div>
		
		<video id="video1" onended="vidEnded();" autoplay="autoplay">
		   
		   <source src="../img/new-mockup/videos/Splash_V2_1080p.webm" type="video/webm">
	  	</video>

		<div class="container">
			<div class="col-md-12 text-center color-white" style="z-index: 5;">
				<div class="white-space-40 hidden-sm hidden-xs"></div>
				<div class="white-space-10 hidden-sm hidden-xs"></div>
				<div class="heading">
					<h3>Smarter Marketing through visual intelligence</h3>	
				</div>
				<div class="white-space-40 hidden-sm hidden-xs"></div>
				<div class="white-space-10 hidden-sm hidden-xs "></div>
				<!-- <div class="logo">
					<img src="../img/adhero_image.png" class="img-10p">
				</div> -->
				
			</div>
		</div>


	</section>

	<section id="content-two" class="block-section-md">
		<div class="container">
			<div class="col-md-12 text-center">
				<div class="header-icon">
					<img src="../img/logo/PBit logo black transparent.png" class="img-20p">
				</div>
				<!-- <div class="header">
					<h2>Experience Cloud™</h2>
				</div> -->
				<div class="white-space-20"></div>
				<div class="white-space-10"></div>
				<p class="color-gray-mute">
					We’ve created a new class of enterprise software that helps employees across the front office come together and engage with customers through one unified platform. With PrecisionBit, brands can truly know their customers – not as data points, but as people. With deeper, richer context across touchpoints, organizations can seamlessly craft meaningful experiences customers will want to share with others.
				</p>
				<div class="white-space-20"></div>
				<div class="white-space-20 text-center">
					<button class="btn btn-blue" style="padding: 7px 90px !important;">LEARN MORE</button>
				</div>

				
			</div>
		</div>
	</section>

	<section id="content-three" class="block-section-md parallax-window" data-parallax="scroll" data-image-src="img/Singapore-Night-Skyline-Background as Smart Object-1.png">
		<div class="overlay"></div>
		<div class="container">
			<div class="col-md-6 map-left no-padding-left text-left color-white">
				<div class="white-space-40"></div>
				<div class="white-space-40"></div>
				<p style="font-size: 30px;">THE WAY TO</p>
				<p style="font-size: 40px;"><b>CUSTOMER-FIRST</b></p>
				<p style="font-size: 35px;">FOR MARKETERS</p>
				<p style="font-size: 16px;">Rally your company around <b>digital transformation</b></p>
				<button class="btn btn-blue btn-sky" style="padding:10px 70px !important;background:#1C8ACB !important; ">RSVP FOR THE LIVECAST</button>
			</div>
			<div class="col-md-6 map-right">
				<img src="../img/dot map copy (1).png" class="img-full">
				<img src="../img/sg-circle.png" class="" style="position: absolute;top: 10px;left: 5px;width: 90%;">
			</div>
		</div>
	</section>

	<section id="content-four" class="block-section-sm ">
		<div class="container-fluid">
			<div class="col-md-12 no-padding text-center">
				<div class="white-space-10"></div>
			
				<p>We provide the world’s most complete enterprise social technology to 1,000+ brands, including:</p>
				<div class="white-space-20"></div>
				
				<div class="col-md-12 logos" >
					<div class="fadeLeft"></div>
					<div class="fadeLeft2"></div>
					<div class="fadeLeft3"></div>
					<div class="fadeLeft4"></div>
					<div class="fadeLeft5"></div>
					<div class="fadeLeft6"></div>
					<div class="fadeLeft7"></div>
					<div class="fadeLeft8"></div>
					<div class="fadeRight"></div>
					<div class="fadeRight2"></div>
					<div class="fadeRight3"></div>
					<div class="fadeRight4"></div>
					<div class="fadeRight5"></div>
					<div class="fadeRight6"></div>
					<div class="fadeRight7"></div>
					<div class="fadeRight8"></div>
					<div class="fadeRight"></div>

					<div class="logo-container">
						
					</div>
					
				</div>
				
			</div>
		</div>
	</section>

	<section id="content-five" class="block-section-sm">
		<div class="container-fluid">
			<div class="col-md-12 no-padding text-center">
			
				<p>Check out live content from the PrecisionBit community:</p>
				<div class="white-space-40"></div>
				<div class="gallery-container">
					<div class="img-container" style="background: url('../img/new-mockup/1.jpg')">
						<div class="bg"></div>
					</div>
					<div class="img-container" style="background: url('../img/new-mockup/2.jpg')">
						<div class="bg"></div>
					</div>
					<div class="img-container" style="background: url('../img/new-mockup/3.jpg')">
						<div class="bg"></div>
					</div>
					<div class="img-container" style="background: url('../img/new-mockup/4.jpg')">
						<div class="bg"></div>
					</div>
					<div class="img-container" style="background: url('../img/new-mockup/5.jpg')">
						<div class="bg"></div>
					</div>
					<div class="img-container" style="background: url('../img/new-mockup/6.jpg')">
						<div class="bg"></div>
					</div>
					<div class="img-container" style="background: url('../img/new-mockup/7.jpg')">
						<div class="bg"></div>
					</div>
					<div class="img-container" style="background: url('../img/new-mockup/8.jpg')">
						<div class="bg"></div>
					</div>
					<div class="img-container" style="background: url('../img/new-mockup/9.jpg')">
						<div class="bg"></div>
					</div>
					<div class="img-container" style="background: url('../img/new-mockup/10.jpg')">
						<div class="bg"></div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section id="content-six" class="block-section-sm bg-color2">
		<div class="container-fluid">
			<div class="col-md-12 text-center">
				<div class="header-icon">
					<img src="../img/logo/PBit logo black transparent.png" class="img-20p">
				</div>
				<div class="white-space-20"></div>
				<div class="white-space-20"></div>

				<form class="text-left color-gray-mute">
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<label>First Name*</label>
							<input type="text" class="form-control">
						</div>
						<div class="form-group">
							<label>Business Email*</label>
							<input type="text" class="form-control">
						</div>
						<div class="form-group">
							<label>Role*</label>
							<select class="form-control">
								<option selected=""></option>
								<option>Manager</option>
								<option>Client</option>
								<option>Employee</option>
							</select>
						</div>
					</div>
					<div class="col-md-6 col-sm-12">
						<div class="form-group">
							<label>Last Name*</label>
							<input type="text" class="form-control">
						</div>
						<div class="form-group">
							<label>Business Phone*</label>
							<input type="text" class="form-control">
						</div>
						<div class="form-group">
							<label>Company Name*</label>
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<label>How will your company use PrecisionBit?*</label>
							<select class="form-control">
								<option selected=""></option>
								<option>End User</option>
								<option>Agency</option>
							</select>
						</div>
						<div class="form-group">
							<label>Company Revenue*</label>
							<select class="form-control">
								<option selected=""></option>
								<option>Under $10M</option>
								<option>$10M to $100</option>
							</select>
						</div>
					</div>

					<div class="col-md-12 text-center" style="margin-top: 40px">
						<button class="btn btn-blue" style="padding: 15px 55px !important;font-size: 16px !important">REQUEST A DEMO</button>
					</div>
				</form>

			</div>
		</div>
	</section>

	<footer>
		<div class="container-fluid">
			<!-- <div class="col-md-6 left">
				<div class="img-logo" style="padding: 6px 21px 6px 0;width: 120px;">
					<img src="../img/logo/PBit logo white transparent.png" class="img-20p" style="position: absolute;top: 0">
				</div>
				<div class="lists">
					<ul class="list-inline">
						<li><a href="">Support</a></li>
						<li><a href="">Developers</a></li>
						<li><a href="">Careers</a></li>
						<li><a href="">Contact</a></li>
					</ul>
					<ul class="list-inline">
						<li><a>&copy;2016 PrecisionBit INC.</a></li>
						<li><a href="">Privacy</a></li>
						<li><a href="">Terms</a></li>
						<li><a href="">Licenses</a></li>
						<li><a href="">Languages</a></li>
					</ul>
				</div>
			</div> -->
			<!-- <div class="col-md-6 text-right right">
				<ul class="list-inline">
					<li><a href=""><i class="fa fa-facebook"></i></a></li>
					<li><a href=""><i class="fa fa-twitter"></i></a></li>
					<li><a href=""><i class="fa fa-youtube-play"></i></a></li>
					<li><a href=""><i class="fa fa-linkedin-square"></i></a></li>
				</ul>
				<ul class="list-inline">
					<li><a href=""><i class="fa fa-apple"></i></a></li>
					<li><a href=""><i class="fa fa-android"></i></a></li>
				</ul>
			</div> -->
			<div class="col-md-7 col-sm-12 col-xs-12">
				<div class="lists">
					<ul class="list-inline one">
						<li><a href="">About</a></li>
						<li><a href="">Blog</a></li>
						<li><a href="">Support</a></li>
						<li><a href="">Careers</a></li>
					</ul>
					<ul class="list-inline two">
						<!-- <li><a>&copy;2016 PrecisionBit INC.</a></li> -->
						<li>
							<a href="">
								Boston HQ<br>
								36 Bromfield St<br>
								Boston, MA 02108
							</a>
						</li>
						<li>
							<a href="">
								San Francisco<br>
								44 Tehama St<br>
								San Francisco, CA 94105
							</a>
						</li>
						<li>
							<a href="">
								New York<br>
								115 W 18th St<br>
								New York, NY 10017
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-5 col-sm-12 col-xs-12 text-right">
				<div class="lists">
					<ul class="list-inline three">
						<li><a href=""><i class="fa fa-twitter"></i></a></li>
						<li><a href=""><i class="fa fa-instagram"></i></a></li><br>
						<li><a href=""><i class="fa fa-linkedin-square"></i></a></li>
						<li><a href=""><i class="fa fa-facebook-square"></i></a></li>
					</ul>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12">
				<div class="lists">
					<ul class="list-inline four">
						<li>
							<a>
								Crafted Lovingly by CO Everywhere<br>
								© 2012 - 2016 CO Everywhere, Inc. Patent Pending
							</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="col-md-6 col-sm-12 col-xs-12 text-right">
				<div class="lists">
					<ul class="list-inline five">
						<li class="separator"><a href="">Terms of Service</a></li>
						<li><a href="">Privacy Policy</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
</body>

<script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<script src="{{ asset('/js/parallax.min.js') }}"></script>
<!-- <script src="{{ asset('/js/bootstrap.min.js') }}"></script> -->

<script type="text/javascript">
	$(function(){

		
		

	    var $window = $(window),
	        $header = $('#header'),
	        $this   = $(this); // <-----here you can cache your selectors

	    $window.on('scroll', function(){
	       if($this.scrollTop() > 0 ){
	           $('#header').addClass('header-bg');
	           $('#header .nav > li > a').addClass('color-gray');
	           $('#white').hide();
	           $('#black').show();
	       }else{
	           $('#header').removeClass('header-bg');
	           $('#header .nav > li > a').removeClass('color-gray');
	           $('#white').show();
	           $('#black').hide();
	       }
	    }).scroll();

	    var vid = document.getElementById("video1");
		
		vid.onended = function() {
			console.log("ended")
		    vid.load();
		    vid.play();
		};

		var ctr = 0;
		var ctr2 = 0;
		var ctrWidth = 3245;
		var add = 0;

		var brands = [
			['img/new-mockup/logos/Augure.png',''],
			['img/new-mockup/logos/Brandwatch.png',''],
			['img/new-mockup/logos/Hootsuite.jpg',''],
			['img/new-mockup/logos/Cision.png',80],
			['img/new-mockup/logos/Engagement Labs.png',''],
			['img/new-mockup/logos/Hottolink.jpg',''],
			['img/new-mockup/logos/Innodata.png',''],
			['img/new-mockup/logos/iSentia.jpg',''],
			['img/new-mockup/logos/link fluence.png',''],
			['img/new-mockup/logos/Meltwater.png',''],
			['img/new-mockup/logos/News Group International.jpg',''],
			['img/new-mockup/logos/NM Incite.jpg',''],
			['img/new-mockup/logos/Oracle.png',''],
			['img/new-mockup/logos/Apple.png',''],
			['img/new-mockup/logos/Lithium Technologies.png','']
		]

		function initializeBrands(){
			for( var x = 0; x < brands.length; x++ ){
				$(".logo-container").append('<div class="img-logo"><img src="'+brands[x][0]+'" style="height:'+brands[x][1]+'%"></div>');
			}
		}

	    function slideLogos(){

	    	ctr--;
	    	ctr2++;
	    	ctrWidth+=20;

	    	$(".logo-container").css({left: ctr + 'px', width:ctrWidth });

	    	setTimeout(function(){
	    		if(ctr2 >= 100){	
	    			// $(".img-logo:nth-child(1)").remove();
	    			$(".logo-container").append('<div class="img-logo"><img src="'+brands[add][0]+'" style="height:'+brands[add][1]+'%"></div>');
	    			ctr2 = 0;

	    			if(add == 14){
	    				add = 0;
	    			}else{
	    				add++;
	    			}
	    		}

			    slideLogos();
		    },10);
		    
	    }

	    initializeBrands();

	    slideLogos();

	    var clickCtr = 0;

	    $('#open_navbar').click(function(){
	    	if(clickCtr == 0){
	    		$('header').css({height:'225px'});
	    		clickCtr = 1;
	    	}else{
	    		$('header').css({height:'63px'});
	    		clickCtr = 0;
	    	}
	    	
	    });

	});
</script>

</html>