@extends('tpl.tpl-dashboard-client')
@section('title', 'PrecisionBit | Dashboard')


@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('/css/angular-material.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/autocomplete.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/fileinput.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/sweetalert.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/loading-bar.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/jquery.mCustomScrollbar.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/autocomplete.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/iCheck/all.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/flot.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/new-animation.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/font-awesome.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.css') }}">
  <!-- <link href='https://fonts.googleapis.com/css?family=Raleway:800,400' rel='stylesheet' type='text/css'> -->
  <link rel="stylesheet" href="{{ asset('/css/fontello.css') }}">
  <link rel="stylesheet" href="{{ asset('/fontcustom-neuro/fontcustom.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/precision.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/angular-chart.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/client-side.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/new-dashboard.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/predict-result-v2.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/influencers-v2.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/index-v6.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/travelers.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset( '/library/owl-carousel/owl.carousel.css' ) }}">
  <link rel="stylesheet" type="text/css" href="{{ asset( '/library/owl-carousel/owl.theme.css' ) }}">
  <link rel="stylesheet" type="text/css" href="{{ asset( '/library/bootstrap/bootstrap-switch.min.css' ) }}">

  <script src="{{ asset('/library/bootstrap/jquery.min.js') }}"></script>
  <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <script src="{{ asset('/plugins/chartjs/Chart.js') }}"></script>
  <script src="{{ asset('/library/charts/Chart.Spie.js') }}"></script>
  <script src="{{ asset('/library/charts/d3.v3.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/wow.min.js' ) }}"></script>
  <script src="{{ asset('/library/bootstrap/jquery.easing.min.js') }}"></script>
  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?libraries=drawing,places"></script>

@endsection

@section('client-dashboard')
  <div class="loader-wrapper">
      <div class="loader-wrapper-info text-center">
        <!-- <h4 style="font-family: 'Helvetica'">PrecisionBit is loading the Page...</h4> -->
        <img src="img\logo\PBit logo black transparent.png" style="width: 60%">
      </div>
  </div>
	<div class="wrapper custom-wapper">
		<header class="main-header">
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" style="background-color: #273341 !important;" role="navigation">
          <!-- Sidebar toggle button-->
          <!-- <a href="#" class="sidebar-toggle custom-side-bar-toggle" data-toggle="offcanvas" role="button" style="color: #28A6A0;">
            <span class="sr-only">Toggle navigation</span>
          </a> -->
          <div class="navbar-logo">
            <ul class="nav navbar-nav">
              <li>
                <!-- Logo -->
                <a ui-sref="home" style="padding: 2px 15px !important;" class="logo">
                  <!-- mini logo for sidebar mini 50x50 pixels -->
                  <span class="logo-mini">PB</span>
                  <!-- logo for regular state and mobile devices -->
                  <span class="logo-lg log-text logo-container"><img src="../img/logo/PBit logo white transparent.png"></span>
                </a>
              </li>
            </ul>
          </div>

          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav ul-borders">
              <li class="dropdown search-nav-btn-custom">
                <a href="" class="menu-header-global dropdown-toggle" data-toggle="modal" data-target="#searchModal">
                  <span>New Search +</span>
                </a>
              </li>
              <li class="dropdown">
                <a ui-sref="home" class="menu-header-global home-a dropdown-toggle active-menu" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-home fa-icon" ></span>
                </a>
              </li>
              <li class="dropdown">
                <a ui-sref="social_listening" class="menu-header-global predict-a dropdown-toggle" data-toggle="dropdown">
                  <span class="icon-microscope" style="color: #fff"></span>
                </a>
              </li>
              <!-- <li class="dropdown">
                <a ui-sref="location" class="menu-header-global pin-a dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-map-marker fa-icon" style="color: #fff"></span>
                </a>
              </li> -->
              <li class="dropdown">
                <a ui-sref="micro-influencer"  class="menu-header-global user-a dropdown-toggle" data-toggle="dropdown">
                  <span class="glyphicon glyphicon-user fa-icon" style="color: #fff"></span>
                </a>
              </li>
              <li class="dropdown">
                <a ui-sref="content-planning" class="menu-header-global bulb-a dropdown-toggle" data-toggle="dropdown">
                  <!-- <span class="glyphicon glyphicon-user fa-icon" style="color: #fff"></span> -->
                  <span><img src="{{ asset( 'img/light-bulb.png' ) }}" class="img-responsive" style="width: 15px;padding-bottom: 5px"></span>
                </a>
              </li>
              <li class="dropdown">
                <a ui-sref="brands" class="menu-header-global brands-a dropdown-toggle" data-toggle="dropdown">
                  <!-- <span class="glyphicon glyphicon-user fa-icon" style="color: #fff"></span> -->
                  <span><img src="{{ asset( 'img/brands.png' ) }}" class="img-responsive" style="width: 15px;padding-bottom: 5px"></span>
                </a>
              </li>
            </ul>
            <ul class="nav navbar-nav navi-right">
              <!-- Messages: style can be found in dropdown.less-->
                <li class="dropdown messages-menu">
                  <a href="javascript:void(0)" class="menu-header-global settings-a dropdown-toggle" data-toggle="dropdown" style="overflow:hidden">
                    <!-- <i class="icon-gear custom-glyphicon-cog" style="color: #fff"></i> -->
                    <div class="pull-left user-profile-img" style="border: 2px solid #fff;border-radius:50%">
                      <img src="{{ asset( 'img/avatar5.png' ) }}" class="img-circle img-responsive" width="29">
                    </div>
                    <i class="fa fa-caret-down pull-right" style="font-size:18px;position:relative;top:8px;margin-left:5px;"></i>
                  </a>
                  <ul class="dropdown-menu wow fadeInUp" style="top:54px;background: #0E9F8F;border:#283342;padding:26px 0px 0px;" data-wow-delay="0.5s" data-wow-duration="0.5s">
                    <li>
                      <div class="profile-pic-wrapper text-center wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s">
                          <img src="{{ asset( 'img/avatar5.png' ) }}" class="img-circle img-responsive" width="145" style="width:145px;border: 2px solid #fff;margin:auto;">
                          <p class="user-name">Signed in as <span>{{ ucfirst(Auth::user()->firstname)}}&nbsp;{{ucfirst(Auth::user()->lastname)}}</span></p>
                      </div>
                    </li>
                   <!--  <li role="separator" class="divider"></li> -->
                      <div class="exit-bar">
                        <a href="#" class="pull-left wow fadeInUp" style="display:inline-block;clear:none;font-size:21px;background:transparent!important;" data-wow-delay="0.6s" data-wow-duration="0.6s">
                          <i class="icon-settings custom-icon-style"></i>  
                        </a>
                        <a href="/logout" class="pull-right wow fadeInUp" style="display:inline-block;clear:none;font-size:21px;background:transparent!important;" data-wow-delay="0.7s" data-wow-duration="0.7s">
                          <i class="icon-exit custom-icon-style"></i>  
                        </a>
                      </div>
                  </ul>
                    <!-- <li> -->
                    <!-- </li> -->
                </li>
            </ul>

          </div>
        </nav>
        <section class="company-name-container" hidden>
	        	<p class="pull-left text-first"></p>
	        	<p class="pull-right text-second">Signed in as <span>{{Auth::user()->firstname}}&nbsp;{{Auth::user()->lastname}}</span>  <a href="{{url('logout')}}" style="color: #181A27;">Logout</a></p>
	        	<section class="clear-both"></section>
	        </section>
	     </header>
	    <div class="content-wrapper">
      	<section class="content">

      		<div class="section-content row">
            <!-- <div ui-view="side-menu" class="ui-view-container"></div> -->
      			<div ui-view="main" class="ui-view-container"></div>
      		</div>



  	        <section class="network-status" hidden>
  	          <section class="content-header">
  	            <h1>
  	              500 Error Page
  	            </h1>
  	          </section>
  	          <section class="content">
  	            <div class="error-page">
  	              <h2 class="headline text-red">500</h2>
  	              <div class="error-content">
  	                <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
  	                <p>
  	                  Please make sure that you are connected to the internet.
  	                </p>
  	              </div>
  	            </div><!-- /.error-page -->
  	          </section>
  	        </section>
  	      </section>
        </div>

      <!-- pop menu -->
        <md-fab-speed-dial md-open="menu.isOpen" md-direction="up" ng-class="md-fling" class="md-fab-bottom-right md-scale hidden-md hidden-lg visible-xs visible-sm lock-size pop-up-menu" ng-cloak>
          <md-fab-trigger>
            <md-button aria-label="menu" class="md-fab md-warn">
              <md-icon md-font-icon="glyphicon glyphicon-menu-hamburger"></md-icon>
            </md-button>
          </md-fab-trigger>
          <md-fab-actions>
            <md-button aria-label="Home" class="md-fab md-raised md-mini" ui-sref="home">
              <md-icon md-font-icon="fa fa-home" aria-label="Home"></md-icon>
            </md-button>
            <md-button aria-label="Predict" class="md-fab md-raised md-mini" ui-sref="predict">
              <md-icon md-font-icon="fa fa-flask" aria-label="Predict"></md-icon>
            </md-button>
            <md-button aria-label="Report" class="md-fab md-raised md-mini" ui-sref="home">
              <md-icon md-font-icon="fa fa-map-marker" aria-label="Report"></md-icon>
            </md-button>
            <md-button aria-label="User" class="md-fab md-raised md-mini" ui-sref="home">
              <md-icon md-font-icon="fa fa-user" aria-label="User"></md-icon>
            </md-button>
            <md-button aria-label="Report" class="md-fab md-raised md-mini" ui-sref="upload">
              <md-icon md-font-icon="icon-photograph15" aria-label="Report"></md-icon>
            </md-button>
            <md-button aria-label="Report" class="md-fab md-raised md-mini" ui-sref="summary">
              <md-icon md-font-icon="icon-suitcase54" aria-label="Report"></md-icon>
            </md-button>
            <md-button aria-label="Settings" class="md-fab md-raised md-mini" ui-sref="home">
              <md-icon md-font-icon="icon-gear" aria-label="Settings"></md-icon>
            </md-button>
          </md-fab-actions>
        </md-fab-speed-dial>
      <!-- footer -->
      <!-- <footer class="main-footer">
        <span>Copyright <a href="https://www.facebook.com/youradhero" style="color:#5f6b64;font-weight:bold;">PrecisionBit</a> 2016. All rights reserved.</span>
      </footer> -->
   </div>
@endsection

@section( 'scripts' )
  <script src="{{ asset( 'js/jquery-ui.min.js' ) }}"></script>
  <script src="{{ asset( 'library/angular/lodash.js' ) }}"></script>
	<script src="{{ asset( 'library/bootstrap/randomColor.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.toaster.js' ) }}"></script>
	<script src="{{ asset( 'library/bootstrap/bootstrap.min.js' ) }}"></script>
	<script src="{{ asset('plugins/canvas-to-blob.min.js') }}"></script>
	<script src="{{ asset('library/bootstrap/fileinput.min.js') }}"></script>
	<script src="{{ asset( 'js/moment.min.js' ) }}"></script>
	<script src="{{ asset( 'plugins/fastclick/fastclick.min.js' ) }}"></script>
  <script src="{{ asset('/library/charts/d3pie.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/charts/plotly-latest.min.js' ) }}"></script>
  <script src="{{ asset('library/bootstrap/animatescroll.min.js') }}" type="text/javascript"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/sweetalert.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/bootstrap-switch.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.mCustomScrollbar.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/owl-carousel/owl.carousel.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-animate.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-aria.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-material.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/owl-carousel/angular-owl-carousel.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/charts/angular-chart.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/ng-map.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-ui-router.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/angular-filter.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-simple-logger.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-google-maps-street-view_dev_mapped.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/vs-google-autocomplete.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/vs-autocomplete-validator.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-google-maps.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/ngStorage.min.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/angular.country-select.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/loading-bar.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'library/angular/ng-file-upload.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/ordinal-browser.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-counter.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular-d3.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/user-info.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/brief.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/image-request.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/notification.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/report.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/notification.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/graph.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/map.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/directive/influencers.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/upload.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/predict.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/locations.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/directive/dashboard-client.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/main/client.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/factory/factory.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/service/service.js' ) }}"></script>
	<script type="text/javascript" src="{{ asset( 'js/controller/client.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/dirPagination.js' ) }}"></script>
  <script type="text/javascript">
  $(document).ready(function( ) {
    new WOW().init();
  });
</script>
@endsection