@extends('tpl.tpl-dashboard')
@section('title', 'PrecisionBit | Dashboard')
@section('stylesheets')
  <link rel="stylesheet" href="{{ asset('/css/angular-material.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/bootstrap.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/jasny-bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/sweetalert.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/custom.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/loading-bar.css') }}">
  <link rel="stylesheet" href="{{ asset('/fontcustom/fontcustom.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/iCheck/all.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/flot.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/dist/css/AdminLTE.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/skins/_all-skins.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/iCheck/flat/blue.css') }}">
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="{{ asset('/plugins/ionslider/ion.rangeSlider.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/ionslider/ion.rangeSlider.skinNice.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/bootstrap-slider/slider.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/morris/morris.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/datepicker/datepicker3.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/daterangepicker/daterangepicker-bs3.css') }}">
  <link rel="stylesheet" href="{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
  <link rel="stylesheet" href="{{ asset('/css/jquery.selectlist.css') }}">

  <script src="{{ asset('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
  <!--script src="{{ asset('/plugins/chartjs/Chart.js') }}"></script-->
  <script src="{{ asset('/plugins/ionslider/ion.rangeSlider.min.js') }}"></script>
  <script src="{{ asset('/plugins/bootstrap-slider/bootstrap-slider.js') }}"></script>
  <script src="{{ asset('/library/bootstrap/legend.js') }}"></script>
  <script src="{{ asset('/library/bootstrap/legend.legacy.js') }}"></script>
  <script src="{{ asset('/library/bootstrap/canvasjs.min.js') }}"></script>
  <script src="{{ asset('/plugins/iCheck/icheck.min.js') }}"></script>
@endsection

@section('dashboard')
  <style type="text/css">
    #user-info, #dashboard, #dashboard-data, #user-data, #create-creative, #create-campaign, #create-creative-header, #create-campaign-header, .custom-side-bar-toggle
    {
      display: none;
    }
    .username-responsive
    {
      width: 160px;
      overflow: hidden;
      text-overflow: ellipsis;
    }
    p {margin: 40px 20px 0 20px}
    [data-style=primary] + .popover.right > .arrow:after { bottom: -10px;left: 1px;content: " ";border-right-color: #28A6A0;border-left-width: 0;}
    [data-style=primary] + .popover {background:  #28A6A0; border-radius: 0;}
    [data-style=primary] + .popover .popover-content {padding: 9px 14px;color: white;background: #28A6A0;}
    [data-style=primary] + .popover .popover-title {border-bottom: none;background-color: #28A6A0; border-radius: 0; color: white;}
  </style>
  <div class="wrapper custom-wapper" ng-app="client" ng-controller="userInfo" network>
    <header class="main-header">
        <!-- Logo -->
        <a href="/dashboard" style="background-color: #FEFEFE!important;" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><img src="{{ asset('img/adhero-mini.png') }}" class="img-responsive custom-header-img-mini wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s"></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><img src="{{ asset('img/adhero-mainlogo-replace.png') }}" class="img-responsive custom-header-img-large wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s"></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" style="background-color: #FEFEFE;!important;" role="navigation">
          <!-- <select class="form-control" style="width: 15%;position: absolute; margin-top: 10px;">
            <option>Automtive</option>
            <option>Automtive</option>
            <option>Automtive</option>
          </select> -->
          <md-input-container style="position: absolute;z-index: 100;width: 100%;">
            <md-select placeholder="Select Category" ng-model="category" style="width: 15%;position: absolute; margin-top: 10px;">
              <md-option style="margin-top: 20px;">Automotive</md-option>
              <md-option >Financial Service</md-option>
              <md-option >Telecomunication</md-option>
            </md-select>
          </md-input-container>
          <section style="width: 100%;position: absolute;margin-left: 30%;">
            <p style="margin-top: 10px;color: #887A7A;font-size: 20px;">Maximise your ads performance before publising</p>
          </section>
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle custom-side-bar-toggle" data-toggle="offcanvas" role="button" style="color: #28A6A0;">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope-o" style="color: #28A6A0"></i>
                  <span class="label label-success">4</span>
                </a>
                <ul class="dropdown-menu wow fadeInUp" data-wow-duration="0.5" data-wow-delay="0.5s">
                  <li class="header">You have 4 messages</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu wow fadeInUp" data-wow-duration="0.5" data-wow-delay="0.5s">
                      <li><!-- start message -->
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Support Team
                            <small><i class="fa fa-clock-o"></i> 5 mins</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li><!-- end message -->
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('dist/img/user3-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            AdminLTE Design Team
                            <small><i class="fa fa-clock-o"></i> 2 hours</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('dist/img/user4-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Developers
                            <small><i class="fa fa-clock-o"></i> Today</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('dist/img/user3-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Sales Department
                            <small><i class="fa fa-clock-o"></i> Yesterday</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <div class="pull-left">
                            <img src="{{ asset('dist/img/user4-128x128.jpg') }}" class="img-circle" alt="User Image">
                          </div>
                          <h4>
                            Reviewers
                            <small><i class="fa fa-clock-o"></i> 2 days</small>
                          </h4>
                          <p>Why not buy a new awesome theme?</p>
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">See All Messages</a></li>
                </ul>
              </li>
              <!-- Notifications: style can be found in dropdown.less -->
              <li class="dropdown notifications-menu" style="display:none">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell-o" style="color: #28A6A0"></i>
                  <span class="label label-warning">10</span>
                </a>
                <ul class="dropdown-menu wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                  <li class="header">You have 10 notifications</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-aqua"></i> 5 new members joined today
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the page and may cause design problems
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-users text-red"></i> 5 new members joined
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <i class="fa fa-user text-red"></i> You changed your username
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li class="footer"><a href="#">View all</a></li>
                </ul>
              </li-->
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img ng-src="http://graph.facebook.com/@{{ user.uid }}/picture?type=large" class="user-image" alt="User Image">
                  <span class="hidden-xs" style="color: #28A6A0"></span>
                  <span class="glyphicon glyphicon-chevron-down" style="color: #28A6A0"></span>
                </a>
                <ul class="dropdown-menu wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
                  <!-- User image -->
                  <li class="user-header">
                    <img ng-src="http://graph.facebook.com/@{{ user.uid }}/picture?type=large" class="img-circle" alt="User Image">
                    <p>
                      <span >{{ session('user.name')[0] }}</span>
                      <small>Member since Nov. 2012</small>
                    </p>
                  </li>
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="#" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-right">
                      <a ng-click="logout()" href="{{ url('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>

       <aside class="main-sidebar custom-side-nav">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu text-center" navigator>
            <li class="active treeview dashboard-menu popoverOption" data-placement="right" data-content="Dashboard for your Ads" rel="popover" data-original-title="HOME">
              <a data-wow-duration="1s" data-wow-delay="0.3s" ui-sref="home" class="custom-a-move-padding wow slideInLeft ">
                <i class="fa fa-home custom-icon"></i> 
                <br />
                <span>Home</span>
                <span class="indi-active-span-dashboard active-menu-span"></span>
              </a>
            </li>
            <!-- <li class="treeview ideas-menu popoverIdeas" rel="popover" data-style="primary" data-content="Generate testing ideas" title="IDEAFACTORY" >
              <a data-wow-duration="1s" data-wow-delay="0.7s" ui-sref="ideas" class="idea-hover custom-a-move-padding wow slideInLeft ">
                <i class="icon-light custom-icon" style="margin-left: 5px;"></i>
                <br />
                <span>Ideas</span>
                <span class="indi-active-span-preview-ad active-menu-span"></span>
              </a>
            </li> -->
            <li class="treeview predict popoverOption" data-placement="right" data-content="Predict your new Ad" rel="popover" data-original-title="PREDICT">
              <a data-wow-duration="1s" data-wow-delay="0.9s" ui-sref="predict-menu" class="custom-a-move-padding wow slideInLeft ">
                <i class="icon-ads-brief-result custom-icon"></i>
                <br />
                <span>Predict</span>
                <span class="indi-active-span-ads-brief active-menu-span"></span>
              </a>
            </li>
            <!-- <li class="treeview create-menu popoverOption" data-placement="right" data-content="Create Brief for your Ads" rel="popover" data-original-title="CREATE">
              <a data-wow-duration="1s" data-wow-delay="0.5s" ui-sref="create-brief" class="custom-a-move-padding wow slideInLeft ">
                <i class="fa fa-plus custom-icon" style="margin-left: 5px;"></i>
                <br />
                <span>Create Brief</span>
                <span class="indi-active-span-creative-brief active-menu-span"></span>
              </a>
            </li> -->
           <!--  <li class="treeview preview-ad-menu popoverOption" data-placement="right" data-content="Shows list of Ads" rel="popover" data-original-title="PREVIEW">
              <a data-wow-duration="1s" data-wow-delay="0.7s" ui-sref="preview-ad" class="custom-a-move-padding wow slideInLeft ">
                <i class="icon-create-brief custom-icon" style="margin-left: 5px;"></i>
                <br />
                <span>Preview Ad</span>
                <span class="indi-active-span-preview-ad active-menu-span"></span>
              </a>
            </li> -->
            <li class="treeview ads-result popoverOption" data-placement="right" data-content="Report for your Ads" rel="popover" data-original-title="REPORTS">
              <a data-wow-duration="1s" data-wow-delay="0.9s" ui-sref="campaign-report" class="custom-a-move-padding wow slideInLeft ">
                <i class="icon-ads-brief-result custom-icon"></i>
                <br />
                <span>Report</span>
                <span class="indi-active-span-ads-brief active-menu-span"></span>
              </a>
            </li>
            <li class="treeview payment-menu popoverOption" data-placement="right" data-content="View your ad payments" rel="popover" data-original-title="PAYMENT">
              <a data-wow-duration="1s" data-wow-delay="1.1s" ui-sref="payment" class="custom-a-move-padding wow slideInLeft ">
                <i class="icon-credit-card custom-icon"></i>
                <br />
                <span>Payment</span>
                <span class="indi-active-span-payment active-menu-span"></span>
              </a>
            </li>
            <li class="treeview contact-us popoverOption" data-placement="right" data-content="Contact Us" rel="popover" data-original-title="CONTACTS">
              <a data-wow-duration="1s" data-wow-delay="1.3s" ui-sref="contact" class="custom-a-move-padding wow slideInLeft ">
                <i class="icon-chat-custom custom-icon"></i>
                <br />
                <span>Contact Us</span>
                <span class="indi-active-span-contact active-menu-span"></span>
              </a>
            </li>
            <li class="treeview settings popoverOption" data-placement="right" data-content="Settings for your Account" rel="popover" data-original-title="SETTINGS">
              <a data-wow-duration="1s" data-wow-delay="1.5s" ui-sref="settings" ng-click="getSettings()" class="custom-a-move-padding wow slideInLeft ">
                <i class="fa fa-cog custom-icon"></i>
                <br />
                <span>Settings</span>
                <span class="indi-active-span-settings active-menu-span"></span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header" id="dashboard-data">
         
          </section>
          <section class="content-header" id="user-data">
          <h1>
            User Information
            <small>Please complete the user information to proceed to your dashboard</small>
          </h1>
          </section>
          <section class="content-header" id="create-campaign-header" ng-if="userType == 'creative'">
          <h1>
            THERE ARE 4 LIVE CONTESTS
            <br />
            <small>Take your pick of the projects you fancy</small>
          </h1>
          </section>
          
          <section class="content" id="create-creative" style="margin-top: 50px;" ng-if="userType == 'creative'">
          <div class="row">
            <div class="col-md-12">
              <div class="col-md-4">
                <img src="assets/img/century.png" class="thumbnail wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
              </div>
              <div class="col-md-4">
                <img src="assets/img/antel.png" class="thumbnail wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
              </div>
              <div class="col-md-4">
                <img src="assets/img/paramount.png" class="thumbnail wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
              </div>
              <div class="col-md-4" >
                <img src="assets/img/trump.png" class="thumbnail wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
              </div>
              <div class="col-md-4" >
                <img src="assets/img/trump.png" class="thumbnail wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
              </div>
              <div class="col-md-4" >
                <img src="assets/img/century.png" class="thumbnail wow slideInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
              </div>
            </div>
          </div>
          </section>
          <!-- Main content -->
          <section class="content" id="user-info" style="margin-top: 50px;">
          <div class="row">
            <form ng-submit="updateInfo()">
            <div class="col-md-3"></div>
            <div class="col-md-6">
              <div class="box box-success box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">User Information</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <div class="form-group">
                      <label>Full Name</label>
                      <input type="text" class="form-control" id="exampleInputEmail1" disabled>
                  </div>
                  <div class="form-group">
                    <label>Region/Country</label>
                    <select class="form-control" required>
                      <option value="AF">Afghanistan</option>
                      <option value="AL">Albania</option>
                      <option value="DZ">Algeria</option>
                      <option value="AS">American Samoa</option>
                      <option value="AD">Andorra</option>
                      <option value="AG">Angola</option>
                      <option value="AI">Anguilla</option>
                      <option value="AG">Antigua &amp; Barbuda</option>
                      <option value="AR">Argentina</option>
                      <option value="AA">Armenia</option>
                      <option value="AW">Aruba</option>
                      <option value="AU">Australia</option>
                      <option value="AT">Austria</option>
                      <option value="AZ">Azerbaijan</option>
                      <option value="BS">Bahamas</option>
                      <option value="BH">Bahrain</option>
                      <option value="BD">Bangladesh</option>
                      <option value="BB">Barbados</option>
                      <option value="BY">Belarus</option>
                      <option value="BE">Belgium</option>
                      <option value="BZ">Belize</option>
                      <option value="BJ">Benin</option>
                      <option value="BM">Bermuda</option>
                      <option value="BT">Bhutan</option>
                      <option value="BO">Bolivia</option>
                      <option value="BL">Bonaire</option>
                      <option value="BA">Bosnia &amp; Herzegovina</option>
                      <option value="BW">Botswana</option>
                      <option value="BR">Brazil</option>
                      <option value="BC">British Indian Ocean Ter</option>
                      <option value="BN">Brunei</option>
                      <option value="BG">Bulgaria</option>
                      <option value="BF">Burkina Faso</option>
                      <option value="BI">Burundi</option>
                      <option value="KH">Cambodia</option>
                      <option value="CM">Cameroon</option>
                      <option value="CA">Canada</option>
                      <option value="IC">Canary Islands</option>
                      <option value="CV">Cape Verde</option>
                      <option value="KY">Cayman Islands</option>
                      <option value="CF">Central African Republic</option>
                      <option value="TD">Chad</option>
                      <option value="CD">Channel Islands</option>
                      <option value="CL">Chile</option>
                      <option value="CN">China</option>
                      <option value="CI">Christmas Island</option>
                      <option value="CS">Cocos Island</option>
                      <option value="CO">Colombia</option>
                      <option value="CC">Comoros</option>
                      <option value="CG">Congo</option>
                      <option value="CK">Cook Islands</option>
                      <option value="CR">Costa Rica</option>
                      <option value="CT">Cote D'Ivoire</option>
                      <option value="HR">Croatia</option>
                      <option value="CU">Cuba</option>
                      <option value="CB">Curacao</option>
                      <option value="CY">Cyprus</option>
                      <option value="CZ">Czech Republic</option>
                      <option value="DK">Denmark</option>
                      <option value="DJ">Djibouti</option>
                      <option value="DM">Dominica</option>
                      <option value="DO">Dominican Republic</option>
                      <option value="TM">East Timor</option>
                      <option value="EC">Ecuador</option>
                      <option value="EG">Egypt</option>
                      <option value="SV">El Salvador</option>
                      <option value="GQ">Equatorial Guinea</option>
                      <option value="ER">Eritrea</option>
                      <option value="EE">Estonia</option>
                      <option value="ET">Ethiopia</option>
                      <option value="FA">Falkland Islands</option>
                      <option value="FO">Faroe Islands</option>
                      <option value="FJ">Fiji</option>
                      <option value="FI">Finland</option>
                      <option value="FR">France</option>
                      <option value="GF">French Guiana</option>
                      <option value="PF">French Polynesia</option>
                      <option value="FS">French Southern Ter</option>
                      <option value="GA">Gabon</option>
                      <option value="GM">Gambia</option>
                      <option value="GE">Georgia</option>
                      <option value="DE">Germany</option>
                      <option value="GH">Ghana</option>
                      <option value="GI">Gibraltar</option>
                      <option value="GB">Great Britain</option>
                      <option value="GR">Greece</option>
                      <option value="GL">Greenland</option>
                      <option value="GD">Grenada</option>
                      <option value="GP">Guadeloupe</option>
                      <option value="GU">Guam</option>
                      <option value="GT">Guatemala</option>
                      <option value="GN">Guinea</option>
                      <option value="GY">Guyana</option>
                      <option value="HT">Haiti</option>
                      <option value="HW">Hawaii</option>
                      <option value="HN">Honduras</option>
                      <option value="HK">Hong Kong</option>
                      <option value="HU">Hungary</option>
                      <option value="IS">Iceland</option>
                      <option value="IN">India</option>
                      <option value="ID">Indonesia</option>
                      <option value="IA">Iran</option>
                      <option value="IQ">Iraq</option>
                      <option value="IR">Ireland</option>
                      <option value="IM">Isle of Man</option>
                      <option value="IL">Israel</option>
                      <option value="IT">Italy</option>
                      <option value="JM">Jamaica</option>
                      <option value="JP">Japan</option>
                      <option value="JO">Jordan</option>
                      <option value="KZ">Kazakhstan</option>
                      <option value="KE">Kenya</option>
                      <option value="KI">Kiribati</option>
                      <option value="NK">Korea North</option>
                      <option value="KS">Korea South</option>
                      <option value="KW">Kuwait</option>
                      <option value="KG">Kyrgyzstan</option>
                      <option value="LA">Laos</option>
                      <option value="LV">Latvia</option>
                      <option value="LB">Lebanon</option>
                      <option value="LS">Lesotho</option>
                      <option value="LR">Liberia</option>
                      <option value="LY">Libya</option>
                      <option value="LI">Liechtenstein</option>
                      <option value="LT">Lithuania</option>
                      <option value="LU">Luxembourg</option>
                      <option value="MO">Macau</option>
                      <option value="MK">Macedonia</option>
                      <option value="MG">Madagascar</option>
                      <option value="MY">Malaysia</option>
                      <option value="MW">Malawi</option>
                      <option value="MV">Maldives</option>
                      <option value="ML">Mali</option>
                      <option value="MT">Malta</option>
                      <option value="MH">Marshall Islands</option>
                      <option value="MQ">Martinique</option>
                      <option value="MR">Mauritania</option>
                      <option value="MU">Mauritius</option>
                      <option value="ME">Mayotte</option>
                      <option value="MX">Mexico</option>
                      <option value="MI">Midway Islands</option>
                      <option value="MD">Moldova</option>
                      <option value="MC">Monaco</option>
                      <option value="MN">Mongolia</option>
                      <option value="MS">Montserrat</option>
                      <option value="MA">Morocco</option>
                      <option value="MZ">Mozambique</option>
                      <option value="MM">Myanmar</option>
                      <option value="NA">Nambia</option>
                      <option value="NU">Nauru</option>
                      <option value="NP">Nepal</option>
                      <option value="AN">Netherland Antilles</option>
                      <option value="NL">Netherlands (Holland, Europe)</option>
                      <option value="NV">Nevis</option>
                      <option value="NC">New Caledonia</option>
                      <option value="NZ">New Zealand</option>
                      <option value="NI">Nicaragua</option>
                      <option value="NE">Niger</option>
                      <option value="NG">Nigeria</option>
                      <option value="NW">Niue</option>
                      <option value="NF">Norfolk Island</option>
                      <option value="NO">Norway</option>
                      <option value="OM">Oman</option>
                      <option value="PK">Pakistan</option>
                      <option value="PW">Palau Island</option>
                      <option value="PS">Palestine</option>
                      <option value="PA">Panama</option>
                      <option value="PG">Papua New Guinea</option>
                      <option value="PY">Paraguay</option>
                      <option value="PE">Peru</option>
                      <option value="PH">Philippines</option>
                      <option value="PO">Pitcairn Island</option>
                      <option value="PL">Poland</option>
                      <option value="PT">Portugal</option>
                      <option value="PR">Puerto Rico</option>
                      <option value="QA">Qatar</option>
                      <option value="ME">Republic of Montenegro</option>
                      <option value="RS">Republic of Serbia</option>
                      <option value="RE">Reunion</option>
                      <option value="RO">Romania</option>
                      <option value="RU">Russia</option>
                      <option value="RW">Rwanda</option>
                      <option value="NT">St Barthelemy</option>
                      <option value="EU">St Eustatius</option>
                      <option value="HE">St Helena</option>
                      <option value="KN">St Kitts-Nevis</option>
                      <option value="LC">St Lucia</option>
                      <option value="MB">St Maarten</option>
                      <option value="PM">St Pierre &amp; Miquelon</option>
                      <option value="VC">St Vincent &amp; Grenadines</option>
                      <option value="SP">Saipan</option>
                      <option value="SO">Samoa</option>
                      <option value="AS">Samoa American</option>
                      <option value="SM">San Marino</option>
                      <option value="ST">Sao Tome &amp; Principe</option>
                      <option value="SA">Saudi Arabia</option>
                      <option value="SN">Senegal</option>
                      <option value="RS">Serbia</option>
                      <option value="SC">Seychelles</option>
                      <option value="SL">Sierra Leone</option>
                      <option value="SG">Singapore</option>
                      <option value="SK">Slovakia</option>
                      <option value="SI">Slovenia</option>
                      <option value="SB">Solomon Islands</option>
                      <option value="OI">Somalia</option>
                      <option value="ZA">South Africa</option>
                      <option value="ES">Spain</option>
                      <option value="LK">Sri Lanka</option>
                      <option value="SD">Sudan</option>
                      <option value="SR">Suriname</option>
                      <option value="SZ">Swaziland</option>
                      <option value="SE">Sweden</option>
                      <option value="CH">Switzerland</option>
                      <option value="SY">Syria</option>
                      <option value="TA">Tahiti</option>
                      <option value="TW">Taiwan</option>
                      <option value="TJ">Tajikistan</option>
                      <option value="TZ">Tanzania</option>
                      <option value="TH">Thailand</option>
                      <option value="TG">Togo</option>
                      <option value="TK">Tokelau</option>
                      <option value="TO">Tonga</option>
                      <option value="TT">Trinidad &amp; Tobago</option>
                      <option value="TN">Tunisia</option>
                      <option value="TR">Turkey</option>
                      <option value="TU">Turkmenistan</option>
                      <option value="TC">Turks &amp; Caicos Is</option>
                      <option value="TV">Tuvalu</option>
                      <option value="UG">Uganda</option>
                      <option value="UA">Ukraine</option>
                      <option value="AE">United Arab Emirates</option>
                      <option value="GB">United Kingdom</option>
                      <option value="US">United States of America</option>
                      <option value="UY">Uruguay</option>
                      <option value="UZ">Uzbekistan</option>
                      <option value="VU">Vanuatu</option>
                      <option value="VS">Vatican City State</option>
                      <option value="VE">Venezuela</option>
                      <option value="VN">Vietnam</option>
                      <option value="VB">Virgin Islands (Brit)</option>
                      <option value="VA">Virgin Islands (USA)</option>
                      <option value="WK">Wake Island</option>
                      <option value="WF">Wallis &amp; Futana Is</option>
                      <option value="YE">Yemen</option>
                      <option value="ZR">Zaire</option>
                      <option value="ZM">Zambia</option>
                      <option value="ZW">Zimbabwe</option>
                    </select>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-12 text-center">
              <button type="submit" class="btn bg-purple btn-flat margin">Update</button>
            </div>
            </form>
          </div>
          </section>
          <!-- id="dashboard" -->
        <section class="content" style="background-color: #e7eaef;padding-bottom:0;">
          <section ui-view class="all-views"></section>
          <section class="network-status" hidden>
            <section class="content-header">
              <h1>
                500 Error Page
              </h1>
            </section>
            <section class="content">
              <div class="error-page">
                <h2 class="headline text-red">500</h2>
                <div class="error-content">
                  <h3><i class="fa fa-warning text-red"></i> Oops! Something went wrong.</h3>
                  <p>
                    Please make sure that you are connected to the internet.
                  </p>
                </div>
              </div><!-- /.error-page -->
            </section>
          </section>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.2.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="https://www.facebook.com/youradhero">AdHero</a>.</strong> All rights reserved.
      </footer>
  </div>

@endsection


@section( 'scripts' )
<script src="{{ asset( 'js/jquery-ui.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.toaster.js' ) }}"></script>
<script src="{{ asset( 'library/bootstrap/bootstrap.min.js' ) }}"></script>
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<script src="{{ asset( 'js/raphael-min.js' ) }}"></script>
<script src="{{ asset( 'library/bootstrap/livicons-1.4.min.js' ) }}"></script>
<script src="{{ asset( 'plugins/knob/jquery.knob.js' ) }}"></script>
<script src="{{ asset( 'js/moment.min.js' ) }}"></script>
<script src="{{ asset( 'plugins/daterangepicker/daterangepicker.js' ) }}"></script>
<script src="{{ asset( 'plugins/datepicker/bootstrap-datepicker.js' ) }}"></script>
<script src="{{ asset( 'plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js' ) }}"></script>
<script src="{{ asset( 'plugins/slimScroll/jquery.slimscroll.min.js' ) }}"></script>
<script src="{{ asset( 'plugins/fastclick/fastclick.min.js' ) }}"></script>
<script src="{{ asset( 'dist/js/app.min.js' ) }}"></script>
<script src="{{ asset( 'dist/js/pages/dashboard.js' ) }}"></script>
<script src="{{ asset( 'dist/js/demo.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'js/wow.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/bootstrap/animatescroll.min.js' ) }}"></script>
<script type="text/javascript">
  $(document).ready(function( ){
    new WOW().init();
    // $('[data-toggle="tooltip"]').tooltip();
    $('.popoverOption').popover({ trigger: 'hover' });

    var popoverTemplate = ['<div class="popover fade right in" role="tooltip" id="popover261543"><div class="arrow" style="top: 50%;"></div><h3 class="popover-title">IDEAFACTORY</h3><div class="popover-content">Generate testing ideas</div></div>'];
    var sample = 'hello'
    $('.popoverIdeas').popover({placement: 'right',trigger: 'hover', content: sample, html: true});
  });
</script>

<script type="text/javascript" src="{{ asset( 'js/jasny-bootstrap.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/bootstrap/sweetalert.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/angular.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/angular-animate.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/angular-aria.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/angular-material.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/angular-ui-router.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/ngStorage.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/angular.country-select.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/loading-bar.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/ng-file-upload.min.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'library/angular/ordinal-browser.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'js/main/dashboard-client.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'js/factory/factory.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'js/service/service.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'js/directive/dashboard-client.js' ) }}"></script>
<script type="text/javascript" src="{{ asset( 'js/controller/controller-client.js' ) }}"></script>
@endsection