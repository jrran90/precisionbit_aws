<!DOCTYPE html>
<html ng-app="home">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>@yield('title')</title>
  <link rel="shortcut icon" href="{{ asset('/img/favicon.jpg') }}" />
  <link href="{{ asset('/css/bootstrap.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/animate.css') }}" rel="stylesheet">
  <link href="{{ asset('/css/flexslider.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('/fontcustom/fontcustom.css') }}">
  <link rel="stylesheet" href="{{ asset('/fontcustom-slider/fontcustom.css') }}">
  <link rel="stylesheet" href="{{ asset('/font-awesome/css/font-awesome.min.css') }}">
  <link href="{{ asset('/css/landing.css') }}" rel="stylesheet">
  <!-- <link href='https://fonts.googleapis.com/css?family=Raleway:800,400' rel='stylesheet' type='text/css'> -->
  <!-- <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Ubuntu:regular,bold&subset=Latin"> -->
  <link href="{{ asset('/css/jquery.bxslider.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('/css/sweetalert.css') }}">
  @yield( 'stylesheets' )
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery-1.9.1.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/wow.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/query.bxslider.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/animatescroll.min.js' ) }}"></script>
  <script>

  // Mouseover/ Click sound effect- by JavaScript Kit (www.javascriptkit.com)
  // Visit JavaScript Kit at http://www.javascriptkit.com/ for full source code

  //** Usage: Instantiate script by calling: var uniquevar=createsoundbite("soundfile1", "fallbackfile2", "fallebacksound3", etc)
  //** Call: uniquevar.playclip() to play sound

  var html5_audiotypes={ //define list of audio file extensions and their associated audio types. Add to it if your specified audio file isn't on this list:
    "mp3": "audio/mpeg",
    "mp4": "audio/mp4",
    "ogg": "audio/ogg",
    "wav": "audio/wav"
  }

  function createsoundbite(sound){
    var html5audio=document.createElement('audio')
    if (html5audio.canPlayType){ //check support for HTML5 audio
      for (var i=0; i<arguments.length; i++){
        var sourceel=document.createElement('source')
        sourceel.setAttribute('src', arguments[i])
        if (arguments[i].match(/\.(\w+)$/i))
          sourceel.setAttribute('type', html5_audiotypes[RegExp.$1])
        html5audio.appendChild(sourceel)
      }
      html5audio.load()
      html5audio.playclip=function(){
        html5audio.pause()
        html5audio.currentTime=0
        html5audio.play()
      }
      return html5audio
    }
    else{
      return {playclip:function(){throw new Error("Your browser doesn't support HTML5 audio unfortunately")}}
    }
  }

  //Initialize two sound clips with 1 fallback file each:

  var mouseoversound=createsoundbite("../sounds/knock.wav", "sound/knock.mp3");
  // var clicksound=createsoundbite("click.ogg", "click.mp3")

  var clicksound=createsoundbite("../sounds/knock.wav", "sound/knock.mp3");

  </script>
</head>
<body ng-controller="emailController">

@include('home.header')
@yield('main')
@yield('pricing')
@yield('testimonials')
@yield('sample-ad')
@yield('login-customer')
@yield('creative')
@yield('login-creative')
@yield('privacy')
@yield('terms')
@yield('creative-term')
@yield('about')
@yield('graymatic')
@yield('scripts')
@include('home.footer')
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/bootstrap.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/sweetalert.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/angular/angular.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/main/home.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/factory/factory.js' ) }}"></script>

  <script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.flexslider-min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.flexslider-min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'library/bootstrap/jquery.nicescroll.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/parallax.min.js' ) }}"></script>
  <script type="text/javascript" src="{{ asset( 'js/effects.js' ) }}"></script>
  <script type="text/javascript">
  $(document).ready(function( ) {
    new WOW().init();
    $('.bxslider').bxSlider(
      {
        auto: true,
        mode: 'fade',
        pause: 3000,
        speed: 1500,
        autoHover: true
      });
     // $('.bxslider').bxSlider();
  });
</script>
</body>
</html>