<p class="bottom-content" onclick="$('body').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});"><i class="icon-arrow-in-circle-point-to-up bottom-top-call"></i></p>
<!-- <div class="wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s"> -->
<!-- <div class="col-lg-12 main-footer-section-1">
    <div class="col-lg-6 custom-col-lg-6-footer-section">
      <section class="main-footer-title-text">
        <p>Manage, Maintain, And Evolve: Can Startup Culture Survive Growth?</p>
      </section>
      <section class="main-footer-title-p">
        <p>This is a three-part series of articles on building, growing and supporting company culture - something the teams at Kin, DoneDone.com and We Are Mammoth have all wrestled with...</p>
        <a href="#"> READ MORE </a>
      </section>
    </div>
    <div class="col-lg-6 custom-col-lg-6-footer-section">
      <section class="main-footer-title-text">
        <p><i class="fa fa-twitter" style="margin-right: 10px;"></i> The AdHeroTwitter</p>
      </section>
      <section class="main-footer-title-p">
        <p>"The people you invite into your company ot extend and expand its capabilities ARE YOUR CULTURE" - <a href="#"> @hunterwalk </a> via <a href="#"> http://t.co/E7AI0pJjPO</a></p>
        <a href="#"> @KIN_HR </a>
      </section>
    </div>
  </div> -->
  <!-- <div class="col-lg-12 secondary-footer-section">
    <div class="col-lg-2 custom-div">
      <p>PRODUCT</p>
      <ul class="secondary-footer-ul">
        <li><a href="#">Tour</a></li>
        <li><a href="/pricing">Pricing</a></li>
        <li><a href="#">Security</a></li>
        <li><a href="/privacy">Privacy</a></li>
        <li><a href="/terms">Terms of Service</a></li>
        <li><a href="creative-terms">Creative Services Agreement</a></li>
      </ul>
    </div>
    <div class="col-lg-2">
      <p>USERS</p>
      <ul class="secondary-footer-ul">
        <li><a href="">Sign In</a></li>
        <li><a href="">Support</a></li>
        <li><a href="">FAQs</a></li>
        <li><a href="">Stories</a></li>
        <li><a href="/testimonials">Testimonials</a></li>
      </ul>
    </div>
    <div class="col-lg-2">
      <p>COMPANY</p>
      <ul class="secondary-footer-ul">
        <li><a href="">About Us</a></li>
        <li><a href="">Our Team</a></li>
        <li><a href="">Blog</a></li>
        <li><a href="">Contact</a></li>
        <li><a href="">Press</a></li>
        <li><a href="/testimonials">Testimonials</a></li>
        <li><a href="">Partner Program</a></li>
      </ul>
    </div>
    <div class="col-lg-2">
      <p>FOLLOW</p>
      <ul class="secondary-footer-ul">
        <li><a href="">Facebook</a></li>
        <li><a href="">Twitter</a></li>
        <li><a href="">LinkIn</a></li>
        <li><a href="">Vimeo</a></li>
        <li><a href="">Google+</a></li>
      </ul>
    </div>
    <div class="col-lg-3">
      <ul class="secondary-footer-info">
        <li><button class="btn btn-large custom-btn-secondary-footer">TRY ADHERO FOR FREE</button></li>
        <li><a href="" style="color: #868686; text-decoration: none;">Sign up for a free 14-day trial.</a></li>
        <li style="margin-bottom: 20px;"><a href="" style="color: #bcbcbc; text-decoration: none;">No credit card required.</a></li>
        <li style="font-weight: bold; color: #868686;">CALL US</li>
        <li><a href="" style="color: #A7A3A3; text-decoration: none;">+1 (312)380-6650</a></li>
      </ul>
    </div>
  </div>
  <div class="col-md-12" style="height: 100px; background-color: rgba(64, 63, 63, 0.94);">
    <p style="margin-top: 10px; margin-left: 70px; color: #888686; font-size: 12px; margin: 0; padding: 20px;">&copy; ADHERO 2015 ALL RIGHT RESERVED</p>
  </div> -->
<!-- </div> -->
<div id="container_main">
  <div id="footer">
    <div class="wrapper">
        <p>© Copyright PrecisionBit 2016. All rights reserved.</p>
      <div class="social">
      <ul>
          <li><a href="https://www.linkedin.com/company/precisionbit" target="__blank"><img src="../img/linkedin.png" alt="LinkedIn" class="footer-social"></a></li>
          <li><a href="https://www.facebook.com/PrecisionBit/?fref=ts" target="__blank"><img src="../img/facebook.png" alt="Facebook" class="footer-social"></a></li>
      </ul>
      </div>                
      <div class="menu footer-menu">
        <ul>
          <li><a href="/">Home</a></li>
          <!-- <li><a onclick="$('.content_5').animatescroll();" class="dropdown-toggle cursor-pointer">Solution</span></a></li> -->
          <li><a href="/about">About Us</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="back-to-top text-center hidden-sm hidden-lg hidden-md" onclick="$('body').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});">
    <span>Back To Top</span>
  </div>
</div>