@extends('tpl.tpl-home')
@section('title', 'PrecisionBit')
@section( 'stylesheets' )
<link href="{{ asset('/css/animation.css') }}" rel="stylesheet">
@endsection

@section('mains')
    <style type="text/css">
    #bgVideo
    {
      position: absolute;
      right: 0;
      bottom: 0;
      top: 0;
      width: auto;
      min-width: 100%;
      height: 100vh;
      min-height: 100%;
      z-index: -100;    
      background-size: cover;
      background-color: black;
    }
    </style>
  <!-- main -->
  <!-- <div class="row"> -->
  <div class="col-lg-12 main-section-1 text-center">
    <video id="bgVideo" preload="true" autoplay loop>
      <source src="{{ asset('videos/adhero.mp4') }}" type="video/mp4" > 
    </video>
    <h1 class="wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">Get Conversations from a Winning <br /> Social Media Ad</h1>
    <h4>We launch a social media ad camaign that's tailored to your needs with content specially crafted by experts</h4>
    <button class="btn-lg custom-button-try-adhero wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">Try AdHero Risk-Free</button>
    <section class="arrow-container">
      <a class="scroll floating floating-custom" href="#ads_2"><span class="fa fa-chevron-down custom-arrow-down"></span></a>
    </section>
  </div>
  <div class="col-lg-12 how-it-works">
    <section class="how-it-works-container text-center">
      <p>HOW IT WORKS</p>
    </section>
    <section class="how-it-works-container-box">
      <div class="row" style="margin-bottom: 0;">
        <div class="col-md-4 col-md-offset-2 fix-box-steps box-1-steps">
          <p class="how-it-works-steps active-how-it-works">
            Step 1 Fill up our online brief. Tell us about your business, your target market,
            and your campaign objectives, so we can create the right ad for you.
          </p>
          <section class="how-it-works-img-container">
            
          </section>
        </div>
        <div class="col-md-4 fix-box-steps box-2-steps">
          <p class="how-it-works-steps">
            Step 2 Our content experts create ad content that's right for your industry and country.
          </p>
          <section class="how-it-works-img-container">
            
          </section>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 fix-box-steps  col-md-offset-2 box-3-steps">
          <p class="how-it-works-steps">
            Step 3 We identity the top 3 ads. Using your feedback and machine-learning platform, we put
            together the highest-converting text, images, and calls-to-action to strengthen your ads.
          </p>
          <section class="how-it-works-img-container">
            
          </section>
        </div>
        <div class="col-md-4 fix-box-steps box-4-steps">
          <p class="how-it-works-steps">
            Step 4 You receive the best-performing ad. After a quick testing period, you get the ad 
            that got the most conversions.
          </p>
          <section class="how-it-works-img-container">
            
          </section>
        </div>
      </div>
    </section>
  </div>
  <div class="col-lg-12 what-our-customer-are-saying">
    <section class="what-our-customer-are-saying-container text-center">
        <p>WHAT OUR CUSTOMER ARE SAYING...</p>
    </section>
    <div class="row" style="margin-bottom: 20px;">
      <div class="col-md-4 col-md-offset-2 text-center">
        <span class="fa fa-user custom-user"></span>
      </div>
      <div class="col-md-4">
        <section class="customer-description-box">
          <p>
            <span>"Lorem ipsum dolor sit amet!"</span>
            <br />
            <span>- Customer, Position, Company</span>
          </p>
        </section>
        <a href="#" class="see-customer-more">See how Adhero worked for them</a>
      </div>
    </div>
  </div>
  <div class="col-lg-12 brands-worked">
    <section class="brands-worked-container text-center">
        <p>BRANDS WE'VE WORKED WITH</p>
    </section>
    <div class="row" style="margin-bottom: 20px;">
      <div class="col-md-2 col-md-offset-1">
        <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" class="brand-img">
      </div>
      <div class="col-md-2">
        <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" class="brand-img">
      </div>
      <div class="col-md-2">
        <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" class="brand-img">
      </div>
      <div class="col-md-2">
        <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" class="brand-img">
      </div>
      <div class="col-md-2">
        <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" class="brand-img">
      </div>
    </div>
  </div>
  <div class="col-lg-12 main-section-2 text-center" id="ads_2">
    <div class="col-lg-6" style="padding: 20px;">
      <img src="{{ asset('img/fb.png') }}" alt="fb" class="wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s">
    </div>
    <div class="col-lg-6 section-2-text-p wow slideInRight" style="padding: 20px;" data-wow-duration="0.5s" data-wow-delay="0.5s">
      <p>Ads designed to make users stop on their Newsfeed and convert</p>
      <h4>Designing a perfect ad from scratch is difficult. Coming up with a catchy headline and impactful image to get customers to convert is not easy. AdHero designs the most effective ad for your business by identifying the most relevant images and text for your audience and industry, and launching what works the best.</h4>
    </div>
  </div>
  <div class="col-md-12 col-lg-12 main-section-3 text-center">
    <p class="main-section-3-text-p wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">Personalized Ad Designs for Different Audiences</p>
    <h6 class="main-section-3-text-h6 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">We design the most relevant social media ad for your target audience based on their interests, language and demographics. Launch local or global campaigns with AdHero and get more conversions for every dollar you spend.</h6>
    <img src="{{ asset('img/HP2.png') }}" alt="location">
  </div>
  <div class="col-md-12 col-lg-12 main-section-4 text-center">
    <p class="main-section-4-text-p wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">The Most Effective Campaigns On-Demand</p>
    <h6 class="main-section-4-text-h6 wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">AdHero’s machine-learning platform identifies the highest-converting images, text and calls-to-action for your industry and country. We incorporate these into your campaigns to strengthen your ads before they go live on a wide scale.</h6>
    <img src="{{ asset('img/3usp.png') }}" alt="location" class="wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">
  </div>
  <div class="col-md-12 col-lg-12 main-section-5 text-center">
    <div class="col-md-12 col-lg-12 custom-how-it-works-title">
      <p class="main-section-5-text-p wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">HOW IT WORKS</p>
      <p class="advertising-text wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">Deliver the best ad to your audience without having to learn the complexities of Facebook advertising.</p>
    </div>
    <div class="col-md-3 custom-center-how-it-works wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">
      <img src="{{ asset('img/logo_note.jpg') }}" alt="note">
      <p>FILL OUT</p>
      <P>CAMPAIGN BRIEF</P>
    </div>
    <div class="col-md-3 custom-center-how-it-works wow fadeInDown" data-wow-duration="0.6s" data-wow-delay="0.6s">
      <img src="{{ asset('img/logo_bulb.jpg') }}" alt="note">
      <p>EXPERTS GET TO WORK</p>
      <P>ON YOUR CAMPAIGN</P>
    </div>
    <div class="col-md-3 custom-center-how-it-works wow fadeInDown" data-wow-duration="0.7s" data-wow-delay="0.7s">
      <img src="{{ asset('img/logo_star.jpg') }}" alt="note">
      <p>ADHERO IDENTIFIES</p>
      <P>BEST CAMPAIGN</P>
    </div>
    <div class="col-md-3 custom-center-how-it-works wow fadeInDown" data-wow-duration="0.8s" data-wow-delay="0.8s">
      <img src="{{ asset('img/logo_medal.jpg') }}" alt="note">
      <p>YOU RECEIVE THE BEST</p>
      <P>PERFORMING AD</P>
    </div>

    <a href="#" class="learn-more wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">Learn more</a>
  </div>
  <div class="col-md-12 col-lg-12 main-section-6 text-center">
      <p class="main-section-6-text-p wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">What Our Customers Are Saying...</p>
      <div class="col-lg-6" style="padding: 50px;">
        <section class="info-text-container wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">
          <p style="color: #808080; font-size: 18px; font-style: italic;">"My sales team has never had more leads for this target segment, we now use AdHero for all our products!"</p>
        </section>
        <section class="person-container" style="padding: 20px;">
          <section>
            <p>
              <img src="{{ asset('img/owner1.jpg') }}" style="width: 60px; border-radius: 50%;" class="wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s">
              <span style="color: #262626; font-weight: 700; margin-left: 5px;" class="wow slideInLeft" data-wow-duration="0.5s" data-delay="0.5s">JAMES TOUHEY <br /> <span style="position: absolute; margin-top: -22px; font-weight: 400; color: #808080; margin-left: -22px; font-size: 10px;" class="wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s">Digital Marketing Manager</span></span>
            </p>
          </section>
        </section>
      </div>
      <div class="col-lg-6" style="padding: 50px;">
        <section class="info-text-container">
          <p style="color: #808080; font-size: 18px; font-style: italic;" class="wow fadeInDown" data-wow-duration="0.5s" data-wow-delay="0.5s">"My sales team has never had more leads for this target segment, we now use AdHero for all our products!"</p>
        </section>
        <section class="person-container" style="padding: 20px;">
          <section>
            <p>
              <img src="{{ asset('img/owner1.jpg') }}" style="width: 60px; border-radius: 50%;" class="wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s">
              <span style="color: #262626; font-weight: 700; margin-left: 5px;" class="wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s">JAMES TOUHEY <br /> <span style="position: absolute; margin-top: -22px; font-weight: 400; color: #808080; margin-left: -22px; font-size: 10px;" class="wow slideInLeft" data-wow-duration="0.5s" data-wow-delay="0.5s">Digital Marketing Manager</span></span>
            </p>
          </section>
        </section>
      </div>
      <a href="" class="learn-more wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">Read a few more kind words from happy customers</a>
  </div>
  <div class="col-md-12 col-lg-12 main-section-8 text-center">
  <p class="main-section-8-text-p">COMPANIES USING ADHERO</p>
  <div class="col-md-3">
    <img src="{{ asset('img/trump.png') }}" class="bottom-banks-partners wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.5s">
  </div>
  <div class="col-md-3">
    <img src="{{ asset('img/century.png') }}" class="bottom-banks-partners wow fadeInUp" data-wow-duration="0.6s" data-wow-delay="0.6s">
  </div>
  <div class="col-md-3">
    <img src="{{ asset('img/paramount.png') }}" class="bottom-banks-partners wow fadeInUp" data-wow-duration="0.7s" data-wow-delay="0.7s">
  </div>
  <div class="col-md-3">
    <img src="{{ asset('img/antel.png') }}" class="bottom-banks-partners wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.8s">
  </div>
</div>
<div class="col-lg-12 main-questions text-center">
  <section class="question-container">
      <p><span class="question-text">Still have question?</span> <button class="btn btn-large btn-custom-question">CHAT WITH US <i class="fa fa-comment" style="margin-left: 10px;"></i></button></p>
  </section>
</div>
@endsection
@section( 'scripts' )
<script type="text/javascript">
  $(document).ready(function( ) {
    $('video').on('ended', function () {
      this.load();
      this.play();
    });
  });
</script>
@endsection