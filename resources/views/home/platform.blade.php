@extends('tpl.tpl-home')
@section('title', 'PrecisionBit')
@section( 'stylesheets' )
	<link href="{{ asset('/css/animation.css') }}" rel="stylesheet">
@endsection
@section('about')
<div class="row remove-row">
	<!-- <section class="bg-container">
		<section class="platorm-first-container">
		
		</section>
	</section> -->
	<div class="row remove-row video-playing-container platform-background">
    <div class="content_1 platform-content">
    	<section class="col-md-12">
    		<div class="content_1-box">
    			<h2>Self-Serve Platfrom</h2>
    			<p>Create new Facebook, Youtube, Instagram and Twitter campaigns with a single, easy-to-use tool.</p>
    		</div>
    	</section>
    	<section class="col-md-12 content_1-status">
    		<a href="#" class="contact-us-btn" style="margin-left: 50px;">REQUEST A DEMO</a>
    	</section>
    	<section class="col-md-12 floating-direction text-center" style="margin-top: 100px;">
    		<a class="scroll floating floating-custom" href="#ads_2"><span class="fa fa-chevron-down custom-arrow-down"></span></a>
    	</section>
    </div>
	</div>

	<div class="row remove-row a-b-testing-container">
		<section class="text-center">
			<p>One-Click A/B Split Testing</p>
			<br />
			<p class="a-b-testing-container-p">One-click split feature to easily A/B test multiple targeting and creative groups to drive stronger campaign performance across Facebook, Twitter and Instagram.</p>
		</section>
		<section class="text-center">
			<img src="../img/platform.png" class="img-responsive platfrom-img">
		</section>
	</div>

	<div class="row remove-row creative-image-bank">
		<div class="col-md-6">
			<img src="../img/image-library.gif" class="pull-right img-responsive">
		</div>
		<div class="col-md-6">
			<section class="creative-image-bank-second-container">
				<p>Creative Image Bank</p>
				<p>Collaborate with your co-workers in real-time by easily saving, reusing and editing collections of your most engaging and top performing creative.</p>
			</section>
		</div>
	</div>

	<div class="row remove-row targeting-libraries">
		<div class="col-md-12 target-row">
			<div class="col-md-5 col-md-offset-2">
				<section>
					<h2>Targeting <br /> Libraries</h2>
					<p>Save time and maximize performance by easily organizing, reusing, and editing libraries of your most profitable and engaging audiences.</p>
				</section>
			</div>
			<div class="col-md-4">
				<section>
					<img src="../img/targeting_libraries.png" class="img-responsive">
				</section>
			</div>
		</div>
	</div>

	<div class="row remove-row actionable-reporting">
		<section class="col-md-12 text-center">
			<h2>Actionable Reporting</h2>
			<p>AdParlor is the first social campaign platform to merge reporting with management capabilities. Uncover insights through our advanced reporting, identify the drivers of this trend, and immediately take action to optimize your campaign spends.</p>
			<img src="../img/action-reporting.png" class="img-responsive">
		</section>
	</div>

	<div class="row remove-row reporting-api" data-parallax="scroll" data-image-src="../img/reporting-api-bg.png">
		<section class="col-md-12 text-center">
			<h2>Reporting API</h2>
			<p>Build out a strong internal audience data set by connecting to our reporting API. Programmatically ingest all your campaign performance data and metrics into your proprietary database and reporting systems.</p>
		</section>
	</div>

	<div class="row remove-row agency-summary-board">
		<div class="col-md-12">
			<section class="col-md-6 agency-summary-board-img">
				<img src="../img/agency-summary-board.jpg" class="img-responsive">
			</section>
			<section class="col-md-4 no-padding">
				<h2>Agency Summary Board</h2>
				<p>Access, view, and report across multiple advertising accounts, Facebook pages, Twitter handles, and campaigns all in one interface to keep track of delivery, pacing, and many more key agency metrics.</p>
			</section>
		</div>
	</div>

	<div class="row remove-row self-serve-support">
		<section class="col-md-6">
			<section class="self-serve-support-text-container">
				<h2>Self-Serve Support</h2>
			<p>AdParlor’s advanced ad trafficking platform, complemented with expert campaign support, data analysis and creative services.</p>
			</section>
			<section class="self-serve-support-text-check-container">
				<p><i class="fa fa-check accent-color self-serve-fa-check"></i> <span>Full onboarding program and campaign launch support</span></p>
				<p><i class="fa fa-check accent-color self-serve-fa-check"></i> <span>Continuous feature and publisher training</span></p>
				<p><i class="fa fa-check accent-color self-serve-fa-check"></i> <span>Bi-weekly campaign strategy meetings</span></p>
				<p><i class="fa fa-check accent-color self-serve-fa-check"></i> <span>Dedicated technical account manager</span></p>
				<p><i class="fa fa-check accent-color self-serve-fa-check"></i> <span>Data analysis and creative services consultations</span></p>
			</section>
		</section>
		<section class="col-md-5">
			<section class="self-serve-support-img-container">
				<img src="../img/self-serve-support.png" class="img-responsive">
			</section>
		</section>
	</div>
</div>
@endsection