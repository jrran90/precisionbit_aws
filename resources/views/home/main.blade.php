@extends('tpl.tpl-home')
@section('title', 'PrecisionBit')
@section( 'stylesheets' )
<link href="{{ asset('/css/animation.css') }}" rel="stylesheet">
@endsection

@section('main')

	<style type="text/css">
    #bgVideo
    {
      position: absolute;
      right: 0;
      bottom: 0;
      top: 0;
      width: auto;
      min-width: 100%;
      height: 600px;
      /*min-height: 100%;*/
      z-index: -100;    
      /*background-size: 100% 100%;*/
      /*object-fit: inherit;*/
      background-color: black;
    }
   </style>
   <script type="text/javascript">
     $(document).ready(function( ) {
      
     });
   </script>
	<div class="row-container">
		<div class="video-playing-container">
	    <div class="content_1">
        <ul class="bxslider">

          <li>
            <img src="../img/pbit-assets-v2/1.jpg" />
            <div class="content_info_1">
              <p>Smarter marketing through visual intelligence</p>
              <button class="btn btn-default btn-request" onclick="$('.form-content').animatescroll();">REQUEST DEMO</button>
              <p class="slide-text">
                  PrecisionBit helps brands and agencies make data-driven creative decision by unlocking consumer insight from user-generated photos 
              </p>
            </div>
            <div class="info-container-slider">
              <i class="icon-plus-button icon-layout_1" onmouseover="mouseoversound.playclip()"></i>
              <div class="face-tracer"></div>
               <div class="waist-tracer"></div>
               <div class="surrounding-tracer"></div>

              <div class="info-first-container">
                <table class="table">
                  <tbody>
                    <tr style="background:#EF8201">
                      <td><p style="font-size:18px;color:#fff;margin:0">Emotion</p></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Neutral:</td>
                      <td>0.02</td>
                    </tr>
                    <tr style="background:#EF8201">
                      <td>Happiness:</td>
                      <td>0.81</td>
                    </tr>
                    <tr>
                      <td>Surprise:</td>
                      <td>0.17</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="info-waist-container">
                <table class="table">
                  <tbody>
                    <tr style="background:#0177A7">
                      <td><p style="font-size:18px;color:#fff;margin:0">Appeal</p></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Neutral:</td>
                      <td>0.02</td>
                    </tr>
                    <tr style="background:#0177A7">
                      <td>Happiness:</td>
                      <td>0.81</td>
                    </tr>
                    <tr>
                      <td>Surprise:</td>
                      <td>0.17</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="info-surrounding-container">
                <table class="table">
                  <tbody>
                    <tr style="background:#FB7972">
                      <td><p style="font-size:18px;color:#fff;margin:0">Relevance</p></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Outdoor:</td>
                      <td>0.02</td>
                    </tr>
                    <tr style="background:#FB7972">
                      <td>Summer:</td>
                      <td>0.88</td>
                    </tr>
                    <tr>
                      <td>Beach:</td>
                      <td>0.10</td>
                    </tr>
                  </tbody>
                </table>
              </div>


              <div class="slider-ctr text-center">
                <span style="color:#fff;">Predicted</span>
                <p style="color:#fff;">CTR</p>
                <div class="slider-long-bar">
                </div>
                <div class="circle-indicator">
                  <span style="color:#fff;font-size:14px;position:relative;left:-45px;top:20px;">Low</span>
                  <span style="color:#fff;font-size:14px;position:relative;right:-257px;">High</span>
                </div>
              </div>
              <i class="icon-plus-button icon-layout_2" onmouseover="mouseoversound.playclip()"></i>
              <div class="slider-color-pallete-container">
                <p style="margin:0;font-size:12px">Colour</p>
                <p style="margin:0;font-size:12px">Palette</p>
                <p style="margin:0;font-size:12px">Breakdown</p>
                <div class="color-container-slider">
                  <div class="color_1"></div>
                  <div class="color_2"></div>
                  <div class="color_3"></div>
                  <div class="color_4"></div>
                  <div class="color_5"></div>
                </div>
              </div>
            </div>
          </li>








          <li>
            <img src="../img/pbit-assets-v2/2.jpg" />
            
            <div class="content_info_1">
              <p>Smarter marketing through visual intelligence</p>
              <button class="btn btn-default btn-request" onclick="$('.form-content').animatescroll();">REQUEST DEMO</button>
              <p class="slide-text">
                  PrecisionBit helps brands and agencies predict and identify the best visual content that will maximise engagement based on emotion, relevance and customer appeal
              </p>
            </div>
            <div class="info-container-slider">
              <i class="icon-plus-button icon-layout-slider-2-plus-1" onmouseover="mouseoversound.playclip()"></i>
              <div class="info-first-container slide-two-content-three">
                <table class="table">
                  <tbody>
                    <tr style="background:#0177A7">
                      <td><p style="font-size:18px;color:#fff;margin:0">Appeal</p></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Young Adults:</td>
                      <td>0.03</td>
                    </tr>
                    <tr style="background:#0177A7">
                      <td>Kids:</td>
                      <td>0.80</td>
                    </tr>
                    <tr>
                      <td>Celebrity:</td>
                      <td>0.17</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="info-first-container slide-two-content-two">
                <table class="table">
                  <tbody>
                    <tr style="background:#EF8201">
                      <td><p style="font-size:18px;color:#fff;margin:0">Emotion</p></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Neutral:</td>
                      <td>0.02</td>
                    </tr>
                    <tr style="background:#EF8201">
                      <td>Happiness:</td>
                      <td>0.81</td>
                    </tr>
                    <tr>
                      <td>Surprise:</td>
                      <td>0.17</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="info-first-container slide-two-content-four">
                <table class="table">
                  <tbody>
                    <tr style="background:#FB7972">
                      <td><p style="font-size:18px;color:#fff;margin:0">Relevance</p></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Car:</td>
                      <td>0.02</td>
                    </tr>
                    <tr style="background:#FB7972">
                      <td>Animation:</td>
                      <td>0.88</td>
                    </tr>
                    <tr>
                      <td>Yard:</td>
                      <td>0.10</td>
                    </tr>
                  </tbody>
                </table>
              </div>


              <div class="face-tracer-one"></div>
              <div class="face-tracer-two"></div>
              <div class="face-tracer-four"></div>


              <div class="slider-ctr slider-2-ctr text-center">
                <span style="color: white!important">Predicted</span>
                <p style="color: white!important">CTR</p>
                <div class="slider-long-bar">
                </div>
                <div class="circle-indicator">
                  <span style="color:#fff;font-size:14px;position:relative;left:-45px;top:20px;">Low</span>
                  <span style="color:#fff;font-size:14px;position:relative;right:-257px;">High</span>
                </div>
              </div>
              <i class="icon-plus-button icon-layout-slider-2-plus-2" onmouseover="mouseoversound.playclip()"></i>
              <div class="tags-box">
                <div class="">
                   <div class="" style="float: right;position: relative;top: -392px;">
                    
                    <p style="margin:0;font-size:12px;color:#fff;">Colour</p>
                    <p style="margin:0;font-size:12px;color:#fff;">Palette</p>
                    <p style="margin:0;font-size:12px;color:#fff;">Breakdown</p>
                    <div class="color-container-slider" style="height:300px">
                      <div class="color_1" style="background: #523C32!important;"></div>
                      <div class="color_2" style="background: #7E7375;"></div>
                      <div class="color_3" style="background: #231818!important;"></div>
                      <div class="color_4" style="background: #8A8683!important;"></div>
                      <div class="color_5" style="background: #B38476!important;"></div>
                      <div class="color_5" style="background: #9A5941!important;"></div>
                      <div class="color_5" style="background: #D26748!important;"></div>
                      <div class="color_5" style="background: #E14939!important;"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </li>
 




          <li>
            <img src="../img/pbit-assets-v2/coffe.jpg" />
            
            <div class="content_info_1">
              <p>Smarter marketing through visual intelligence</p>
              <button class="btn btn-default btn-request" onclick="$('.form-content').animatescroll();">REQUEST DEMO</button>
              <p class="slide-text">
                  PrecisionBit helps brands and agencies predict and identify the best visual content that will maximise engagement based on emotion, relevance and customer appeal
              </p>
            </div>
            <div class="info-container-slider">
              <div class="tags-slider-box-container">
                <!-- <div class="row">
                  <div class="tags-box-slider-1" style="margin-left: 6%;"><i class="glyphicon glyphicon-ok custom-ok-slider-1"></i> coffe</div>
                  <div class="tags-box-slider-2">table</div>
                  <div class="tags-box-slider-2">tableware</div>
                </div>
                <div class="row" style="margin-top: 5px;">
                  <div class="tags-box-slider-1"><i class="glyphicon glyphicon-ok custom-ok-slider-2"></i> restaurant</div>
                  <div class="tags-box-slider-2">breakfast</div>
                  <div class="tags-box-slider-1" style="margin-left: 10px;"><i class="glyphicon glyphicon-ok custom-ok-slider-3"></i> indoors</div>
                </div>
                <div class="row" style="margin-top: 5px;">
                  <div class="tags-box-slider-2" style="margin-left: 10%;">flare</div>
                  <div class="tags-box-slider-1" style="margin-left: 10px"><i class="glyphicon glyphicon-ok custom-ok-slider-4"></i> cup</div>
                  <div class="tags-box-slider-1" style="margin-left: 10px;"><i class="glyphicon glyphicon-ok custom-ok-slider-5"></i> dining</div>
                </div> -->
              </div>
              <i class="icon-plus-button icon-layout_3" onmouseover="mouseoversound.playclip()"></i>

              <div class="box1-tracer"></div>
              <div class="box2-tracer"></div>

              <div class="slider-ctr slider-3-ctr text-center" style="margin-left:0 !important;">
                <span style="color: white!important">Predicted</span>
                <p style="color: white!important">CTR</p>
                <div class="slider-long-bar">
                </div>
                <div class="circle-indicator">

                  <span style="color:#fff;font-size:14px;position:relative;left:-45px;top:20px;">Low</span>
                  <span style="color:#fff;font-size:14px;position:relative;right:-257px;">High</span>
                </div>
              </div>

              <div class="info-first-container info-box1-container">
                <table class="table">
                  <tbody>
                    <tr style="background:#EA827A">
                      <td><p style="font-size:18px;color:#fff;margin:0">Relevance</p></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Indoors:</td>
                      <td>0.02</td>
                    </tr>
                    <tr style="background:#EA827A">
                      <td>Coffee:</td>
                      <td>0.97</td>
                    </tr>
                    <tr>
                      <td>Retail:</td>
                      <td>0.01</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <div class="info-first-container info-box2-container">
                <table class="table">
                  <tbody>
                    <tr style="background:#2E78A4">
                      <td><p style="font-size:18px;color:#fff;margin:0">Appeal</p></td>
                      <td></td>
                    </tr>
                    <tr>
                      <td>Kids:</td>
                      <td>0.06</td>
                    </tr>
                    <tr style="background:#2E78A4">
                      <td>Adults:</td>
                      <td>0.71</td>
                    </tr>
                    <tr>
                      <td>Mature:</td>
                      <td>0.23</td>
                    </tr>
                  </tbody>
                </table>
              </div>

              <i class="icon-plus-button icon-layout_4" onmouseover="mouseoversound.playclip()"></i>
              <div class="slider-color-pallete-container slider-pallete-4" style="margin-top:30px!important;">
                
                <p style="margin:0;font-size:12px">Colour</p>
                <p style="margin:0;font-size:12px">Palette</p>
                <p style="margin:0;font-size:12px">Breakdown</p>
                <div class="color-container-slider">
                  <div class="color_1" style="height: 45%!important; width:100% ;background: #D9D6D9!important;"></div>
                  <div class="color_2" style="height: 30%!important; width:100% ;background: #9F9192;"></div>
                  <div class="color_3" style="height: 15%!important; width:100% ;background: #675D5E!important;"></div>
                  <div class="color_4" style="height: 6%!important; width:100% ;background: #2F2F28!important;"></div>
                  <div class="color_5" style="height: 4%!important; width:100% ;background: #3E5C50!important;"></div>
                </div>
              </div>
            </div>
          </li>
        </ul>
	    </div>
		</div>
		<div class="col-md-12 content_2">
            <div class="row col-md-12 text-center section-desc" style="padding:40px;">
				<h1>Consumers are now visual - we help you impact that conversation.</h1>
				<p>PrecisionBit uses an intelligent machine that is analyzing more than</p>
                <p><span class="txt-green counter numbers-with-commas">123,456,123,789</span></p>
                <p>user-generated visual content from social streams.</p>
			</div>
		</div>
		<div class="col-md-12 content_3">
            <section class="bg-overlay"></section>
            <!-- <div class="col-md-12 attr-contents"> -->
                <!-- <div class="col-md-12 content-attr-lists"> -->
<!-- 
                    <div class="attr-content left attr-object">
                        <i class="icon-square"></i>  
                        <span>Object</span>
                    </div>

                    <div class="attr-content left attr-gender">
                        <i class="icon-Gender"></i>
                        <span>Gender</span>
                    </div>

                    <div class="attr-content left attr-activities">
                        <i class="icon-tennis"></i>
                        <span>Activities</span>
                    </div>

                    <div class="attr-content left attr-age">
                        <i class="icon-GorKDr47F0"></i>
                        <span>Age</span>
                    </div>

                    <div class="attr-content left attr-focus">
                        <i class="icon-Focus"></i>
                        <span>Focus</span>
                    </div>

                    <div class="attr-content left attr-clothing">
                        <i class="icon-Clothing"></i>
                        <span>Clothing</span>
                    </div>

                     -->

                    <!-- <div class="img-center"> -->
                        
                    <!-- </div> -->

<!-- 
                    <div class="attr-content right attr-emotions">
                        <span>Emotions</span>
                        <i class="icon-Emotion"></i>
                    </div>
                    <div class="attr-content right attr-colors">
                        <span>Colors</span>
                        <i class="icon-Colour"></i>
                    </div>
                    <div class="attr-content right attr-scenarios">
                        <span>Scenarios</span>
                        <i class="icon-Scenario"></i>
                    </div>
                    <div class="attr-content right attr-themes">
                        <span>Themes</span>
                        <i class="icon-Thene"></i>
                    </div>
                    <div class="attr-content right attr-concepts">
                        <span>Concepts</span>
                        <i class="icon-Concept_2"></i>
                    </div>
                    <div class="attr-content right attr-aesthetics">
                        <span>Aesthetics</span>
                        <i class="icon-Aesthetic" style="position:relative;top:9px;"></i>
                    </div> -->

                <!-- </div> -->
            <!-- </div> -->
            <div class="col-md-12 text-center">
              <img src="../img/pbit-assets-v2/FB_Spidergrey-blue1.PNG" class="img-responsive img-spider" style="margin: 0 auto;">
                <p>We help companies get insight from social photos for improved audience profiling and messaging</p>
            </div>            			
		</div>
		<!-- <div class="col-md-12 content_4">
			<div class="col-md-8 content_4-text-container">
				<h1>Actionable intelligence from the visual web</h1>

                <div class="col-md-12 content_4-list-ok">
                    <i class="glyphicon glyphicon-ok custom-glyphicon-ok"></i>
                    <span class="content_4-second-text">Validate creative campaign ideas</span>
                </div> 
                <div class="col-md-12 content_4-list-ok">
                    <i class="glyphicon glyphicon-ok custom-glyphicon-ok"></i>
                    <span class="content_4-second-text">Discover visual content oppurtunities</span>
                </div> 
                <div class="col-md-12 content_4-list-ok">
                    <i class="glyphicon glyphicon-ok custom-glyphicon-ok"></i>
                    <span class="content_4-second-text">Outdo the competition using predictive intelligence</span>
                </div> 
			</div>
            <div class="col-md-4" style="text-align:center;">
                <img src="../img/pbit-assets-v2/pc-no-apple.png" class="pc-image pull-right" width="330">
            </div>
		</div> -->
        <div class="col-md-12 contact_form_wrapper">
            <div class="container form-content">
                <div class="col-md-6 col-md-offset-3">
                    <form class="form form-inputflds" ng-submit="sendEmail()">
                        <div class="form-label text-center">
                            <h3>READY TO GIVE IT A TRY? GET A DEMO</h3>
                            <br>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" ng-model="mail.firstname" placeholder="First Name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" ng-model="mail.lastname" placeholder="Last Name" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" ng-model="mail.company_name" placeholder="Company Name" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="email" class="form-control" ng-model="mail.email" placeholder="Email" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <textarea class="form-control" ng-model="mail.message" placeholder="Message (Optional)"></textarea>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-default form-control submit" id="btn-submit"><strong>Submit</strong></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
		<div class="col-md-12 content_5" hidden>
            <section class="bg-overlay"></section>            
			<section class="content_5-text-container"> 
				<h2>KNOW WHAT WILL RESONATE BEFORE PUBLISHING</h2>
				<p style="color:#fff"> We find patterns and themes hidden within consumer-generated visual data.</p>
				<br>
                <button class="btn btn-default btn-request" onclick="$('.form-content').animatescroll();">REQUEST DEMO</button>
			</section>
		</div>
        <div class="col-md-12 content_images_sample" hidden>
            <div class="container">
                <div class="col-md-4 text-center">
                    <div class="text-wrapper">
                        <h4 class="num"></h4>
                        <h4 class="attr" style="margin-top: 100px;">Data Collection</h4>
                        <p class="text-center"> 
                            Query a keyword, location or social ID
                        </p>
                    </div>
                    <img src="../img/home_images/1.png" class="img-responsive">
                </div>
                <div class="col-md-4 text-center">
                    <div class="text-wrapper">
                        <h4 class="num"></h4>
                        <h4 class="attr" style="margin-top: 100px;">Filtering and Classification</h4>
                        <p class="text-center"> 
                            Analyze images from a content feed and classify
                        </p>
                    </div>
                    <img src="../img/home_images/2.png" class="img-responsive">
                </div>
                <div class="col-md-4 text-center">
                    <div class="text-wrapper">
                        <h4 class="num"></h4>
                        <h4 class="attr" style="margin-top: 100px;">Audience Insight</h4>
                        <p class="text-center"> 
                            Find patterns and trends about interest profiles 
                        </p>
                    </div>
                    <img src="../img/home_images/3.png" class="img-responsive">
                </div>
            </div>
        </div>
		<!-- <div class="col-md-12 content_6">
            <section class="row" style="min-height:300px;padding:50px;background:#F2F2F2">
                <div class="container">
                    <div class="col-md-5 hidden-sm">
                      <img src="../img/home_images/image7.png" class="img-responsive" style="width: 100px;display: inline-block;">
                      <h4 style="display: inline-block;font-size: 15px">Query: chinoisfemme ( social id )</h4>
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <h2>Profile your customers</h2>
                        <p>Evaluate someone's persona by their feed content and engagement; identify who is most likely to be interested in a product</p>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding" style="position: relative;">
                        
                        <img src="../img/home_images/image4.png" class="img-responsive hidden-sm" style="width: 400px;display: inline-block;">
                        <img src="../img/home_images/image8.png" class="img-responsive hidden-sm" style="width: 420px;display: inline-block;">
                        <img src="../img/home_images/image5.png" class="img-responsive hidden-sm" style="width: 160px;display: inline-block;position: absolute;top:300px;left:450px;">
                        <h4 class="hidden-sm" style="text-align:center;position: absolute;top: 0;right: 70px">Customer Persona</h4>
                        <img src="../img/home_images/image6.png" class="img-responsive" style="width: 300px;display: inline-block;">
                    </div>
                </div>
            </section>
            <section class="row" style="min-height:300px;padding:50px;background:#DBDBDB">
                <div class="container">
                    <div class="col-md-5 hidden-sm">
                      <h4 style="margin-top:50px;font-size: 15px;text-align: center;">Query: Orchard Ion mall (location)</h4>
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <h2>Location-based customer analytics</h2>
                        <p>Analyze content feeds of people who visit a location to identify purchase preferences and motivations.</p>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding" style="position: relative;">
                        
                        <img src="../img/home_images/image9.png" class="img-responsive hidden-sm" style="width: 400px;display: inline-block;">
                        <img src="../img/home_images/image8.png" class="img-responsive hidden-sm" style="width: 420px;display: inline-block;">
                        <img src="../img/home_images/image5.png" class="img-responsive hidden-sm" style="width: 160px;display: inline-block;position: absolute;top:300px;left:450px;">
                        <h4 class="hidden-sm" style="text-align:center;position: absolute;top: 30px;right: 50px">Shopper lifestyle preferences</h4>
                        <img src="../img/home_images/image10.png" class="img-responsive" style="width: 300px;display: inline-block;">
                    </div>
                </div>
            </section>
            <section class="row" style="min-height:300px;padding:50px;background:#F2F2F2">
                <div class="container">
                    <div class="col-md-5 hidden-sm">
                      <h4 style="margin-top:50px;font-size: 15px;text-align: center;">Query: NLEX, SLEX (locations); #NLEX # SLEX</h4>
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                        <h2>Develop data-driven messaging</h2>
                        <p>Analyze content feeds of people who post about a location, event or topic to develop personalized messaging based on interests and lifestyles.</p>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding" style="position: relative;">
                        
                        <img src="../img/home_images/image11.png" class="img-responsive hidden-sm" style="width: 400px;display: inline-block;">
                        <img src="../img/home_images/image8.png" class="img-responsive hidden-sm" style="width: 420px;display: inline-block;">
                        <img src="../img/home_images/image5.png" class="img-responsive hidden-sm" style="width: 160px;display: inline-block;position: absolute;top:300px;left:450px;">
                        <h4 class="hidden-sm" style="text-align:center;position: absolute;top: 50px;right: 125px">Persona</h4>
                        <img src="../img/home_images/image12.png" class="img-responsive" style="width: 300px;display: inline-block;">
                    </div>
                </div>
            </section>
		</div> -->

		<!-- <div class="col-md-12 content_7">
			<section class="content_7-text-container text-center">
				<div class="container">
						<h2>Service and Support</h2>
						<h3></h3>
						<h3>We believe in supporting our clients whether they’re utilizing our self-serve or fully-managed services, and allow complete flexibility to switch between each option at any time.</h3>

						<div class="row" style="padding-top: 60px;">
							
							<div class="col-md-3 col-md-offset-3">
								<div class="vc_col-sm-3 wpb_column column_container col boxed centered-text padding-2-percent has-animation one-fourths right-edge" data-padding-pos="all" data-hover-bg="" data-animation="fade-in" data-delay="0" style="opacity: 1;"><a class="column-link" href="/managed-services"></a>
									<div class="wpb_wrapper">									
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
												<i class="circle-border">
													<i class="icon-3x fa fa-users accent-color"></i>
												</i>
												<h5>Fully Managed</h5>
												<p>Our team of experts handles all aspects of your campaign from strategy, creation, media buying, reporting, and optimization.</p>
											</div> 
										</div> 
									</div> 
								</div>
							</div>
							
							<div class="col-md-3">
								<div class="vc_col-sm-3 wpb_column column_container col boxed centered-text padding-2-percent has-animation one-fourths right-edge" data-padding-pos="all" data-hover-bg="" data-animation="fade-in" data-delay="0" style="opacity: 1;"><a class="column-link" href="/managed-services"></a>
									<div class="wpb_wrapper">									
										<div class="wpb_text_column wpb_content_element ">
											<div class="wpb_wrapper">
												<i class="circle-border">
													<i class="icon-3x fa fa-laptop accent-color"></i>
												</i>
												<h5>Self Serve</h5>
													<p>AdParlor’s advanced ad trafficking platform, complemented with expert campaign support, data analysis, and creative services.</p>
											</div> 
										</div> 
									</div> 
								</div>
							</div>

						</div>

				</div>
			</section>
		</div>
		<div class="col-md-12 content_8 text-center" data-parallax="scroll" data-image-src="../img/parallax.png">
			<section class="content_8-text-container">
				<h2>Success Stories</h2>
				<h3>
				Let's talk about your business goals and why <br />
			  PrecisionBit is the best marketing partner to help you <br />
			  achieve them.
				</h3>
			</section>
			<a href="#" class="content_8-success-stories">Read Success Stories</a>
		</div> -->
		<!-- <div class="col-md-12 content_9">
			<section class="content_9-platform-solutions-container">
				<h2>Platform Solutions</h2>
			</section>
		</div> -->
		<!-- <div class="col-md-12 content_10">
			<section class="content_10-platform-solutions-container text-center">
				<h2>Platform Solutions</h2>
			</section>
			<section class="content_10-platform-links">
				<a href="#" class="platform-links fb">Facebook</a>
				<a href="#" class="platform-links twit">Twitter</a>
				<a href="#" class="platform-links ins">Instagram</a>
				<a href="#" class="platform-links yt">Youtube</a>
			</section>
		</div> -->
		<!-- <div class="col-md-12 content_11">
			<section class="content_11-text-container">
				<h4 style="float: left;">Drive business with Facebook, Twitter, Instagram and Youtube advertising</h4>
				<button class="btn btn-large btn-custom-question" style="float: right;"> CONTACT US </button>
			</section>
		</div> -->
	</div>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.3/waypoints.min.js"></script> 
  <script type="text/javascript" src="{{ asset('/js/jquery.counterup.min.js') }}"></script>
  <script type="text/javascript">
    jQuery(document).ready(function( $ ) {
        $('.counter').counterUp({
            delay: 10,
            time: 60000
        });
    });

    $(function(){
      $('.icon-layout_1').hover(function(){
        $(this).siblings('.face-tracer').addClass('scale-box-layout');
        $(this).siblings('.waist-tracer').addClass('scale-box-layout');
        $(this).siblings('.surrounding-tracer').addClass('scale-box-layout');        
        $(this).siblings('.info-first-container').addClass('scale-box-layout');
        $(this).siblings('.info-waist-container').addClass('scale-box-layout');
        $(this).siblings('.info-surrounding-container').addClass('scale-box-layout');
        $(this).siblings('.slider-ctr').slideDown();
      }, function(){
        $(this).siblings('.face-tracer').removeClass('scale-box-layout');
        $(this).siblings('.info-first-container').removeClass('scale-box-layout');


        $(this).siblings('.waist-tracer').removeClass('scale-box-layout');
        $(this).siblings('.surrounding-tracer').removeClass('scale-box-layout');    
        $(this).siblings('.info-waist-container').removeClass('scale-box-layout');
        $(this).siblings('.info-surrounding-container').removeClass('scale-box-layout');

        $(this).siblings('.slider-ctr').slideUp();
      });

      $('.icon-layout_2').hover(function(){
        $(this).siblings('.slider-color-pallete-container').addClass('activate-slider-color-pallete-container');
        $('.slider-color-pallete-container p').addClass("active-slide-p");
      }, function(){
        $(this).siblings('.slider-color-pallete-container').removeClass('activate-slider-color-pallete-container');
        $('.slider-color-pallete-container p').removeClass("active-slide-p");
      });

      $('.icon-layout-slider-2-plus-1').hover(function(){
        $(this).siblings('.face-tracer-one').addClass('scale-box-layout');
        $(this).siblings('.face-tracer-two').addClass('scale-box-layout');
        $(this).siblings('.face-tracer-four').addClass('scale-box-layout');
        $(this).siblings('.slide-two-content-two').addClass('scale-box-layout');
        $(this).siblings('.slide-two-content-three').addClass('scale-box-layout');
        $(this).siblings('.slide-two-content-four').addClass('scale-box-layout');
        $(this).siblings('.slider-ctr').slideDown();
      }, function(){
        $(this).siblings('.face-tracer-one').removeClass('scale-box-layout');
        $(this).siblings('.face-tracer-two').removeClass('scale-box-layout');
        $(this).siblings('.face-tracer-four').removeClass('scale-box-layout');
        $(this).siblings('.slider-ctr').slideUp();
        $(this).siblings('.slide-two-content-two').removeClass('scale-box-layout');
        $(this).siblings('.slide-two-content-four').removeClass('scale-box-layout');
        $(this).siblings('.slide-two-content-three').removeClass('scale-box-layout');
      })

      $('.icon-layout-slider-2-plus-2').hover(function(){
        $(this).siblings('.tags-box').addClass('scale-box-layout');
      }, function(){
        $(this).siblings('.tags-box').removeClass('scale-box-layout');
      });

      $('.icon-layout_3').hover(function(){
        $('.tags-slider-box-container').addClass('scale-box-layout');
        $('.info-box1-container').addClass('scale-box-layout');
        $('.info-box2-container').addClass('scale-box-layout');
        $('.box1-tracer').addClass('scale-box-layout');
        $('.box2-tracer').addClass('scale-box-layout');
        $('.slider-3-ctr').slideDown();
      }, function(){
        $('.tags-slider-box-container').removeClass('scale-box-layout');
        $('.info-box1-container').removeClass('scale-box-layout');
        $('.info-box2-container').removeClass('scale-box-layout');
        $('.box1-tracer').removeClass('scale-box-layout');
        $('.box2-tracer').removeClass('scale-box-layout');
        $('.slider-3-ctr').slideUp();
      });

      $('.icon-layout_4').hover(function(){
        $(this).siblings('.slider-pallete-4').addClass('activate-slider-color-pallete-container');
        $('.slider-pallete-4 p').addClass("active-slide-p");
      }, function(){
        $(this).siblings('.slider-pallete-4').removeClass('activate-slider-color-pallete-container');
        $('.slider-pallete-4 p').removeClass("active-slide-p");
      });
    });
  </script>
@endsection