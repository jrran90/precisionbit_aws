@extends('tpl.tpl-home')
@section('title', 'PrecisionBit')

@section('about')
<script type="text/javascript">
  $(function(){
    $('.goForm').click(function(){
      localStorage.setItem("emailTrigger","true");
      window.location =  window.location.origin + "/";
    });
  });
</script>
<div class="col-md-12 about-container">
	<div class="col-md-10 col-md-offset-1 about-container-wrapper" style="">
		
		<div class="panel panel-default about-container-panel" style="">
			<div class="panel-heading custom-panel-heading-about text-center">
				<h3>About PrecisionBit</h3>
			</div>
			<div class="panel-body">
				<div class="col-md-9">
					<section class="about-details-container">
						<h5>What is PrescisionBit?</h5>
						<p>PrecisionBit is a machine-learning platform that offers insight and information of the images that drive conversions.</p>
					</section>
					<section class="about-details-container">
						<h5>How does it work?</h5>
						<p>Using computer vision and social performance data, PrecisionBit translates visual elements such as emotions and themes from images into data, to understand what drives conversions from a social audience, and identify new imagery with similar attributes that can be used to improve performance. </p>
					</section>
					<section class="about-details-container">
						<h5>How can I find out more?</h5>
						<p>We would love to hear from you. To get in touch or request for a demo, go <a href="#" class="goForm">here</a></p>
					</section>
				</div>
				<!-- <div class="col-md-3">
					<div class="panel panel-default" style="background-color: #CCCCCC!important;">
						<div class="panel-heading custom-panel-heading-about border-bottom-style">
							<p>Who We Are</p>
						</div>
						<div class="panel-body" style="background-color: #CCCCCC!important;">
							<section class="about-details-side-container">
								<h5>Meet the founders</h5>
								<p>Brian Craighead, Julie Frikken, Kevin Lam and Natasha Foong. We've come together to bring to life a shared vision for creating accessible, rich insight for brands and communication across cultures and markets. We believe in no boundaries and no borders but a respect for the massive online market, the technology that drives it and equally the different individuals that make it up.</p>
							</section>
						</div>
						<div class="panel-body" style="background-color: #CCCCCC!important;border-top: 1px dashed white;">
							<section class="about-details-side-container">
								<p>As individuals from different corners of the globe we've been responsible for developing and executing strategy and creative direction for complex digital campaigns and groundbreaking brand engagement campaigns. Directing international brands into new markets and buying and selling businesses globally.</p>
							</section>
						</div>
					</div>
				</div> -->
			</div>
		</div>
		
	</div>
</div>
@endsection