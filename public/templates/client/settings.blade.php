<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;
">
    <section style="font-size: 20px!important;">
        <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size:15px;margin: 10px 0 0 10px;">
        Settings<br>
        <small>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</small>
        </h1>
	    <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.5s" style="margin:0;font-size:10px;background-color: #E7EAEF;">
	      <li>
	      	<a ui-sref="create-brief" class="btn btn-medium custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
        </li>
	    </ol>
    </section>
</div>


<div class="row" style="background-color: #E7EAEF;" id="settings-page">
	<section class="content" >
		<div class="row" id="submenu-settings">
			<div>
				<ul class="list-unstyled">
					<li><a href="#">Profile Detail</a></li>
					<li><a href="#">Billing Detail</a></li>
				</ul>
			</div>	
		</div>
		<div class="panel panel-default">
			<div class="panel-heading overflow">
				<div class="col-md-5">
					<p class="remove-margin header-title-section">Personal Info</p>
				</div>
				<div class="col-md-7 text-right">
					<button class="btn custom-button"><i class="fa fa-save"></i>&nbsp;Save</button>
					<button class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Discard</button>
				</div>
			</div>
			<div class="panel-body">
				<form>
					<div class="form-group">
						<div class="col-md-3">
							<div class="fileinput fileinput-new" data-provides="fileinput">
							  <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"></div>
							  <div>
							    <span class="btn btn-default btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" name="..."></span>
							    <a href="#" class="btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
							  </div>
							</div>
						</div>
						<div class="col-md-4">
							<label>First Name</label>
							<input type="text" class="form-control">
							<label>Last Name</label>
							<input type="text" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-7">
							<label>Email</label>
							<input type="email" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-7">
							<label>Phone Number</label>
							<input type="email" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-7">
							<label>Country</label>
							<select class="form-control">
								<option>ewq</option>
								<option>ewq</option>
								<option>ewq</option>
								<option>ewq</option>
								<option>ewq</option>
							</select>
						</div>
					</div>
				</form>				
			</div>
		</div>


		
		<div class="panel panel-default">
			<div class="panel-heading overflow">
				<div class="col-md-5">
					<p class="remove-margin header-title-section">Company Information</p>
				</div>
				<div class="col-md-7 text-right">
					<button class="btn custom-button"><i class="fa fa-save"></i>&nbsp;Save</button>
					<button class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Discard</button>
				</div>
			</div>
			<div class="panel-body">
				<form>				
					<div class="form-group">
						<div class="col-md-6">
							<label>Company Name</label>
							<input type="text" class="form-control">
						</div>
						<div class="col-md-6">
							<label>Industry</label>
							<select class="form-control">
								<option>Industry</option>
								<option>ewq</option>
								<option>ewq</option>
								<option>ewq</option>
								<option>ewq</option>
							</select>
						</div>
					</div>			
					<div class="form-group">
						<div class="col-md-6">
							<label>Company Website</label>
							<input type="text" class="form-control">
						</div>
						<div class="col-md-6">
							<label>Size of Company</label>
							<select class="form-control">
								<option>Company Size</option>
								<option>ewq</option>
								<option>ewq</option>
								<option>ewq</option>
								<option>ewq</option>
							</select>
						</div>
					</div>				
				</form>
			</div>
		</div>

		
		<div class="panel panel-default">
			<div class="panel-heading overflow">
				<div class="col-md-5">
					<p class="remove-margin header-title-section">Change Password</p>
				</div>
				<div class="col-md-7 text-right">
					<button class="btn custom-button"><i class="fa fa-save"></i>&nbsp;Save</button>
					<button class="btn btn-danger"><i class="fa fa-trash"></i>&nbsp;Discard</button>
				</div>
			</div>
			<div class="panel-body">
				<form>				
					<div class="form-group">
						<div class="col-md-6">
							<label>Current Password</label>
							<input type="password" class="form-control">
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-6">
							<label>New Password</label>
							<input type="password" class="form-control">
						</div>
					</div>			
					<div class="form-group">
						<div class="col-md-6">
							<label>Confirm Password</label>
							<input type="password" class="form-control">
						</div>
					</div>			
				</form>
			</div>
		</div>

	</section>
</div>

