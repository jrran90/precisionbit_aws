<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">
      <div class="panel-heading">
        <select class="form-control pull-left" style="width: 150px; margin-right: 10px;">
            <option>Key Metrix</option>
            <option>Cost</option>
            <option>Lead Conversion</option>
            <option>Cost per Lead</option>
          </select>
          <div class="input-group" style="width: 240px">
            <div class="input-group-addon">
              <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control" id="drop-select-date">
          </div>
      </div>
      <div class="panel-body" style="width: 100%;">
        <div class="chart">
          <canvas id="show-campaign-result" style="width: 100%; height: 200px;"></canvas>
          <div id="legend-indi"></div>
        </div>          
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-4" style="padding-right: 5px">
    <div class="panel panel-default" style="margin: 0;">
      <div class="panel-body">
        <section>
          <p style="font-size: 20px;">Currently Winning Ad</p>
        </section>
        <section class="col-md-5">
          <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 60px; width: 60px; display: block; border-radius: 50%;">
        </section>
        <section class="col-m-2">
          <p style="margin-top: 20px;font-size: 18px;">Ads A</p>
        </section>
        <section class="col-md-12" style="margin-left: -10px; margin-top: 5px;">
        Confidence percentage: <span style="font-size: 30px;margin-left: 20px;margin-top: -12px;position: absolute;">30%</span>
        </section>
      </div>
    </div>
    <div class="panel panel-default" style="margin: 0;margin-top: 5px;">
      <div class="panel-body">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <p><span style="font-size: 20px;font-weight: bold;">3</span> <span style="margin-left: 2px;">days</span> <span style="margin-left: 15px;font-weight: bold;font-size: 20px;">5</span> <span style="margin-left: 2px;">Minutes</span> <span style="font-size: 20px;font-weight: bold;margin-left: 15px;">39</span> <span>Hours</span></p>
        </div>
      </div>
      <p style="margin-left: 30px; margin-top: -20px;">RUNTIME</p>
    </div>
  </div>
  <div class="col-md-4" style="padding: 0;">
    <div class="panel panel-default" style="padding: 13px;margin: 0;">
      <div class="panel-body">
        <p style="font-size: 37px;">Results</p>
      </div>
    </div>
    <div class="panel panel-default" style="padding: 13px;margin: 0;margin-top: 5px;">
      <div class="panel-body">
        <p style="font-size: 37px;">Reach</p>
      </div>
    </div>
  </div>
  <div class="col-md-4" style="padding: 0; padding-left: 5px; padding-right: 10px;">
    <div class="panel panel-default" style="padding: 13px;margin: 0;">
      <div class="panel-body">
        <p style="font-size: 37px;">Cost</p>
      </div>
    </div>
    <div class="panel panel-default" style="padding: 13px;margin: 0;margin-top: 5px;">
      <div class="panel-body">
        <p style="font-size: 37px;">Amount Spend</p>
      </div>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-md-12" style="margin-top: 10px;">
    <div class="panel panel-default">
      <div class="panel-body">
        <table class="table table-bordered">
            <tbody>
            <tr>
              <th><span>AD Variation</span></th>
              <th><span>Results</span> <span class="glyphicon glyphicon-question-sign"></span></th>
              <th><span>Reach</span> <span class="glyphicon glyphicon-question-sign"></span></th>
              <th><span>Cost</span> <span class="glyphicon glyphicon-question-sign"></span></th>
              <th><span>Amount spend</span> <span class="glyphicon glyphicon-question-sign"></span></th>
            </tr>
            <tr>
              <td>
                <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 60px; width: 60px;" class="pull-left">
                <p style="padding: 10px;font-size: 20px;margin-top: 5px;margin-left: 12px;" class="pull-left">Ad A</p>
              </td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>
                <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 60px; width: 60px;" class="pull-left">
                <p style="padding: 10px;font-size: 20px;margin-top: 5px;margin-left: 12px;" class="pull-left">Ad B</p>
              </td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td>
                <img data-src="holder.js/100%x180" alt="100%x180" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iVVRGLTgiIHN0YW5kYWxvbmU9InllcyI/PjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB3aWR0aD0iMTcxIiBoZWlnaHQ9IjE4MCIgdmlld0JveD0iMCAwIDE3MSAxODAiIHByZXNlcnZlQXNwZWN0UmF0aW89Im5vbmUiPjwhLS0KU291cmNlIFVSTDogaG9sZGVyLmpzLzEwMCV4MTgwCkNyZWF0ZWQgd2l0aCBIb2xkZXIuanMgMi42LjAuCkxlYXJuIG1vcmUgYXQgaHR0cDovL2hvbGRlcmpzLmNvbQooYykgMjAxMi0yMDE1IEl2YW4gTWFsb3BpbnNreSAtIGh0dHA6Ly9pbXNreS5jbwotLT48ZGVmcz48c3R5bGUgdHlwZT0idGV4dC9jc3MiPjwhW0NEQVRBWyNob2xkZXJfMTUwODkwNGM3ZjggdGV4dCB7IGZpbGw6I0FBQUFBQTtmb250LXdlaWdodDpib2xkO2ZvbnQtZmFtaWx5OkFyaWFsLCBIZWx2ZXRpY2EsIE9wZW4gU2Fucywgc2Fucy1zZXJpZiwgbW9ub3NwYWNlO2ZvbnQtc2l6ZToxMHB0IH0gXV0+PC9zdHlsZT48L2RlZnM+PGcgaWQ9ImhvbGRlcl8xNTA4OTA0YzdmOCI+PHJlY3Qgd2lkdGg9IjE3MSIgaGVpZ2h0PSIxODAiIGZpbGw9IiNFRUVFRUUiLz48Zz48dGV4dCB4PSI1OS41NjI1IiB5PSI5NC41Ij4xNzF4MTgwPC90ZXh0PjwvZz48L2c+PC9zdmc+" data-holder-rendered="true" style="height: 60px; width: 60px;" class="pull-left">
                <p style="padding: 10px;font-size: 20px;margin-top: 5px;margin-left: 12px;" class="pull-left">Ad C</p>
              </td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
  </div>
</div>
<script type="text/javascript">
    $(function( ){
      $('#drop-select-date').daterangepicker();
    });
</script>