<div ng-controller="predictController" brief-predict style="padding: 15px 55px;">
	<div class="col-md-8 top-trigger-experiment" id="experiment-table" style="margin-top: 30px;">		
		<div class="dropdown" style="margin-bottom: -30px;">
		  	<!-- <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
			    Sort by date/time
			    <span class="caret"></span>
		  	</button>
		  
		  	<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
				<li><a href="javascript:void(0)" ng-click="sortResult( 'latest' )">Latest</a></li>
				<li><a href="javascript:void(0)" ng-click="sortResult( 'oldest' )">Oldest</a></li>
		  	</ul> -->

		  	<!-- <a href="javascript:void(0)" ng-click="showPredictionForm( 'create' )" class="pull-right btn custom-btn-prediction">New Brief</a>
			<a href="javascript:void(0)" ng-click="showPredictionForm( 'cancel' )" class="pull-right btn custom-btn-prediction-cancel" hidden>Cancel</a> -->
			<div class="pull-left">
				<span>Showing 10 of 100:</span> 
				<md-input-container>
			        <!-- <label>Show 10</label> -->
			        <md-select ng-model="ctrl.userState">
			          <md-option value="1" selected>Show 10</md-option>
			          <md-option value="2">Show 20</md-option>
			          <md-option value="3">Show 30</md-option>
			          <md-option value="4">Show 40</md-option>
			          <md-option value="5">Show 50</md-option>
			        </md-select>
			      </md-input-container>
			</div>
			<div class="pull-right">
				<div class="status-box" style="border-right: 1px solid #BBB;">
					<i class="glyphicon glyphicon-triangle-left"></i>
					Prev
				</div>
				<div class="status-box">
					<i class="glyphicon glyphicon-triangle-right"></i>
					Next
				</div>
			</div>
			<div class="clear-both"></div>
		</div>
		<div></div>
		<hr>
		<table class="table table-responsive table-striped table-brief">
			<thead>
				<th><span class="icon-file"></span> Topic</th>
				<th><span class="icon-small-calendar"></span> Date <i class="glyphicon glyphicon-triangle-top"></i></th>
				<th style="width:250px"><span class="icon-bar-chart"></span> Status <i class="glyphicon glyphicon-triangle-top"></i></th>
			</thead>
			<tbody>
				
				<!-- <tr>
					<td>
						<span class="hash-color">#Sample</span>
					</td>
					<td>
						<span>Sample</span>,
						<span>
							<span ng-bind="gender"></span>
							Both
						</span>
					</td>
					<td>
						<span>04-28-2016 04:58 PM</span>
					</td>
					<td class="text-center">
						<span class="pull-left">Completed</span>

						<a href="javascript:void(0)" ui-sref="predict-results-template">
							<i class="fa fa-eye" style="color:#A19FA0; padding: 10px;"></i>
						</a>
						<a href="javascript:void(0)" >
							<i class="fa fa-trash" style="color:#A19FA0"></i>
						</a>
					</td>
				</tr> -->
				<!-- <tr>
					<td>
						<span class="hash-color">#SG50</span>
					</td>
					<td>
						<span>Singapore</span>,
						<span>
							Both
						</span>
					</td>
					<td>
						<span>04-29-2016 04:58 PM</span>
					</td>
					<td class="text-center">
						<span class="pull-left">Completed</span>

						<a href="javascript:void(0)" ui-sref="predict-results-template2">
							<i class="fa fa-eye" style="color:#A19FA0; padding: 10px;"></i>
						</a>
						<a href="javascript:void(0)" >
							<i class="fa fa-trash" style="color:#A19FA0"></i>
						</a>
					</td>
				</tr> -->
				<!--  class="animate-repeat" -->
				<tr ng-repeat="list in clientObject.predict | orderBy:'+':statusView" id="{{list.predict_id}}">
					<td>
						<span ng-repeat="hash in list.hashtags">
							<span ng-bind="hash.hash" data-wow-duration="1.{{ $index + 1 }}" data-wow-delay="1.{{ $index + 1 }}" style="background:{{ hash.hashColor }}" class="hash-color wow fadeInUp"></span><span ng-if="!$last"></span>
						</span>
					</td>
					<!-- <td>
						<span ng-bind="list.country" style="margin-top: 10px; display: inline-block;"></span>
						<br />
						<span ng-bind="list.gender"></span>
					</td> -->
					<td>
						<span ng-bind="list.date_created" style="margin-top: 10px; display: inline-block;"></span>
					</td>
					<td class="text-center">
						<span class="pull-left" ng-if="list.status == 'In Progress'" style="margin-top: 15px;">
							<a href="javascript:void(0)" class="pending"></a>
							Pending
						</span>

						<span class="pull-left" ng-if="list.status == 'Completed'" style="margin-top: 15px;">
							<a href="javascript:void(0)" class="completed"></a>
							Completed
						</span>

						<a ui-sref="predict-results-template" class="brief-completed" ng-if="list.status == 'Completed'">
							Show
						</a>
						<a class="brief-delete" href="javascript:void(0)" ng-click="deleteExperiment( $event, list )">
							Delete
						</a>
					</td>
				</tr>
			</tbody>
		</table>	
	</div>

	<div class="col-md-4" id="predict-form-content" style="box-shadow: 1px 2px 1px #BBB;">
		<form>		
			<div class="content-heading">
				<label>Add Brief</label>
			</div>
			<div layout-gt-sm="row">
				<md-input-container class="md-block" flex-gt-sm>
					<label style="font-weight:normal">Campaign Name</label>
					<input type="text" ng-model="predictObject.campaign_name">
				</md-input-container>
			</div>
			<div layout-gt-sm="row">
				<md-input-container class="md-block" flex-gt-sm>
					<label style="font-weight:normal">Product Name</label>
					<input type="text" ng-model="predictObject.product_name">
				</md-input-container>
			</div>

			<!-- <div class="content-heading">
				<label>Audience (optional)</label>
			</div>
			<div layout-gt-sm="row"> -->
			<!-- <md-input-container class="md-block" flex-gt-sm style="width:50%;padding-right:5px;">
					<label style="font-weight:normal">Country</label>
					<input type="text" ng-model="predictObject.country">
				</md-input-container> -->
				<!-- <md-select class="brief-select" placeholder="Country" ng-model="predictObject.country" md-on-open="" style="width: 50%; padding-left: 5px; border: 1px solid #C6C6C6; padding: 5px;margin-right: 5px;">
					<md-option ng-repeat="country in countryList" value="{{country.name}}"><span ng-bind="country.name"></span></md-option>
				</md-select>

				<md-select class="brief-select" placeholder="Gender" ng-model="predictObject.gender" md-on-open="" style="width: 50%; padding-left: 5px; border: 1px solid #C6C6C6; padding: 5px;">
					<md-option value="Male">Male</md-option>
					<md-option value="Female">Female</md-option>
					<md-option value="Both">Both</md-option>
				</md-select>
			</div> -->

			<!-- <div class="content-heading">
				<label>Objective</label>
			</div>
			<div layout-gt-sm="row">
				<md-select placeholder="Objective" ng-model="predictObject.objective" md-on-open="" style="width:100%">
					<md-option value="Clicks to Website">Clicks to Website: Send people to your website.</md-option>
					<md-option value="Website Conversions">Website Conversions: Increase conversions on your website. --> <!-- You'll need a conversion pixel for your website before you can create this ad. --><!-- </md-option>
					<md-option value="Page Post Engagement">Page Post Engagement: Boost your posts.</md-option> -->
					<!-- <md-option value="Page Likes">Page Likes: Promote your Page and get Page likes to connect. --> <!-- with more of the people who matter to you. --><!-- </md-option>
					<md-option value="App Installs">App Installs: Get installs of your app.</md-option>
					<md-option value="App Engagement">App Engagement: Increase engagement in your app.</md-option>
					<md-option value="Offer Claims">Offer Claims: Create offers for people to redeem in your store.</md-option>
					<md-option value="Local Awareness">Local Awareness: Reach people near your business.</md-option>
					<md-option value="Event Responses">Event Responses: Raise attendance at your event.</md-option>
					<md-option value="Video Views">Video Views: Create ads that get more people to view a video.</md-option>
					<md-option value="Increase Sales">Increase Sales</md-option>
				</md-select>
			</div> -->
			<!-- <div class="col-md-12 no-padding social-media-publish">
				<p>Publish your work in:</p>
				<button ng-click="selectedMedia('Facebook')" id="Facebook" class="btn-social-exp">
					<i class="fa fa-facebook pull-left" style="margin-top: 3px;"></i> | <span class="pull-right">Facebook</span>
				</button>
				<button ng-click="selectedMedia('Instagram')" id="Instagram" class="btn-social-exp">
					<i class="fa fa-instagram pull-left" style="margin-top: 3px;"></i> | <span class="pull-right">Instagram</span>
				</button>
				<br>
				<br>
			</div> -->

			<div class="content-heading">
				<label>Fill in your hashtag groups</label>
			</div>
			<div class="col-md-12 no-padding">
				<md-input-container class="md-block col-md-4 no-padding" style="padding:0 5px !important" flex-gt-sm ng-repeat="tags in hashtags">
					<label style="font-weight:normal">#hashtag</label>
					<input type="text" ng-model="tags.hash">
				</md-input-container>
			</div>
			<div class="col-md-12 no-padding formSubmit">
				<button type="submit" class="submit" ng-click="createExperiment()">Submit</button>
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function( ) {
		setTimeout(function() {
			$('.completed').css({"padding-top":"10px"});
			$('.pending').css({"padding-top":"10px"});
		}, 1500);
	});
</script>