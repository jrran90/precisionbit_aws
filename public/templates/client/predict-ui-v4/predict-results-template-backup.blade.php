
<div class="col-md-12 predict-results-wrapper" predict-result style="padding: 15px 55px;">
	

	<div class="col-md-12 summary-filters no-padding-left custom-margin-top" style="background: #FFF;padding-bottom: 10px;box-shadow: 1px 2px 1px #BBB;">
		<div class="col-md-12" style="text-align: center">
			<h2 class="summary-title" style="color: #025A8C;margin: 5px;">Summary</h2>
		</div>	
		<div class="col-md-2 no-padding filterss ">
			<span>Campaign</span>
		</div>
		<div class="col-md-2 no-padding filterss ">
			<span>Easy Pay</span>
		</div>
		<div class="col-md-2 no-padding filterss ">
			<span>Singapore</span>
		</div>
		<div class="col-md-2 no-padding filterss ">
			<span>Both</span>
		</div>
		<div class="col-md-2 no-padding filterss ">
			<span>Clicks</span>
		</div>
		<div class="col-md-2 no-padding filterss ">
			<span>#SG50, #Gameofthrone</span>
		</div>

	</div>

	<div class="col-md-12 no-padding" predict-result-graph>
		<div class="col-md-7 no-padding-left" style="padding-right:10px;">
			<div class="col-md-12 themes-wrapper" style="overflow: hidden;box-shadow: 1px 2px 1px #BBB;">
				<div class="col-md-12"><span>Theme</span></div>
				<div class="col-md-6">
					<div id="graph" style="margin-left: -40px; margin-top: 60px;"></div>
				</div>
				<div class="col-md-6 no-padding">
					<div class="col-md-9 no-padding">
						<h4 style="color: #9E9FA4;">Theme</h4>
						<table class="table custom-table">
							<tbody>
								<tr ng-repeat="list in data_pie">
									<td><span class="legend-box" style="background: {{ list.color }};"></span> <span ng-bind="list.label" style="margin-left: 10px; font-size: 10px;"></span></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-md-3 no-padding">
						<h4 style="color: #9E9FA4;">Frequency</h4>
						<table class="table custom-table">
							<tbody>
								<tr ng-repeat="list in data_pie">
									<td><span ng-bind="list.value"></span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- <div class="col-md-12 performance-wrapper">
				Performance
				<ul class="line-menu">
					<li><a class="engagement active-line-menu" href="javascript:void(0)" ng-click="lineGraph('engagement')">Engagement</a></li>
					<li><a class="impressions" href="javascript:void(0)" ng-click="lineGraph('impressions')">Impressions</a></li>
					<li><a class="likes" href="javascript:void(0)" ng-click="lineGraph('likes')">Likes</a></li>
				</ul>
				<canvas id="line" class="chart chart-line" chart-data="graph.data"
				  chart-labels="graph.labels" chart-legend="true" chart-series="graph.series" chart-options="graph.options">
				</canvas> 
			</div> -->

			<div class="col-md-12 demography-wrapper" style="overflow: hidden;box-shadow: 1px 2px 1px #BBB;">
				<div class="col-md-12">Demography</div>
				<div class="col-md-8" style="padding: 0;">
					<div id="bar-graph" style="margin-left: -50px;"></div>
					<!-- <span style="color: #A9ADB0;">Age</span> -->
				</div>
				<div class="col-md-4" style="padding: 0;">
					<div id="pie-graph" style="margin-left: -90px; margin-top: -20px; margin-left: -90px;"></div>
					<ul class="pie-graph-box">
						<li><span class="first-pie"></span> Male</li>
						<li><span class="second-pie"></span> Female</li>
					</ul>
				</div>
			</div>

			<div class="col-md-12 geo-wrapper" style="box-shadow: 1px 2px 1px #BBB;">
				<div class="map-container" map-lazy-load="https://maps.google.com/maps/api/js">
				  <ng-map center="Singapore" zoom="11"></ng-map>
				</div>
			</div>

			<!-- <div class="col-md-12 interests-wrapper">
				Interests
				<div class="row graph-line-box">
					<div class="boxes-interest">
						<div class="color-circles">
							<div class="data-circle" ng-repeat="list in circle" style="transform: scale({{list.data}}); background: {{list.color}}!important;">
								<div class="data-info">
									<label>
										{{list.title}}
										<small ng-bind="list.subtitle"></small>
									</label>
								</div>
								<md-tooltip>
		            	<span ng-bind="list.title"></span>
		          	</md-tooltip>
							</div>
						</div>
					</div>
				</div>
			</div> -->
		</div>
		<div class="col-md-5 no-padding-left">
			<div class="right-wrapper gallery-summary">
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal1">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/1.jpg" class="img-avatar">
						<img src="img/new-gallery/1.jpg" class="img-responsive">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal2">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/2.jpg" class="img-avatar">
						<img src="img/new-gallery/2.jpg" class="img-responsive">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal3">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/3.jpg" class="img-avatar">
						<img src="img/new-gallery/3.jpg" class="img-responsive">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal4">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/4.jpg" class="img-avatar">
						<img src="img/new-gallery/4.jpg" class="img-responsive">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal5">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/5.jpg" class="img-avatar">
						<img src="img/new-gallery/5.jpg" class="img-responsive">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal6">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/6.jpg" class="img-avatar">
						<img src="img/new-gallery/6.jpg" class="img-responsive">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal7">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/7.jpg" class="img-avatar">
						<img src="img/new-gallery/7.jpg" class="img-responsive">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal8">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/8.jpg" class="img-avatar">
						<img src="img/new-gallery/8.jpg" class="img-responsive">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal9">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/9.jpg" class="img-avatar">
						<img src="img/new-gallery/9.jpg" class="img-responsive">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal10">
						<div class="overlay"></div>
						<img src="img/new-gallery/User pics/10.jpg" class="img-avatar">
						<img src="img/new-gallery/10.jpg" class="img-responsive">
					</a>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- Modal -->
<div class="modal fade predict-modal" id="predict-modal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/1.jpg" class="img-avatar">
	        		<img src="img/new-gallery/1.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Female</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>25-30</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:80%;"></div>
			        		</div>
		        		<div class="value">80%</div>
		        	</div>

		        	<div class="data">
		        		<span>Neutral</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:72%;"></div>
			        		</div>
		        		<div class="value">72%</div>
		        	</div>

		        	<div class="data">
		        		<span>Caucasian</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:89%;"></div>
			        		</div>
		        		<div class="value">89%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div> -->
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div> -->
			        	</div>

			        	<!-- <div class="data">
		        			<span>Lake</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div>

			        	<div class="data">
		        			<span>Swimming Pool</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div> -->
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<div class="modal fade predict-modal" id="predict-modal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/2.jpg" class="img-avatar">
	        		<img src="img/new-gallery/2.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Male</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>25-30</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:81%;"></div>
			        		</div>
		        		<div class="value">81%</div>
		        	</div>

		        	<div class="data">
		        		<span>Neutral</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:80%;"></div>
			        		</div>
		        		<div class="value">80%</div>
		        	</div>

		        	<div class="data">
		        		<span>Caucasian</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:91%;"></div>
			        		</div>
		        		<div class="value">91%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div> -->
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>Brick Wall</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:74%;"></div>
			        		</div>
		        			<div class="value">74%</div>
			        	</div>

			        	<!-- <div class="data">
		        			<span>Lake</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div>

			        	<div class="data">
		        			<span>Swimming Pool</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div> -->
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<div class="modal fade predict-modal" id="predict-modal3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/3.jpg" class="img-avatar">
	        		<img src="img/new-gallery/3.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Female</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>25-30</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:80%;"></div>
			        		</div>
		        		<div class="value">80%</div>
		        	</div>

		        	<div class="data">
		        		<span>Joy</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:43%;"></div>
			        		</div>
		        		<div class="value">43%</div>
		        	</div>

		        	<div class="data">
		        		<span>Caucasian</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:93%;"></div>
			        		</div>
		        		<div class="value">93%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div> -->
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:74%;"></div>
			        		</div>
		        			<div class="value">74%</div> -->
			        	</div>

			        	<!-- <div class="data">
		        			<span>Lake</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div>

			        	<div class="data">
		        			<span>Swimming Pool</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div> -->
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<div class="modal fade predict-modal" id="predict-modal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/4.jpg" class="img-avatar">
	        		<img src="img/new-gallery/4.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Male</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>35-40</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:74%;"></div>
			        		</div>
		        		<div class="value">74%</div>
		        	</div>

		        	<div class="data">
		        		<span>Joy</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:80%;"></div>
			        		</div>
		        		<div class="value">80%</div>
		        	</div>

		        	<div class="data">
		        		<span>Spaniard</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:91%;"></div>
			        		</div>
		        		<div class="value">91%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>Outdoors</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div>
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>Oceanside</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:78%;"></div>
			        		</div>
		        			<div class="value">78%</div>
			        	</div>

			        	<!-- <div class="data">
		        			<span>Lake</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div>

			        	<div class="data">
		        			<span>Swimming Pool</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div> -->
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<div class="modal fade predict-modal" id="predict-modal5" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/5.jpg" class="img-avatar">
	        		<img src="img/new-gallery/5.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Male</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>25-30</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:77%;"></div>
			        		</div>
		        		<div class="value">77%</div>
		        	</div>

		        	<div class="data">
		        		<span>Neutral</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:79%;"></div>
			        		</div>
		        		<div class="value">79%</div>
		        	</div>

		        	<div class="data">
		        		<span>Caucasian</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:90%;"></div>
			        		</div>
		        		<div class="value">90%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div> -->
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:78%;"></div>
			        		</div>
		        			<div class="value">78%</div> -->
			        	</div>

			        	<!-- <div class="data">
		        			<span>Lake</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div>

			        	<div class="data">
		        			<span>Swimming Pool</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div> -->
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<div class="modal fade predict-modal" id="predict-modal6" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/6.jpg" class="img-avatar">
	        		<img src="img/new-gallery/6.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Female</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>35-40</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:72%;"></div>
			        		</div>
		        		<div class="value">72%</div>
		        	</div>

		        	<div class="data">
		        		<span>Joy</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:77%;"></div>
			        		</div>
		        		<div class="value">77%</div>
		        	</div>

		        	<div class="data">
		        		<span>Caucasian</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:81%;"></div>
			        		</div>
		        		<div class="value">81%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>Outdoors</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div>
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>Forest</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:69%;"></div>
			        		</div>
		        			<div class="value">69%</div>
			        	</div>

			        	<!-- <div class="data">
		        			<span>Lake</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div>

			        	<div class="data">
		        			<span>Swimming Pool</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        			<div class="value">75%</div>
			        	</div> -->
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<div class="modal fade predict-modal" id="predict-modal7" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/7.jpg" class="img-avatar">
	        		<img src="img/new-gallery/7.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Female</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>20-25</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:82%;"></div>
			        		</div>
		        		<div class="value">82%</div>
		        	</div>

		        	<div class="data">
		        		<span>Joy</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:56%;"></div>
			        		</div>
		        		<div class="value">56%</div>
		        	</div>

		        	<div class="data">
		        		<span>Caucasian</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:85%;"></div>
			        		</div>
		        		<div class="value">85%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div> -->
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:69%;"></div>
			        		</div>
		        			<div class="value">69%</div> -->
			        	</div>
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<div class="modal fade predict-modal" id="predict-modal8" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/8.jpg" class="img-avatar">
	        		<img src="img/new-gallery/8.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Female</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>35-40</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:89%;"></div>
			        		</div>
		        		<div class="value">89%</div>
		        	</div>

		        	<div class="data">
		        		<span>Anger</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:52%;"></div>
			        		</div>
		        		<div class="value">52%</div>
		        	</div>

		        	<div class="data">
		        		<span>Caucasian</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:83%;"></div>
			        		</div>
		        		<div class="value">83%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div> -->
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>NIL</span>
			        		<!-- <div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:69%;"></div>
			        		</div>
		        			<div class="value">69%</div> -->
			        	</div>
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<div class="modal fade predict-modal" id="predict-modal9" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/9.jpg" class="img-avatar">
	        		<img src="img/new-gallery/9.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Female</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>45-50</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:71%;"></div>
			        		</div>
		        		<div class="value">71%</div>
		        	</div>

		        	<div class="data">
		        		<span>Vigilance</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        		<div class="value">75%</div>
		        	</div>

		        	<div class="data">
		        		<span>Caucasian</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:85%;"></div>
			        		</div>
		        		<div class="value">85%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>Outdoors</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div>
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>Brick Wall</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:78%;"></div>
			        		</div>
		        			<div class="value">78%</div>
			        	</div>
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<div class="modal fade predict-modal" id="predict-modal10" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 5px;overflow: hidden;">
      <div class="modal-header" style="padding: 10px 15px 0px 15px;border: 0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
      	<div class="col-md-12" >
	        <div class="col-md-5 left" style="padding:0 15px 0 0;">
	        	<div class="col-md-12 no-padding img-wrapper">
	        		<div class="overlay"></div>
	        		<img src="img/new-gallery/User pics/10.jpg" class="img-avatar">
	        		<img src="img/new-gallery/10.jpg" class="img-responsive">
	        	</div>

	        	<div class="col-md-12 no-padding" style="padding: 0 0 0 25px !important;">
	        		<textarea class="form-control" cols="10" rows="5" placeholder="What can you say about this photo?" style="font-size: 12px;border-radius:2px !important;resize:none;padding: 4px 7px;"></textarea>	
	        	</div> 

	        	<div class="col-md-12 no-padding" style="margin:15px 0;text-align:right;">
	        		<button class="btn btn-default respond-btn" style="color:#888;">Respond</button>
	        	</div>
	        	
	        </div>
	        <div class="col-md-7 right">
	        	<div class="col-md-12 no-padding right-top">
	        		<h4>Predictions</h4>

	        		<div style="text-align:center;"><span class="person" style="margin:0 auto;font-size:20px;">Person</span></div>

		        	<div class="data">
		        		<span>Female</span>
		        		<div class="data-bar-wrapper">
		        			<div class="data-bar" style="width:100%;"></div>
		        		</div>
		        		<div class="value">100%</div>
		        	</div>

		        	<div class="data">
		        		<span>45-50</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:71%;"></div>
			        		</div>
		        		<div class="value">71%</div>
		        	</div>

		        	<div class="data">
		        		<span>Vigilance</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:75%;"></div>
			        		</div>
		        		<div class="value">75%</div>
		        	</div>

		        	<div class="data">
		        		<span>Caucasian</span>
		        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:85%;"></div>
			        		</div>
		        		<div class="value">85%</div>
		        	</div>
	        	</div>

	        	<div class="col-md-12 no-padding right-btm" style="margin-top:30px;">
	        		<div class="col-md-6 no-padding right-btm-left" style="">
	        			<h4>Theme</h4>

	        			<div class="data">
		        			<span>Outdoors</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:100%;"></div>
			        		</div>
		        			<div class="value">100%</div>
			        	</div>
	        		</div>
	        		<div class="col-md-6 no-padding right-btm-right" style="">
	        			<h4>Scene</h4>

	        			<div class="data">
		        			<span>Brick Wall</span>
			        		<div class="data-bar-wrapper">
			        			<div class="data-bar" style="width:78%;"></div>
			        		</div>
		        			<div class="value">78%</div>
			        	</div>
	        		</div>
	        		
	        	</div>

	        </div>
        </div>
      </div>
      <div class="modal-footer" style="padding: 10px 15px 20px 15px;border: 0;">
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
	$(function( ) {
			$(function(){
	    var $window = $(window),
	        $header = $('.main-header'),
	        $this   = $(this); // <-----here you can cache your selectors

	    $window.on('scroll', function(){
	       if($this.scrollTop() > 0 && $window.innerWidth() > 990 ){
	           $('.summary-filters').addClass('summary-menu');
	           $('.gallery-summary').addClass('influencers-sticky');
	           $('.summary-title').addClass('summary-menu-title');
	       }else{
	           $('.summary-filters').removeClass('summary-menu');
	           $('.gallery-summary').removeClass('influencers-sticky');
	           $('.summary-title').removeClass('summary-menu-title');
	       }
	    }).scroll();
		});
	});
</script>