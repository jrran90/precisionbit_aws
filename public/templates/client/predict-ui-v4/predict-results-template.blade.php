<div class="col-md-12 predict-result-wrapper predict-result-v2" predict-result-average style="margin-bottom: 20px">
	<div class="col-md-12 audience-insights">
		<div class="col-md-12">
			<h4>Topic Insights</h4>
		</div>

		<div class="col-md-12">
			<!-- <div class="crumbs">
				<h2>24K</h2>
				<p>People Posting</p>
			</div>
			<div class="crumbs">
				<h2>31K</h2	>
				<p>Posts Here</p>
			</div> -->
			<!-- <div class="crumbs">
				<img src="img/groundsignal/bar2.PNG" style="width: 50px">
				<p>$75 - 150K <br> Income</p>
			</div> -->
			
			<div class="crumbs" style="position: relative;width: 100px">
				<!-- <img src="img/groundsignal/bar3.PNG" style="width: 100px">
				<p>0 - 1K <br> Influence</p> -->
				<div id="influ-bar-graph" style="position: absolute;top: -200px;left: -60px;">
					<p style="font-weight: 600;font-size: 14px;position: absolute;bottom: 45px;left: 109px;">0 - 1K</p>
					<p style="font-weight: 600;font-size: 12px;position: absolute;bottom: 29px;left: 101px;"><span>Influence</span></p>
				</div>
			</div>
			<div class="crumbs" style="position: relative;overflow: visible">
				<div id="age-pie-graph" style="display: inline-block;overflow: visible">
					<span id="age-icon" style="position: absolute;margin-top: 30px;margin-left: 40px;font-size: 30px;">
						<i id="female-icon" class="fa fa-venus" style="font-weight: 600"></i>
						<i id="male-icon" class="fa fa-male" style="font-weight: 600;display: none"></i>
					</span>
				</div>
				<!-- <p><span id="gender">Female</span>&nbsp;<span id="gender-average"> 60% </span></p> -->
				<!-- <p><span id="gender">Female</span>&nbsp;<span id="gender-average"> 69.57% </span></p> -->
				<p><span id="gender">Gender </span></p>

				<!-- <ul class="pie-graph-average" style="display: inline-block;position: absolute;right: 0">
					<li><span class="first-pie"></span> Male</li><br>
					<li><span class="second-pie"></span> Female</li>
				</ul> -->
			</div>

			<!-- <div class="crumbs" style="position: relative;">
				<div id="age-pie-graph" style="display: inline-block;position: relative;">
					
				</div>
				
			</div> -->
			
			<div class="crumbs" style="position: relative;width: 100px">
				<div id="average-bar-graph" style="position: absolute;top: -200px;left: -60px;">
					<p style="font-weight: 600;font-size: 14px;position: absolute;bottom: 45px;left: 109px;">30-40</p>
					<p style="font-weight: 600;font-size: 12px;position: absolute;bottom: 29px;left: 115px;"><span>Age</span></p>
				</div>
				<!-- <p><span id="age-average"></span> <br> Age</p> -->
			</div>
			
		</div>
	</div>

	<div class="col-md-12 interest-influ">
		<div class="col-md-4 col-sm-12 col-xs-12">
			<div class="col-md-12 no-padding">
				<h4>Interests</h4>
			</div>

			<div class="col-md-12 no-padding interest-content-wrapper" style="margin-top: 20px">
				<div class="col-md-12 col-sm-4 col-xs-12 interests" style="padding-bottom: 10px">
					<div class="col-md-12 no-padding" style="margin-top: 10px">
						<h5 style="font-weight: 600;">This audience is most interested in</h5>
					</div>

					<div class="col-md-12 no-padding" style="margin-top: 10px">
						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 4.8K &nbsp; 25% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 90%"> Parenting </div>
							</div>
						</div>

						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 3.2K &nbsp; 18% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 70%"> Surfing </div>
							</div>
						</div>

						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 2.7K &nbsp; 15% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 55%"> Travel </div>
							</div>
						</div>

						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 2.4K &nbsp; 13% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 45%"> Wedding </div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-sm-4 col-xs-12 interests" style="padding-bottom: 10px">
					<div class="col-md-12 no-padding" style="margin-top: 10px">
						<h5 style="font-weight: 600;">Compared with others, this audience is more into</h5>
					</div>

					<div class="col-md-12 no-padding" style="margin-top: 10px">
						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 4.8K &nbsp; 25% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 90%"> Cooking </div>
							</div>
						</div>

						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 3.2K &nbsp; 18% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 70%"> Gardening </div>
							</div>
						</div>

						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 2.7K &nbsp; 15% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 55%"> Art </div>
							</div>
						</div>

						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 2.4K &nbsp; 13% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 45%"> Running </div>
							</div>
						</div>
					</div>
				</div>

				<div class="col-md-12 col-sm-4 col-xs-12 interests" style="padding-bottom: 10px">
					<div class="col-md-12 no-padding" style="margin-top: 10px">
						<h5 style="font-weight: 600;">They majority visit these types of venues</h5>
					</div>

					<div class="col-md-12 no-padding" style="margin-top: 10px">
						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 4.8K &nbsp; 25% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 90%"> Food </div>
							</div>
						</div>

						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 3.2K &nbsp; 18% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 70%"> Outdoors</div>
							</div>
						</div>

						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 2.7K &nbsp; 15% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 55%"> Travel </div>
							</div>
						</div>

						<div class="col-md-12 no-padding interest-info">
							<div class="col-md-4" style="padding-left: 8px">
								<div class="details">
									<div class="tri"></div>
									<p><i class="fa fa-user"></i> 2.4K &nbsp; 13% </p>
								</div>
							</div>

							<div class="col-md-8 no-padding-left">
								<div class="data" style="width: 45%"> Wedding </div>
							</div>
						</div>
					</div>
				</div>

			</div>
			
		</div>

		<div class="col-md-8 col-sm-12 col-xs-12">
			<div class="col-md-12 no-padding-right">
				<div class="col-md-12 no-padding">
					<h4>Influencers</h4>
				</div>

				<div class="col-md-12 no-padding " style="margin-top: 20px">
					<ul class="nav nav-pills">
					  <li class="active"><a href="">Top</a></li>
					  <li><a href="">Most Active</a></li>
					  <li><a href="">New</a></li>
					</ul>
				</div>

				<div class="col-md-12 no-padding influencer-content-wrapper" style="margin-top: 10px">
					<div class="col-md-4 col-sm-12 col-xs-12 influencer">
						<div class="ig-wrapper">
							<div class="ig-img" style="background: url('../img/location-gallery/1.jpg') no-repeat; background-size: 100%">
								<!-- <img src="img/location-gallery/1.jpg"> -->
							</div>
							<div class="ig-pp">
								<img src="img/location-gallery/User_Pics/1.jpg">
								<p class="ig-name">@Amstelveen</p>
							</div>
							<div class="details">
								<div class="col-md-12 stats">
									<div class="col-md-4">
										<p>Posts Here</p>
										<p>20</p>
									</div>
									<div class="col-md-4">
										<p>Followers</p>
										<p>21.5M</p>
									</div>
									<div class="col-md-4">
										<p>Avg.Engagement</p>
										<p>15K</p>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12">
										<p style="color: #333;font-weight: 600">Interests</p>
									</div>
									<div class="col-md-12">
										<div class="tags">
											Cars
										</div>
										<div class="tags">
											Food
										</div>
										<div class="tags">
											Travel
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-12 col-xs-12 influencer">
						<div class="ig-wrapper">
							<div class="ig-img" style="background: url('../img/location-gallery/3.jpg') no-repeat; background-size: 100%">
								<!-- <img src="img/location-gallery/1.jpg"> -->
							</div>
							<div class="ig-pp">
								<img src="img/location-gallery/User_Pics/3.jpg">
								<p class="ig-name">@skiptotheru</p>
							</div>
							<div class="details">
								<div class="col-md-12 stats">
									<div class="col-md-4">
										<p>Posts Here</p>
										<p>5</p>
									</div>
									<div class="col-md-4">
										<p>Followers</p>
										<p>20.2M</p>
									</div>
									<div class="col-md-4">
										<p>Avg.Engagement</p>
										<p>14K</p>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12">
										<p style="color: #333;font-weight: 600">Interests</p>
									</div>
									<div class="col-md-12">
										<div class="tags">
											Cars
										</div>
										<div class="tags">
											Food
										</div>
										<div class="tags">
											Travel
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-4 col-sm-12 col-xs-12 influencer">
						<div class="ig-wrapper">
							<div class="ig-img" style="background: url('../img/location-gallery/4.jpg') no-repeat; background-size: 100%">
								<!-- <img src="img/location-gallery/1.jpg"> -->
							</div>
							<div class="ig-pp">
								<img src="img/location-gallery/User_Pics/4.jpg">
								<p class="ig-name">@Sittard-Geleen</p>
							</div>
							<div class="details">
								<div class="col-md-12 stats">
									<div class="col-md-4">
										<p>Posts Here</p>
										<p>8</p>
									</div>
									<div class="col-md-4">
										<p>Followers</p>
										<p>20M</p>
									</div>
									<div class="col-md-4">
										<p>Avg.Engagement</p>
										<p>14K</p>
									</div>
								</div>

								<div class="col-md-12 no-padding">
									<div class="col-md-12">
										<p style="color: #333;font-weight: 600">Interests</p>
									</div>
									<div class="col-md-12">
										<div class="tags">
											Cars
										</div>
										<div class="tags">
											Food
										</div>
										<div class="tags">
											Travel
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 no-padding-right" style="margin-top: 20px">
				<div class="col-md-12 no-padding">
					<h4>Talking About</h4>
				</div>

				<div class="col-md-12 no-padding talking-content-wrapper" style="margin-top: 10px;">
					<div class="col-md-3 col-sm-12 col-xs-12 talk">
						<div class="talking">
							<div class="col-md-12">
								<h5>#Montecito</h5>
							</div>

							<div class="col-md-12 no-padding talking-content">
								<img src="img/pbit-assets-v4/1/1.jpg">
								<img src="img/pbit-assets-v4/1/2.jpg">
								<img src="img/pbit-assets-v4/1/3.jpg">
								<img src="img/pbit-assets-v4/1/4.jpg">
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-12 col-xs-12 talk">
						<div class="talking">
							<div class="col-md-12">
								<h5>#ClassicalWest</h5>
							</div>

							<div class="col-md-12 no-padding talking-content">
								<img src="img/pbit-assets-v4/2/1.jpg">
								<img src="img/pbit-assets-v4/2/2.jpg">
								<img src="img/pbit-assets-v4/2/3.jpg">
								<img src="img/pbit-assets-v4/2/4.jpg">
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-12 col-xs-12 talk">
						<div class="talking">
							<div class="col-md-12">
								<h5>#Mancostlin</h5>
							</div>

							<div class="col-md-12 no-padding talking-content">
								<img src="img/pbit-assets-v4/3/1.jpg">
								<img src="img/pbit-assets-v4/3/2.jpg">
								<img src="img/pbit-assets-v4/3/3.jpg">
								<img src="img/pbit-assets-v4/3/4.jpg">
							</div>
						</div>
					</div>

					<div class="col-md-3 col-sm-12 col-xs-12 talk">
						<div class="talking">
							<div class="col-md-12">
								<h5>#CoralCasino</h5>
							</div>

							<div class="col-md-12 no-padding talking-content">
								<img src="img/pbit-assets-v4/4/1.jpg">
								<img src="img/pbit-assets-v4/4/2.jpg">
								<img src="img/pbit-assets-v4/4/3.jpg">
								<img src="img/pbit-assets-v4/4/4.jpg">
							</div>
						</div>
					</div>

				</div>


			</div>
			

			
		</div>
	</div>
</div>