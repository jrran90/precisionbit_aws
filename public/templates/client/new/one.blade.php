
<div class="home col-md-12 no-padding custom-ui-view" home>
	<div class="col-md-12 engagement">
		<div class="col-md-12 no-padding">
			<h4 style="color:#999;"><b>Your Engagements</b></h4>
		</div>

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<span class="fa fa-comments" style="background:#D84D52;color:#fff;"></span>
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#D84D52;">209,412</p>
				<p class="desc"> Posts </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<span class="fa fa-user" style="background:#F57268;color:#fff;"></span>
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#F57268;">209,412</p>
				<p class="desc"> Users </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<span class="fa fa-file-text-o" style="background:#F1B150;color:#fff;"></span>
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#F1B150;">2,926,971</p>
				<p class="desc"> Reads </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<span class="fa fa-eye" style="background:#5395FF;color:#fff;"></span>
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#5395FF;">1,982,572</p>
				<p class="desc"> Impressions </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos">
			<div class="col-md-5 eng-icon">
				<img src="img/theme.png" style="background:#946EB7;padding: 8px;width: 50px;height: 50px;margin: 10px 0;border-radius: 50%;">
			</div>
			<div class="col-md-7 specs" >
				<p class="digits" style="color:#946EB7;">24</p>
				<p class="desc"> Themes </p>
			</div>
		</div>	

		<div class="col-md-2 col-sm-10 engagement-infos" >
			<div class="col-md-5 eng-icon">
				<img src="img/shake.png" style="background:#323D7B;padding: 8px;width: 50px;height: 50px;margin: 10px 0;border-radius: 50%;">
			</div>
			<div class="col-md-7 specs">
				<p class="digits" style="color:#323D7B;">14.3%</p>
				<p class="desc"> Engagements </p>
			</div>
		</div>	
	</div>
	<!-- <div class="col-md-12 top-content-wrapper">
		<div class="col-md-12">
			<h4 style="color:#999;"><b>Your Engagements</b></h4>
		</div>
		<div class="col-md-12" style="margin-top: 10px;">
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>209,000</p>
				<p>Posts</p>
				
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>1,203,192</p>
				<p>Users</p>
				
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>9,321</p>
				<p>Reads</p>
				
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>200,192</p>
				<p>Impressions</p>
				
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>412</p>
				<p>Themes</p>
				
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6 box-info">
				<p>45.6%</p>
				<p>Engagements</p>
				
			</div>
		</div>
	</div> -->


	<div class="col-md-12 one-bottom-wrapper" >
		<div class="col-md-6 ">
			<div class="col-md-12 one-left-wrapper">
				<div class="col-md-6">
					<p>HASHTAG TRACKING <a href="" class="button" ><i class="fa fa-plus" style="margin-right:5px;color: #5f6b64;"></i></a></p>

					<p>#Gameofthrones</p>
					<p>#hajiwoninSG</p>
					<p>#SG50</p>
					<p>#BranStarkinSG</p>
					<p>#OOTD</p>
				</div>
				<div class="col-md-6">
					<p>TOP TRENDING HASHTAGS <a href="" ><i class="fa fa-caret-up" style="margin-right:5px;color: #5f6b64;"></i></a> </p>

					<p>#SG50</p>
					<p>#Music</p>
					<p>#TBT</p>
					<p>#Cray</p>
					<p>#Singapore</p>
				</div>
			</div>
		</div>

		<div class="col-md-6 one-right">
			<div class="col-md-12 no-padding influencer-title">
				<p style="font-size:17px;color: #999;font-weight: 600;"><span>Influencers</span> <a href="" ><i class="fa fa-caret-up" style="margin-right:5px;color: #5f6b64;"></i></a> </p>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Amanda Wong.jpg') no-repeat;background-position: center; ">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Amanda Wong.jpg" class="img-avatar">
						<p>Amanda Wong</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>1,170</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>37.8%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Andrea Chong.jpg') no-repeat;background-position: center;">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Andrea Chong.jpg" class="img-avatar">
						<p>Andrea Chong</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>1,887</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>24.5%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Bella Koh.jpg') no-repeat;background-position: center;">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Bella Koh.jpg" class="img-avatar">
						<p>Bella Koh</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>2369</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>67%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Nellie Lim.jpg') no-repeat;background-position: center;">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Nellie Lim.jpg" class="img-avatar">
						<p>Nellie Lim</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>2,402</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>42.7%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>

			<div class="col-md-12 one-right-wrapper" style="margin-top: 5px;">
				<div class="col-md-12 no-padding img-wrapper" style="background: url('../img/pbit-assets-v4/Nicole Wong.jpg') no-repeat;background-position: center;">
					<div class="img-blur"></div>
					<div class="img-detail">
						<img src="img/pbit-assets-v4/Nicole Wong.jpg" class="img-avatar">
						<p>Nicole Wong</p>
					</div>
				</div>
				<div class="col-md-12 details">
					<div class="col-md-3 col-sm-12 col-xs-12 detail-info">
						<p>Followers</p>
						<p>969</p>
					</div>
					<div class="col-md-4 col-sm-12 col-xs-12 detail-info">
						<p>Engagement Score</p>
						<p>49.6%</p>
					</div>
					<div class="col-md-5 col-sm-12 col-xs-12 detail-info">
						<p>Interests</p>
						<div class="tags">Arts</div>
						<div class="tags">Technology</div>
						<div class="tags">Sports</div>
					</div>
				</div>
			</div>




		</div>
	</div>
	

</div>


<script type="text/javascript">
	$(function( ) {

		$('#trigger-menu').click(function(){
			$('.table-influ').slideDown(100);
			$('.settings-gear').addClass('rotate');
		});

		$('#trigger-menu').hover(function(){
			$('.table-influ').slideDown(100);
			$('.settings-gear').addClass('rotate');
		});

		$('body').click(function( ) {
			$('.table-influ').slideUp(100);
			$('.settings-gear').removeClass('rotate');
		});

		$(function(){
		    var $window = $(window),
		        $header = $('.main-header'),
		        $this   = $(this); // <-----here you can cache your selectors

		    $window.on('scroll', function(){
		       if($this.scrollTop() > 0 && $window.innerWidth() > 990 ){
		           $('.engagement').addClass('top-content-wrapper-fix');
		           $('.one-left-wrapper').addClass('one-left-wrapper-fix');
		           $('.one-right-wrapper').addClass('one-right-wrapper-fix');
		           $('.one-right').addClass('one-right-fix');
		           $('.influencer-title').addClass('influencer-title-fix');
		       }else{
		           $('.engagement').removeClass('top-content-wrapper-fix');
		           $('.one-left-wrapper').removeClass('one-left-wrapper-fix');
		           $('.one-right-wrapper').removeClass('one-right-wrapper-fix');
		           $('.one-right').removeClass('one-right-fix');
		           $('.influencer-title').removeClass('influencer-title-fix');
		       }
		    }).scroll();
		});
	});
</script>