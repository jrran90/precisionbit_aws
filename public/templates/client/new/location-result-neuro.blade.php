<div class="container" style="padding:57px 15px;" predict-show>
	

	<div class="all-container audience-custom-wrap">

	<div class="col-md-12 audience-insights" style="border-bottom: 1px solid #DBDBDB;">
			<div class="col-md-12">
				<h4>Topic Insights</h4>
			</div>

			<div class="col-md-12">
				<!-- <div class="crumbs">
					<h2>24K</h2>
					<p>People Posting</p>
				</div>
				<div class="crumbs">
					<h2>31K</h2	>
					<p>Posts Here</p>
				</div> -->
				<!-- <div class="crumbs">
					<img src="img/groundsignal/bar2.PNG" style="width: 50px">
					<p>$75 - 150K <br> Income</p>
				</div> -->
				
				<div class="crumbs" style="position: relative;width: 100px">
					<!-- <img src="img/groundsignal/bar3.PNG" style="width: 100px">
					<p>0 - 1K <br> Influence</p> -->
					<div id="influ-bar-graph" style="position: absolute;top: -200px;left: -60px;">
						<p style="font-weight: 600;font-size: 14px;position: absolute;bottom: 45px;left: 109px;">0 - 1K</p>
						<p style="font-weight: 600;font-size: 12px;position: absolute;bottom: 29px;left: 101px;"><span>Influence</span></p>
					</div>
				</div>
				<div class="crumbs" style="position: relative;overflow: visible">
					<div id="age-pie-graph" style="display: inline-block;overflow: visible">
						<span id="age-icon" style="position: absolute;margin-top: 30px;margin-left: 40px;font-size: 30px;">
							<i id="female-icon" class="fa fa-venus" style="font-weight: 600"></i>
							<i id="male-icon" class="fa fa-male" style="font-weight: 600;display: none"></i>
						</span>
					</div>
					<!-- <p><span id="gender">Female</span>&nbsp;<span id="gender-average"> 60% </span></p> -->
					<!-- <p><span id="gender">Female</span>&nbsp;<span id="gender-average"> 69.57% </span></p> -->
					<p><span id="gender">Gender </span></p>

					<!-- <ul class="pie-graph-average" style="display: inline-block;position: absolute;right: 0">
						<li><span class="first-pie"></span> Male</li><br>
						<li><span class="second-pie"></span> Female</li>
					</ul> -->
				</div>

				<!-- <div class="crumbs" style="position: relative;">
					<div id="age-pie-graph" style="display: inline-block;position: relative;">
						
					</div>
					
				</div> -->
				
				<div class="crumbs" style="position: relative;width: 100px">
					<div id="average-bar-graph" style="position: absolute;top: -200px;left: -60px;">
						<p style="font-weight: 600;font-size: 14px;position: absolute;bottom: 45px;left: 109px;">30-40</p>
						<p style="font-weight: 600;font-size: 12px;position: absolute;bottom: 29px;left: 115px;"><span>Age</span></p>
					</div>
					<!-- <p><span id="age-average"></span> <br> Age</p> -->
				</div>
				
			</div>
		</div>

		<div class="col-md-4 no-padding">
			<div class="wrapper-right">
				<div class="heading-row">
					<div class="col-md-8">
						<h3 style="color: #909497;margin-left: 10px;">People
							<span style="font-weight: 700;font-size: 18px;color: #333;margin-left: 15px;"><span ng-bind="user_list_num" class=""></span><span> of 69</span></span>
						</h3>

					</div>
					<div class="col-md-4 text-right">
						<p style="margin-top: 18px;margin-bottom: 20px;">
							<a href="" ng-click="users_back()" class="location-arrow-left"><i class="fa fa-chevron-left"></i></a>
							<a href="" ng-click="users_next()" class="location-arrow-left" style="margin-left: -4px;"><i class="fa fa-chevron-right"></i></a>
						</p>
						<h4 style="margin: 0px 38px 8px 0;color: #909497;">Affinity</h4>
					</div>
				</div>

				<div class="rows-content">
					<div class="table-container">
						<table class="table table-responsive table-striped table-location-result">
							<tr ng-repeat="list in user_list" ng-click="viewUser(list)">
								<td class="global-active user_{{list.profile_id}}">
									<div class="row" style="display:flex;align-items:center">
										<div class="col-md-2">
											<img ng-src="../img/audience_img/{{ list.profile_image }}" style="width:31px;border-radius:50%;">
										</div>
										<div class="col-md-6 user-side-menu-selection">
											<label>@<span ng-bind="list.username"></span></label>
											<p style="font-size:11px;">Loves sports, Entertainment</p>
										</div>
										<div class="col-md-4 user-side-menu-selection">
											<p style="font-size:12px; margin: 0;" ng-repeat="list in list.affinities track by $index">
												<span ng-bind="list"></span>
											</p>
										</div>
									</div>
								</td>
							</tr>

						</table>
					</div>
				</div>

			</div>
		</div>

		<div class="col-md-8">
			<div class="row">
				<div class="profile">
					<div class="col-md-4 profile-img">
						<img ng-src="../img/audience_img/{{ current_user.profile_image }}" class="global-profile-img" id="img-user_{{current_user.profile_id}}">
					</div>
					<div class="col-md-8">
						<div class="col-md-12 no-padding">
							<div class="info-container-location">
								<a href="{{current_user.profile_link}}" target="_blank"><h1 style="color: #8A8E97;">@<span ng-bind="current_user.username"></span></h1></a>
								<p style="color: #8A8E97;">Loves Travel, Food</p>
							</div>
						</div>
						<div class="col-md-4 location-color-page-box">
							<h4><strong ng-bind="current_user.posts"></strong></h4>
							<p><small>Posts</small></p>
						</div>
						<div class="col-md-4 location-color-page-box">
							<h4><strong ng-bind="current_user.followers"></strong></h4>
							<p><small>Followers</small></p>
						</div>
						<div class="col-md-4 location-color-page-box">
							<h4><strong ng-bind="current_user.following"></strong></h4>
							<p><small>Following</small></p>
						</div>
						<div class="col-md-6 location-color-page-box">
							<div class="row">

								<div class="col-md-4">
									<h4 style="color: #84878C;">Affinity:</h4>
								</div>
								<div class="col-md-8 badge-location">
									<p style="margin-top: 10px;" ng-repeat="list in current_user.affinities track by $index">
										<label class="label label-primary" ng-bind="list"></label>
									</p>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="row">
								<div class="col-md-12">
									<p><i style="width:10px;height:10px;background:#09D59E;display:inline-block;"></i> 85%</p>
									<p><i style="width:10px;height:10px;background:#E8CB2F;display:inline-block;"></i> 75%</p>
								</div>
							</div>
						</div>
						<br>
						<br>
						<br>

					</div>

					<div class="col-md-12" style="margin-top:65px;">

						

						<div ng-repeat="image in current_user.gallery | startFrom: pagination.page * pagination.perPage | limitTo: pagination.perPage" class="col-md-4 col-sm-4 col-xs-6 repeat-image-gallery" style="margin-bottom:40px;">
							<div class="img-bordered" style="padding:5px;padding:5px;border: 1px solid #aaa;background:#fff;">
								<div class="image-wrapper" style="background: #FFFFFF;height: 200px; display: flex;align-items: center;overflow: hidden;">
									<img src="{{image.image_url}}" width="100%" style="vertical-align: middle;">
								</div>
								
								<div class="details" style="font-size: 11px;padding: 10px 0px">
									<i class="icon-favorite"></i> {{image.likes}} likes &nbsp;&nbsp;&nbsp; <i class="icon-comment"></i> {{image.comments}} Comments
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12" style="margin-top: 10px;text-align: center;">
						<ul class="pagination">
						  <li><a href="" ng-hide="pagination.page == 0" ng-click="galleryChange();pagination.prevPage()">&laquo;</a></li>
						  <!-- <li ng-repeat="n in [] | range: pagination.numPages" ng-class="{active: n == pagination.page}">
						  <a href="" ng-click="galleryChange();pagination.toPageId(n)">{{n + 1}}</a>
						  </li> -->
						  <li><a href="">Page {{pagination.page + 1}} of {{pagination.numPages}}</a></li>
						  <li><a href="" ng-hide="pagination.page + 1 >= pagination.numPages" ng-click="galleryChange();pagination.nextPage()">&raquo;</a></li>
						</ul>
					</div>
						
				</div>

				
			</div>
		</div>
	</div>
</div>