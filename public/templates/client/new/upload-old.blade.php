<div upload-client>
	
	<div class="col-md-2" style="width: 12.666667%;">
		<div class="navigation">

			<ul class="nav">
				<!-- <div class="triangle" style="background:#000;"></div> -->
				<li><a ui-sref="home"><i class="fa fa-home"></i></a></li>
				<li><a ui-sref="predict"><i class="fa fa-flask"></i></a></li>
				<li><a ui-sref="summary"><i class="fa fa-bar-chart"></i></a></li>
			</ul>

		</div>
	</div>

	<div class="col-md-10 sub-content-wrapper" style="background:#DCDEE0;">
		<div class="images-container row" style="background:#DCDEE0;">
			<div class="uploaded-images col-md-6" >
				<div class="row">
					<div class="col-md-12" style="">
						<div class="img-preview-container" style="background:#FFFFFF;text-align:center;border-radius:10px;">
							<h2 style="background:#A6AAA9;color:#FFF;padding:10px 10px 20px 10px;border-radius:10px 10px 0 0;">Preview</h2>
							<div class="img-preview" style="padding:20px;">
								<!-- <img src="img/photo3.jpg" class="img-responsive img-thumbnail" ng-if="!has_img"> --><!-- 
								<img src="telstra_images/10.jpg" class="img-responsive img-thumbnail" id="img-default" ng-show="!images;!has_img;">

								<img src="http://52.77.17.243/get_client_personal_upload/{{selectedImg.filename}}" class="img-responsive img-thumbnail" ng-show="has_img"> -->
								<img src="{{preview}}" class="img-responsive img-thumbnail">
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-lg-12">
						<button class="btn btn-info" ng-click="downloadImg()" style="float:right;"><i class="fa fa-download"></i> Download</button>
					</div>
				</div>
			</div>
			<div class="uploaded-images col-md-6" >
				<div class="row" id="imgupload-wrapper" hidden>
					 <form enctype="multipart/form-data">
					    <div class="form-group">
					      <div id="kv-error-1" style="margin-top:10px;display:none"></div>
					      <div id="kv-success-1" class="alert alert-success fade in" style="margin-top:10px;display:none"></div>
					      <input id="input-id" type="file" multiple class="file-loading" name="files" data-preview-file-type="text">
					    </div>
					  </form>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h3 >
							Uploaded Images 
							<i class="fa fa-info-circle" style="color:#A6AAA9;" data-toggle="tooltip" data-placement="right" title="List of images recently uploaded."></i>
							<button class="" ng-click="showUploadInput()" style="float:right;font-size:13px;padding:5px 10px;box-shadow:1px 2px 5px #666;">Upload image</button>
						</h3>
						<div class="images" >
							<a href="javascript:void(0)" ng-if="images" ng-repeat="img in uploadsImg" ng-click="previewImg(img)" id="{{img}}"><img ng-src="http://52.77.17.243/get_client_personal_upload/{{img}}" class="img-responsive img-thumbnail"></a>
							<a href="javascript:void(0)" ng-click="previewImgDemo('10.jpg')" id="{{img}}"><img src="telstra_images/10.jpg" class="img-responsive img-thumbnail"></a>
							<a href="javascript:void(0)" ng-click="previewImgDemo('100.jpg')" id="{{img}}"><img src="telstra_images/100.jpg" class="img-responsive img-thumbnail"></a>
							<a href="javascript:void(0)" ng-click="previewImgDemo('101.jpg')" id="{{img}}"><img src="telstra_images/101.jpg" class="img-responsive img-thumbnail"></a>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<h3>Saved Images <i class="fa fa-info-circle" style="color:#A6AAA9;" data-toggle="tooltip" data-placement="right" title="List of uploaded and saved images."></i></h3>
						<div class="images">
							<a href="javascript:void(0)" ng-repeat="img in userImageUploads | orderBy:'+':true" ng-click="previewImg(img.filename)" style="position:relative">
								<label class="delete-wrapper" ng-click="deleteImg($event, img)">								
									<i class="glyphicon glyphicon-trash"></i>								
								</label>
								<img ng-src="http://52.77.17.243/get_client_personal_upload/{{img.filename}}" id="{{img.filename}}" class="img-responsive img-thumbnail">
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})	
</script>