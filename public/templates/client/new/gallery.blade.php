<div class="container" style="background:#fff;padding: 45px 70px;box-shadow: 1px 2px 1px #BBB;margin-bottom: 40px;">
	<div class="col-md-12">
		<div class="row">
			<div class="col-md-12">
				<div class="buttons-wrapper col-md-4 col-sm-4 col-sm-4 text-center col-md-offset-4">
					<button class="btn btn-white btn-lg">ION Orchard</button>
				</div>
				<div class="buttons-wrapper col-md-4 col-sm-4 col-sm-4 text-right">
					<button class="btn btn-white ">03/14</button>
					<button class="btn btn-white ">04/14</button>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top:50px;">
			<div class="col-md-3 col-sm-3 col-xs-12 text-center">
				<div class="heading-left" style="margin-bottom:60px;border-radius: 5px;">
					<h3 style="margin: 5px 0 0 0;">Query</h3>
				</div>
				<div class="heading-left" style="border-radius: 5px;">
					<h3 style="margin: 5px 0 0 0;">Tags</h3>
				</div>
				<ul class="list-unstyled galler-tags">
					<li>Foodie</li>
					<li>Time with others</li>
					<li>Relaxation alone</li>
					<li>Travel Adventure</li>
					<li>Retail Therapy</li>
				</ul>
			</div>
			<div class="col-md-9 col-sm-9 col-xs-12 location-gal">
				<div class="gallery-images">
					
					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/1.jpg" >
							<span>@itsfranxcesca</span>
						</div>
						<img src="img/location-gal/1.jpg">
					</div>

					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/2.jpg" >
							<span>@seanwongg</span>
						</div>
						<img src="img/location-gal/2.jpg">
					</div>

					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/3.jpg" >
							<span>@chatouphol</span>
						</div>
						<img src="img/location-gal/3.jpg">
					</div>

					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/4.jpg" >
							<span>@jhwphay</span>
						</div>
						<img src="img/location-gal/4.jpg">
					</div>

					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/5.jpg" >
							<span>@leeqiaoer</span>
						</div>
						<img src="img/location-gal/5.jpg">
					</div>

					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/6.jpg" >
							<span>@5.214.17</span>
						</div>
						<img src="img/location-gal/6.jpg">
					</div>

					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/7.jpg" >
							<span>@jane.sibmodel</span>
						</div>
						<img src="img/location-gal/7.jpg">
					</div>

					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/8.jpg" >
							<span>@omgoing</span>
						</div>
						<img src="img/location-gal/8.jpg">
					</div>

					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/9.jpg" >
							<span>@onion_photo</span>
						</div>
						<img src="img/location-gal/9.jpg">
					</div>
					
					<div class="gallery-images-content">
						<div class="detail">
							<img src="img/location-gal/User_Pics/10.jpg" >
							<span>@syahtikamuly</span>
						</div>
						<img src="img/location-gal/10.jpg">
					</div>
						
					
				</div>
			</div>
		</div>
	</div>
</div>