<div class="col-md-2 remove-side-padding" graph-plotly>
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
		<!-- <div class="panel panel-default">
			<div class="panel-heading" role="tab" id="headingOne">
				<h4 class="panel-title text-center">
			    	<a role="button" ng-click="animateAction(1)" id="link-1" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapse-1">
			      	<i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="Tooltip on left"></i>
			      	Content
			    	</a>
			  	</h4>
			</div>
			<div id="collapse-1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
			    	<ul class="list-unstyled">
			    		<li><a ng-click="isDisabled || plotReport('emotionTrigger')">Emotion Trigger</a></li>
			    		<li><a ng-click="plotReport('brandValue')">Brand Value</a></li>
			    		<li><a ng-click="plotReport('animals')">Animals</a></li>
			    		<li><a ng-click="plotReport('silentObjects')">Silent Objects</a></li>
			    	</ul>
				</div>
			</div>
		</div> -->
	  	<div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingTwo">
		      	<h4 class="panel-title text-center">
		        	<a class="collapsed" role="button" ng-click="animateAction(2)" id="link-2" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapse-2">
				  		<i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="People attributes that work best for your audience"></i>
		          		People
		        	</a>
		      </h4>
		    </div>
		    <div id="collapse-2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		      	<div class="panel-body">
			    	<ul class="list-unstyled">
			    		<!-- <li><a href="javascript:void(0)">Visibility</a></li> -->
			    		<li><a ng-click="plotReport('gender')">Gender</a></li>
			    		<li><a ng-click="plotReport('hair colour')">Hair Colour</a></li>
			    		<!-- <li><a href="javascript:void(0)">Ethnicity</a></li> -->
			    		<li><a ng-click="plotReport('age')">Age</a></li>
			    		<li><a ng-click="plotReport('clothing')">Clothing</a></li>
			    		<li><a ng-click="plotReport('activities')">Activities</a></li>
			    	</ul>
		      	</div>
		    </div>
	  	</div>
	  	<div class="panel panel-default">
		    <div class="panel-heading" role="tab" id="headingTwo">
		      	<h4 class="panel-title text-center">
		        	<a class="collapsed" role="button" ng-click="animateAction(6)" id="link-6" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapse-6">
				  		<i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="Image emotion that works best for your audience"></i>						        
		          		Emotions
		        	</a>
		      </h4>
		    </div>
		    <div id="collapse-6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
		      	<div class="panel-body">
			    	<ul class="list-unstyled">
			    		<li><a ng-click="plotReport('emotions')">Image Emotions</a></li>
			    	</ul>
		      	</div>
		    </div>
	  	</div>
	  	<div class="panel panel-default">
	    	<div class="panel-heading" role="tab" id="headingThree">
	      		<h4 class="panel-title text-center">
	        		<a class="collapsed" role="button" ng-click="animateAction(3)" id="link-3" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapse-3">
			      	<i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="Scenarios that work best for your audience"></i>
	          		Theme
	        		</a>
	      		</h4>
	    	</div>
	    	<div id="collapse-3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
	      		<div class="panel-body">
			    	<ul class="list-unstyled">
			    		<li><a ng-click="plotReport('indoor/outdoor')">Indoor/Outdoor</a></li>
			    		<li><a ng-click="plotReport('scenario')">Scenario</a></li>
			    		<!-- <li><a ng-click="plotReport('layout')">Layout</a></li> -->
			    		<li><a ng-click="plotReport('weather')">Weather</a></li>
			    		<!-- <li><a href="javascript:void(0)">Scene Type</a></li>
			    		<li><a href="javascript:void(0)">Sky Illumination</a></li> -->
			    	</ul>
	      		</div>
	    	</div>
	  	</div>
	  	<div class="panel panel-default">
	    	<div class="panel-heading" role="tab" id="headingFour">
	      		<h4 class="panel-title text-center">
	        		<a class="collapsed" role="button" ng-click="animateAction(4)" id="link-4" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapse-4">
			      	<i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="Colors that work best for your images"></i>
	          		Colors
	        		</a>
	      		</h4>
	    	</div>
	    	<div id="collapse-4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
	      		<div class="panel-body">
			    	<ul class="list-unstyled">
			    		<li><a ng-click="plotReport('dominant colour')">Dominant Color</a></li>
			    		<!-- <li><a href="javscript:void(0)">Intensity</a></li>
			    		<li><a href="javscript:void(0)">Texture</a></li>
			    		<li><a href="javscript:void(0)">Rule of Thirds</a></li>
			    		<li><a href="javscript:void(0)">Depths of Field</a></li>
			    		<li><a href="javscript:void(0)">Opposing Colors</a></li> -->
			    	</ul>
	      		</div>
	    	</div>
	  	</div>
	  	<div class="panel panel-default">
	    	<div class="panel-heading" role="tab" id="headingFive">
	      		<h4 class="panel-title text-center">
	        		<a class="collapsed" role="button" ng-click="animateAction(5)" id="link-5" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapse-5">
			      	<i class="fa fa-info-circle pull-left" data-toggle="tooltip" data-placement="top" title="Types of objects that work best for your audience"></i>
	          		Focus
	        		</a>
	      		</h4>
	    	</div>
	    	<div id="collapse-5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
	      		<div class="panel-body">
			    	<ul class="list-unstyled">
			    		<li><a ng-click="plotReport('salient object')">Salient Object</a></li>
			    	</ul>
	      		</div>
	    	</div>
	  	</div>
	</div>					
</div>



<div class="col-md-8 telstra" style="width: 70.666667%;">
	
	<div class="sub-content-wrapper" style="min-height: 670px;">
		<div class="row">
			<div class="col-lg-12">
				<button ui-sref="summary" type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin: -10px 10px 10px 0;font-size: 30px;"><span aria-hidden="true">&times;</span></button>
			</div>
		</div>
		<div class="row " style="margin-left:15px;"> 
			<div class="col-lg-9" style="width: 72.485%;padding-right:0px;padding-left: 0px;">
				<div class="breadcrumb-wrapper">
					<label>GOAL : </label>
					<ul class="breadcrumb-list">
						<li><a href="#">Acquisition</a></li>
						<li><a href="#">Convert</a></li>
						<li><a href="#">Upgrade</a></li>
						<li><a href="#">Recontract</a></li>
						<li><a href="#">Loyalty Expression</a></li>
						<li><a href="#"></a></li>
					</ul>
				</div>
			</div>
			<div class="col-lg-4" style="text-align:right;width: 24.333333%;margin-left:0px;padding-left: 0px;padding-right: 0px;">
				<div class="start form-inline">
					<label>Start</label>
					<input type='text' id="date-start" class="form-control">
				</div>
				<br>
				<div class="end form-inline">
					<label>End</label>
					<input type='text' id="date-end" class="form-control" >
				</div>
			</div>
		</div>
		<span style="position:absolute;left: 90px;z-index: 1"><h1>Telstra</h1></span>
		<div class="row" style="position: relative;clear:both">
			<div class="col-md-12">
				<!-- <div id="my-graph" style="width: 100%; height: 400px;"></div>	 -->
				<div id="avePlot" style="width: 100%; height: 400px;"></div>	
			</div>
		</div>
		<br>
		<div class="row chart-btm-box" style="padding: 0px 30px;" hidden>
			<div class="col-md-12" >
				<div class="tag-val row">
					<div class="col-md-10" style="vertical-align:middle;padding-right:0;width: 79.333333%;">
						<p style="margin-top: 5px;display:none;">On <span class="option-selected"></span>, <span class="ctr-selected"></span> resulted in an average CTR of <span class="ctr-min"></span>%, It is <span class="high-low"><span class="ctr-high" style="font-weight:bold;"></span>% <span class="ctr-status"> </span> than</span><span class="equal" hidden>equal with</span>  your average CTR of <span class="ctr-avg"></span>%.</p>
						Surprise content has been performing 50% better than average for Upgrade goal campaings
					</div>
					<div class="col-md-2" style="padding-left:0;text-align: center;width: 20.666667%;">
						<div class="tag-list" style="padding:5px 15px;border-radius:4px;margin: 0 auto;text-align: center;">
							<a class="opt-back" style="color:#000;" ><i class="fa fa-chevron-left" style="float:left;margin-top: 2px;"></i></a>
								<i class="selected-opt-num" hidden></i>
								<span class="selected-opt"></span>
							<a class="opt-next" style="color:#000;" ><i class="fa fa-chevron-right" style="float:right;margin-top: 2px;"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="ModalFirstTime">
	<div class="modal-dialog" >
		<div class="modal-content">
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<br>
					
			</div>
		</div>
	</div>
</div>


<div class="modal fade" tabindex="-1" role="dialog" id="myModal">
	<div class="modal-dialog modal-lg" style="width:800px;">
		<div class="modal-content " >
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<!-- <h4 class="modal-title" style="font-size:16px">{{ title }} {{image}}</h4> -->
				<h4 class="modal-title" style="font-size:16px">What "<span class="selected-cat"></span>" looks like:</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-12 right-col">
						<!-- <h4 > Other content with the same <span class="sel-top"></span>: </h4 > -->
						<div class="options">
							<a class="btn btn-default">Telstra</a>
							<a class="btn btn-default">Competitors</a>
							<a class="btn btn-default">Sort by</a>
						</div>
						<br>
						<ul class="list-inline text-center related-gallery">
							<!-- <li>
								<a href="" class="thumbnail"><img class="img-responsive" src="telstra_images/2.jpg" alt="" /></a>
								<div class="bot">
									<div><span class="ctr">0.7%</span></div>
									<i class="fa fa-long-arrow-down" style=""></i>
									<i class="fa avg-arrow" style=""></i>
									<div class="ctr-bar"></div>
									<div><span class="avg">0.4% (Avg)</span></div>
									<br>
									<span class="low" style="float:left;"><b>low</b></span>
									<span class="high" style="float:right;"><b>high</b></span>
								</div>
							</li> -->
						</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script type="text/javascript">
	
	$(function(){
		// $('#myModal').modal('show');

		$('#telstra-btn').click(function(){
			$('.summ').css('z-index',0);
			$('.competitors').css('display','none');
			$('.telstra').css('z-index',99);
		});

		$('#hide-summary').click(function(){
			$('.isFirstTime').fadeOut();
		});

		$( "#date-start" ).datepicker();
		$( "#date-end" ).datepicker();
		$('[data-toggle="tooltip"]').tooltip();

		$('.prev-img-info').hover(
  			function(){
  				$('.details-info').show();
  			},
  			function(){
  				$('.details-info').hide();
  			}
  		);
	})
</script>