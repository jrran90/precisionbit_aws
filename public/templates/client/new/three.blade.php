<div class="col-md-10 summ" style="z-index:99;">
	<div class="container-fluid sub-content-wrapper" style="padding-top:0px;">
		<div class="col-md-12 head-title ">
			<span>Themes You Are Tracking</span>

			<!-- <div class="btn-group" style="float:right;">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="background: #28A6A0;color: #FFF;">
			    Filter <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
			    <li><a href="#">#Family</a></li>
			    <li><a href="#">#Gym</a></li>
			    <li><a href="#">#Yoga</a></li>
			    <li><a href="#">#Healthy</a></li>
			    <li><a href="#">#Running</a></li>
			  </ul>
			</div> -->
		</div>	

		<div class="col-md-6">
			<div class="hash-container">
				<div class="col-md-12 hash-title">
					<span>#Sports Car Photoshoot, #Sports, #Entertainment</span>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="btn-group" style="margin-left:15px;">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
				    <span class="filt-name1">Rugby Action Shots</span> <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="" ng-click="filterOne('sport')">Rugby Action Shots</a></li>
				    <li><a href="" ng-click="filterOne('music')">Acoustic Guitar Performance</a></li>
				    <li><a href="" ng-click="filterOne('car')">Sports Car Photoshoot</a></li>
				  </ul>
				</div>
				<div class="hash-images car" hidden>

					<a href="" ><img src="img/car/s1.png" class="img-responsive"></a>
					<a href="" ><img src="img/car/s2.png" class="img-responsive"></a>
					<a href="" ><img src="img/car/s3.png" class="img-responsive"></a>
					<a href="" ><img src="img/car/s4.png" class="img-responsive"></a>
					<a href="" ><img src="img/car/s5.png" class="img-responsive"></a>
					<a href="" ><img src="img/car/s6.png" class="img-responsive"></a>
				</div>
				<span></span>
				<div class="hash-images phone" hidden>
					<a href="" ><img src="img/acoustic/ac2.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/ac3.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/ac4.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/guitar1.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/ac5.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/ac6.png" class="img-responsive"></a>
				</div>
 
				<div class="hash-images music" hidden>

					<a href="" ><img src="img/acoustic/ac2.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/ac3.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/ac4.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/guitar1.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/ac5.png" class="img-responsive"></a>
					<a href="" ><img src="img/acoustic/ac6.png" class="img-responsive"></a>
				</div>

				<div class="hash-images sports">
					<a href="" ><img src="img/rugby/rugby4.png" class="img-responsive"></a>
					<a href="" ><img src="img/rugby/rugby5.png" class="img-responsive"></a>
					<a href="" ><img src="img/rugby/rugby6.png" class="img-responsive"></a>
					<a href="" ><img src="img/rugby/rugby1.png" class="img-responsive"></a>
					<a href="" ><img src="img/rugby/rugby2.png" class="img-responsive"></a>
					<a href="" ><img src="img/rugby/rugby3.png" class="img-responsive"></a>
				</div>
			</div>
		</div>

		<div class="col-md-6 second-row">
			<div class="hash-container">
				<div class="col-md-12 hash-title">
					<span>#Health, #Food, #Summer</span>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				</div>
				<div class="btn-group" style="margin-left:15px;">
				  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
				    <span class="filt-name2">Outdoor Yoga Poses</span> <span class="caret"></span>
				  </button>
				  <ul class="dropdown-menu">
				    <li><a href="" ng-click="filterTwo('yoga')">Outdoor Yoga Poses</a></li>
				    <li><a href="" ng-click="filterTwo('meals')">Plated Meals</a></li>
				    <li><a href="" ng-click="filterTwo('summer')">Weekend Relaxation</a></li>
				  </ul>
				</div>

				<div class="hash-images yoga">
					<a href="" ><img src="img/Yoga/yoga1.png" class="img-responsive"></a>
					<a href="" ><img src="img/Yoga/yoga2.png" class="img-responsive"></a>
					<a href="" ><img src="img/Yoga/yoga3.png" class="img-responsive"></a>
					<a href="" ><img src="img/Yoga/yoga4.png" class="img-responsive"></a>
					<a href="" ><img src="img/Yoga/yoga5.png" class="img-responsive"></a>
					<a href="" ><img src="img/Yoga/yoga6.png" class="img-responsive"></a>
				</div>

				<div class="hash-images meals" hidden>
					<a href="" ><img src="img/plated meals/plated1.png" class="img-responsive"></a>
					<a href="" ><img src="img/plated meals/plate2.png" class="img-responsive"></a>
					<a href="" ><img src="img/plated meals/plated3.png" class="img-responsive"></a>
					<a href="" ><img src="img/plated meals/plated4.png" class="img-responsive"></a>
					<a href="" ><img src="img/plated meals/plated5.png" class="img-responsive"></a>
					<a href="" ><img src="img/plated meals/plated6.jpg" class="img-responsive"></a>
				</div>

				<div class="hash-images weekend" hidden>
					<a href="" ><img src="img/summertime/sum1.png" class="img-responsive"></a>
					<a href="" ><img src="img/summertime/su2.png" class="img-responsive"></a>
					<a href="" ><img src="img/summertime/sum3.png" class="img-responsive"></a>
					<a href="" ><img src="img/summertime/sum4.png" class="img-responsive"></a>
					<a href="" ><img src="img/summertime/sum5.png" class="img-responsive"></a>
					<a href="" ><img src="img/summertime/sum6.png" class="img-responsive"></a>
				</div>
			</div>
		</div>

		<div class="col-md-12 bottom-report">

			<div class="row your-engagement">
				<div class="category-options">
					<img src="img/Assets/Overall Engagement V2.png" class="img-responsive">
				</div>
				<div class="col-md-6">
					<img src="img/Your Performance2.png" class="img-responsive">
				</div>
				<div class="col-md-6">
					<img src="img/VS Competitors2.png" class="img-responsive">
				</div>
			</div>
			<div class="row your-top-post">
				<div class="col-md-7">
					<h4>Your Top Posts</h4>
					<div class="left">
						<!-- <table class="table table-responsive" data-toggle="tooltip" data-placement="top" title="Yoga Poses"> -->
						<table class="table table-responsive table-condensed">
							<tbody style="text-align:center;">
								<tr>
									<td></td>
									<td></td>
									<td><img src="img/Assets/Clapper_icon.png" style="width:30px;height:30px;"></td>
									<td><img src="img/Assets/Likes.png" style="width:30px;height:30px;"></td>
									<td><img src="img/Assets/Comments.png" style="width:30px;height:30px;"></td>
									<td><img src="img/Assets/Shares.png" style="width:30px;height:30px;"></td>
									<td><img src="img/Assets/Engagement.png" style="width:30px;height:30px;"></td>
								</tr>
								<tr>
									<td><img src="img/images/1.jpg" style="width:50px;height:50px;"></td>
									<td>10-Feb-16</td>
									<td>Techonology</td>
									<td>55673</td>
									<td>1019</td>
									<td>187</td>
									<td>17.2%</td>
								</tr>
								<tr>
									<td><img src="img/images/2.jpg" style="width:50px;height:50px;"></td>
									<td>30-Dec-15</td>
									<td>Sports</td>
									<td>47193</td>
									<td>817</td>
									<td>179</td>
									<td>15.30%</td>
								</tr>
								<tr>
									<td><img src="img/images/3.jpg" style="width:50px;height:50px;"></td>
									<td>24-Jan-16</td>
									<td>Family</td>
									<td>42066</td>
									<td>729</td>
									<td>171</td>
									<td>13.80%</td>
								</tr>
								<tr>
									<td><img src="img/images/4.jpg" style="width:50px;height:50px;"></td>
									<td>04-Feb-16</td>
									<td>Music</td>
									<td>39102</td>
									<td>659</td>
									<td>164</td>
									<td>13.20%</td>
								</tr>
								<tr>
									<td><img src="img/images/5.jpg" style="width:50px;height:50px;"></td>
									<td>17-Dec-15</td>
									<td>Lifestyle</td>
									<td>33751</td>
									<td>612</td>
									<td>149</td>
									<td>11.00%</td>
								</tr>
								<tr>
									<td><img src="img/images/6.jpeg" style="width:50px;height:50px;"></td>
									<td>13-Jan-16</td>
									<td>Sports</td>
									<td>27052</td>
									<td>498</td>
									<td>134</td>
									<td>10.40%</td>
								</tr>
								<tr>
									<td><img src="img/images/7.jpeg" style="width:50px;height:50px;"></td>
									<td>01-Jan-16</td>
									<td>Music</td>
									<td>19712</td>
									<td>342</td>
									<td>121</td>
									<td>9.70%</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="col-md-5">
					<h4>Your Best Performing Themes</h4>
					<div class="right">
						<table class="table table-responsive">
							<tbody style="text-align:center;">
								<tr>
									<td><img src="img/Assets/topic.png" style="width:30px;height:30px;"></td>
									<td><img src="img/Assets/Likes.png" style="width:30px;height:30px;"></td>
									<td><img src="img/Assets/Comments.png" style="width:30px;height:30px;"></td>
									<td><img src="img/Assets/Engagement.png" style="width:30px;height:30px;"></td>
								</tr>
								<tr>
									<td>Cell Phone Photography</td>
									<td>116012</td>
									<td>1713</td>
									<td>99.3</td>
								</tr>
								<tr>
									<td>Rugby Action Shots</td>
									<td>105876</td>
									<td>1490</td>
									<td>78.5</td>
								</tr>
								<tr>
									<td>Acoustic Guitar Performance</td>
									<td>91095</td>
									<td>1268</td>
									<td>66.3</td>
								</tr>
								<tr>
									<td>Outdoor Yoga Poses</td>
									<td>89542</td>
									<td>1199</td>
									<td>64.9</td>
								</tr>
								<tr>
									<td>Plated Meals</td>
									<td>77064</td>
									<td>1005</td>
									<td>43.3</td>
								</tr>
								<tr>
									<td>Weekend Relaxation</td>
									<td>62369</td>
									<td>968</td>
									<td>33.1</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
		</div>

	</div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalPrev">
	<div class="modal-dialog" >
		<div class="modal-content" style="border-radius: 10px;">
			<div class="modal-header" style="background:#7F7F7F;text-align:center;border-radius:5px 5px 0 0;color:#FFF;">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <h3 class="modal-title" id="myModalLabel">#Family</h3>
		    </div>
			<div class="modal-body">
				<img src="img/10.jpg" class="img-responsive" style="padding:10px;background:#FAFAFA;border:1px solid #DBDBDB;border-radius:4px;box-shadow:1px 1px 3px #888;">	
			</div>
		</div>
	</div>
</div>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">

<script type="text/javascript">
	$(function () {
	  $('[data-toggle="tooltip"]').tooltip();
	})
</script>