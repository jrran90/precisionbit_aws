<div class="col-md-12 clarifai-custom" style="overflow: hidden; margin-bottom:20px; padding-bottom:20px;background: #FFF">

  <div class="col-md-12 text-right">
    <div class="white-space-10"></div>
    <form class="form-inline">
      <div class="form-group">
        <label>Search : &nbsp; </label>
        <input type="text" class="form-control"  ng-model="clarifiaSearch">
      </div>
      &nbsp;&nbsp;
      <div class="form-group">
        <label>Filter Tags : &nbsp;</label>
        <select class="form-control" ng-model="clarifiaFilter">
          <option value="">All</option>
          <option ng-repeat="tags in clarifaiTags">{{ tags }}</option>
        </select>
      </div>
    </form>
    <div class="white-space-10"></div>
  </div>

  <div class="col-md-12">
    <div class="white-space-20"></div>
    <table class="table table-striped table-responsive">
      <thead>
        <tr>
          <th class="col-md-3">Username</th>
          <th class="col-md-3">Gender</th>
          <th class="col-md-6">Images</th>
        </tr>
      </thead>

      <tbody>
        <tr ng-repeat="user in clarifaiData | filter:clarifiaSearch" >
          <td>{{user.username}}</td>
          <td>{{user.gender}}</td>
          <td>
            <img ng-repeat="image in user.images | filter: clarifiaFilter" src="{{image.img}}" class="img-40p img-inline">
          </td>
        </tr>
      </tbody>
    </table>

    
  </div>
    
</div>


