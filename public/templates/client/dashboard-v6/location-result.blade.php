<div class="col-md-12 capitaland" location-data style="overflow: hidden">
	<div class="col-md-3 no-padding">
		<div class="left-bar">
			<div class="locations-section">
				<h4>Locations</h4>
				<div class="one text-center">
					
					<!-- SEARCH FILTER -->
					<input id="searchFilter" ng-model="searchFilter" type="text" placeholder="Type your location" class="form-control">

					<!-- SEARCH LOCATION RESULTS -->
					<div class="search-location-result" style="height: 90px;overflow: hidden">
						<p class="text-right">Number of posts</p>
						<p ng-repeat="result in search_result" class="text-left result">
							<a href="" ng-click="get_summary()">{{result}}
								<span class="badge float-right">50</span>
							</a>
						</p>
					</div>
					
					<button id="view-more-result-btn" class="btn btn-default">View More</button>
				</div>
				<div class="two">
					<button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
					<button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
					<button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
				</div>
				
			</div>
			<div class="duration-section">
				<h4>Duration</h4>
				<div class="form-group">
					<label>From</label><br>
					<input type="date" class="form-control">
					<input type="time" class="form-control">
				</div>
				<div class="form-group">
					<label>To</label><br>
					<input type="date" class="form-control">
					<input type="time" class="form-control">
				</div>
				<div class="form-inline">
					<button class="btn btn-default">Apply</button>
					<button class="btn btn-default">Clear</button>
				</div>
			</div>
			<div class="networks-section">
				<h4>Networks</h4>
				<p class="text-right">Number of posts</p>
				<p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
				<p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
				<p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
			</div>
		</div>
	</div>

	<div class="col-md-9 no-padding">
		<div class="col-md-6 no-padding">
			<div id="map" style="min-height: 275px;width: 100%;border-right:2px solid #777;"></div>
		</div>

		<div class="col-md-6 no-padding">
			<div id="map2" style="min-height: 275px;width: 100%"></div>
		</div>
		

		<div class="summary">
			<div class="col-md-12 top">
				<div class="col-md-4" >
					<div class="name">
						<p>Location: <span>Raffles City</span></p>
					</div>
					<div class="stats">
						<div class="section">
							<p class="value ">
								<h4 class="text-center" style="font-size:35px;">97K</h4>
							</p>
							<p>Profiles Analyzed</p>
						</div>
						<div class="section">
							<div id="age-pie-graph" style="text-align: center;">
								<span id="age-icon" style="position: absolute;margin-top: 20px;margin-left: 23px;font-size: 20px;">
									<i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>
									<i id="male-icon" class="fa fa-male" style="font-weight: 600;display: none"></i>
								</span>
							</div>
							<p>Females 60%</p>
						</div>
						<div class="section" style="text-align: center;width:55px">
							<div id="average-bar-graph" style="position: absolute;top: -45px;left: 117px;">
								<p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 87px;">30-40</p>
							</div>
							<p>Age</p>
						</div>
						<div class="section" style="text-align: center;width:55px">
							<div id="influ-bar-graph" style="position: absolute;top: -45px;left: 181px;">
								<p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 90px;">0 - 1K</p>
							</div>
							<p>Influence</p>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div id="lineChart" style="position: absolute;top: -90px;left: -20px;">
					</div>
				</div>
				<div class="col-md-4" style="overflow: hidden">
					<div class="name">
						<p>Location: <span>ION Orchard</span></p>
					</div>
					<div class="stats">
						<div class="section">
							<p class="value ">
								<h4 class="text-center" style="font-size:35px;">97K</h4>
							</p>
							<p>Profiles Analyzed</p>
						</div>
						<div class="section">
							<div id="age-pie-graph2" style="text-align: center;">
								<span id="age-icon" style="position: absolute;margin-top: 20px;margin-left: 23px;font-size: 20px;">
									<i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>
									<i id="male-icon" class="fa fa-male" style="font-weight: 600;display: none"></i>
								</span>
							</div>
							<p>Females 60%</p>
						</div>
						<div class="section" style="text-align: center;width:55px">
							<div id="average-bar-graph2" style="position: absolute;top: -45px;left: 117px;">
								<p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 87px;">30-40</p>
							</div>
							<p>Age</p>
						</div>
						<div class="section" style="text-align: center;width:55px">
							<div id="influ-bar-graph2" style="position: absolute;top: -45px;left: 181px;">
								<p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 90px;">0 - 1K</p>
							</div>
							<p>Influence</p>
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-md-12 bottom">
				<div class="col-md-12 text-right">
					<div class="white-space-10"></div>

					<div class="dropdown drop-btn float-right">
					  <button class="btn btn-default dropdown-toggle" type="button" id="dropbtn">

					    <span class="tag">Foodies</span>
					    
					    <span class="caret"></span>
					  </button>
					  <ul id="dropdrop" class="dropdown-menu" >
					    <li>
					    	<div class="checkbox">
								<label>
									<input type="checkbox" checked="checked"> Foodies
								</label>
							</div>
					    </li>
					    <li>
					    	<div class="checkbox">
								<label>
									<input type="checkbox"> Family Traveller
								</label>
							</div>
					    </li>
					    <li>
					    	<div class="checkbox">
								<label>
									<input type="checkbox"> Technophiles
								</label>
							</div>
					    </li>
					    <li>
					    	<div class="checkbox">
								<label>
									<input type="checkbox"> Social Gamers
								</label>
							</div>
					    </li>
					    <li>
					    	<div class="checkbox">
								<label>
									<input type="checkbox"> Art Lovers
								</label>
							</div>
					    </li>
					  </ul>
					</div>
				</div>
				<div class="col-md-12 text-center">
					<h4>This audience is most interested in:</h4>	
				</div>	
				<div class="col-md-12 " style="position: relative;">
					<div class="tag-desc">
						<p style="margin-left: 15%">Foodies</p>
						<p style="margin-left: 30%">Foodies</p>
						<p style="margin-left: 10%">Foodies</p>
						<p style="margin-left: 23%">Foodies</p>
					</div>
					<div class="col-md-6 no-padding-right summary-left-bar" style="border-right: 1px solid #DBDBDB">
						<p>% of total</p>
						<div class="percentage"><div class="one per">70%</div>  <div class="bar" style="left: 30%"></div></div>
						<div class="percentage"><div class="two per">40%</div> <div class="bar" style="left: 60%"></div></div>
						<div class="percentage"><div class="three per">80%</div> <div class="bar" style="left: 20%"></div></div>
						<div class="percentage"><div class="four per">55%</div> <div class="bar" style="left: 45%"></div></div>
					</div>	
					<div class="col-md-6 text-right no-padding-left summary-right-bar">
						<div>% of total</div>
						<div class="percentage" style="margin-top: 10px;"><div class="bar" style="right: 70%"></div> <div class="five per">30%</div></div>
						<div class="percentage"><div class="bar" style="right: 40%"></div> <div class="six per">60%</div></div>
						<div class="percentage"><div class="bar" style="right: 80%"></div> <div class="seven per">20%</div></div>
						<div class="percentage"><div class="bar" style="right: 55%"></div> <div class="eight per">45%</div></div>
					</div>	
				</div>
					

			</div>
			<div class="graph-section">
				<div class="col-md-12 text-right" style="position: relative;background: #F1F1F1">
					<a href="" class="" style="margin-right: 40px;" ng-click="toggleDrop()"><i class="fa fa-chevron-down" style="font-size: 20px;"></i></a>
					<div class="white-space-20"></div>
				</div>
				<div class="col-md-12 section" style="position: relative;background: #FFF">
					<div class="col-md-12 text-center">

						<div id="comp-bar-graph" class=" text-center" style="">
						</div>
					</div>
					<div class="col-md-6 ">
						<table class="table table-responsive table-bordered">
							<thead class="text-center">
								<tr>
									<th colspan="3">Top 5 Interests</th>
								</tr>
							</thead>
							<tbody>
								<tr class="text-center">
									<td class="col-md-1"></td>
									<td>ION Orchard</td>
									<td>Raffles City</td>
								</tr>
								<tr>
									<td>1</td>
									<td>Fashion Lovers</td>
									<td>Beauty Mavens</td>
								</tr>
								<tr>
									<td>2</td>
									<td>Luxury Shoppers</td>
									<td>Art Lovers</td>
								</tr>
								<tr>
									<td>3</td>
									<td>Foodies</td>
									<td>Luxury Shoppers</td>
								</tr>
								<tr>
									<td>4</td>
									<td>Beauty Mavens</td>
									<td>Fashion Lovers</td>
								</tr>
								<tr>
									<td>5</td>
									<td>Art Lovers</td>
									<td>Fitness Enthusiasts</td>
								</tr>
							</tbody>
						</table>
					</div>	
					<div class="col-md-6 text-center">
						<div id="time-graph" class=" text-center" style="position: absolute;left: -70px;top:-50px;">
						</div>
					</div>
					<div class="col-md-12 text-right">
					<a href="" class="" style="margin-right: 40px;" ng-click="toggleDrop()"><i class="fa fa-chevron-down" style="font-size: 20px;"></i></a>
					<div class="white-space-20"></div>
				</div>
				</div>
			</div>	
		</div>

	</div>

	<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" map-data>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <label class="modal-title" id="myModalLabel">Pick your search category</label>
        </div>
        <div class="modal-body">
          <!-- Nav tabs -->
          <div class="row">
            <div class="col-md-12">
              <div class="modal-wrapper">
                <ul id="search-tabs" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="" data-target="#segment" aria-controls="segment" role="tab" data-toggle="tab">Segment</a></li>
              <li role="presentation"><a href="" data-target="#locations" aria-controls="locations" role="tab" data-toggle="tab" ng-click="runMap()">Locations</a></li>
              <li role="presentation"><a href="" data-target="#brands" aria-controls="brands" role="tab" data-toggle="tab">Brands</a></li>
            </ul>
            <div class="white-space-20"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="segment">
                <form>
                  <div class="form-inline">
                    <label>What Segment ? </label>
                    <input type="text" class="form-control">
                  </div>
                  
                </form>
              </div>
              <div role="tabpanel" class="capitaland2 tab-pane fade" id="locations" >
                <div class="col-md-3 no-padding">
                  <div class="left-bar">
                    <div class="locations-section">
                      <h4>Locations</h4>
                      <div class="one text-center">
                        
                        <!-- SEARCH FILTER -->
                        <input id="searchFilter" name="places" ng-model="searchFilter.location" type="text" placeholder="Type your location" class="form-control" vs-google-autocomplete ng-blur="getLocation(searchFitler.location)" >

                        <!-- SEARCH LOCATION RESULTS -->
                        <div class="search-location-result" style="height: 90px;overflow: hidden">
                          <p class="text-right">Number of posts</p>
                          <p ng-repeat="result in search_result | filter:searchFilter" class="text-left result" hidden>
                            <a href="" ng-click="get_summary()">{{result}}
                              <span class="badge float-right">50</span>
                            </a>
                          </p>
                        </div>
                        
                        <button id="view-more-result-btn" class="btn btn-default">View More</button>
                      </div>
                      <div class="two">
                        <button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                        <button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
                        <button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
                      </div>
                      
                    </div>
                    <div class="duration-section">
                      <h4>Duration</h4>
                      <div class="form-group">
                        <label>From</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>To</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-inline">
                        <button class="btn btn-default">Apply</button>
                        <button class="btn btn-default">Clear</button>
                      </div>
                    </div>
                    <div class="networks-section">
                      <h4>Networks</h4>
                      <p class="text-right">Number of posts</p>
                      <p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
                      <p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
                      <p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 no-padding" style="margin-bottom: 50px;">
                  <div id="map" style="min-height: 762px;width: 100%"></div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="brands">
                <form>
                  <div class="form-inline">
                    <label>What Brand do you want to analyze ? </label>
                    <input type="text" class="form-control">
                  </div>
                </form>
              </div>
              <div class="white-space-20"></div>
              <button id="search-explore-btn" ng-click="brand_explore();segment()" data-dismiss="modal" class="btn btn-default" style="position: absolute;bottom: 0;right: 10px">Explore</button>
            </div>
            <div class="white-space-20"></div>
          </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>
