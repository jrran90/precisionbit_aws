<div class="col-md-12 mockup-v3 content-planning-v3" style="overflow: hidden; margin-bottom:20px; padding-bottom:20px;" content-planning-graph>



  <div class="col-md-2 left-bar no-padding">
    <ul class="nav">
      <h4>Search by</h4>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav1" aria-expanded="false">Affinity <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav1">
            <li><a href="" class="icon-right">Travelers <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav2" aria-expanded="false">Locations <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav2">
            <li><a href="" class="icon-right">All Locations <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav3" aria-expanded="false">Content Themes <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav3">
            <li><a href="" class="icon-right">Air Travel <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Shopping <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav4" aria-expanded="false">Sharer Profile <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav4">
            <li><a href="" class="icon-right">Influencers <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Micro Influencers <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Joe Pub <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav5" aria-expanded="false">Gender <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav5">
            <li><a href="" class="icon-right">Male <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Female <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Both <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav6" aria-expanded="false">Age Group <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav6">
            <li><a href="" class="icon-right">10 - 20 <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">30 - 40 <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">All <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
    </ul>
  </div>

  <div class="col-md-10 content_right">
    <div class="col-md-12 no-padding">
      <div class="white-space-20"></div>

      <ul class="nav nav-inline"> 
        <li><i class="fa fa-calendar"></i>&nbsp;
        Monday 10 June - Sunday 17 June
        </li>
      </ul>  
      <ul class="nav nav-inline navbar-right nav-content" style="position: absolute;right: 10px;top: 15px;margin-right: 0;">
          <li><a href="">Year</a></li>
          <li><a href="">Quarter</a></li>
          <li><a href="">Month</a></li>
          <li><a href="">Week</a></li>
        </ul>

      <div class="white-space-10"></div>
    </div>

    <div class="col-md-12 no-padding">
      <div class="col-md-3 no-padding">
        <div class="content-section-wrapper">
          <div class="white-space-10"></div>
          <p>Last updated on 17 June <span class="pull-right text-right" style="margin-top: -8px;">12:00pm <br> <i class="fa fa-instagram" style="margin-right: 5px;"></i></span></p>

          <div class="content-img-wrapper">
            <div class="info">
              <p style="margin: 10px 0" class="text-right">
                <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 8K Posts
              </p>
            </div>
            <div class="desc">
              <h3 class="text-center">2016 Top of Bukit Timah</h3>
            </div>
            <img src="img/social/content-planning/1.png" class="img-full">
          </div>
        </div>

        <div class="content-section-wrapper">
          <div class="white-space-10"></div>
          <p>Last updated on 17 June <span class="pull-right text-right" style="margin-top: -8px;">12:00pm <br> <i class="fa fa-instagram" style="margin-right: 5px;"></i></span></p>

          <div class="content-img-wrapper">
            <div class="info">
              <p style="margin: 10px 0" class="text-right">
                <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 5K Posts
              </p>
            </div>
            <div class="desc" style="top: 35%;">
              <h3 class="text-center">Breakfast in a Jar</h3>
            </div>
            <img src="img/social/content-planning/2.png" class="img-full">
          </div>
        </div>
      </div>

      <div class="col-md-6 no-padding">
        <div class="content-section-wrapper">
          <div class="white-space-10"></div>
          <p style=" border-top:2px solid #4BB6E5">Last updated on 17 June <span class="pull-right text-right" style="margin-top: -8px;">8:00am <br> <i class="fa fa-twitter" style="margin-right: 5px;"></i></span></p>

          <div class="content-img-wrapper">
            <div class="info">
              <p style="margin: 10px 0;" class="text-right">
                <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 12K Posts
              </p>
            </div>
            <div class="desc" style="margin-top: 30px;">
              <h3 class="text-center">Snapchat flower filter</h3>
            </div>
            <img src="img/social/content-planning/3.png" class="img-full">
          </div>
        </div>

        <div class="col-md-6 no-padding">
          <div class="content-section-wrapper">
            <div class="white-space-10"></div>
            <p>Last updated on 17 June <span class="pull-right text-right" style="margin-top: -8px;">12:00am <br> <i class="fa fa-instagram" style="margin-right: 5px;"></i></span></p>

            <div class="content-img-wrapper">
              <div class="info">
                <p style="margin: 10px 0;" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 3K Posts
                </p>
              </div>
              <div class="desc">
                <h3 class="text-center">Leaving on a jet plane</h3>
              </div>
              <img src="img/social/content-planning/4.png" class="img-full">
            </div>
          </div>
        </div>

        <div class="col-md-6 no-padding">
          <div class="content-section-wrapper">
            <div class="white-space-10"></div>
            <p>Last updated on 17 June <span class="pull-right text-right" style="margin-top: -8px;">12:00am <br> <i class="fa fa-instagram" style="margin-right: 5px;"></i></span></p>

            <div class="content-img-wrapper">
              <div class="info">
                <p style="margin: 10px 0;" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 3K Posts
                </p>
              </div>
              <div class="desc">
                <h3 class="text-center">An afternoon with the dog</h3>
              </div>
              <img src="img/social/content-planning/5.png" class="img-full">
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-3 no-padding">
        <div class="content-section-wrapper">
          <div class="white-space-10"></div>
          <p>Last updated on 17 June <span class="pull-right text-right" style="margin-top: -8px;">12:00pm <br> <i class="fa fa-instagram" style="margin-right: 5px;"></i></span></p>

          <div class="content-img-wrapper">
            <div class="info">
              <p style="margin: 10px 0" class="text-right">
                <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 1K Posts
              </p>
            </div>
            <div class="desc">
              <h3 class="text-center">First day of school</h3>
            </div>
            <img src="img/social/content-planning/6.png" class="img-full">
          </div>
        </div>

        <div class="content-section-wrapper">
          <div class="white-space-10"></div>
          <p>Last updated on 17 June <span class="pull-right text-right" style="margin-top: -8px;">8:00am <br> <i class="fa fa-twitter" style="margin-right: 5px;"></i></span></p>

          <div class="content-img-wrapper">
            <div class="info">
              <p style="margin: 10px 0" class="text-right">
                <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 15K Posts
              </p>
            </div>
            <div class="desc" style="top: 35%;">
              <h3 class="text-center">5K Run in Sentosa</h3>
            </div>
            <img src="img/social/content-planning/7.png" class="img-full">
          </div>
        </div>
      </div>
    </div>
  </div>  


  <div class="col-md-10 content_right" hidden>
    <div class="header-container">
      <div class="row_links row">
        <ul class="link_list">
          <li><a href="" ng-click="next_tab(1)">Analytics</a><div class="caret-up" style="left: 45%"></div></li>
          <li><a href="" ng-click="next_tab(2)">Location</a><div class="caret-up" style="left: 45%"></div></li>
          <li><a href="" ng-click="next_tab(3)">Audience</a><div class="caret-up" style="left: 45%"></div></li>
        </ul>
      </div>
    </div>

    <div class="temporary" style="width: 100%;min-height: 700px;">
      
    </div>
    
    <div class="row_contents not-audience-section" hidden>
      <div class="row">
        <div class="col-md-6">
          <div class="panel-box">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <h4>Audience Insight</h4>
                </div>
              </div>

              <div class="col-md-12 panel_data">
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center ">
                  <h4 class="text-center" style="font-size:40px;">97K</h4>
                  <p>People Posting</p>
                </div>
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                  <div id="age-pie-graph" style="text-align: center;">
                    <span id="age-icon" style="position: absolute;margin-top: 20px;margin-left: 23px;font-size: 20px;">
                      <i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>
                      <i id="male-icon" class="fa fa-male" style="font-weight: 600;display: none"></i>
                    </span>
                  </div>
                  <p>Female <span>60%</span></p>
                </div>
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                  <div id="average-bar-graph" style="position: absolute;top: -100px;left: -45px;">
                    <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 93px;">20-30</p>
                    <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 94px;">Age</p>
                  </div>
                  
                </div>
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                  <div id="influ-bar-graph" style="position: absolute;top: -100px;left: -55px;">
                    <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 95px;">0 - 1K</p>
                    <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 80px;">Influence</p>
                  </div>
                  <p></p>
                </div>
              </div>
            </div>
          </div>

          <div class="row ">
            <div class="col-md-6 analytics-section" style="padding-right: 5px">
              <div class="panel-box">
                <div class="row">
                  <!-- Locations -->
                  <div class="col-md-12 panel-content">
                    <div class="panel_title" style="padding: 10px 5px;">
                      <h4>Locations <small class="pull-right">Posts</small></h4>
                    </div>
                    <div class="panel_data" style="margin-bottom: 1px;">
                      <div class="data_row">
                        <div class="icon-marker">
                          <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="location-name">
                          <span>Changi Aiport</span>
                        </div>
                        <div class="counter">
                          80K
                        </div>
                      </div>
                      <div class="data_row">
                        <div class="icon-marker">
                          <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="location-name">
                          <span>Marina Bay Sands</span>
                        </div>
                        <div class="counter">
                          7K
                        </div>
                      </div>
                      <div class="data_row">
                        <div class="icon-marker">
                          <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="location-name">
                          <span>Sentosa</span>
                        </div>
                        <div class="counter">
                          2K
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6 analytics-section" style="padding-left: 5px">
              <div class="panel-box">
                <div class="row">
                  <!-- Keyword Cloud -->
                  <div class="col-md-12 panel-content">
                    <div class="panel_title" style="padding: 10px 5px;">
                      <h4>Top Moments</h4>
                    </div>
                    <div class="panel_data" style="padding-bottom: 11px;">
                      <div class="tab-content">
                        <div class="tab">
                          <div class="row_object">
                            <div class="object_name">
                              <label>Passport</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                    
                                  </div>
                                  <span class="">42%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Ticket</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 31%">
                                    
                                  </div>
                                  <span class="">31%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Luggage</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                    
                                  </div>
                                  <span class="">15%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Aircraft</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                    
                                  </div>
                                  <span class="">10%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Gathering</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                                    
                                  </div>
                                  <span class="">7%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Smiling</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 3%">
                                    
                                  </div>
                                  <span class="">3%</span>
                              </div>
                            </div>  
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6 location-section" style="padding-right: 5px" hidden>
              <div class="panel-box">
                <div class="row">
                  <!-- Keyword Cloud -->
                  <div class="col-md-12 panel-content">
                    <div class="panel_title" style="border-bottom: none">
                      <ul class="nav nav-tabs" role="tablist" style="border: none;">
                        <li role="presentation" class="active">
                          <a href="" data-target="#key-cloud" aria-controls="key-cloud" role="tab" data-toggle="tab" style="padding:10px 8px">Keyword Cloud</a>
                        </li>
                        <li role="presentation">
                          <a href="" data-target="#top-key" aria-controls="top-key" role="tab" data-toggle="tab" style="padding:10px 8px">Top Keywords</a>
                        </li>
                      </ul>
                    </div>
                    <div class="panel_data">
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="key-cloud" >
                          <div class="tags-wrapper2" style="border: 2px solid #EEE"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="top-key" style="margin-bottom: 3px;">
                          <div class="row_object">
                            <div class="object_name">
                              <label>Changi Airport</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                    
                                  </div>
                                  <span class="">42%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#airplane</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 31%">
                                    
                                  </div>
                                  <span class="">31%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#finally</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                    
                                  </div>
                                  <span class="">15%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#travel</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                    
                                  </div>
                                  <span class="">10%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#Goals</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                                    
                                  </div>
                                  <span class="">7%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#summer</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 3%">
                                    
                                  </div>
                                  <span class="">3%</span>
                              </div>
                            </div>  
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6 location-section" style="padding-left: 5px" hidden>
              <div class="panel-box">
                <div class="row">
                  <!-- Keyword Cloud -->
                  <div class="col-md-12 panel-content">
                    <div class="panel_title" style="border-bottom: none">
                      <ul class="nav nav-tabs" role="tablist" style="border: none;">
                        <li role="presentation" class="active">
                          <a href="" data-target="#obj-cloud" aria-controls="obj-cloud" role="tab" data-toggle="tab">Object Cloud</a>
                        </li>
                        <li role="presentation">
                          <a href="" data-target="#top-obj" aria-controls="top-obj" role="tab" data-toggle="tab" >Top Object</a>
                        </li>
                      </ul>
                    </div>
                    <div class="panel_data">
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="obj-cloud" >
                          <div class="tags-wrapper" style="border: 2px solid #EEE"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="top-obj">
                          <div class="row_object">
                            <div class="object_name">
                              <label>Passport</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                    
                                  </div>
                                  <span class="">42%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Ticket</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 31%">
                                    
                                  </div>
                                  <span class="">31%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Luggage</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                    
                                  </div>
                                  <span class="">15%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Aircraft</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                    
                                  </div>
                                  <span class="">10%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Gathering</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                                    
                                  </div>
                                  <span class="">7%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Smiling</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 3%">
                                    
                                  </div>
                                  <span class="">3%</span>
                              </div>
                            </div>  
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 no-padding-left">
          <div class="panel-box">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <h4>Affinity Wheel</h4>
                </div>
              </div>

              <div class="col-md-12 panel_data">
                <div id="persona-pie-graph" class="chart-custom" style="">
                  <ul class="nav persona-nav2">
                    <li><div></div>Travel Buff <h4>35%</h4></li>
                    <li><div></div>Luxury Shopper <h4>10%</h4></li>
                    <li><div></div>Soccer Fans <h4>20%</h4></li>
                    <li><div></div>Casual Gamers <h4>10%</h4></li>
                    <li><div></div>Technophile <h4>20%</h4></li>
                    
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  

      <div class="row">
        <div class="col-md-6 analytics-section ">
          <!-- Moments Gallery -->
          <div class="panel-box" style="min-height: 447px">
            <div class="panel_title">
              <h4>Moments Gallery <span class="pull-right font-12 color-gray-mute">Updated as of 13/06/2016</span></h4>
            </div>
            <div class="panel_data">
              <div class="row">
                <div class="col-md-6 collage-col">
                  <a href="">
                    <div class="imgs-collage">
                      <div class="overlay-5 text-center">
                        <div class="white-space-40p"></div>
                        <h4 class="color-white">Birthday Celebration</h4>
                      </div>
                      <img src="img/predict_gallery_images/1.jpg">
                      <div class="stat"><span style="font-size:12px;line-height:18px;color: #FFF"><i class="fa fa-photo"></i> 3K</span></div>
                     
                    </div>
                  </a>
                </div>

                <div class="col-md-6 collage-col">
                  <a href="">
                    <div class="imgs-collage">
                      <div class="overlay-5 text-center">
                        <div class="white-space-40p"></div>
                        <h4 class="color-white">Birthday Celebration</h4>
                      </div>
                      <img src="img/predict_gallery_images/1.jpg">
                      <div class="stat"><span style="font-size:12px;line-height:18px;color: #FFF"><i class="fa fa-photo"></i> 3K</span></div>
                     
                    </div>
                  </a>
                </div>

                <div class="col-md-6 collage-col">
                  <a href="">
                    <div class="imgs-collage">
                      <div class="overlay-5 text-center">
                        <div class="white-space-40p"></div>
                        <h4 class="color-white">Birthday Celebration</h4>
                      </div>
                      <img src="img/predict_gallery_images/1.jpg">
                      <div class="stat"><span style="font-size:12px;line-height:18px;color: #FFF"><i class="fa fa-photo"></i> 3K</span></div>
                     
                    </div>
                  </a>
                </div>

                <div class="col-md-6 collage-col">
                  <a href="">
                    <div class="imgs-collage">
                      <div class="overlay-5 text-center">
                        <div class="white-space-40p"></div>
                        <h4 class="color-white">Birthday Celebration</h4>
                      </div>
                      <img src="img/predict_gallery_images/1.jpg">
                      <div class="stat"><span style="font-size:12px;line-height:18px;color: #FFF"><i class="fa fa-photo"></i> 3K</span></div>
                     
                    </div>
                  </a>
                </div>
                
              </div>
            </div>
          </div>  


        </div>

        <div class="col-md-6 analytics-section no-padding-left">
            
            <!-- Timeline Affinities -->
            <div class="panel-box" >
              <div class="panel_title">
                <h4>Timeline for Moments</h4>
              </div>
              <div class="panel_data" style="position: relative;min-height: 331px;">
                <div id="affinity-graph" class=" text-center" style="position: absolute;top: -90px;left: -75px">
                </div>
              </div>
            </div>    

        </div>

        <div class="col-md-12 location-section" hidden>
          <div class="panel-box" >
              <div class="panel_title">
                <h4>Image Gallery
                  <select class="pull-right" style="position: relative;font-size: 13px;top: -7px;height: 25px;">
                    <option>Filter</option>
                    <option>ION Orchard</option>
                  </select>
                </h4>

                
              </div>
              <div class="panel_data gallery" style="position: relative;min-height: 331px;">
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps1.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps2.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps3.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps4.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps5.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps6.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps7.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps8.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps9.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps10.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps11.png');">
                  </div>
                </div>
                <div class="col-md-2 col-sm-4 col-xs-12">
                  <div class="img-wrapper" style="background-image: url('../img/persona segmentation/ps12.png');">
                  </div>
                </div>
              </div>
          </div>    
        </div>            
      </div>

    </div>


    <div class="row_contents audience-section" hidden>
      <div class="row">
        <!-- Audience -->
        <div class="col-md-5">
          <div class="table-container" style="background: #F8F8F8">
            <table class="table table-responsive table-striped table-location-result">
              <thead>
                <tr>
                  <div class="col-md-3">
                    <h4>People </h4>
                  </div>
                  <div class="col-md-4">
                    <h4 style="font-size: 15px;color:#AAA">32K people</h4>
                  </div>
                  <div class="col-md-5 text-right">
                    <h4>
                    <select>
                      <option>By Location</option>
                    </select>
                    </h4>
                  </div>
                </tr>
                <div class="white-space-10"></div>
                <tr>
                  <div class="col-md-4 text-center">
                    <div class="white-space-10"></div>
                    <button class="btn-sm btn btn-info" style="padding: 2px 5px;border-radius: 2px;">Export</button>
                    <div class="white-space-10"></div>
                  </div>
                  <div class="col-md-3   text-center">
                    <div class="white-space-10"></div>
                    <p style="font-size: 12px;font-weight: 600;">Post Here</p>
                    <div class="white-space-10"></div>
                  </div>
                  <div class="col-md-2 text-center">
                    <div class="white-space-10"></div>
                    <p style="font-size: 12px;font-weight: 600;">Followers</p>
                    <div class="white-space-10"></div>
                  </div>
                  <div class="col-md-3 text-center" style="padding-right: 10px">
                    <div class="white-space-10"></div>
                    <p style="font-size: 10px;font-weight: 600;">Avg. Engagement</p>
                    <div class="white-space-10"></div>
                  </div>
                </tr>
              </thead>

              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="global-active">
                  <div class="row" style="display:flex;align-items:center">
                    <div class="col-md-4">
                      <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                      <div class="user_details">
                        <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                        <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                      </div>
                    </div>
                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">20</p>
                    </div>

                    <div class="col-md-2 text-center">
                      <p style="font-size:10px;font-weight: 600">21.5M</p>
                    </div>

                    <div class="col-md-3 text-center">
                      <p style="font-size:10px;font-weight: 600">15k</p>
                    </div>
                  </div>
                </td>
              </tr>

            </table>
          </div>
        </div>
        <div class="col-md-7 no-padding-left">
          <div class="row">
            <div class="profile">
              <div class="col-md-4 text-center">
                <img src="/img/audience_img/2.jpg" width="90%" style="border-radius:50%; margin-top: 40px;">
                <div class="white-space-20"></div>
                <button class="btn btn-success" style="background: none !important;border: 2px solid #769CB1; color: #769CB1;border-radius: 1px;"><i class="fa fa-plus"></i> Follow</button>
              </div>
              <div class="col-md-8">
                <div class="col-md-12 no-padding">
                  <div class="info-container-location">
                    <a href="#" target="_blank"><h1 style="color: #8A8E97;font-size:24px;">Christina Freeman | @<span>Sample</span></h1></a>
                    <div class="description-profile" style="font-size:14px;">
                      <p>primarily posts from <strong>New York</strong></p>
                      <p>
                        East Coast | Sagittarius | Old Soul | Southern Belle
                      </p>
                      <p>
                        Actress | Model | Muse | Lover of all things car
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-md-4 location-color-page-box">
                  <p class="color-black"><small>Followers</small></p>
                  <h4><strong>21.5M</strong></h4>
                  <br>
                  <p class="color-black"><small>Influencer Score</small></p>
                </div>
                <div class="col-md-4 location-color-page-box">
                  <p class="color-black"><small>Interested In</small></p>
                  <div class="col-md-8 badge-location">
                    <p style="margin-top: 10px;">
                      <label class="label label-primary" style="margin-top:5px;display:inline-table">Travel</label>
                      <label class="label label-primary" style="margin-top:5px;display:inline-table">Parenting</label>
                      <label class="label label-primary" style="margin-top:5px;display:inline-table">Car</label>
                      <!-- <label class="label label-primary" style="margin-top:5px;display:inline-table">Fashion</label> -->
                    </p>
                  </div>
                </div>
                <div class="col-md-4 location-color-page-box">
                  <p class="color-black"><small>Has Posted From</small></p>
                  <div class="col-md-8 badge-location">
                    <p style="margin-top: 10px;">
                      <label class="label label-primary" style="margin-top:5px;display:inline-table">New York</label>
                      <label class="label label-primary" style="margin-top:5px;display:inline-table">Los Angeles</label>
                      <label class="label label-primary" style="margin-top:5px;display:inline-table">Chicago</label>
                    </p>
                  </div>
                </div>


              </div>

              <div class="col-md-12" >
                <label>Their most recent post in this area</label>
              </div>

              <div class="col-md-5 no-padding-right" style="margin-top:25px;">         
                <div class="img-wrapper">
                  <img src="/img/audience_img/2.jpg" class="img-full">
                </div>
              </div>
              <div class="col-md-7" style="margin-top:25px;background: #FFF;padding: 10px;">         
                <article>
                  <p>
                    <i class="fa fa-map-marker"></i> 4 Seasons Santa Barbara
                  </p>
                  <p>
                    <i class="fa fa-clock-o"></i> 3 hours ago
                  </p>
                  <p>
                    <i class="fa fa-heart"></i> Julia Barreto, Sunshine Cruz, William Smith, Alexa Solis and 41 others like this
                  </p>
                  <p>
                    <i class="fa fa-comment"></i> <b>Username</b> comments go here
                  </p>
                  <p style="padding-left:20px;">
                    <strong>Bowweee</strong> &nbsp; i wish were here <br>
                  </p>
                  <p style="padding-left:20px;">
                    <strong>Samie</strong> &nbsp; Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                    tempor incididunt ut labore et dolore magna aliqua.  <br>
                  </p>
                </article>
              </div>
                
            </div>

            
          </div>
        </div>
      </div>
      
    </div>

    </div>

  </div>


</div>

<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" map-data>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <label class="modal-title" id="myModalLabel">Pick your search category</label>
        </div>
        <div class="modal-body">
          <!-- Nav tabs -->
          <div class="row">
            <div class="col-md-12">
              <div class="modal-wrapper">
                <ul id="search-tabs" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="" data-target="#segment" aria-controls="segment" role="tab" data-toggle="tab">Segment</a></li>
              <li role="presentation"><a href="" data-target="#locations" aria-controls="locations" role="tab" data-toggle="tab" ng-click="runMap()">Locations</a></li>
              <li role="presentation"><a href="" data-target="#brands" aria-controls="brands" role="tab" data-toggle="tab">Brands</a></li>
            </ul>
            <div class="white-space-20"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="segment">
                <form>
                  <div class="form-inline">
                    <label>What Segment ? </label>
                    <input type="text" class="form-control">
                  </div>
                  
                </form>
              </div>
              <div role="tabpanel" class="capitaland2 tab-pane fade" id="locations" >
                <div class="col-md-3 no-padding">
                  <div class="left-bar">
                    <div class="locations-section">
                      <h4>Locations</h4>
                      <div class="one text-center">
                        
                        <!-- SEARCH FILTER -->
                        <input id="searchFilter" name="places" ng-model="searchFilter.location" type="text" placeholder="Type your location" class="form-control" vs-google-autocomplete ng-blur="getLocation(searchFitler.location)" >

                        <!-- SEARCH LOCATION RESULTS -->
                        <div class="search-location-result" style="height: 90px;overflow: hidden">
                          <p class="text-right">Number of posts</p>
                          <p ng-repeat="result in search_result | filter:searchFilter" class="text-left result" hidden>
                            <a href="" ng-click="get_summary()">{{result}}
                              <span class="badge float-right">50</span>
                            </a>
                          </p>
                        </div>
                        
                        <button id="view-more-result-btn" class="btn btn-default">View More</button>
                      </div>
                      <div class="two">
                        <button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                        <button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
                        <button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
                      </div>
                      
                    </div>
                    <div class="duration-section">
                      <h4>Duration</h4>
                      <div class="form-group">
                        <label>From</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>To</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-inline">
                        <button class="btn btn-default">Apply</button>
                        <button class="btn btn-default">Clear</button>
                      </div>
                    </div>
                    <div class="networks-section">
                      <h4>Networks</h4>
                      <p class="text-right">Number of posts</p>
                      <p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
                      <p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
                      <p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 no-padding" style="margin-bottom: 50px;">
                  <div id="map" style="min-height: 762px;width: 100%"></div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="brands">
                <form>
                  <div class="form-inline">
                    <label>What Brand do you want to analyze ? </label>
                    <input type="text" class="form-control">
                  </div>
                </form>
              </div>
              <div class="white-space-20"></div>
              <button id="search-explore-btn" ng-click="brand_explore();segment()" data-dismiss="modal" class="btn btn-default" style="position: absolute;bottom: 0;right: 10px">Explore</button>
            </div>
            <div class="white-space-20"></div>
          </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

<div class="col-md-12 home-v2" style="overflow: hidden; margin-bottom:20px; padding-bottom:20px" home-data-v2 home hidden>

  <div class="col-md-2">
    <div class="white-space-20"></div>

    <div class="left-container text-center">
      <h4>Segments</h4>
      <ul class="nav left-nav">
        <li><a href="" ng-click="segment()">Youth</a></li>
        <li><a href="">Family</a></li>
        <li><a href="">Tourist</a></li>
        <li><a href="">Fashionista</a></li>
      </ul>
    </div>
    <div class="left-menu2" hidden>
      <div class="left-two-container text-center">
        <h4>Brand List</h4>
        <ul class="nav left-nav">
          <li><a href="">Starhub</a></li>
          <li><a href="">Singtel</a></li>
          <li><a href="">M1</a></li>
        </ul>
      </div>
      <div class="white-space-20"></div>
      <div class="left-three-container text-center">
        <h4>Compare</h4>
        <ul class="nav left-nav">
          <li><a href="">Starhub</a></li>
          <li style="background:none;border:none">&</li>
          <li><a href="">M1</a></li>
        </ul>
        <div class="text-right">
          <div class="white-space-10"></div>
          <button class="btn btn-primary btn-sm">Explore</button>
        </div>
        
      </div>
    </div>
  </div>

   <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" map-data>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <label class="modal-title" id="myModalLabel">Pick your search category</label>
        </div>
        <div class="modal-body">
          <!-- Nav tabs -->
          <div class="row">
            <div class="col-md-12">
              <div class="modal-wrapper">
                <ul id="search-tabs" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="" data-target="#segment" aria-controls="segment" role="tab" data-toggle="tab">Segment</a></li>
              <li role="presentation"><a href="" data-target="#locations" aria-controls="locations" role="tab" data-toggle="tab" ng-click="runMap()">Locations</a></li>
              <li role="presentation"><a href="" data-target="#brands" aria-controls="brands" role="tab" data-toggle="tab">Brands</a></li>
            </ul>
            <div class="white-space-20"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="segment">
                <form>
                  <div class="form-inline">
                    <label>What Segment ? </label>
                    <input type="text" class="form-control">
                  </div>
                  
                </form>
              </div>
              <div role="tabpanel" class="capitaland2 tab-pane fade" id="locations" >
                <div class="col-md-3 no-padding">
                  <div class="left-bar">
                    <div class="locations-section">
                      <h4>Locations</h4>
                      <div class="one text-center">
                        
                        <!-- SEARCH FILTER -->
                        <input id="searchFilter" name="places" ng-model="searchFilter.location" type="text" placeholder="Type your location" class="form-control" vs-google-autocomplete ng-blur="getLocation(searchFitler.location)" >

                        <!-- SEARCH LOCATION RESULTS -->
                        <div class="search-location-result" style="height: 90px;overflow: hidden">
                          <p class="text-right">Number of posts</p>
                          <p ng-repeat="result in search_result | filter:searchFilter" class="text-left result" hidden>
                            <a href="" ng-click="get_summary()">{{result}}
                              <span class="badge float-right">50</span>
                            </a>
                          </p>
                        </div>
                        
                        <button id="view-more-result-btn" class="btn btn-default">View More</button>
                      </div>
                      <div class="two">
                        <button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                        <button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
                        <button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
                      </div>
                      
                    </div>
                    <div class="duration-section">
                      <h4>Duration</h4>
                      <div class="form-group">
                        <label>From</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>To</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-inline">
                        <button class="btn btn-default">Apply</button>
                        <button class="btn btn-default">Clear</button>
                      </div>
                    </div>
                    <div class="networks-section">
                      <h4>Networks</h4>
                      <p class="text-right">Number of posts</p>
                      <p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
                      <p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
                      <p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 no-padding" style="margin-bottom: 50px;">
                  <div id="map" style="min-height: 762px;width: 100%"></div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="brands">
                <form>
                  <div class="form-inline">
                    <label>What Brand do you want to analyze ? </label>
                    <input type="text" class="form-control">
                  </div>
                </form>
              </div>
              <div class="white-space-20"></div>
              <button id="search-explore-btn" ng-click="brand_explore();segment()" data-dismiss="modal" class="btn btn-default" style="position: absolute;bottom: 0;right: 10px">Explore</button>
            </div>
            <div class="white-space-20"></div>
          </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>

</div>


<script type="text/javascript">
  $('.left-bar ul li .collapse').collapse('hide');

  $('.left-bar ul li.drop > a').click(function(){
    $(this).toggleClass('active');
  });

</script>


