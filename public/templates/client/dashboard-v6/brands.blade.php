<div class="col-md-12 mockup-v3 brands-v3" style="overflow: hidden; margin-bottom:20px; padding-bottom:20px;" brands-graph>

  <div class="col-md-2 left-bar no-padding">
    <ul class="nav">
      <h4>Filters</h4>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav1" aria-expanded="false">Brands <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav1">
            <li><a href="" class="icon-right">Starhub <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Singtel <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav2" aria-expanded="false">Duration <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav2">
            <li><a href="">From</a></li>
            <li><a href="" class="text-center" style="font-size: 14px !important">30/05/2016</a></li>

            <li style="margin-top: 10px;"><a href="">To</a></li>
            <li><a href="" class="text-center" style="font-size: 14px !important">05/06/2016</a></li>
          </ul>
      </li>
    </ul>
  </div>

  <div class="col-md-10 content_right">
    <div class="header-container">
      <div class="row_heading row"> 
        <h1>Brand Compare Report</h1>
      </div>
      <!-- <div class="row_links row">
        <ul class="link_list">
          <li class="active"><a href="" ng-click="next_tab(1)">Analytics</a></li>
          <li><a href="" ng-click="next_tab(2)">Location</a></li>
          <li><a href="" ng-click="next_tab(3)">Audience</a></li>
        </ul>
      </div> -->
    </div>

    <div class="row_contents">
      <div class="row">
        <div class="col-md-12">
          <div class="panel-box">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-2 text-center">
                <img src="img/linkedin.png" class="img-full">
                <div class="white-space-10"></div>
                <p class="font-12">Updated as of 13/06/2016</p>
              </div>
              <div class="col-md-10 panel-content">
                <div class="panel_title">
                  <h4>Audience Insight</h4>
                </div>
                <div class="panel_data">
                  <div class="data_graph_row">
                    <div class="col-md-12">
                      <div class="col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                        <div class="white-space-5"></div>
                        <h4 class="text-center" style="font-size:90px;">97K</h4>
                        <p>People Posting</p>
                      </div>
                      <div class=" col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                        <div id="age-pie-graph" style="text-align: center;">
                          <span id="age-icon" style="position: absolute;margin-top: 35px;margin-left: 47px;font-size: 40px;">
                            <i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>
                          </span>
                        </div>
                        <p>Female <span>60%</span></p>
                      </div>
                      <div class=" col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                        <div id="average-bar-graph" style="position: absolute;top: -90px;left: -45px;">
                          <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 118px;">20-30</p>
                          <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 119px;">Age</p>
                        </div>
                        
                      </div>
                      <div class=" col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                        <div id="influ-bar-graph" style="position: absolute;top: -90px;left: -55px;">
                          <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 120px;">0 - 1K</p>
                          <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 106px;">Influence</p>
                        </div>
                        <p></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="col-md-3">
                <div id="affinity-wheel-graph" class="chart-custom" style="">
                  <h4 class="text-center">Affinity Wheel</h4>
                  <ul class="nav wheel-nav">
                    <li><div></div>Travel Buff <h5>35%</h5></li>
                    <li><div></div>Luxury Shopper <h5>10%</h5></li>
                    <li><div></div>Soccer Fans <h5>20%</h5></li>
                    <li><div></div>Casual Gamers <h5>10%</h5></li>
                    <li><div></div>Technophile <h5>20%</h5></li>
                    
                  </ul>
                </div>

              </div>

              <div class="col-md-3">
                <div id="keyboard-wheel-graph" class="chart-custom" style="">
                  <h4 class="text-center">Keyboard Wheel</h4>
                  <ul class="nav wheel-nav">
                    <li><div></div>Travel Buff <h5>35%</h5></li>
                    <li><div></div>Luxury Shopper <h5>10%</h5></li>
                    <li><div></div>Soccer Fans <h5>20%</h5></li>
                    <li><div></div>Casual Gamers <h5>10%</h5></li>
                    <li><div></div>Technophile <h5>20%</h5></li>
                    
                  </ul>
                </div>

              </div>

              <div class="col-md-3">
                <div id="object-wheel-graph" class="chart-custom" style="">
                  <h4 class="text-center">Object Wheel</h4>
                  <ul class="nav wheel-nav">
                    <li><div></div>Travel Buff <h5>35%</h5></li>
                    <li><div></div>Luxury Shopper <h5>10%</h5></li>
                    <li><div></div>Soccer Fans <h5>20%</h5></li>
                    <li><div></div>Casual Gamers <h5>10%</h5></li>
                    <li><div></div>Technophile <h5>20%</h5></li>
                    
                  </ul>
                </div>

              </div>

              <div class="col-md-3">
                <div id="location-wheel-graph" class="chart-custom" style="">
                  <h4 class="text-center">Location Wheel</h4>
                  <ul class="nav wheel-nav">
                    <li><div></div>Travel Buff <h5>35%</h5></li>
                    <li><div></div>Luxury Shopper <h5>10%</h5></li>
                    <li><div></div>Soccer Fans <h5>20%</h5></li>
                    <li><div></div>Casual Gamers <h5>10%</h5></li>
                    <li><div></div>Technophile <h5>20%</h5></li>
                    
                  </ul>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-md-6">
          <div class="panel-box">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <h4>Timeline for Posts</h4>
                </div>
                <div class="panel_data" style="position: relative;min-height: 120px;">
                  <div id="affinity-graph" class=" text-center" style="position: absolute;top: -90px;left: -75px">
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel-box">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <ul class="nav nav-pills nav-justified" role="tablist">
                    <li role="presentation">
                      <a href="#keyword-cloud">Keyword Cloud</a>
                    </li>
                    <li role="presentation">
                      <a href="#top-obj">Top Object</a>
                    </li>
                  </ul>
                </div>              
                <div class="panel_data brandVbrand">
                  <div class="tab-content">
                    <div class="row">
                      <div class="col-md-6" id="keyword-cloud">
                        <div class="title-row">
                          <h4 class="text-left"><strong>Starhub</strong></h4>
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Passport</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                  
                                </div>
                                <span class="">42%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Ticket</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 31%">
                                  
                                </div>
                                <span class="">31%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Luggage</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                  
                                </div>
                                <span class="">15%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Aircraft</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                  
                                </div>
                                <span class="">10%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Gathering</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                                  
                                </div>
                                <span class="">7%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Smiling</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 3%">
                                  
                                </div>
                                <span class="">3%</span>
                            </div>
                          </div>  
                        </div>
                      </div>
                      <div class="col-md-6" id="top-obj">
                        <div class="title-row">
                          <h4 class="text-left"><strong>Singtel</strong></h4>
                        </div>  
                        <div class="row_object">
                          <div class="object_name">
                            <label>Passport</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                  
                                </div>
                                <span class="">42%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Ticket</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 31%">
                                  
                                </div>
                                <span class="">31%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Luggage</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                  
                                </div>
                                <span class="">15%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Aircraft</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                  
                                </div>
                                <span class="">10%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Gathering</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                                  
                                </div>
                                <span class="">7%</span>
                            </div>
                          </div>  
                        </div>
                        <div class="row_object">
                          <div class="object_name">
                            <label>Smiling</label>
                          </div>
                          <div class="progress_obj">
                            <div class="progress">
                              <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 3%">
                                  
                                </div>
                                <span class="">3%</span>
                            </div>
                          </div>  
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel-box">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <h4>Object Cloud</h4>
                </div>
                <div class="panel_data">
                  <div class="row">
                    <div class="col-md-6 text-random">
                      <h5>Starhub</h5>
                      <div class="tags-wrapper" style="border: 2px solid #EEE"></div>
                    </div>
                    <div class="col-md-6 text-random">
                      <h5>Singtel</h5>
                      <div class="tags-wrapper2" style="border: 2px solid #EEE"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6 no-padding-left">
          <div class="panel-box">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <h4>Affinity Wheel</h4>
                </div>
                <div class="panel_data" style="overflow: visible;">
                  <div class="col-md-6 no-padding">
                    <div id="one-pie-graph" class="chart-custom" style="">
                      <h5>Starhub</h5>
                      <ul class="nav wheel-nav">
                        <li><div></div>Travel Buff <h5>35%</h5></li>
                        <li><div></div>Luxury Shopper <h5>10%</h5></li>
                        <li><div></div>Soccer Fans <h5>20%</h5></li>
                        <li><div></div>Casual Gamers <h5>10%</h5></li>
                        <li><div></div>Technophile <h5>20%</h5></li>
                        
                      </ul>
                    </div>
                  </div>

                  <div class="col-md-6 no-padding">
                    <div id="two-pie-graph" class="chart-custom" style="">
                      <h5>Singtel</h5>
                      <ul class="nav wheel-nav">
                        <li><div></div>Travel Buff <h5>35%</h5></li>
                        <li><div></div>Luxury Shopper <h5>10%</h5></li>
                        <li><div></div>Soccer Fans <h5>20%</h5></li>
                        <li><div></div>Casual Gamers <h5>10%</h5></li>
                        <li><div></div>Technophile <h5>20%</h5></li>
                        
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel-box" style="padding-bottom: 22px;">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <h4>Audience Insight</h4>
                </div>
                <div class="panel_data">
                  <div class="col-md-12 no-padding data_graph_row">
                    <h4>Starhub</h4>
                    <div class="col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                      <div class="white-space-5"></div>
                      <h4 class="text-center" style="font-size:55px;">97K</h4>
                      <p>People Posting</p>
                    </div>
                    <div class=" col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                      <div id="age-pie-graph2" style="text-align: center;">
                        <span id="age-icon" style="position: absolute;margin-top: 22px;margin-left: 30px;font-size: 30px;">
                          <i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>
                        </span>
                      </div>
                      <p>Female <span>60%</span></p>
                    </div>
                    <div class=" col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                      <div id="average-bar-graph2" style="position: absolute;top: -90px;left: -45px;">
                        <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 100px;">20-30</p>
                        <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 101px;">Age</p>
                      </div>
                      
                    </div>
                    <div class=" col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                      <div id="influ-bar-graph2" style="position: absolute;top: -90px;left: -55px;">
                        <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 100px;">0 - 1K</p>
                        <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 85px;">Influence</p>
                      </div>
                      <p></p>
                    </div>
                  </div>

                  <div class="col-md-12 no-padding data_graph_row">
                    <h4>Singtel</h4>
                    <div class="col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                      <div class="white-space-5"></div>
                      <h4 class="text-center" style="font-size:55px;">97K</h4>
                      <p>People Posting</p>
                    </div>
                    <div class=" col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                      <div id="age-pie-graph3" style="text-align: center;">
                        <span id="age-icon" style="position: absolute;margin-top: 22px;margin-left: 30px;font-size: 30px;">
                          <i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>
                        </span>
                      </div>
                      <p>Female <span>60%</span></p>
                    </div>
                    <div class=" col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                      <div id="average-bar-graph3" style="position: absolute;top: -90px;left: -45px;">
                        <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 100px;">20-30</p>
                        <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 101px;">Age</p>
                      </div>
                      
                    </div>
                    <div class=" col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                      <div id="influ-bar-graph3" style="position: absolute;top: -90px;left: -55px;">
                        <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 100px;">0 - 1K</p>
                        <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 85px;">Influence</p>
                      </div>
                      <p></p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    

  </div>  
    

  
</div>

<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" map-data>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <label class="modal-title" id="myModalLabel">Pick your search category</label>
        </div>
        <div class="modal-body">
          <!-- Nav tabs -->
          <div class="row">
            <div class="col-md-12">
              <div class="modal-wrapper">
                <ul id="search-tabs" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="" data-target="#segment" aria-controls="segment" role="tab" data-toggle="tab">Segment</a></li>
              <li role="presentation"><a href="" data-target="#locations" aria-controls="locations" role="tab" data-toggle="tab" ng-click="runMap()">Locations</a></li>
              <li role="presentation"><a href="" data-target="#brands" aria-controls="brands" role="tab" data-toggle="tab">Brands</a></li>
            </ul>
            <div class="white-space-20"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="segment">
                <form>
                  <div class="form-inline">
                    <label>What Segment ? </label>
                    <input type="text" class="form-control">
                  </div>
                  
                </form>
              </div>
              <div role="tabpanel" class="capitaland2 tab-pane fade" id="locations" >
                <div class="col-md-3 no-padding">
                  <div class="left-bar">
                    <div class="locations-section">
                      <h4>Locations</h4>
                      <div class="one text-center">
                        
                        <!-- SEARCH FILTER -->
                        <input id="searchFilter" name="places" ng-model="searchFilter.location" type="text" placeholder="Type your location" class="form-control" vs-google-autocomplete ng-blur="getLocation(searchFitler.location)" >

                        <!-- SEARCH LOCATION RESULTS -->
                        <div class="search-location-result" style="height: 90px;overflow: hidden">
                          <p class="text-right">Number of posts</p>
                          <p ng-repeat="result in search_result | filter:searchFilter" class="text-left result" hidden>
                            <a href="" ng-click="get_summary()">{{result}}
                              <span class="badge float-right">50</span>
                            </a>
                          </p>
                        </div>
                        
                        <button id="view-more-result-btn" class="btn btn-default">View More</button>
                      </div>
                      <div class="two">
                        <button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                        <button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
                        <button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
                      </div>
                      
                    </div>
                    <div class="duration-section">
                      <h4>Duration</h4>
                      <div class="form-group">
                        <label>From</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>To</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-inline">
                        <button class="btn btn-default">Apply</button>
                        <button class="btn btn-default">Clear</button>
                      </div>
                    </div>
                    <div class="networks-section">
                      <h4>Networks</h4>
                      <p class="text-right">Number of posts</p>
                      <p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
                      <p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
                      <p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 no-padding" style="margin-bottom: 50px;">
                  <div id="map" style="min-height: 762px;width: 100%"></div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="brands">
                <form>
                  <div class="form-inline">
                    <label>What Brand do you want to analyze ? </label>
                    <input type="text" class="form-control">
                  </div>
                </form>
              </div>
              <div class="white-space-20"></div>
              <button id="search-explore-btn" ng-click="brand_explore();segment()" data-dismiss="modal" class="btn btn-default" style="position: absolute;bottom: 0;right: 10px">Explore</button>
            </div>
            <div class="white-space-20"></div>
          </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
  $('.left-bar ul li .collapse').collapse('hide');

  $('.left-bar ul li.drop > a').click(function(){
    $(this).toggleClass('active');
  });

</script>