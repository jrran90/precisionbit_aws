<div class="col-md-12 socialListening_mockup-v3" style="overflow: hidden;padding-bottom:20px;background: #FFF" social-graph>
  
  <div class="col-md-12" style="background: #ECF0F1;position: fixed;z-index: 10">
    <ul class="nav nav-inline nav-social">
      <li><a href="" ng-click="socialClick(2)">Analytics</a></li>
      <li><a href="" ng-click="socialClick(3)">Location</a></li>
      <li><a href="" ng-click="socialClick(4)">People</a></li>
    </ul>
  </div>

  <div class="col-md-12 no-padding-left no-padding-right" style="padding-top: 42px;" >
    <div id="microscope" >
      <div class="social-left-side col-md-2 text-center">
        <h3 class="text-underline">Overview</h3>
        <h3>Segment: <span>Youth</span></h3>

        <div class="col-md-12 no-padding text-left" style="position: relative;">
          <div id="age-pie-graph">
            <span id="age-icon" style="position: absolute;margin-top: 30px;margin-left: 45px;font-size: 27px;">
              <!-- <i id="female-icon" class="fa fa-user" style="font-weight: 600"></i> -->
              <img src="img/social/user.png" style="width: 15%">
            </span>
            <span style="position: absolute;margin-top: 65px;margin-left: 45px;font-size: 18px;">2K</span>
            <span id="age-icon" style="position: absolute;margin-top: 15px;right:-83px;font-size: 26px;">
              <!-- <i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>  -->
              <img src="img/social/female.png" style="width:10%">
              60%
            </span>
            <span id="age-icon" style="position: absolute;margin-top: 55px;right:-75px;font-size: 26px;">
              <!-- <i id="female-icon" class="fa fa-male" style="font-weight: 600"></i>  -->
              <img src="img/social/male.png" style="width:10%">
              40%
            </span>

          </div>
        </div>

        <div class="col-md-12" style="position: relative;min-height: 120px;">
          <div id="average-bar-graph" style="position: absolute;top: -85px;left: -75px;">
            <span style="font-weight: 600;font-size: 15px;position: absolute;top: 110px;right: 35px;">Age:</span>
            <span style="font-weight: 500;font-size: 15px;position: absolute;top: 135px;right: 18px;">20 - 30</span>
          </div>
        </div>

        <div class="col-md-12">
          <p style="font-size: 23px"><img src="img/social/picture.png" style="width: 20%; margin-right: 1px;"> : 10K</p>
          <div class="white-space-10"></div>
          <p style="font-size: 23px"><img src="img/social/tag.png" style="width: 20%; margin-right: 9px;"> : 1.5M</p>
        </div>
      </div>

      <div id="main" class="social-right-side col-md-10 " >
        <div class="col-md-12 ">
          <h3 class="border-bottom">Passion Points 
            <a href=""><img src="img/social/refresh-arrow.png" class="pull-right" style="width: 20px"></a>

          </h3>
        </div>
        <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
          <a href="" ng-click="socialClick(1)">
            <div class="social-image-container" style="background: url('img/social/1.png')">
              <div class="overlay text-center color-white">
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <h2>Family<br>22.3%</h2>
              </div>
              <div class="info">
                <p style="margin: 10px 0" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 200 Posts
                </p>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
          <a href="">
            <div class="social-image-container" style="background: url('img/social/2.png')">
              <div class="overlay text-center color-white">
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <h2>Entertainment<br>20.7%</h2>
              </div>
              <div class="info">
                <p style="margin: 10px 0" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 150 Posts
                </p>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
          <a href="">
            <div class="social-image-container" style="background: url('img/social/3.png')">
              <div class="overlay text-center color-white">
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <h2>Travel<br>20.7%</h2>
              </div>
              <div class="info">
                <p style="margin: 10px 0" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 20 Posts
                </p>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
          <a href="">
            <div class="social-image-container" style="background: url('img/social/4.png')">
              <div class="overlay text-center color-white">
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <h2>Healthy<br>Living<br>11.5%</h2>
              </div>
              <div class="info">
                <p style="margin: 10px 0" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 15 Posts
                </p>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
          <a href="">
            <div class="social-image-container" style="background: url('img/social/5.png')">
              <div class="overlay text-center color-white">
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <h2>Outdoor Hobbies <br> 6.8%</h2>
              </div>
              <div class="info">
                <p style="margin: 10px 0" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 95 Posts
                </p>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
          <a href="">
            <div class="social-image-container" style="background: url('img/social/6.png')">
              <div class="overlay text-center color-white">
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <h2>Relationships <br> 6.2%</h2>
              </div>
              <div class="info">
                <p style="margin: 10px 0" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 120 Posts
                </p>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
          <a href="">
            <div class="social-image-container" style="background: url('img/social/7.png')">
              <div class="overlay text-center color-white">
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <h2>Pets <br> 5.8%</h2>
              </div>
              <div class="info">
                <p style="margin: 10px 0" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 200 Posts
                </p>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
          <a href="">
            <div class="social-image-container" style="background: url('img/social/8.png')">
              <div class="overlay text-center color-white">
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <h2>Business <br> 6.8%</h2>
              </div>
              <div class="info">
                <p style="margin: 10px 0" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 200 Posts
                </p>
              </div>
            </div>
          </a>
        </div>

        <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
          <a href="">
            <div class="social-image-container" style="background: url('img/social/9.png')">
              <div class="overlay text-center color-white">
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <div class="white-space-20"></div>
                <h2>Recreation <br> 4.9%</h2>
              </div>
              <div class="info">
                <p style="margin: 10px 0" class="text-right">
                  <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 200 Posts
                </p>
              </div>
            </div>
          </a>
        </div>
      </div>

      <div id="family" class="social-right-side col-md-10 " hidden>
        <div class="col-md-12" style="padding-right: 40px;">
          
          <a id="show-insights" href=""><img class="arrow-right"  src="img/social/family/right-arrow.png" style="width: 35px;"></a>

          <div class="col-md-12 ">
            <h3 class="border-bottom">Explore themes based on real consumer moments. Find new things to say. 
              <a href=""><img src="img/social/refresh-arrow.png" class="pull-right" style="width: 20px"></a>

            </h3>
            <p class="color-gray"> Passion points <i class="fa fa-chevron-right"></i> <span class="text-underline">Family</span> </p>
          </div>
          <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
            
              <div class="social-image-container" style="background: url('img/social/family/1.png')">
                <div class="overlay text-center color-white">
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <h2><a href="">Watching Finding Dory</a></h2>
                </div>
                <div class="info">
                  <p style="margin: 10px 0" class="text-right">
                    <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 200 Posts
                  </p>
                </div>
                <div id="bad" class="status" hidden>
                  <p ><a href=""><img src="img/social/family/warning.png" style="width: 20px;"></a></p>
                </div>
                <div id="good" class="status">
                  <p><a href="" data-toggle="collapse" data-target="#collapse1" aria-expanded="false"><img src="img/social/family/line-chart.png" style="width: 20px;"></p>
                </div>
              </div>
              <div class="collapse" id="collapse1" style="margin: 0 10px;border-radius: 0 0 3px 3px;box-shadow: 1px 1px 1px #AAA;">
                <p class="text-center" style="background: #273041;color: #FFF;padding: 5px;margin-bottom: 0;">Who's talking about this</p>
                <div class="text-center" style="border-left: 1px solid #DBDBDB;padding: 10px;">
                  
                </div>
              </div>
            
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
            
              <div class="social-image-container" style="background: url('img/social/family/7.png')">
                <div class="overlay text-center color-white">
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <h2><a href="">Time with Grandpa</a></h2>
                </div>
                <div class="info">
                  <p style="margin: 10px 0" class="text-right">
                    <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 150 Posts
                  </p>
                </div>
                <div id="bad" class="status" hidden>
                  <p ><a href=""><img src="img/social/family/warning.png" style="width: 20px;"></a></p>
                </div>
                <div id="good" class="status" hidden>
                  <p><a href=""><img src="img/social/family/line-chart.png" style="width: 20px;"></a></p>
                </div>
              </div>
              <div class="collapse" id="collapse2" style="margin: 0 10px;border-radius: 0 0 3px 3px;box-shadow: 1px 1px 1px #AAA;">
                <p class="text-center" style="background: #273041;color: #FFF;padding: 5px;margin-bottom: 0;">Who's talking about this</p>
                <div class="text-center" style="border-left: 1px solid #DBDBDB;padding: 10px;">
                  <img src="img/social/family/m1.png" style="width: 20%">
                  <img src="img/social/family/singtel.png" style="width: 20%">
                </div>
              </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
            
              <div class="social-image-container" style="background: url('img/social/family/3.png')">
                <div class="overlay text-center color-white">
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <h2><a href="">Babies with Dogs</a></h2>
                </div>
                <div class="info">
                  <p style="margin: 10px 0" class="text-right">
                    <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 20 Posts
                  </p>
                </div>
                <div id="bad" class="status" >
                  <p ><a href="" data-toggle="collapse" data-target="#collapse3" aria-expanded="false"><img src="img/social/family/warning.png" style="width: 20px;"></a></p>
                </div>
                <div id="good" class="status" hidden>
                  <p><a href=""><img src="img/social/family/line-chart.png" style="width: 20px;"></a></p>
                </div>
              </div>
              <div class="collapse" id="collapse3" style="margin: 0 10px;border-radius: 0 0 3px 3px;box-shadow: 1px 1px 1px #AAA;">
                <p class="text-center" style="background: #273041;color: #FFF;padding: 5px;margin-bottom: 0;">Who's talking about this</p>
                <div class="text-center" style="border-left: 1px solid #DBDBDB;padding: 10px;">
                  <img src="img/social/family/m1.png" style="width: 20%">
                  <img src="img/social/family/singtel.png" style="width: 20%">
                </div>
              </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
            
              <div class="social-image-container" style="background: url('img/social/family/4.png')">
                <div class="overlay text-center color-white">
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <h2><a href="">Weekend Bike Ride at<br>East Coast Park</a></h2>
                </div>
                <div class="info">
                  <p style="margin: 10px 0" class="text-right">
                    <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 15 Posts
                  </p>
                </div>
                <div id="bad" class="status" hidden>
                  <p ><a href=""><img src="img/social/family/warning.png" style="width: 20px;"></a></p>
                </div>
                <div id="good" class="status" hidden>
                  <p><a href=""><img src="img/social/family/line-chart.png" style="width: 20px;"></a></p>
                </div>
              </div>
              <div class="collapse" id="collapse4" style="margin: 0 10px;border-radius: 0 0 3px 3px;box-shadow: 1px 1px 1px #AAA;">
                <p class="text-center" style="background: #273041;color: #FFF;padding: 5px;margin-bottom: 0;">Who's talking about this</p>
                <div class="text-center" style="border-left: 1px solid #DBDBDB;padding: 10px;">
                  <img src="img/social/family/m1.png" style="width: 20%">
                  <img src="img/social/family/singtel.png" style="width: 20%">
                </div>
              </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
            
              <div class="social-image-container" style="background: url('img/social/family/5.png')">
                <div class="overlay text-center color-white">
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <h2><a href="">Beach day with Baby</a></h2>
                </div>
                <div class="info">
                  <p style="margin: 10px 0" class="text-right">
                    <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 95 Posts
                  </p>
                </div>
                <div id="bad" class="status" >
                  <p ><a href="" data-toggle="collapse" data-target="#collapse5" aria-expanded="false"><img src="img/social/family/warning.png" style="width: 20px;"></a></p>
                </div>
                <div id="good" class="status" hidden>
                  <p><a href=""><img src="img/social/family/line-chart.png" style="width: 20px;"></a></p>
                </div>
              </div>
              <div class="collapse" id="collapse5" style="margin: 0 10px;border-radius: 0 0 3px 3px;box-shadow: 1px 1px 1px #AAA;">
                <p class="text-center" style="background: #273041;color: #FFF;padding: 5px;margin-bottom: 0;">Who's talking about this</p>
                <div class="text-center" style="border-left: 1px solid #DBDBDB;padding: 10px;">
                  <img src="img/social/family/m1.png" style="width: 20%">
                  <img src="img/social/family/singtel.png" style="width: 20%">
                </div>
              </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
            
              <div class="social-image-container" style="background: url('img/social/family/6.png')">
                <div class="overlay text-center color-white">
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <h2><a href="">Kids Wearing Iron Man</a></h2>
                </div>
                <div class="info">
                  <p style="margin: 10px 0" class="text-right">
                    <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 120 Posts
                  </p>
                </div>
                <div id="bad" class="status" >
                  <p ><a href="" data-toggle="collapse" data-target="#collapse6" aria-expanded="false"><img src="img/social/family/warning.png" style="width: 20px;"></a></p>
                </div>
                <div id="good" class="status" hidden>
                  <p><a href=""><img src="img/social/family/line-chart.png" style="width: 20px;"></a></p>
                </div>
              </div>
              <div class="collapse" id="collapse6" style="margin: 0 10px;border-radius: 0 0 3px 3px;box-shadow: 1px 1px 1px #AAA;">
                <p class="text-center" style="background: #273041;color: #FFF;padding: 5px;margin-bottom: 0;">Who's talking about this</p>
                <div class="text-center" style="border-left: 1px solid #DBDBDB;padding: 10px;">
                  <img src="img/social/family/m1.png" style="width: 20%">
                  <img src="img/social/family/singtel.png" style="width: 20%">
                </div>
              </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
            
              <div class="social-image-container" style="background: url('img/social/family/7.png')">
                <div class="overlay text-center color-white">
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <h2><a href="">Time with Grandpa</a></h2>
                </div>
                <div class="info">
                  <p style="margin: 10px 0" class="text-right">
                    <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 200 Posts
                  </p>
                </div>
                <div id="bad" class="status" hidden>
                  <p ><a href=""><img src="img/social/family/warning.png" style="width: 20px;"></a></p>
                </div>
                <div id="good" class="status" hidden>
                  <p><a href=""><img src="img/social/family/line-chart.png" style="width: 20px;"></a></p>
                </div>
              </div>
              <div class="collapse" id="collapse7" style="margin: 0 10px;border-radius: 0 0 3px 3px;box-shadow: 1px 1px 1px #AAA;">
                <p class="text-center" style="background: #273041;color: #FFF;padding: 5px;margin-bottom: 0;">Who's talking about this</p>
                <div class="text-center" style="border-left: 1px solid #DBDBDB;padding: 10px;">
                  <img src="img/social/family/m1.png" style="width: 20%">
                  <img src="img/social/family/singtel.png" style="width: 20%">
                </div>
              </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
            
              <div class="social-image-container" style="background: url('img/social/family/8.png')">
                <div class="overlay text-center color-white">
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <h2><a href="">Sunday morning family time</a></h2>
                </div>
                <div class="info">
                  <p style="margin: 10px 0" class="text-right">
                    <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 200 Posts
                  </p>
                </div>
                <div id="bad" class="status" hidden>
                  <p ><a href=""><img src="img/social/family/warning.png" style="width: 20px;"></a></p>
                </div>
                <div id="good" class="status" hidden>
                  <p><a href=""><img src="img/social/family/line-chart.png" style="width: 20px;"></a></p>
                </div>
              </div>
              <div class="collapse" id="collapse8" style="margin: 0 10px;border-radius: 0 0 3px 3px;box-shadow: 1px 1px 1px #AAA;">
                <p class="text-center" style="background: #273041;color: #FFF;padding: 5px;margin-bottom: 0;">Who's talking about this</p>
                <div class="text-center" style="border-left: 1px solid #DBDBDB;padding: 10px;">
                  <img src="img/social/family/m1.png" style="width: 20%">
                  <img src="img/social/family/singtel.png" style="width: 20%">
                </div>
              </div>
          </div>

          <div class="col-md-4 col-sm-6 col-xs-12 no-padding">
            
              <div class="social-image-container" style="background: url('img/social/family/9.png')">
                <div class="overlay text-center color-white">
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <div class="white-space-20"></div>
                  <h2><a href="">Bath Time with Baby</a></h2>
                </div>
                <div class="info">
                  <p style="margin: 10px 0" class="text-right">
                    <img src="img/social/irregular-squares-outlines.png" style="width: 6%;"> 200 Posts
                  </p>
                </div>
                <div id="bad" class="status" hidden>
                  <p ><a href=""><img src="img/social/family/warning.png" style="width: 20px;"></a></p>
                </div>
                <div id="good" class="status" hidden>
                  <p><a href=""><img src="img/social/family/line-chart.png" style="width: 20px;"></a></p>
                </div>
              </div>
              <div class="collapse" id="collapse9" style="margin: 0 10px;border-radius: 0 0 3px 3px;box-shadow: 1px 1px 1px #AAA;">
                <p class="text-center" style="background: #273041;color: #FFF;padding: 5px;margin-bottom: 0;">Who's talking about this</p>
                <div class="text-center" style="border-left: 1px solid #DBDBDB;padding: 10px;">
                  <img src="img/social/family/m1.png" style="width: 20%">
                  <img src="img/social/family/singtel.png" style="width: 20%">
                </div>
              </div>
          </div>
        </div>
      </div>

      <div id="insight" class="social-right-side col-md-10 no-padding" style="min-height: 580px">
        <div class="col-md-12 no-padding">
          <!-- <a id="show-insights" href=""><img class="arrow-right"  src="img/social/family/left-arrow.png" style="width: 35px;position: absolute;top: 50%;right: 10px;z-index: 10"></a> -->
          <div class="col-md-9 no-padding">
            <div class="col-md-12 no-padding text-right">
              <div class="white-space-20"></div>
              <img src="img/social/family/calendar.png" style="width: 20px">
              <select class="" style="width: 20%;height: 25px;border-radius: 3px;">
                <option value="">Filter by</option>
              </select>
            </div>
            <div class="col-md-12 no-padding">
              <img src="img/social/family/CHART.png" style="width: 100%">
            </div>
          </div>
          <div class="col-md-3 no-padding">
            <div class="white-space-20"></div>
            <div class="white-space-20"></div>
            <div class="white-space-20"></div>
            <div class="white-space-20"></div>
            <img src="img/social/family/INSIGHTS STREAM.png" style="width: 85%">
          </div>
        </div>
      </div>
    </div>

    <div id="analytics" hidden>
      <div class="col-md-12">

        <div class="col-md-4">
          <div class="white-space-20"></div>
          <h2 class="border-bottom" style="padding-bottom: 10px;margin-bottom: 20px;">Beach day with baby</h2>
          <p class="color-gray"> 
            Passion points 
            <i class="fa fa-chevron-right"></i> &nbsp;Family</span>
            <i class="fa fa-chevron-right"></i> &nbsp; <span class="text-underline">Beach Day with Baby</span> 
          </p>
          <div class="white-space-20"></div>
        </div>

        <div class="col-md-8 no-padding-right panel_data" style="padding-left: 30px;">
          <div class="white-space-20"></div>
          <div class="panel_data_graph col-md-1 col-sm-4 col-xs-12 no-padding text-center ">
            <h4 class="text-center" style="font-size:40px;margin-top: 32px;">97K</h4>
            <p style="font-size: 10px;">People Posting</p>
          </div>
          <div class="panel_data_graph col-md-2 col-sm-4 col-xs-12 no-padding text-center">
            <div id="analytic-age-pie-graph" style="text-align: center;">
              <span id="age-icon" style="position: absolute;margin-top: 25px;margin-left: 31px;font-size: 25px;">
                <i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>
                <i id="male-icon" class="fa fa-male" style="font-weight: 600;display: none"></i>
              </span>
            </div>
            <p>Female <span>60%</span></p>
          </div>
          <div class="panel_data_graph col-md-2 col-sm-4 col-xs-12 no-padding text-center">
            <div id="analytic-average-bar-graph" style="position: absolute;top: -96px;left: -66px;">
              <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 120px;">20-30</p>
              <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 121px;">Age</p>
            </div>
            
          </div>
          <div class="panel_data_graph col-md-4 col-sm-12 col-xs-12 no-padding text-center">
            <div id="analytic-line-graph" style="position: absolute;top: -85px;left: -55px;">
              <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 45px;left: 200px;">Most Post during Friday 6 - 8 pm</p>
            </div>
          </div>
          <div class="white-space-20"></div>
        </div>
      </div>

      <div class="col-md-12" style="background: #F3F3F3;padding: 10px;">
        <div class="col-md-5 no-padding">
          <div class="section-wrapper">
            <div class="col-md-12 no-padding">
              <div class="section-main-img-wrapper" style="background: url('img/social/family/5.png')">
                <div class="status">
                  <p class="color-white">
                    <span style="margin-right: 5px"><i class="fa fa-heart" style="font-size: 20px;"></i>&nbsp;87</span> <span><img src="img/social/analytics/chat.png" style="width: 20px;margin-top: -10px;">&nbsp;4</span>
                    <span class="pull-right"><img src="img/social/analytics/mycupofthea.png" style="width: 20%;">&nbsp; mycupofthea</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 no-padding">
              <div class="section-img-wrapper" style="background: url('img/social/analytics/1.png')">
                <div class="status">
                  <p class="color-white" style="font-size: 8px">
                    <span><i class="fa fa-heart" style="font-size: 10px;"></i>&nbsp;87</span> <span><img src="img/social/analytics/chat.png" style="width: 10px;margin-top: -4px;">&nbsp;4</span>
                    <span class="pull-right"><img src="img/social/analytics/mariaderawie.png" style="width: 10%;">&nbsp; mycupofthea</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 no-padding">
              <div class="section-img-wrapper" style="background: url('img/social/analytics/2.png')">
                <div class="status">
                  <p class="color-white" style="font-size: 8px">
                    <span><i class="fa fa-heart" style="font-size: 10px;"></i>&nbsp;87</span> <span><img src="img/social/analytics/chat.png" style="width: 10px;margin-top: -4px;">&nbsp;4</span>
                    <span class="pull-right"><img src="img/social/analytics/mariaderawie.png" style="width: 10%;">&nbsp; mycupofthea</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 no-padding">
              <div class="section-img-wrapper" style="background: url('img/social/analytics/3.png')">
                <div class="status">
                  <p class="color-white" style="font-size: 8px">
                    <span><i class="fa fa-heart" style="font-size: 10px;"></i>&nbsp;87</span> <span><img src="img/social/analytics/chat.png" style="width: 10px;margin-top: -4px;">&nbsp;4</span>
                    <span class="pull-right"><img src="img/social/analytics/mariaderawie.png" style="width: 10%;">&nbsp; mycupofthea</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 no-padding">
              <div class="section-img-wrapper" style="background: url('img/social/analytics/6.png')">
                <div class="status">
                  <p class="color-white" style="font-size: 8px">
                    <span><i class="fa fa-heart" style="font-size: 10px;"></i>&nbsp;87</span> <span><img src="img/social/analytics/chat.png" style="width: 10px;margin-top: -4px;">&nbsp;4</span>
                    <span class="pull-right"><img src="img/social/analytics/mariaderawie.png" style="width: 10%;">&nbsp; mycupofthea</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 no-padding">
              <div class="section-img-wrapper" style="background: url('img/social/analytics/5.png')">
                <div class="status">
                  <p class="color-white" style="font-size: 8px">
                    <span><i class="fa fa-heart" style="font-size: 10px;"></i>&nbsp;87</span> <span><img src="img/social/analytics/chat.png" style="width: 10px;margin-top: -4px;">&nbsp;4</span>
                    <span class="pull-right"><img src="img/social/analytics/mariaderawie.png" style="width: 10%;">&nbsp; mycupofthea</span>
                  </p>
                </div>
              </div>
            </div>
            <div class="col-md-4 no-padding">
              <div class="section-img-wrapper" style="background: url('img/social/analytics/4.png')">
                <div class="status">
                  <p class="color-white" style="font-size: 8px">
                    <span><i class="fa fa-heart" style="font-size: 10px;"></i>&nbsp;87</span> <span><img src="img/social/analytics/chat.png" style="width: 10px;margin-top: -4px;">&nbsp;4</span>
                    <span class="pull-right"><img src="img/social/analytics/mariaderawie.png" style="width: 10%;">&nbsp; mycupofthea</span>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-3 no-padding">
          <div class="col-md-12 no-padding">
            <div class="white-section-wrapper">
              <h4 class="text-underline-green">Competitors talking about this</h4>
              <a href="" data-toggle="modal" data-target="#competitorsModal"><img src="img/social/analytics/competitor post.png" style="width: 50%"></a>
            </div>
          </div>

          <div class="col-md-12 no-padding">
            <div class="white-section-wrapper" style="padding-bottom: 93px;">
              <h4 class="text-underline-green">Associated keywords</h4>
              
                <div role="tabpanel" class="tab-pane" id="top-obj" style="margin-bottom: 3px;">
                  <div class="row_object" style="margin-top: 10px;">
                    <div class="object_name">
                      <label>Passport</label>
                    </div>
                    <div class="progress_obj">
                      <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                            
                          </div>
                          <span class="">42%</span>
                      </div>
                    </div>  
                  </div>
                  <div class="row_object">
                    <div class="object_name">
                      <label>Ticket</label>
                    </div>
                    <div class="progress_obj">
                      <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 31%">
                            
                          </div>
                          <span class="">31%</span>
                      </div>
                    </div>  
                  </div>
                  <div class="row_object">
                    <div class="object_name">
                      <label>Luggage</label>
                    </div>
                    <div class="progress_obj">
                      <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                            
                          </div>
                          <span class="">15%</span>
                      </div>
                    </div>  
                  </div>
                  <div class="row_object">
                    <div class="object_name">
                      <label>Aircraft</label>
                    </div>
                    <div class="progress_obj">
                      <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                            
                          </div>
                          <span class="">10%</span>
                      </div>
                    </div>  
                  </div>
                </div>
              
            </div>
          </div>
          
        </div>

        <div class="col-md-4 no-padding">
          <div class="col-md-12 no-padding">
            <div class="white-section-wrapper">
              <h4 class="text-underline-green">Where this theme is happening</h4>
              <div id="analytics-map" style="width: 100%;height: 235px;">
              </div>  
            </div>
          </div>
          <div class="col-md-12 no-padding">
            <div class="white-section-wrapper">
              <h4 class="text-underline-green">Who you can activate</h4>
              <div class="col-md-6 no-padding">
                <h5>Influencers</h5>
                <img src="img/social/analytics/influencers.png" style="width: 86%;">
              </div>
              <div class="col-md-6 no-padding">
                <h5>Celebrities / Brands</h5>
                <img src="img/social/analytics/celebs & brands.png" style="width: 86%;">
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div id="people" hidden>
      <div class="col-md-12" style="padding-top: 20px;background: #F3F3F3;">
        <div class="row">
          <!-- Audience -->
          <div class="col-md-4">
            <div class="table-container" style="background: #F8F8F8">
              <table class="table table-responsive table-striped table-location-result">
                <thead>
                  <tr>
                    <div class="col-md-3">
                      <h4>People </h4>
                    </div>
                    <div class="col-md-4">
                      <h4 style="font-size: 15px;color:#AAA">32K people</h4>
                    </div>
                    <div class="col-md-5 text-right">
                      <h4>
                      <select style="font-size: 14px;">
                        <option>By Passion Point</option>
                      </select>
                      </h4>
                    </div>
                  </tr>
                  <div class="white-space-10"></div>
                  <tr>
                    <div class="col-md-4 text-center">
                      <div class="white-space-10"></div>
                      <button class="btn-sm btn btn-info" style="padding: 2px 5px;border-radius: 2px;">Export</button>
                      <div class="white-space-10"></div>
                    </div>
                    <div class="col-md-3   text-center">
                      <div class="white-space-10"></div>
                      <p style="font-size: 12px;font-weight: 600;">Post Here</p>
                      <div class="white-space-10"></div>
                    </div>
                    <div class="col-md-2 text-center">
                      <div class="white-space-10"></div>
                      <p style="font-size: 12px;font-weight: 600;">Followers</p>
                      <div class="white-space-10"></div>
                    </div>
                    <div class="col-md-3 text-center" style="padding-right: 10px">
                      <div class="white-space-10"></div>
                      <p style="font-size: 10px;font-weight: 600;">Avg. Engagement</p>
                      <div class="white-space-10"></div>
                    </div>
                  </tr>
                </thead>

                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-4">
                        <img src="/img/audience_img/2.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:10px;margin:0"><strong>@Amsteelvin</strong></p>
                          <p style="font-size:10px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600">20</p>
                      </div>

                      <div class="col-md-2 text-center">
                        <p style="font-size:10px;font-weight: 600">21.5M</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:10px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i>15k</p>
                      </div>
                    </div>
                  </td>
                </tr>

              </table>
            </div>
          </div>
          <div class="col-md-8">
            <div class="row">
              <div class="profile">
                <div class="col-md-3 text-center">
                  <img src="/img/audience_img/2.jpg" width="90%" style="border-radius:50%; margin-top: 40px;">
                  <div class="white-space-20"></div>
                  <button class="btn btn-success" style="background: none !important;border: 2px solid #45A9DF; color: #45A9DF ;border-radius: 1px;"><i class="fa fa-plus"></i> Follow</button>
                </div>
                <div class="col-md-9">
                  <div class="col-md-12 no-padding">
                    <div class="info-container-location">
                      <a href="#" target="_blank"><h1 style="color: #8A8E97;font-size:24px;">Christina Freeman | @<span>Sample</span></h1></a>
                      <div class="description-profile" style="font-size:14px;">
                        <p>primarily posts from <strong>New York</strong></p>
                        <p>
                          East Coast | Sagittarius | Old Soul | Southern Belle
                        </p>
                        <p>
                          Actress | Model | Muse | Lover of all things car
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2 no-padding location-color-page-box">
                    <p class="color-black"><small>Followers</small></p>
                    <h4><strong>21.5M</strong></h4>
                    <p class="color-black"><small>Influencer Score</small></p>
                    <p> <i class="hor-tube-wrapper"> <i class="hor-tube"></i> </i> </p>
                  </div>
                  <div class="col-md-5 location-color-page-box">
                    <p class="color-black"><small>Interested In</small></p>
                    <div class="col-md-8 no-padding badge-location">
                      <p>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Travel</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Parenting</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Car</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Parenting</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Travel</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Car</label>
                        <!-- <label class="label label-primary" style="margin-top:5px;display:inline-table">Fashion</label> -->
                      </p>
                    </div>
                  </div>
                  <div class="col-md-5 location-color-page-box">
                    <p class="color-black"><small>Has Posted From</small></p>
                    <div class="col-md-8 no-padding badge-location">
                      <p>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">New York</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Los Angeles</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Chicago</label>
                      </p>
                    </div>
                  </div>


                </div>

                <div class="col-md-12" style="margin-top: 10px;">
                  <label>Their most recent post in this area</label>
                </div>

                <div class="col-md-4 no-padding-right" style="margin-top:25px;">         
                  <div class="img-wrapper">
                    <img src="/img/audience_img/2.jpg" class="img-full">
                  </div>
                </div>
                <div class="col-md-7" style="margin-top:25px;background: #FFF;padding: 15px 10px 20px 10px;">         
                  <article style="font-size: 13px;">
                    <p>
                      <i class="fa fa-map-marker"></i> 4 Seasons Santa Barbara
                    </p>
                    <p>
                      <i class="fa fa-clock-o"></i> 3 hours ago
                    </p>
                    <p>
                      <i class="fa fa-heart"></i> Julia Barreto, Sunshine Cruz, William Smith, Alexa Solis and 41 others like this
                    </p>
                    <p>
                      <i class="fa fa-comment"></i> <b>Username</b> comments go here
                    </p>
                    <p style="padding-left:20px;">
                      <strong>Bowweee</strong> &nbsp; i wish were here <br>
                    </p>
                    <p style="padding-left:20px;">
                      <strong>Samie</strong> &nbsp; Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
                    </p>
                    <p class="text-underline" style="background: #D9D9D9; color: #425370 ; padding:15px 10px;font-weight: 600;margin: 0 -10px;margin-bottom: -20px;">Add Instagram account to engage with content</p>
                  </article>
                </div>
                  
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
    

  </div>

  <div class="modal fade" id="competitorsModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" map-data>
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <div class="white-space-20"></div>
        </div>
        <div class="modal-body text-center">
          <img src="img/social/analytics/competitor post.png" style="width: 50%" class="img-thumbnail">
        </div>
      </div>
    </div>
  </div>

    

  <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" map-data>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <label class="modal-title" id="myModalLabel">Pick your search category</label>
        </div>
        <div class="modal-body">
          <!-- Nav tabs -->
          <div class="row">
            <div class="col-md-12">
              <div class="modal-wrapper">
                <ul id="search-tabs" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="" data-target="#segment" aria-controls="segment" role="tab" data-toggle="tab">Segment</a></li>
              <li role="presentation"><a href="" data-target="#locations" aria-controls="locations" role="tab" data-toggle="tab" ng-click="runMap()">Locations</a></li>
              <li role="presentation"><a href="" data-target="#brands" aria-controls="brands" role="tab" data-toggle="tab">Brands</a></li>
            </ul>
            <div class="white-space-20"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="segment">
                <form>
                  <div class="form-inline">
                    <label>What Segment ? </label>
                    <input type="text" class="form-control">
                  </div>
                  
                </form>
              </div>
              <div role="tabpanel" class="capitaland2 tab-pane fade" id="locations" >
                <div class="col-md-3 no-padding">
                  <div class="left-bar">
                    <div class="locations-section">
                      <h4>Locations</h4>
                      <div class="one text-center">
                        
                        <!-- SEARCH FILTER -->
                        <input id="searchFilter" name="places" ng-model="searchFilter.location" type="text" placeholder="Type your location" class="form-control" vs-google-autocomplete ng-blur="getLocation(searchFitler.location)" >

                        <!-- SEARCH LOCATION RESULTS -->
                        <div class="search-location-result" style="height: 90px;overflow: hidden">
                          <p class="text-right">Number of posts</p>
                          <p ng-repeat="result in search_result | filter:searchFilter" class="text-left result" hidden>
                            <a href="" ng-click="get_summary()">{{result}}
                              <span class="badge float-right">50</span>
                            </a>
                          </p>
                        </div>
                        
                        <button id="view-more-result-btn" class="btn btn-default">View More</button>
                      </div>
                      <div class="two">
                        <button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                        <button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
                        <button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
                      </div>
                      
                    </div>
                    <div class="duration-section">
                      <h4>Duration</h4>
                      <div class="form-group">
                        <label>From</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>To</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-inline">
                        <button class="btn btn-default">Apply</button>
                        <button class="btn btn-default">Clear</button>
                      </div>
                    </div>
                    <div class="networks-section">
                      <h4>Networks</h4>
                      <p class="text-right">Number of posts</p>
                      <p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
                      <p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
                      <p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 no-padding" style="margin-bottom: 50px;">
                  <div id="map" style="min-height: 762px;width: 100%"></div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="brands">
                <form>
                  <div class="form-inline">
                    <label>What Brand do you want to analyze ? </label>
                    <input type="text" class="form-control">
                  </div>
                </form>
              </div>
              <div class="white-space-20"></div>
              <button id="search-explore-btn" ng-click="brand_explore();segment()" data-dismiss="modal" class="btn btn-default" style="position: absolute;bottom: 0;right: 10px">Explore</button>
            </div>
            <div class="white-space-20"></div>
          </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">

  

  var ctr = 0;

  $( "#show-insights" ).click(function(){
    if( ctr == 0){
      $("#insight").css({left:"225px"});
      ctr = 1;
    }else{
      $("#insight").css({left:"-84%"});
      ctr = 0;
    }
    
  });

</script>