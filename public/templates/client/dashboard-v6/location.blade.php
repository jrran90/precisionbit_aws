<div class="col-md-12 capitaland" style="overflow: hidden" map-data>
	<div class="col-md-3 no-padding">
		<div class="left-bar">
			<div class="locations-section">
				<h4>Locations</h4>
				<div class="one text-center">
					
					<!-- SEARCH FILTER -->
					<input id="searchFilter" name="places" ng-model="searchFilter.location" type="text" placeholder="Type your location" class="form-control" vs-google-autocomplete ng-blur="getLocation(searchFitler.location)" >

					<!-- SEARCH LOCATION RESULTS -->
					<div class="search-location-result" style="height: 90px;overflow: hidden">
						<p class="text-right">Number of posts</p>
						<p ng-repeat="result in search_result | filter:searchFilter" class="text-left result" hidden>
							<a href="" ng-click="get_summary()">{{result}}
								<span class="badge float-right">50</span>
							</a>
						</p>
					</div>
					
					<button id="view-more-result-btn" class="btn btn-default">View More</button>
				</div>
				<div class="two">
					<button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
					<button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
					<button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
				</div>
				
			</div>
			<div class="duration-section">
				<h4>Duration</h4>
				<div class="form-group">
					<label>From</label><br>
					<input type="date" class="form-control">
					<input type="time" class="form-control">
				</div>
				<div class="form-group">
					<label>To</label><br>
					<input type="date" class="form-control">
					<input type="time" class="form-control">
				</div>
				<div class="form-inline">
					<button class="btn btn-default">Apply</button>
					<button class="btn btn-default">Clear</button>
				</div>
			</div>
			<div class="networks-section">
				<h4>Networks</h4>
				<p class="text-right">Number of posts</p>
				<p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
				<p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
				<p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
			</div>
		</div>
	</div>

	<div class="col-md-9 no-padding">
		<div id="map" style="min-height: 762px;width: 100%"></div>
	</div>

	<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" map-data>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <label class="modal-title" id="myModalLabel">Pick your search category</label>
        </div>
        <div class="modal-body">
          <!-- Nav tabs -->
          <div class="row">
            <div class="col-md-12">
              <div class="modal-wrapper">
                <ul id="search-tabs" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="" data-target="#segment" aria-controls="segment" role="tab" data-toggle="tab">Segment</a></li>
              <li role="presentation"><a href="" data-target="#locations" aria-controls="locations" role="tab" data-toggle="tab" ng-click="runMap()">Locations</a></li>
              <li role="presentation"><a href="" data-target="#brands" aria-controls="brands" role="tab" data-toggle="tab">Brands</a></li>
            </ul>
            <div class="white-space-20"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="segment">
                <form>
                  <div class="form-inline">
                    <label>What Segment ? </label>
                    <input type="text" class="form-control">
                  </div>
                  
                </form>
              </div>
              <div role="tabpanel" class="capitaland2 tab-pane fade" id="locations" >
                <div class="col-md-3 no-padding">
                  <div class="left-bar">
                    <div class="locations-section">
                      <h4>Locations</h4>
                      <div class="one text-center">
                        
                        <!-- SEARCH FILTER -->
                        <input id="searchFilter" name="places" ng-model="searchFilter.location" type="text" placeholder="Type your location" class="form-control" vs-google-autocomplete ng-blur="getLocation(searchFitler.location)" >

                        <!-- SEARCH LOCATION RESULTS -->
                        <div class="search-location-result" style="height: 90px;overflow: hidden">
                          <p class="text-right">Number of posts</p>
                          <p ng-repeat="result in search_result | filter:searchFilter" class="text-left result" hidden>
                            <a href="" ng-click="get_summary()">{{result}}
                              <span class="badge float-right">50</span>
                            </a>
                          </p>
                        </div>
                        
                        <button id="view-more-result-btn" class="btn btn-default">View More</button>
                      </div>
                      <div class="two">
                        <button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                        <button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
                        <button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
                      </div>
                      
                    </div>
                    <div class="duration-section">
                      <h4>Duration</h4>
                      <div class="form-group">
                        <label>From</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>To</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-inline">
                        <button class="btn btn-default">Apply</button>
                        <button class="btn btn-default">Clear</button>
                      </div>
                    </div>
                    <div class="networks-section">
                      <h4>Networks</h4>
                      <p class="text-right">Number of posts</p>
                      <p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
                      <p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
                      <p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 no-padding" style="margin-bottom: 50px;">
                  <div id="map" style="min-height: 762px;width: 100%"></div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="brands">
                <form>
                  <div class="form-inline">
                    <label>What Brand do you want to analyze ? </label>
                    <input type="text" class="form-control">
                  </div>
                </form>
              </div>
              <div class="white-space-20"></div>
              <button id="search-explore-btn" ng-click="brand_explore();segment()" data-dismiss="modal" class="btn btn-default" style="position: absolute;bottom: 0;right: 10px">Explore</button>
            </div>
            <div class="white-space-20"></div>
          </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

	$('#searchFilter').change(function(){

		var data = $('#searchFilter').val();
		console.log(data);

		if( $('#searchFilter').val() != "" ){
			$('.result').show();
			console.log(data);
		}else{
			$('.result').hide();
		}
		
	});
</script>