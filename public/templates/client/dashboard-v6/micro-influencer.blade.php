<div class="col-md-12 mockup-v3 micro-influencer" style="overflow: hidden; margin-bottom:20px; padding-bottom:20px;" micro-graph>

  <div class="col-md-12">
    <div class="header-container">
      <div class="row_heading row"> 
        <h1>Micro influencer & Advocate discovery</span></h1>
      </div>
      <div class="row_links row">
        <ul class="link_list">
          <li class="active"><a href="" ng-click="next_tab(1)">Analytics</a><div class="caret-up" style="left: 45%;display: block"></div></li>
          <li><a href="" ng-click="next_tab(2)">Micro</a><div class="caret-up" style="left: 45%"></div></li>
          <li><a href="" ng-click="next_tab(3)">Advocate</a><div class="caret-up" style="left: 45%"></div></li>
        </ul>
      </div>
    </div>

    <div class="row_contents" id="analytics-section" >
      <div class="row">
        <div class="col-md-3 text-center">
          <div class="panel-box">
            <div class="row">
              <img src="img/starhub.jpg" class="img-50p img-circle margin-center ">
              <h4>492 posts | 3035 Followers</h4>
            </div>
          </div>
        </div>
        <div class="col-md-5 no-padding">
          <div class="panel-box" >
            <div class="row" >
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <h4>Audience Insight</h4>
                </div>
              </div>

              <div class="col-md-12 panel_data" style="padding: 25px 0px;">
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center ">
                  <h4 class="text-center" style="font-size:40px;">97K</h4>
                  <p>People Posting</p>
                </div>
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                  <div id="age-pie-graph" style="text-align: center;">
                    <span id="age-icon" style="position: absolute;margin-top: 20px;margin-left: 23px;font-size: 20px;">
                      <i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>
                      <i id="male-icon" class="fa fa-male" style="font-weight: 600;display: none"></i>
                    </span>
                  </div>
                  <p>Female <span>60%</span></p>
                </div>
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                  <div id="average-bar-graph" style="position: absolute;top: -100px;left: -45px;">
                    <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 93px;">20-30</p>
                    <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 94px;">Age</p>
                  </div>
                  
                </div>
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                  <div id="influ-bar-graph" style="position: absolute;top: -100px;left: -55px;">
                    <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 95px;">0 - 1K</p>
                    <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 80px;">Influence</p>
                  </div>
                  <p></p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="panel-box">
            <div class="row">

              <div class="col-md-12 panel_data">
                <div id="persona-pie-graph" class="chart-custom" style="">
                  <ul class="nav persona-nav2">
                    <li><div style="background: #5E97F6"></div>Micro Influencer</li>
                    <li><div style="background: #DB4437"></div>Advocate</li>                    
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  

    </div>

    <div id="micro-section" class="row_contents" hidden>
      <div class="col-md-12 no-padding">
        <div class="row">
          <!-- Audience -->
          <div class="col-md-4">
            <div class="table-container" style="background: #F8F8F8;overflow-y: scroll;overflow-x: hidden;height: 700px;">
              <table class="table table-responsive table-striped table-location-result">
                <thead>
                  <tr>
                    <div class="col-md-3">
                      <h4>People </h4>
                    </div>
                    <div class="col-md-4">
                      <h4 style="font-size: 15px;color:#AAA">10 people</h4>
                    </div>
                    <div class="col-md-5 text-right">
                      <h4>
                      <select style="font-size: 14px;">
                        <option>By Passion Point</option>
                      </select>
                      </h4>
                    </div>
                  </tr>
                  <div class="white-space-10"></div>
                  <tr>
                    <div class="col-md-5 no-padding-right text-center">
                      <div class="white-space-10"></div>
                      <button class="btn-sm btn btn-info" style="padding: 2px 5px;border-radius: 2px;">Export</button>
                      <div class="white-space-10"></div>
                    </div>
                    <div class="col-md-2 no-padding-left  text-center">
                      <div class="white-space-10"></div>
                      <p style="font-size: 12px;font-weight: 600;">Post Here</p>
                      <div class="white-space-10"></div>
                    </div>
                    <div class="col-md-2 no-padding-left text-center">
                      <div class="white-space-10"></div>
                      <p style="font-size: 12px;font-weight: 600;">Followers</p>
                      <div class="white-space-10"></div>
                    </div>
                    <div class="col-md-3 text-center" style="padding-right: 10px">
                      <div class="white-space-10"></div>
                      <p style="font-size: 10px;font-weight: 600;">Avg. Engagement</p>
                      <div class="white-space-10"></div>
                    </div>
                  </tr>
                </thead>

                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13151165_245462135817836_134076272_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@binosio520</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">104</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">883</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/11202516_923038604442494_84978072_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@huanpuzzz</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">212</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,321</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13398794_1546197652355020_218168301_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@johnlth93</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">860</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">3,862</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13388597_1730489813903584_1503670781_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@kennethmavietnamfc</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">375</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">954</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/11325078_398908766976533_1975523441_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@ritazzee</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">269</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,585</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/11363739_1644289792494945_349303298_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@euniceolsen</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,168</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">3,868</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13740911_256716914710157_447802505_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@damienteo_fan</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">300</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">669</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/11202619_1472661103050054_1375320364_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@bryanlaoshi</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">2,851</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,913</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/10375619_980925195326407_707012435_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@im_msjiing</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">19</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">699</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/11335047_417211591803552_1349643230_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@ian_mckee</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">189</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">869</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>

              </table>
            </div>
          </div>
          <div class="col-md-8">
            <div class="row">
              <div class="profile">
                <div class="col-md-3 text-center">
                  <img src="/img/audience_img/2.jpg" width="90%" style="border-radius:50%; margin-top: 40px;">
                  <div class="white-space-20"></div>
                  <button class="btn btn-success" style="background: none !important;border: 2px solid #45A9DF; color: #45A9DF ;border-radius: 1px;"><i class="fa fa-plus"></i> Follow</button>
                </div>
                <div class="col-md-9">
                  <div class="col-md-12 no-padding">
                    <div class="info-container-location">
                      <a href="#" target="_blank"><h1 style="color: #8A8E97;font-size:24px;">Christina Freeman | @<span>Sample</span></h1></a>
                      <div class="description-profile" style="font-size:14px;">
                        <p>primarily posts from <strong>New York</strong></p>
                        <p>
                          East Coast | Sagittarius | Old Soul | Southern Belle
                        </p>
                        <p>
                          Actress | Model | Muse | Lover of all things car
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-2 no-padding location-color-page-box">
                    <p class="color-black"><small>Followers</small></p>
                    <h4><strong>21.5M</strong></h4>
                    <p class="color-black"><small>Influencer Score</small></p>
                    <p> <i class="hor-tube-wrapper"> <i class="hor-tube"></i> </i> </p>
                  </div>
                  <div class="col-md-5 location-color-page-box">
                    <p class="color-black"><small>Interested In</small></p>
                    <div class="col-md-8 no-padding badge-location">
                      <p>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Travel</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Parenting</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Car</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Parenting</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Travel</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Car</label>
                        <!-- <label class="label label-primary" style="margin-top:5px;display:inline-table">Fashion</label> -->
                      </p>
                    </div>
                  </div>
                  <div class="col-md-5 location-color-page-box">
                    <p class="color-black"><small>Has Posted From</small></p>
                    <div class="col-md-8 no-padding badge-location">
                      <p>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">New York</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Los Angeles</label>
                        <label class="label label-primary" style="margin-top:5px;display:inline-table">Chicago</label>
                      </p>
                    </div>
                  </div>


                </div>

                <div class="col-md-12" style="margin-top: 10px;">
                  <label>Their most recent post in this area</label>
                </div>

                <div class="col-md-4 no-padding-right" style="margin-top:25px;">         
                  <div class="img-wrapper">
                    <img src="/img/audience_img/2.jpg" class="img-full">
                  </div>
                </div>
                <div class="col-md-7" style="margin-top:25px;background: #FFF;padding: 15px 10px 20px 10px;">         
                  <article style="font-size: 13px;">
                    <p>
                      <i class="fa fa-map-marker"></i> 4 Seasons Santa Barbara
                    </p>
                    <p>
                      <i class="fa fa-clock-o"></i> 3 hours ago
                    </p>
                    <p>
                      <i class="fa fa-heart"></i> Julia Barreto, Sunshine Cruz, William Smith, Alexa Solis and 41 others like this
                    </p>
                    <p>
                      <i class="fa fa-comment"></i> <b>Username</b> comments go here
                    </p>
                    <p style="padding-left:20px;">
                      <strong>Bowweee</strong> &nbsp; i wish were here <br>
                    </p>
                    <p style="padding-left:20px;">
                      <strong>Samie</strong> &nbsp; Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                      tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.<br>
                    </p>
                    <p class="text-underline" style="background: #D9D9D9; color: #425370 ; padding:15px 10px;font-weight: 600;margin: 0 -10px;margin-bottom: -20px;">Add Instagram account to engage with content</p>
                  </article>
                </div>
                  
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="advocate-section" class="row_contents" hidden>
      <div class="col-md-12 no-padding">
        <div class="row">
          <!-- Audience -->
          <div class="col-md-4">
            <div class="table-container" style="background: #F8F8F8;overflow-y: scroll;overflow-x: hidden;height: 700px;">
              <table class="table table-responsive table-striped table-location-result">
                <thead>
                  <tr>
                    <div class="col-md-3">
                      <h4>People </h4>
                    </div>
                    <div class="col-md-4">
                      <h4 style="font-size: 15px;color:#AAA">27 people</h4>
                    </div>
                    <div class="col-md-5 text-right">
                      <h4>
                      <select style="font-size: 14px;">
                        <option>By Passion Point</option>
                      </select>
                      </h4>
                    </div>
                  </tr>
                  <div class="white-space-10"></div>
                  <tr>
                    <div class="col-md-4 text-center">
                      <div class="white-space-10"></div>
                      <button class="btn-sm btn btn-info" style="padding: 2px 5px;border-radius: 2px;">Export</button>
                      <div class="white-space-10"></div>
                    </div>
                    <div class="col-md-3   text-center">
                      <div class="white-space-10"></div>
                      <p style="font-size: 12px;font-weight: 600;">Post Here</p>
                      <div class="white-space-10"></div>
                    </div>
                    <div class="col-md-2 text-center">
                      <div class="white-space-10"></div>
                      <p style="font-size: 12px;font-weight: 600;">Followers</p>
                      <div class="white-space-10"></div>
                    </div>
                    <div class="col-md-3 text-center" style="padding-right: 10px">
                      <div class="white-space-10"></div>
                      <p style="font-size: 10px;font-weight: 600;">Avg. Engagement</p>
                      <div class="white-space-10"></div>
                    </div>
                  </tr>
                </thead>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/11351591_1654538214786635_1426644103_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@veeywn</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">311</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,342</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/11809537_1546961645526441_667773329_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@jshkarthiklove</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,405</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">370</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13525360_996809103771358_1018107972_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@akmxluqmxn</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">127</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">636</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13525548_806540159480358_1225735497_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@zhenyying</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">504</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">589</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13402238_1068067243272868_42364757_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@alfi_syah</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">422</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">561</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13534304_1640724356253808_889133996_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@april_owl</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">11,804</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,541</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13649211_1774887686068499_685873492_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@areezhardrisk</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">563</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,500</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/12628035_811419785652944_1081455387_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@chanheisieng1989</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">710</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">857</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/12568801_1673962959549599_1429839979_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@rachfang</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">735</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">504</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13092214_1753040528315456_1013547971_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@shankystarzgal</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">67</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">642</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13739634_213670292366759_518029921_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@thebignugget</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">86</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">542</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/12598981_439808516213622_849377209_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@xxnataliie_</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">214</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">646</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/12142286_518931814940818_368502495_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@cahskb</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">3,451</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">558</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13744255_212444242486534_204715045_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@yukihkt_</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">268</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,847</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/12749902_858307674278649_79985476_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@andrewyongkl</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,816</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">866</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13694565_1124874300902299_1793912452_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@dicksonhung123</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">2379</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">670</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/10475097_173925919631769_1024591768_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@jaynetoh</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,409</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">925</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13261017_1734042953541415_276575718_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@mastaroshie</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">349</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">2,229</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/12934879_1167926863247429_429900156_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@theamirrulraizo</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">134</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">703</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13257025_1280298471999295_270462730_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@ya.dama</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">461</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">524</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/12940220_1017606984995262_1288272170_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@alicemuiyun</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">554</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">519</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13151165_245462135817836_134076272_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@binosio520</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">104</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">883</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13248676_1176042029086376_818197284_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@c_serie</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">524</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">4,701</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/12107576_922714301110345_767858540_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@nuramirahv</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">600</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">1,371</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13531940_1699019240362176_904795147_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@rosetheexplorer</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">595</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">592</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13402684_1555683408061681_895661360_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@suffianhakim</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">656</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">532</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
                 
                 <tr>
                  <td class="global-active">
                    <div class="row" style="display:flex;align-items:center">
                      <div class="col-md-5 no-padding-right">
                        <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13636036_1058014554277524_1806573843_a.jpg" style="width:35px;border-radius:50%;float:left;margin-right:10px">
                        <div class="user_details">
                          <p style="font-size:11px;margin:0"><strong>@tingting_589</strong></p>
                          <p style="font-size:11px;"><small>Loves Cars, Travel</small></p>
                        </div>
                      </div>
                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">4,522</p>
                      </div>

                      <div class="col-md-2 no-padding-left text-center">
                        <p style="font-size:11px;font-weight: 600">2,588</p>
                      </div>

                      <div class="col-md-3 text-center">
                        <p style="font-size:11px;font-weight: 600"><i class="tube-wrapper"> <i class="tube"></i> </i></p>
                      </div>
                    </div>
                  </td>
                </tr>
              </table>
            </div>
          </div>
          <div class="col-md-8">
            <div class="row">
              <div class="profile">
                <div class="col-md-3 text-center">
                  <img src="https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/11351591_1654538214786635_1426644103_a.jpg" style="border-radius:50%; margin-top: 40px;width: 70%">
                  <div class="white-space-20"></div>
                  <button class="btn btn-success" style="background: none !important;border: 2px solid #45A9DF; color: #45A9DF ;border-radius: 1px;"><i class="fa fa-plus"></i> Follow</button>
                </div>
                <div class="col-md-9">
                  <div class="col-md-12 no-padding">
                    <div class="info-container-location">
                      <a href="https://www.instagram.com/veeywn/" target="_blank"><h1 style="color: #8A8E97;font-size:24px;">易薇倪 | @<span>veeywn</span>
                      <span style="font-size: 14px;margin-left: 20px"><i class="fa fa-check" ></i> Not a bot</span>
                      </h1></a>
                      <div class="description-profile" style="font-size:14px;">
                        <p>Female, Age: 30+ </p>
                        <p>
                          313 posts 1,352 Followers 549 following
                        </p>
                        <p>
                          Last time she engaged with you: 6 days ago
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-12 no-padding">
                    <div class="label label-wrapper" style="background:#4CC784;font-size: 14px;padding: 8px 25px 8px 10px;margin-right: 5px">
                      Fashion
                    </div>
                    <div class="label label-wrapper" style="background:#B2DE84;font-size: 14px;padding: 8px 25px 8px 10px;margin-right: 5px">
                      Relationships
                    </div>
                    <div class="label label-wrapper" style="background:#C4E2D9;font-size: 14px;padding: 8px 25px 8px 10px;margin-right: 5px">
                      Celebrations
                    </div>
                  </div>
                  <div class="col-md-12 no-padding-left" style="margin-top: 20px">
                    <div class="stats"><i class="fa fa-close"></i> Graduation</div>
                    <div class="stats"><i class="fa fa-close"></i> Just married</div>
                    <div class="stats"><i class="fa fa-close"></i> Just engaged</div>
                    <div class="stats"><i class="fa fa-close"></i> New mom</div>
                  </div>
                  


                </div>


                <div class="col-md-12 no-padding" style="overflow-y:auto;overflow-x:hidden;height: 420px;margin-top: 20px">
                  <div class="col-md-5 text-center" style="margin-top: 20px;">
                    <h5><b>TOPICS SHE ENGAGES HER NETWORK ON</b></h5>
                  </div>


                  <div class="col-md-7 text-center" style="margin-top: 20px;">
                    <h5><b>Preview of Veeywn’s <b>Fashion Content</b></b></h5>
                  </div>

                  <div class="col-md-5" >
                    <div id="engage-bar-graph" style="position: absolute;top: -50px;left: 40px;">
                    </div>
                  </div>

                  <div class="col-md-7 text-center">
                    
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12383291_1521732501454444_1522023731_n.jpg') ">
                        
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12818974_116366822091724_436233786_n.jpg') ">
                        
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12748430_588345271320063_1142833729_n.jpg') ">
                        
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12346114_206056549728152_2081683211_n.jpg') ">
                        
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12628130_1522094238093778_218596578_n.jpg') ">
                        
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/13329141_1084569938267463_858633883_n.jpg') ">
                        
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/13277845_1609784469348748_1462711444_n.jpg') ">
                        
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12301132_1042340599142873_93864463_n.jpg') ">
                        
                      </div>
                    </div>
                  </div>

                  <div class="col-md-12 text-center" style="margin-top: 20px;">
                    <h5><b>Types of Celebrities She Follows</b></h5>
                  </div>

                  <div class="col-md-12" style="height: 300px">
                    <div id="celeb-bar-graph" style="position: absolute;top: -90px;left: 40px;">
                    </div>
                  </div>

                  <div class="col-md-5 text-center" style="margin-top: 20px;">
                    <h5><b>TYPES OF POP CULTURE SHE FOLLOWS</b></h5>
                  </div>


                  <div class="col-md-7 text-center" style="margin-top: 20px;">
                    <h5><i>List of “Movie” Celebrities She Follows</i></h5>
                  </div>

                  <div class="col-md-5">
                    <div id="culture-pie-graph" style="text-align: center;margin-top: 20px">
                    </div>
                  </div>

                  <div class="col-md-7 text-center">
                    
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13473206_719094914897518_1889488127_a.jpg') ">
                        <div class="btm">
                          <p><b>Carrie Wong</b></p>
                          <p>Singaporean Actress</p>
                          <p>158k followers</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/1724845_631040470349200_827886547_a.jpg') ">
                        <div class="btm">
                          <p><b>Shaun Chen</b></p>
                          <p>Malaysian Actor</p>
                          <p>77.8k followers</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/11357866_970003379716359_1063486167_a.jpg') ">
                        <div class="btm">
                          <p><b>Vivian Hsu</b></p>
                          <p>Taiwanese Actress</p>
                          <p>474k followers</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/11378258_1622466728030135_1863044251_a.jpg') ">
                        <div class="btm">
                          <p><b>Sui Tang Tang</b></p>
                          <p>Taiwanese Actress</p>
                          <p>808k followers</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/10723708_1577687912452632_525022994_a.jpg') ">
                        <div class="btm">
                          <p><b>Irene Ang</b></p>
                          <p>Singaporean Actress</p>
                          <p>26.4k followers</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/11909961_1704860439744518_939754054_a.jpg') ">
                        <div class="btm">
                          <p><b>Zhenggeping</b></p>
                          <p>Singaporean Actress</p>
                          <p>158k followers</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/12424782_206674626368461_717575397_a.jpg') ">
                        <div class="btm">
                          <p><b>Seraph Sun</b></p>
                          <p>Singaporean Actress</p>
                          <p>23.4k followers</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('https://instagram.fmnl4-6.fna.fbcdn.net/t51.2885-19/s150x150/13117813_1615649855425321_1476375969_a.jpg') ">
                        <div class="btm">
                          <p><b>Desmond Tan</b></p>
                          <p>Singaporean Actor</p>
                          <p>123k followers</p>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-md-5 text-center" style="margin-top: 20px;">
                    <h5><b>TYPES OF LOCATIONS SHE’S VISITED</b></h5>
                  </div>


                  <div class="col-md-7 text-center" style="margin-top: 20px;">
                    <h5><b>List of Cafes She’s Visited</b></h5>
                  </div>

                  <div class="col-md-5">
                    <div id="location-bar-graph" style="position: absolute;top: -70px;left: 40px;">
                    </div>
                  </div>

                  <div class="col-md-7 text-center">
                    
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/13408832_487472258109693_1333468075_n.jpg') ">
                        <div class="btm text-center">
                          <p><b>Roost Cafe</b></p>
                          <p>&nbsp;</p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12818974_116366822091724_436233786_n.jpg') ">
                        <div class="btm text-center">
                          <p><b>The Maker Coffee</b></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/13249899_248528925507614_2121168956_n.jpg') ">
                        <div class="btm text-center">
                          <p><b>The Maker Coffee</b></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/13561810_259782567711940_2139671726_n.jpg') ">
                        <div class="btm text-center">
                          <p><b>The Maker Coffee</b></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/13329141_1084569938267463_858633883_n.jpg') ">
                        <div class="btm text-center">
                          <p><b>The Maker Coffee</b></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12905225_1708665726069129_1946378984_n.jpg') ">
                        <div class="btm text-center">
                          <p><b>The Maker Coffee</b></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12237258_616474798490375_170173714_n.jpg') ">
                        <div class="btm text-center">
                          <p><b>The Maker Coffee</b></p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-3 no-padding">
                      <div class="images-wrapper" style="background-image: url('../img/instagram/@veeywn/12907350_237182216634872_621246996_n.jpg') ">
                        <div class="btm text-center">
                          <p><b>The Maker Coffee</b></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                  
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>  
    

  <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" map-data>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <label class="modal-title" id="myModalLabel">Pick your search category</label>
        </div>
        <div class="modal-body">
          <!-- Nav tabs -->
          <div class="row">
            <div class="col-md-12">
              <div class="modal-wrapper">
                <ul id="search-tabs" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="" data-target="#segment" aria-controls="segment" role="tab" data-toggle="tab">Segment</a></li>
              <li role="presentation"><a href="" data-target="#locations" aria-controls="locations" role="tab" data-toggle="tab" ng-click="runMap()">Locations</a></li>
              <li role="presentation"><a href="" data-target="#brands" aria-controls="brands" role="tab" data-toggle="tab">Brands</a></li>
            </ul>
            <div class="white-space-20"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="segment">
                <form>
                  <div class="form-inline">
                    <label>What Segment ? </label>
                    <input type="text" class="form-control">
                  </div>
                  
                </form>
              </div>
              <div role="tabpanel" class="capitaland2 tab-pane fade" id="locations" >
                <div class="col-md-3 no-padding">
                  <div class="left-bar">
                    <div class="locations-section">
                      <h4>Locations</h4>
                      <div class="one text-center">
                        
                        <!-- SEARCH FILTER -->
                        <input id="searchFilter" name="places" ng-model="searchFilter.location" type="text" placeholder="Type your location" class="form-control" vs-google-autocomplete ng-blur="getLocation(searchFitler.location)" >

                        <!-- SEARCH LOCATION RESULTS -->
                        <div class="search-location-result" style="height: 90px;overflow: hidden">
                          <p class="text-right">Number of posts</p>
                          <p ng-repeat="result in search_result | filter:searchFilter" class="text-left result" hidden>
                            <a href="" ng-click="get_summary()">{{result}}
                              <span class="badge float-right">50</span>
                            </a>
                          </p>
                        </div>
                        
                        <button id="view-more-result-btn" class="btn btn-default">View More</button>
                      </div>
                      <div class="two">
                        <button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                        <button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
                        <button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
                      </div>
                      
                    </div>
                    <div class="duration-section">
                      <h4>Duration</h4>
                      <div class="form-group">
                        <label>From</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>To</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-inline">
                        <button class="btn btn-default">Apply</button>
                        <button class="btn btn-default">Clear</button>
                      </div>
                    </div>
                    <div class="networks-section">
                      <h4>Networks</h4>
                      <p class="text-right">Number of posts</p>
                      <p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
                      <p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
                      <p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 no-padding" style="margin-bottom: 50px;">
                  <div id="map" style="min-height: 762px;width: 100%"></div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="brands">
                <form>
                  <div class="form-inline">
                    <label>What Brand do you want to analyze ? </label>
                    <input type="text" class="form-control">
                  </div>
                </form>
              </div>
              <div class="white-space-20"></div>
              <button id="search-explore-btn" ng-click="brand_explore();segment()" data-dismiss="modal" class="btn btn-default" style="position: absolute;bottom: 0;right: 10px">Explore</button>
            </div>
            <div class="white-space-20"></div>
          </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
</div>



<script type="text/javascript">
  // $('.left-bar ul li .collapse').collapse('hide');

  // $('.left-bar ul li.drop > a').click(function(){
  //   $(this).toggleClass('active');
  // });
</script>