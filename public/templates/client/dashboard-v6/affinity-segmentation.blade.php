<div class="col-md-12 mockup-v3" style="overflow: hidden; margin-bottom:20px; padding-bottom:20px;" travelers-graph>

  <div class="col-md-2 left-bar no-padding">
    <ul class="nav">
      <h4>Filters</h4>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav1" aria-expanded="false">Affinity <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav1">
            <li><a href="" class="icon-right">Travelers <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav2" aria-expanded="false">Locations <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav2">
            <li><a href="" class="icon-right">All Locations <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav3" aria-expanded="false">Content Themes <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav3">
            <li><a href="" class="icon-right">Air Travel <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Shopping <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav4" aria-expanded="false">Sharer Profile <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav4">
            <li><a href="" class="icon-right">Influencers <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Micro Influencers <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Joe Pub <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav5" aria-expanded="false">Gender <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav5">
            <li><a href="" class="icon-right">Male <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Female <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">Both <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
      <li class="drop">
        <a href="" class="icon-right" data-toggle="collapse" data-target="#drop-nav6" aria-expanded="false">Age Group <i class="fa fa-caret-right"></i></a>
          <ul class="nav collapse" id="drop-nav6">
            <li><a href="" class="icon-right">10 - 20 <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">30 - 40 <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="icon-right">All <i class="fa fa-minus"></i></a></li>
            <li><a href="" class="text-center"><i class="fa fa-plus"></i></a></li>
          </ul>
      </li>
    </ul>
  </div>

  <div class="col-md-10 content_right">
    <div class="header-container">
      <div class="row_heading row"> 
        <h1>Traveler's Report <span>90K users</span></h1>
      </div>
      <div class="row_links row">
        <ul class="link_list">
          <li class="active"><a href="" ng-click="next_tab(1)">Analytics</a><div class="caret-up" style="left: 45%;display: block"></div></li>
          <li><a href="" ng-click="next_tab(2)">Location</a><div class="caret-up" style="left: 45%"></div></li>
          <li><a href="" ng-click="next_tab(3)">Audience</a><div class="caret-up" style="left: 45%"></div></li>
          <li class="pull-right " >
            <select class="pull-right location-section" style="position: relative;font-size: 13px;top: 10px;right: 35%;width: 200px;height: 35px;" hidden>
              <option>Changi Airport</option>
              <option>Marina Bay Sands</option>
              <option>ION Orchard</option>
            </select>
          </li>
        </ul>
      </div>
    </div>
    
    <div class="row_contents">
      <div class="row">
        <div class="col-md-6">
          <div class="panel-box">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <h4>Audience Insight</h4>
                </div>
              </div>

              <div class="col-md-12 panel_data">
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center ">
                  <h4 class="text-center" style="font-size:40px;">97K</h4>
                  <p>People Posting</p>
                </div>
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                  <div id="age-pie-graph" style="text-align: center;">
                    <span id="age-icon" style="position: absolute;margin-top: 20px;margin-left: 23px;font-size: 20px;">
                      <i id="female-icon" class="fa fa-female" style="font-weight: 600"></i>
                      <i id="male-icon" class="fa fa-male" style="font-weight: 600;display: none"></i>
                    </span>
                  </div>
                  <p>Female <span>60%</span></p>
                </div>
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                  <div id="average-bar-graph" style="position: absolute;top: -100px;left: -45px;">
                    <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 93px;">20-30</p>
                    <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 94px;">Age</p>
                  </div>
                  
                </div>
                <div class="panel_data_graph col-md-3 col-sm-6 col-xs-12 no-padding text-center">
                  <div id="influ-bar-graph" style="position: absolute;top: -100px;left: -55px;">
                    <p style="font-weight: 600;font-size: 11px;position: absolute;bottom: 50px;left: 95px;">0 - 1K</p>
                    <p style="font-weight: 500;font-size: 14px;position: absolute;bottom: 30px;left: 80px;">Influence</p>
                  </div>
                  <p></p>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 summary-section" style="padding-right: 5px">
              <div class="panel-box">
                <div class="row">
                  <!-- Locations -->
                  <div class="col-md-12 panel-content">
                    <div class="panel_title" style="padding: 10px 5px;">
                      <h4>Locations <small class="pull-right">Posts</small></h4>
                    </div>
                    <div class="panel_data" style="margin-bottom: 1px;">
                      <div class="data_row">
                        <div class="icon-marker">
                          <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="location-name">
                          <span>Changi Aiport</span>
                        </div>
                        <div class="counter">
                          80K
                        </div>
                      </div>
                      <div class="data_row">
                        <div class="icon-marker">
                          <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="location-name">
                          <span>Marina Bay Sands</span>
                        </div>
                        <div class="counter">
                          7K
                        </div>
                      </div>
                      <div class="data_row">
                        <div class="icon-marker">
                          <i class="fa fa-map-marker"></i>
                        </div>
                        <div class="location-name">
                          <span>Sentosa</span>
                        </div>
                        <div class="counter">
                          2K
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6 location-section" style="padding-right: 5px" hidden>
              <div class="panel-box">
                <div class="row">
                  <!-- Keyword Cloud -->
                  <div class="col-md-12 panel-content">
                    <div class="panel_title" style="border-bottom: none">
                      <ul class="nav nav-tabs" role="tablist" style="border: none;">
                        <li role="presentation" class="active"  style="border-right:1px solid #EEE;">
                          <a href="" data-target="#key-cloud" aria-controls="key-cloud" role="tab" data-toggle="tab" style="padding: 10px;">Keyword Cloud</a>
                        </li>
                        <li role="presentation">
                          <a href="" data-target="#top-key" aria-controls="top-key" role="tab" data-toggle="tab" style="padding: 10px;">Top Keywords</a>
                        </li>
                      </ul>
                    </div>
                    <div class="panel_data">
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="key-cloud" >
                          <div class="tags-wrapper2" style="border: 2px solid #EEE"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="top-key" style="margin-bottom: 3px;">
                          <div class="row_object">
                            <div class="object_name">
                              <label>Changi Airport</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                    
                                  </div>
                                <span class="">42%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#airplane</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 31%">
                                    
                                  </div>
                                  <span class="">31%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#finally</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                    
                                  </div>
                                  <span class="">15%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#travel</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                    
                                  </div>
                                  <span class="">10%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#Goals</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                                    
                                  </div>
                                  <span class="">7%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>#summer</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 3%">
                                    
                                  </div>
                                  <span class="">3%</span>
                              </div>
                            </div>  
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-md-6 " style="padding-left: 5px">
              <div class="panel-box">
                <div class="row">
                  <!-- Keyword Cloud -->
                  <div class="col-md-12 panel-content">
                    <div class="panel_title" style="border-bottom: none">
                      <ul class="nav nav-tabs" role="tablist" style="border: none;">
                        <li role="presentation" class="active"  style="border-right:1px solid #EEE;">
                          <a href="" data-target="#obj-cloud" aria-controls="obj-cloud" role="tab" data-toggle="tab">Object Cloud</a>
                        </li>
                        <li role="presentation">
                          <a href="" data-target="#top-obj" aria-controls="top-obj" role="tab" data-toggle="tab" >Top Object</a>
                        </li>
                      </ul>
                    </div>
                    <div class="panel_data">
                      <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="obj-cloud" >
                          <div class="tags-wrapper" style="border: 2px solid #EEE"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="top-obj" style="margin-bottom: 3px;">
                          <div class="row_object">
                            <div class="object_name">
                              <label>Passport</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                                    
                                  </div>
                                  <span class="">42%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Ticket</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="31" aria-valuemin="0" aria-valuemax="100" style="width: 31%">
                                    
                                  </div>
                                  <span class="">31%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Luggage</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                                    
                                  </div>
                                  <span class="">15%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Aircraft</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                                    
                                  </div>
                                  <span class="">10%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Gathering</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                                    
                                  </div>
                                  <span class="">7%</span>
                              </div>
                            </div>  
                          </div>
                          <div class="row_object">
                            <div class="object_name">
                              <label>Smiling</label>
                            </div>
                            <div class="progress_obj">
                              <div class="progress">
                                <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="3" aria-valuemin="0" aria-valuemax="100" style="width: 3%">
                                    
                                  </div>
                                  <span class="">3%</span>
                              </div>
                            </div>  
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6 no-padding-left">
          <div class="panel-box">
            <div class="row">
              <!-- Audience Insight -->
              <div class="col-md-12 panel-content">
                <div class="panel_title">
                  <h4>Affinity Wheel</h4>
                </div>
              </div>

              <div class="col-md-12 panel_data">
                <div id="persona-pie-graph" class="chart-custom" style="">
                  <ul class="nav persona-nav2">
                    <li><div></div>Travel Buff <h4>30%</h4></li>
                    <li><div></div>Luxury Shopper <h4>20%</h4></li>
                    <li><div></div>Soccer Fans <h4>20%</h4></li>
                    <li><div></div>Casual Gamers <h4>20%</h4></li>
                    <li><div></div>Technophile <h4>10%</h4></li>
                    
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  

      <div class="row summary-section">
        <div class="col-md-6">
            <div class="panel-box" style="padding: 0;">
              <div id='map_canvas' style='height:340px;width:100%;'></div>
            </div>    
            
            <!-- Timeline Affinities -->
            <div class="panel-box" >
              <div class="panel_title">
                <h4>Timeline for Affinities
                <span class="pull-right">
                  <select style="font-size: 13px;">
                    <option>7 Day</option>
                  </select>
                  <select style="font-size: 13px;">
                    <option>All Affinities</option>
                  </select>
                </span>
                
                </h4>
              </div>
              <div class="panel_data" style="position: relative;min-height: 331px;">
                <div id="affinity-graph" class=" text-center" style="position: absolute;top: -65px;left: -75px">
                  <ul class="nav navbar-inline affinity-graph" style="position: absolute;top:70px;left: 80px;text-align: left;font-size: 10px">
                    <li><div></div>Travel Buffs</li>
                    <li><div></div>Luxury Shoppers</li>
                    <li><div></div>Soccer Fans</li>
                    <li><div></div>Casual Gamers</li>
                    <li><div></div>Technophiles</li>
                  </ul>
                </div>
              </div>
            </div>    

            <!-- Timeline Trending Themes -->
            <div class="panel-box">
              <div class="panel_title">
                <h4>Timeline for Trending Themes
                  <span class="pull-right">
                    <select style="font-size: 13px;">
                      <option>7 Day</option>
                    </select>
                    <select style="font-size: 13px;">
                      <option>All Themes</option>
                    </select>
                  </span>
                </h4>
              </div>
              <div class="panel_data" style="position: relative;min-height: 331px;">
                <div id="trending-graph" class=" text-center" style="position: absolute;top: -65px;left: -75px">
                  <ul class="nav navbar-inline affinity-graph" style="position: absolute;top:70px;left: 80px;text-align: left;font-size: 10px">
                    <li><div></div>Travel Buffs</li>
                    <li><div></div>Luxury Shoppers</li>
                    <li><div></div>Soccer Fans</li>
                    <li><div></div>Casual Gamers</li>
                    <li><div></div>Technophiles</li>
                  </ul>
                </div>
              </div>
            </div>  
        </div>
        <div class="col-md-6 no-padding-left">
          <div class="panel-box">
            <div class="panel_title">
              <h4>Changi Airport &nbsp; <i class="fa fa-map-marker"></i></h4>
            </div>
            <div class="panel_data">
              <div class="row" style="margin: 0;border: 1px solid #BFBFBF;background: #F2F2F2;">
                <div class="col-md-3 box-content text-center">
                  <div class="title-sm">Total Post Shared</div>
                  <div class="counter-num">1,000</div>
                </div>
                <div class="col-md-3 box-content text-center">
                  <div class="title-sm">7 Day Changed</div>
                  <div class="counter-num"><i class="fa fa-caret-up" style="color: #92D050"></i> 8%</div>
                </div>
                <div class="col-md-3 box-content text-center">
                  <div class="title-sm">Total People</div>
                  <div class="counter-num">500</div>
                </div>
                <div class="col-md-3 box-content text-center">
                  <div class="title-sm">7 Day Changed</div>
                  <div class="counter-num"><i class="fa fa-caret-up" style="color: #92D050"></i> 2%</div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-7 box-2-content no-padding-right">
                  <div class="title-box">
                    <h5><strong>Affinities</strong></h5>
                  </div>
                  <div class="content-affinity">
                    <div id="affinity-pie-graph" class="chart-custom" style="">
                      <ul class="nav affinity-nav">
                        <li><div></div>Travel Buff <h5>35%</h5></li>
                        <li><div></div>Luxury Shopper <h5>10%</h5></li>
                        <li><div></div>Soccer Fans <h5>20%</h5></li>
                        <li><div></div>Casual Gamers <h5>10%</h5></li>
                        <li><div></div>Technophile <h5>20%</h5></li>
                        
                      </ul>
                    </div>
                  </div>
                </div>
                <div class="col-md-5 box-2-content no-padding-left">
                  <div class="title-box">
                    <h5><strong>Trending Themes</strong></h5>
                  </div>
                  <div class="content-affinity" >
                    <div class="row_object2" style="margin-top: 10px;">
                      <div class="object_name">
                        <label>Passport</label>
                      </div>
                      <div class="progress_obj">
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%">
                              
                            </div>
                            <span class="">42%</span>
                        </div>
                      </div>  
                    </div>
                    <div class="row_object2">
                      <div class="object_name">
                        <label>Luggage</label>
                      </div>
                      <div class="progress_obj">
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="15" aria-valuemin="0" aria-valuemax="100" style="width: 15%">
                              
                            </div>
                            <span class="">15%</span>
                        </div>
                      </div>  
                    </div>
                    <div class="row_object2">
                      <div class="object_name">
                        <label>Aircraft</label>
                      </div>
                      <div class="progress_obj">
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100" style="width: 10%">
                              
                            </div>
                            <span class="">10%</span>
                        </div>
                      </div>  
                    </div>
                    <div class="row_object2">
                      <div class="object_name">
                        <label>Sunflowers</label>
                      </div>
                      <div class="progress_obj">
                        <div class="progress">
                          <div class="progress-bar progress-bar-striped progress-bar-success active" role="progressbar" aria-valuenow="7" aria-valuemin="0" aria-valuemax="100" style="width: 7%">
                              
                            </div>
                            <span class="">7%</span>
                        </div>
                      </div>  
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="panel-box">
            <div class="panel_title">
              <h4>Post Stream 
                <select class="pull-right" style="position:relative;font-size:13px;">
                  <option>Most Recent</option>
                  <option>Latest</option>
                </select>
              </h4>
            </div>
            <div class="panel_data">
              <div class="post_list_wrapper">
                <div class="row_post row">
                  <div class="col-md-12 post-heading">
                    <div class="image-circle">
                      <img src="img/predict_gallery_images/1.jpg" class="img-circle" width="40">
                    </div>
                    <div class="profile-info">
                      <label>juhigodambe</label>
                      <small>14h ago</small>
                    </div>
                    <div class="tag-lists">
                      <button class="tag btn btn-green">Travel Buff</button>
                      <button class="tag btn btn-blue">Luggage</button>
                    </div>
                  </div>  
                  <div class="col-md-12 post-content">
                    <div class="row">
                      <div class="col-md-3 post-img">
                        <img src="img/predict_gallery_images/1.jpg" class="fullwidth">
                      </div>
                      <div class="col-md-9 post-article">
                        <article>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. 
                          
                          </p>
                        </article>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row_post row">
                  <div class="col-md-12 post-heading">
                    <div class="image-circle">
                      <img src="img/predict_gallery_images/1.jpg" class="img-circle" width="40">
                    </div>
                    <div class="profile-info">
                      <label>juhigodambe</label>
                      <small>14h ago</small>
                    </div>
                    <div class="tag-lists">
                      <button class="tag btn btn-green">Travel Buff</button>
                      <button class="tag btn btn-blue">Luggage</button>
                    </div>
                  </div>  
                  <div class="col-md-12 post-content">
                    <div class="row">
                      <div class="col-md-3 post-img">
                        <img src="img/predict_gallery_images/1.jpg" class="fullwidth">
                      </div>
                      <div class="col-md-9 post-article">
                        <article>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. 
                          
                          </p>
                        </article>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row_post row">
                  <div class="col-md-12 post-heading">
                    <div class="image-circle">
                      <img src="img/predict_gallery_images/1.jpg" class="img-circle" width="40">
                    </div>
                    <div class="profile-info">
                      <label>juhigodambe</label>
                      <small>14h ago</small>
                    </div>
                    <div class="tag-lists">
                      <button class="tag btn btn-green">Travel Buff</button>
                      <button class="tag btn btn-blue">Luggage</button>
                    </div>
                  </div>  
                  <div class="col-md-12 post-content">
                    <div class="row">
                      <div class="col-md-3 post-img">
                        <img src="img/predict_gallery_images/1.jpg" class="fullwidth">
                      </div>
                      <div class="col-md-9 post-article">
                        <article>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. 
                          
                          </p>
                        </article>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row_post row">
                  <div class="col-md-12 post-heading">
                    <div class="image-circle">
                      <img src="img/predict_gallery_images/1.jpg" class="img-circle" width="40">
                    </div>
                    <div class="profile-info">
                      <label>juhigodambe</label>
                      <small>14h ago</small>
                    </div>
                    <div class="tag-lists">
                      <button class="tag btn btn-green">Travel Buff</button>
                      <button class="tag btn btn-blue">Luggage</button>
                    </div>
                  </div>  
                  <div class="col-md-12 post-content">
                    <div class="row">
                      <div class="col-md-3 post-img">
                        <img src="img/predict_gallery_images/1.jpg" class="fullwidth">
                      </div>
                      <div class="col-md-9 post-article">
                        <article>
                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                          consequat. 
                          
                          </p>
                        </article>
                      </div>
                    </div>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="row location-section" hidden>
        <div class="col-md-6">
          <div class="panel-box">
            <div class="panel_title">
              <h4>Top 5 Trending Themse</h4>
            </div>
            <div class="panel_data">
              <div class="col-md-6 text-center">
                <div class="white-space-20"></div>
                <div class="white-space-15"></div>
                <p><b>Passport and Ticket</b></p>
                <p><b>People and Ticket</b></p>
              </div>
              <div class="col-md-6 text-center">
                <h4><b>Posts</b></h4>
                <p>15k</p>
                <p>10K</p>
              </div>
            </div>
          </div>

          <!-- Moments Gallery -->
          <div class="panel-box" style="min-height: 447px">
            <div class="panel_title">
              <h4>Moments Gallery</h4>
            </div>
            <div class="panel_data">
              <div class="row">
                <div class="col-md-6 collage-col">
                  <div class="gallery-title22">
                    <h5>Passport and Ticket <span class="pull-right" style="font-size:12px;line-height:18px;"><i class="fa fa-photo"></i> 3K</span> </h5>
                  </div>
                  <div class="imgs-collage2">
                    <img src="img/predict_gallery_images/1.jpg">
                    <img src="img/predict_gallery_images/1.jpg">
                    <img src="img/predict_gallery_images/1.jpg">
                    <img src="img/predict_gallery_images/1.jpg">
                  </div>
                </div>
                <div class="col-md-6 collage-col">
                  <div class="gallery-title22">
                    <h5>Passport and Ticket <span class="pull-right" style="font-size:12px;line-height:18px;"><i class="fa fa-photo"></i> 3K</span> </h5>
                  </div>
                  <div class="imgs-collage2">
                    <img src="img/predict_gallery_images/1.jpg">
                    <img src="img/predict_gallery_images/1.jpg">
                    <img src="img/predict_gallery_images/1.jpg">
                    <img src="img/predict_gallery_images/1.jpg">
                  </div>
                </div>
              </div>
            </div>
          </div>  


        </div>
        <div class="col-md-6 no-padding-left">
          <div class="panel-box">
            <div class="panel_title">
              <h4>Moments Insight</h4>
            </div>
            <div class="panel_data text-center">
              <div class="col-md-6">
                <div id="passport-pie-graph" style="text-align: center;">
                  <span id="age-icon" style="position: absolute;margin-top: 19px;margin-left: 18px;font-size: 20px;">
                    <!-- <i id="user-icon" class="fa fa-user" style="font-weight: 600"></i> -->
                    <img src="img/tickets.png" style="width: 25px">
                  </span>
                </div>
                <p>Passport and Ticket</p>
              </div>
              <div class="col-md-6">
                <div id="people-pie-graph" style="text-align: center;">
                  <span id="age-icon" style="position: absolute;margin-top: 20px;margin-left: 23px;font-size: 20px;">
                    <i id="user-icon" class="fa fa-user" style="font-weight: 600"></i>
                  </span>
                </div>
                <p>People with Ticket</p>
              </div>
            </div>
          </div>

          <div class="panel-box" >
            <div class="panel_title">
              <h4>People</h4>
            </div>
            <div class="list-panel">
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>
              <div class="row_list">
                <div class="icon-profile">
                  <div class="img-wrapper">
                    <img src="img/predict_gallery_images/1.jpg" class="img-circle">
                    <div class="desc">
                      <div class="account">
                        <strong>@skiptotheru</strong>
                      </div>
                      <div class="desc-list">
                        Loves Sports, Food, Parenting
                      </div>
                    </div>
                  </div>
                  <div class="data_cell num_data_a">
                    <span>5</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>20.2M</span>
                  </div>
                  <div class="data_cell num_data_b">
                    <span>14K</span>
                  </div>  
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>
    </div>


  </div>  
    

  
</div>

<div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" map-data>
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <label class="modal-title" id="myModalLabel">Pick your search category</label>
        </div>
        <div class="modal-body">
          <!-- Nav tabs -->
          <div class="row">
            <div class="col-md-12">
              <div class="modal-wrapper">
                <ul id="search-tabs" class="nav nav-tabs" role="tablist">
              <li role="presentation" class="active"><a href="" data-target="#segment" aria-controls="segment" role="tab" data-toggle="tab">Segment</a></li>
              <li role="presentation"><a href="" data-target="#locations" aria-controls="locations" role="tab" data-toggle="tab" ng-click="runMap()">Locations</a></li>
              <li role="presentation"><a href="" data-target="#brands" aria-controls="brands" role="tab" data-toggle="tab">Brands</a></li>
            </ul>
            <div class="white-space-20"></div>
            <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane fade in active" id="segment">
                <form>
                  <div class="form-inline">
                    <label>What Segment ? </label>
                    <input type="text" class="form-control">
                  </div>
                  
                </form>
              </div>
              <div role="tabpanel" class="capitaland2 tab-pane fade" id="locations" >
                <div class="col-md-3 no-padding">
                  <div class="left-bar">
                    <div class="locations-section">
                      <h4>Locations</h4>
                      <div class="one text-center">
                        
                        <!-- SEARCH FILTER -->
                        <input id="searchFilter" name="places" ng-model="searchFilter.location" type="text" placeholder="Type your location" class="form-control" vs-google-autocomplete ng-blur="getLocation(searchFitler.location)" >

                        <!-- SEARCH LOCATION RESULTS -->
                        <div class="search-location-result" style="height: 90px;overflow: hidden">
                          <p class="text-right">Number of posts</p>
                          <p ng-repeat="result in search_result | filter:searchFilter" class="text-left result" hidden>
                            <a href="" ng-click="get_summary()">{{result}}
                              <span class="badge float-right">50</span>
                            </a>
                          </p>
                        </div>
                        
                        <button id="view-more-result-btn" class="btn btn-default">View More</button>
                      </div>
                      <div class="two">
                        <button class="btn btn-default"><i class="fa fa-refresh"></i> Refresh</button>
                        <button class="btn btn-default"><i class="fa fa-cog"></i> Options</button>
                        <button class="btn btn-default"><i class="fa fa-plus"></i> Load More</button>
                      </div>
                      
                    </div>
                    <div class="duration-section">
                      <h4>Duration</h4>
                      <div class="form-group">
                        <label>From</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-group">
                        <label>To</label><br>
                        <input type="date" class="form-control">
                        <input type="time" class="form-control">
                      </div>
                      <div class="form-inline">
                        <button class="btn btn-default">Apply</button>
                        <button class="btn btn-default">Clear</button>
                      </div>
                    </div>
                    <div class="networks-section">
                      <h4>Networks</h4>
                      <p class="text-right">Number of posts</p>
                      <p><i class="fa fa-twitter"></i> Twitter <span class="badge">50</span></p>
                      <p><i class="fa fa-instagram"></i> Instagram <span class="badge">50</span></p>
                      <p><i class="fa fa-facebook-square"></i> Facebook <span class="badge">50</span></p>
                    </div>
                  </div>
                </div>

                <div class="col-md-9 no-padding" style="margin-bottom: 50px;">
                  <div id="map" style="min-height: 762px;width: 100%"></div>
                </div>
              </div>
              <div role="tabpanel" class="tab-pane fade" id="brands">
                <form>
                  <div class="form-inline">
                    <label>What Brand do you want to analyze ? </label>
                    <input type="text" class="form-control">
                  </div>
                </form>
              </div>
              <div class="white-space-20"></div>
              <button id="search-explore-btn" ng-click="brand_explore();segment()" data-dismiss="modal" class="btn btn-default" style="position: absolute;bottom: 0;right: 10px">Explore</button>
            </div>
            <div class="white-space-20"></div>
          </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">
  $('.left-bar ul li .collapse').collapse('hide');

  $('.left-bar ul li.drop > a').click(function(){
    $(this).toggleClass('active');
  });

</script>