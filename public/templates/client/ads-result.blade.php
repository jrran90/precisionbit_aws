
<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
    <section style="font-size: 20px!important;" navigation-campaign>
        <ol class="pull-right breadcrumb wow slideInRight" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 10px; background-color: #E7EAEF;margin: 0;">
          <li>
            <a ui-sref="create-brief" class="btn btn-medium custom-button"> <i class="icon-create-brief"></i> Create Brief</a>
          </li>
        </ol>
    </section>
</div>
<div class="row wow slideInLeft" id="pop-sidebar" data-wow-duration="0.5s" data-wow-delay="0.5s">
  <div class="col-md-4 sidebar-reports-container remove-side-padding">
    <div class="sidebar-menu-wrapper overflow">
      <ul class="list-unstyled overflow">
        <li>
          <div class="heading-list pull-left">
            <h2>Report </h2>
            <small>Choose Report that you want</small>
          </div>
          <div class="pull-right">
            <span style="background: #602A8A;padding: 7px;font-size: 15px;position: relative;top: -6px;margin-right: 25px;">Coming Soon</span>
            <a href="#" class="btn-close">&times;</a>
          </div>
        </li>
        <li>
         <!-- ui-sref="campaign-result" -->
          <a>
          <div class="list-content">
            <div class="list-icon">
              <span class="icon-campaign-report" style="font-size: 50px;"></span>
            </div>
            <div class="list-title">
              <h4>Campaign Report</h4>
              <small>Adhero run A/B test for your campaign</small>
            </div>
          </div>
          </a>
        </li>
        <li>
        <!-- ui-sref="winning-report" -->
          <a>
          <div class="list-content">
            <div class="list-icon">
             <span class="icon-winning-ads" style="font-size: 50px;"></span>
             </div>
            <div class="list-title">
              <h4>Winning Ads Report</h4>
              <small>After test finish, winning Ads performance can be tracked</small>
            </div>
          </div>
          </a>
        </li>
      </ul>      
    </div>
    

  </div>

  <div class="col-md-8 right-content-container">

  </div>
</div> 