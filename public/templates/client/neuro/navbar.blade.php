
  <!-- Sidebar toggle button-->
  <!-- <a href="#" class="sidebar-toggle custom-side-bar-toggle" data-toggle="offcanvas" role="button" style="color: #28A6A0;">
    <span class="sr-only">Toggle navigation</span>
  </a> -->
  <div class="navbar-logo">
    <ul class="nav navbar-nav">
      <li>
        <!-- Logo -->
        <a href="/dashboard" style="padding: 2px 15px !important;" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini">PB</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg log-text logo-container"><img src="../img/logo/PBit logo white transparent.png"></span>
        </a>
      </li>
    </ul>
  </div>

  <div class="navbar-custom-menu">
    <ul class="nav navbar-nav ul-borders">
      <li class="dropdown">
        <a ui-sref="home" class="menu-header-global home-a dropdown-toggle active-menu" data-toggle="dropdown">
          <span class="glyphicon glyphicon-home fa-icon" ></span>
        </a>
      </li>
      <li class="dropdown">
        <a ui-sref="predict" class="menu-header-global predict-a dropdown-toggle" data-toggle="dropdown">
          <span class="icon-microscope" style="color: #fff"></span>
        </a>
      </li>
      <li class="dropdown">
        <a ui-sref="location" class="menu-header-global pin-a dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-map-marker fa-icon" style="color: #fff"></span>
        </a>
      </li>
      <li class="dropdown">
        <a ui-sref="influencers" class="menu-header-global user-a dropdown-toggle" data-toggle="dropdown">
          <span class="glyphicon glyphicon-user fa-icon" style="color: #fff"></span>
        </a>
      </li>
    </ul>
    <ul class="nav navbar-nav navi-right">
      <!-- Messages: style can be found in dropdown.less-->
        <li class="dropdown messages-menu">
          <a href="javascript:void(0)" class="menu-header-global settings-a dropdown-toggle" data-toggle="dropdown" style="overflow:hidden">
            <!-- <i class="icon-gear custom-glyphicon-cog" style="color: #fff"></i> -->
            <div class="pull-left user-profile-img" style="border: 2px solid #fff;border-radius:50%">
              <img src="{{ asset( 'img/avatar5.png' ) }}" class="img-circle img-responsive" width="29">
            </div>
            <i class="fa fa-caret-down pull-right" style="font-size:18px;position:relative;top:8px;margin-left:5px;"></i>
          </a>
          <ul class="dropdown-menu wow fadeInUp" style="top:54px;background: #0E9F8F;border:#283342;padding:26px 0px 0px;" data-wow-delay="0.5s" data-wow-duration="0.5s">
            <li>
              <div class="profile-pic-wrapper text-center wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s">
                  <img src="../../img/avatar5.png" class="img-circle img-responsive" width="145" style="width:145px;border: 2px solid #fff;margin:auto;">
                 <!--  <p class="user-name">Signed in as <span>{{ ucfirst(Auth::user()->firstname)}}&nbsp;{{ucfirst(Auth::user()->lastname)}}</span></p> -->
              </div>
            </li>
           <!--  <li role="separator" class="divider"></li> -->
              <div class="exit-bar">
                <a href="#" class="pull-left wow fadeInUp" style="display:inline-block;clear:none;font-size:21px;background:transparent!important;" data-wow-delay="0.6s" data-wow-duration="0.6s">
                  <i class="icon-settings custom-icon-style"></i>  
                </a>
                <a href="/logout" class="pull-right wow fadeInUp" style="display:inline-block;clear:none;font-size:21px;background:transparent!important;" data-wow-delay="0.7s" data-wow-duration="0.7s">
                  <i class="icon-exit custom-icon-style"></i>  
                </a>
              </div>
          </ul>
            <!-- <li> -->
            <!-- </li> -->
        </li>
    </ul>

  </div>