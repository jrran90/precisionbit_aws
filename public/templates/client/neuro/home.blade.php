<div class="col-md-12" style="padding: 30px 0 200px 0; margin-bottom: 20px;">
	<div class="col-md-12 no-padding">
		<div class="icon-container">
			<div ui-sref="predict" class="icon-box-container wow fadeInUp" data-wow-delay="0.5s" data-wow-duration="0.5s">
				<div class="icon-info-container">
					<i class="icon-file"></i>
					<p>Predict</p>
				</div>
			</div>
			<div ui-sref="location" class="icon-box-container wow fadeInUp" data-wow-delay="0.7s" data-wow-duration="0.7s">
				<div class="icon-info-container">
					<i class="icon-location-pin"></i>
					<p>Location</p>
				</div>
			</div>
			<div ui-sref="influencers" class="icon-box-container wow fadeInUp" data-wow-delay="0.9s" data-wow-duration="0.9s">
				<div class="icon-info-container">
					<i class="icon-people"></i>
					<p>influencers</p>
				</div>
			</div>
		</div>
	</div>
</div>