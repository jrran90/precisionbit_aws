<div predict-view>
<div class="col-md-9 text-center">
	<h1 style="font-size:30px;margin-top:0;">Predict</h1>
</div>	

<div class="sub-content-wrapper col-md-9">
	<div class=" predict-view-wrapper">
		<section class="alert-msgs-box">
			<div class="alert alert-info msg msg002" id="predict-msg002">
				<p>
					<strong>
						Let us know your target customer, target country and what they are interested in.  With this input, we will identify the key themes that your audience is engaging with.
					</strong>
					<button type="button" class="close" ng-click="close('msg002')"><span aria-hidden="true">&times;</span></button>
				</p>
			</div>
			<div class="alert alert-info msg msg002" id="predict-msg003" hidden>
				<p>
					<strong>
						We are identifying the key themes that your audience is engaging with.
					</strong>
					<button type="button" class="close" ng-click="close('msg002')"><span aria-hidden="true">&times;</span></button>
				</p>
			</div>
			<!-- <div class="alert alert-info msg msg002" id="predict-msg002">
				<p>
					<strong>Predict your next campaigns performance! </strong>Give us the details below and let us do the work.
					<button type="button" class="close" ng-click="close('msg002')"><span aria-hidden="true">&times;</span></button>
				</p>
			</div> -->
			<div class="alert alert-warning msg msg01" id="predict-msg001">
				<p>
					<strong>First Time?</strong>
					Let us show you how it works.
					<button type="button" class="close" ng-click="close('msg001')"><span aria-hidden="true">&times;</span></button>
				</p>
				<button class="btn btn-sm btn-warning btn-yellow demo-btn" ng-click="useraction('demo')">Demo</button>
				<button class="btn btn-sm btn-default expert-btn" ng-click="useraction('skip')">Skip</button>
			</div>
		</section>

		<section class="loading-predict-container" hidden>
			<div class="progress">
			  <div class="progress-bar progress-bar-info progress-bar-striped" id="predict-progress"role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
			    <span class="sr-only">20% Complete</span>
			  </div>
			</div>
			<h4>Estimated time... 24 hours</h4>
			<section class="predict-quote-container">
				<div class="col-md-12" >
					<!-- <div class="col-md-2">
						<img src="../img/avatar.png" class="predict-img-qoute img-responsive">
					</div>
					<div class="col-md-10">
						<div class="panel panel-default" style="border-radius: 10px; margin-top: 10px;border-color: none;">
							<div class="panel-body predict-panel-default">
								<i class="glyphicon glyphicon-triangle-left custom-glyphicon-triangle-left-predict"></i>
								<section class="predict-qoute-text">
									<h3>"During the last 6 months, your audience has been responding to content that is "Technology"</h3>
								</section>
							</div>
						</div>
					</div> -->
				</div>
			</section>
			<section class="row text-center">
				<section class="btn-container">
					<a href="javascript:void(0)" ui-sref="home" class="btn back-to-dasboard-btn">Back to Dashboard</a>
					<a href="javascript:void(0)" class="btn demo-images-btn" ng-click="demoImages()">Proceed to Demo</a>
				</section>
			</section>
		</section>	
		<section class="content-manager">
			<div class="row">
				<div class="col-md-12">
					<div class="predict-wrapper-v3">
						<form>
							<div class="form-group">
								<h3>Product</h3>
								<select class="form-control" ng-model="predictObject.product_category" >
									<option value="Acquisition">Acquisition</option>
									<option value="Brand Awareness">Brand Awareness</option>
									<option value="Convert">Convert</option>
									<option value="Upgrade">Upgrade</option>
									<option value="Recontract">Recontract</option>
									<option value="Loyalty Expression">Loyalty Expression</option>
								</select>
								<input type="text" class="form-control" ng-model="predictObject.product_name" placeholder="Product Name">
							</div>

							<div class="form-group">
								<h3>Audience</h3>
								<div class="row">
									<div class="col-md-6">
										<input type="text" class="form-control" ng-model="predictObject.country" placeholder="Country">
									</div>
									<div class="col-md-6">
										<select ng-model="predictObject.gender" class="form-control">
											<option value="">Gender</option>
											<option value="Male">Male</option>
											<option value="Female">Female</option>
											<option value="Both">Both</option>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group">
								<h3>Objective</h3>
								<select class="form-control" ng-model="predictObject.objective">
									<option value="Clicks to Website">Clicks to Website: Send people to your website.</option>
									<option value="Website Conversions">Website Conversions: Increase conversions on your website. You'll need a conversion pixel for your website before you can create this ad.</option>
									<option value="Page Post Engagement">Page Post Engagement: Boost your posts.</option>
									<option value="Page Likes">Page Likes: Promote your Page and get Page likes to connect with more of the people who matter to you.</option>
									<option value="App Installs">App Installs: Get installs of your app.</option>
									<option value="App Engagement">App Engagement: Increase engagement in your app.</option>
									<option value="Offer Claims">Offer Claims: Create offers for people to redeem in your store.</option>
									<option value="Local Awareness">Local Awareness: Reach people near your business.</option>
									<option value="Event Responses">Event Responses: Raise attendance at your event.</option>
									<option value="Video Views">Video Views: Create ads that get more people to view a video.</option>
									<option value="Increase Sales">Increase Sales</option>
								</select>
								<div class="social-publication">
									<input type="text" class="form-control" ng-model="predictObject.mediaplatform" placeholder="Where do you want to publish?">
									<div class="social-icons">
										<a href="" ng-click="socialPlatform('facebook')"><i class="fa fa-facebook-square"></i></a>
										<a href="" ng-click="socialPlatform('instagram')"><i class="fa fa-instagram"></i></a>
									</div>
								</div>
							</div>

							<div class="form-group">
								<h3>Topics your audience are engaging with</h3>
								<textarea class="form-control" ng-model="predictObject.hashtags" placeholder="List your audience interests.." rows="5"></textarea>
							</div>

							<div class="form-group text-right">
								<button type="submit" ng-click="createPredict()" class="btn btn-success">Submit</button>
							</div>

						</form>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>