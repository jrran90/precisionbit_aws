<!-- <div class="col-md-2" style="width: 20%;">
	<div class="navigation">
		<ul class="nav">
			<div class="triangle" style="background:#000;"></div>
			<li><a ui-sref="home"><i class="fa fa-home"></i></a></li>
			<li style="background:#000;"><a ui-sref="saved-results"><i class="fa fa-flask"></i></a></li>
			<li><a ui-sref="summary"><i class="fa fa-bar-chart"></i></a></li>
		</ul>
	</div>
	<div class="main-desc-wrapper">
		<div class="experiment-description">
			<h3 style="font-size:15px;">EXPERIMENT DESCRIPTION:</h3>
			<div class="description">
				<label>Audience:</label>
				AUS, M&amp;F, Young Adults
			</div>
			<div class="description">
				<label>Channel:</label>
				Facebook 
			</div>
			<div class="description">
				<label>Season/Event:</label>
				Spring
			</div>		
		</div>
		<br>
		<div class="experiment-description">
			<p>You may edit the algorithm suggestions below:</p>
		</div>
		<br>
		<div class="text-option">
			<label>TEXT OVERLAY</label>
			<input type="checkbox" name="overlay-switch" disabled="" class="overlay-switch">
		</div>
		<p>
			Select those best for logos or text
		</p>
	</div>
</div> -->

<div class="sub-content-wrapper col-md-9" predict-result>
	<div class=" predict-view-wrapper">
	<section class="predict-result-container">			
			<div class="row text-left">
				<p>These images have elements with the strongest conversion potential out of X,000,000 data points analyzed!</p>
			</div>
			<div class="gallery-row">
				<div class="col-md-12">
					<div class="top-image-gallery">
						<div class="gallery-img">
							<div id="owl-example" class="owl-carousel">
								<div><img src="../img/predict-gallery-v2/1.png" ng-click="previewGalleryImg( '../img/predict-gallery-v2/1.png' , 1 )" class="img img-thumbnail display-image"></div>
								<div><img src="../img/predict-gallery-v2/2.png" ng-click="previewGalleryImg( '../img/predict-gallery-v2/2.png' , 2 )" class="img img-thumbnail display-image"></div>
								<div><img src="../img/predict-gallery-v2/3.png" ng-click="previewGalleryImg( '../img/predict-gallery-v2/3.png' , 3 )" class="img img-thumbnail display-image"></div>
								<div><img src="../img/predict-gallery-v2/4.png" ng-click="previewGalleryImg( '../img/predict-gallery-v2/4.png' , 4 )" class="img img-thumbnail display-image"></div>
								<div><img src="../img/predict-gallery-v2/5.jpg" ng-click="previewGalleryImg( '../img/predict-gallery-v2/5.jpg' , 5 )" class="img img-thumbnail display-image"></div>
								<div><img src="../img/predict-gallery-v2/6.png" ng-click="previewGalleryImg( '../img/predict-gallery-v2/6.png' , 6 )" class="img img-thumbnail display-image"></div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="section-contents">
				<section class="col-md-6">
					<div class="panel-thumbnail">
						<div class="imgpreview-wrapper" style="border: 1px solid #ECF0F1;padding: 15px;box-shadow: 1px 2px #FFFAFA;">
							<!-- <img src="../telstra_images/1004.jpg" class="img-rounded img-responsive"> -->
							<div class="preview-header">
								<div class="profile-img">
									<img src="../img/telstra-logo.jpg">
								</div>
								<div class="profile-info">
									<span><a href="#">Telstra</a></span>
									<span class="date_created">May 1, 2016</span>
								</div>
							</div>
							<div class="preview-body">
								<div class="body-text">
									<p>
										Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
									</p>
									<div class="img-preview-wrapper">
										<img src="{{imgPreview}}" class="img img-responsive" width="100%">
									</div>
									<div class="img-preview-description">
										<h2 class="img-preview-title">Switch today &amp; Choose</h2>
										<p>
											Choose 1GB data or $1000 of calls to stand Aus numbers, to use in Aus on eligible plans.
										</p>
									</div>
								</div>
							</div>
							<div class="preview-footer">
								<div class="preview-permalink">
									<a href="#">http://teslstra.com.au/mobile-phones/mobile-on-a-plan</a>
									<button class="showpreview-btn">Shop Now</button>
								</div>
								<hr>
								<span class="likes">81 Likes</span>
							</div>
						</div>
					</div>
				</section>
				<section class="col-md-6">
					<div class="panel-overview">
						<div class="imgpreview-wrapper text-center">
							<h3 class="headline-text">Overview</h3>
						</div>
						<div class="predict-performance">
							<div class="row">
								<div class="col-md-5">
									<span>Predicted Performance:</span>
								</div>
								<div class="col-md-7">
									<div class="progress">
										<div class="progress-bar" role="progressbar" aria-valuenow="{{galleryImageData.predicted_performance}}" aria-valuemin="0" aria-valuemax="100" style="width: {{galleryImageData.predicted_performance}}%;">
											<span ng-bind="galleryImageData.predicted_performance"></span>%
										</div>
									</div>
								</div>
							</div><br>
							<div class="row">
								<div class="col-md-5">
									<span>Content Type:</span>
								</div>
								<div class="col-md-7">
									<span ng-bind="galleryImageData.content_type"></span>
								</div>
							</div><br><br>

							<div class="row">
								<div class="col-md-12">
									<p>Image elements and their current performance vs. average</p>
								</div>
							</div>
						</div>
						<div class="emotion-element">
							<div class="color color-green">
								<div>Emotion</div>
							</div>
							<div class="color color-orange">
								<div>People</div>
							</div>
							<div class="color color-red">
								<div>Theme</div>
							</div>
							<div class="color color-blue">
								<div>Colors</div>
							</div>
							<div class="color color-violet">
								<div>Aesthetic</div>
							</div>
						</div>
						<div class="emotion-element-content text-center">
							<div class="img-wrapper ">
								<img src="../img/predict-gallery-v2/unnamed.png" class="img">
								<p ng-bind="galleryImageData.emotion"></p>
							</div>
							<div class="tags-wrapper wrapper-color wrapper-color-orange">
								<div ng-repeat="people in galleryImageData.people">
									<span class="label label-default" ng-bind="people"></span>
								</div>
							</div>
							<div class="tags-wrapper wrapper-color wrapper-color-red">
								<div ng-repeat="theme in galleryImageData.theme">
									<span class="label label-default" ng-bind="theme"></span>
								</div>
							</div>
							<div class="tags-wrapper wrapper-color wrapper-color-blue">
								<div>
									<span class="label label-default" ng-bind="galleryImageData.colours"></span>
								</div>
							</div>
							<div class="tags-wrapper wrapper-color wrapper-color-violet">
								<div>
									<span class="label label-default" ng-bind="galleryImageData.aesthetic"></span>
								</div>
							</div>
						</div>

						<div class="points-wrapper">
							<div class="points">
								<p>+<span ng-bind="galleryImageData.emotion_val"></span>%</p>
							</div>
							<div class="points">
								<p>+<span ng-bind="galleryImageData.people_val"></span>%</p>
							</div>
							<div class="points">
								<p>+<span ng-bind="galleryImageData.theme_val"></span>%</p>
							</div>
							<div class="points">
								<p>+<span ng-bind="galleryImageData.colours_val"></span>%</p>
							</div>
						</div>

					</div>
				</section>
			</div>
		</section>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$("#owl-example").owlCarousel();
		$("[name='overlay-switch']").bootstrapSwitch( 'size' , 'mini' );
	});
</script>