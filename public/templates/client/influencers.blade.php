<!-- <div class="col-md-12" style="padding: 15px 55px;">
	<div class="col-md-12">
		<div class="button-opt text-center" style="margin-top:30px;">
			<button id="influencers" class="btn btn-lg btn-theme active" ng-click="actionView( 'influencers' )">Influencers</button>
			<button id="audiences" class="btn btn-lg btn-theme" ng-click="actionView( 'audiences' )">Audiences</button>
		</div>
		<div class="option-bar overflow" ng-if="defaultView" style="margin-top:30px;">
			<span class="pull-left eng-title">Your Influencers</span>
			<div class="dropdown-option pull-right">
				View By: 
				<select>
					<option value="Followers">Followers</option>
					<option value="Option 1">Option 1</option>
					<option value="Option 2">Option 2</option>
				</select>
			</div>
		</div>
		<br>

		<div class="image-list" style="margin-top:30px;text-align:center;" ng-if="defaultView">
			<div class="col-md-1"></div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/1">
					<img src="../img/pbit-assets-v4/Amanda Wong.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Amanda Wong</span>
					</div>
				</a>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/2">
					<img src="../img/pbit-assets-v4/Andrea Chong.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Andrea Chong</span>
					</div>
				</a>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/3">
					<img src="../img/pbit-assets-v4/Bella Koh.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Bella Koh</span>
					</div>
				</a>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/4">
					<img src="../img/pbit-assets-v4/Nellie Lim.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Nellie Lim</span>
					</div>
				</a>
			</div>
			<div class="col-md-2 col-sm-4 col-xs-6">
				<a href="#/influencer-info/5">
					<img src="../img/pbit-assets-v4/Nicole Wong.jpg" class="img-responsive fullwidth" style="box-shadow: 1px 2px 1px #BBB;">
					<div class="img-detail text-center">
						<span class="name">Nicole Wong</span>
					</div>
				</a>
			</div>
			<div class="col-md-1"></div>
		</div>

		<div class="image-list" style="margin-top:30px;overflow: hidden;background: #fff;padding: 30px 15px;box-shadow: 1px 2px 1px #BBB;" ng-if="!defaultView">
			<div class="col-md-4">
				<div class="col-md-12 text-center">
					<label>Audience Affinity</label> <br>
					<div id="doughnut-graph" style="margin-left: -40px; width: 100%; margin-top: -80px;"></div>
				</div>
				<div class="col-md-12" style="position:relative;top:-50px;">
					<div class="col-md-12">
						<h4 style="color: #9E9FA4;">Theme</h4>
						<table class="table custom-table">
							<tbody>
								<tr ng-repeat="list in labels">
									<td><span class="legend-box" style="background: {{ list.color }};"></span> <span ng-bind="list.label" style="margin-left: 10px;"></span></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-8">
				<div class="table-responsive">
					<table class="table audiences">
						<thead>
							<tr>
								<th>Ranking</th>
								<th>Username</th>
								<th>Followers</th>
								<th>Posts</th>
								<th>Affinities</th>
								<th>Other Interests</th>
							</tr>
						</thead>
						<tbody >
							<tr ng-repeat="profile in profileInfo">
								<td>
									<span ng-bind="$index + 1"></span> &nbsp;
									<img src="{{profile.img_url}}" alt="no image" class="audience-img">
								</td>
								<td>
									<span ng-bind="profile.username"></span>
								</td>
								<td>
									<span ng-bind="profile.followers"></span>
								</td>
								<td>
									<span ng-bind="profile.posts"></span>
								</td>
								<td>
									<span ng-bind="profile.affinities"></span>
								</td>
								<td>
									<span ng-bind="profile.other_int"></span>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div> -->

<div class="col-md-12 influencers-v2" influencers>
	<span id="audiences" hidden>false</span>
	<div class="col-md-4 col-sm-12 col-xs-12">
		<div class="col-md-12 no-padding">
			<div style="height:40px;"></div>
		</div>

		<div class="col-md-12 no-padding " style="margin-top: 20px">
			<table class="table table-striped table-responsive" >
				<thead>
					<tr>
						<th width="5%" style="text-align:center;border-radius: 4px 0 0 0;">No</th>
						<th width="70%">Topic/Location</th>
						<th style="text-align:center;border-radius: 0 4px 0 0;">Date</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td align="center">1</td>
						<td>
							<p><i class="glyphicon glyphicon-map-marker table-icon"></i> Kidzania</p>
							<!-- <p><i class="table-icon table-icon-hash"><img src="img/hash.png" style="width: 12px;height: 12px"></i> hash1, hash2, hash3</p> -->
						</td>
						<td align="center">05-03-16</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="col-md-8 col-sm-12 col-xs-12">
		<div class="col-md-12 no-padding-right">
			<div class="col-md-12 no-padding">
				<h4>Influencers</h4>
			</div>

			<div class="col-md-12 no-padding " style="margin-top: 20px">
				<ul class="nav nav-pills">
				  <li id="top" class="active"><a href="" ng-click="changeTab('top')">Top</a></li>
				  <li id="mostActive"><a href="" ng-click="changeTab('active')">Most Active</a></li>
				  <li id="new" ><a href="" ng-click="changeTab('new')">New</a></li>
				</ul>
			</div>

			<div id="tab1" class="col-md-12 no-padding influencer-content-wrapper" style="margin-top: 10px" >
				<div ng-repeat="influencer in influencer_list" class="col-md-4 col-sm-12 col-xs-12 influencer">
					<div class="ig-wrapper">
						<div class="ig-img" style="background: url('{{influencer.recent}}') center no-repeat; background-size: 100%">
							<!-- <img src="img/location-gallery/1.jpg"> -->
						</div>
						<div class="ig-pp">
							<img src="{{influencer.profile_pic}}">
							<p class="ig-name">@{{influencer.username}}</p>
						</div>
						<div class="details">
							<div class="col-md-12 stats">
								<div class="col-md-4">
									<p>Posts Here</p>
									<p>{{influencer.posts}}</p>
								</div>
								<div class="col-md-4">
									<p>Followers</p>
									<p>{{influencer.followers}}</p>
								</div>
								<div class="col-md-4" hidden>
									<p>Avg.Engagement</p>
									<!-- <p>{{influencer.following}}</p> -->
									<p>15%</p>
								</div>
							</div>

							<div class="col-md-12 no-padding">
								<div class="col-md-12">
									<p style="color: #333;font-weight: 600">Interests</p>
								</div>
								<div class="col-md-7">
									<div class="tags">
										Cars
									</div>
									<div class="tags">
										Food
									</div>
									<div class="tags">
										Travel
									</div>
								</div>
								<!-- <div class="col-md-7">
									<div ng-repeat="tag in influencer.affinities"class="tags">
										{{tag}}
									</div>
								</div> -->
								<div class="col-md-5 no-padding" style="">
									<!-- <p class="my-audiences"><a href="" ui-sref="audiences"><i class="fa fa-users"></i>My Audiences</a></p> -->
								</div>
							</div>
						</div>
					</div>
				</div>

				<!-- <div class="col-md-4 col-sm-12 col-xs-12 influencer">
					<div class="ig-wrapper">
						<div class="ig-img" style="background: url('../img/location-gallery/3.jpg') no-repeat; background-size: 100%">
							<img src="img/location-gallery/1.jpg">
						</div>
						<div class="ig-pp">
							<img src="img/location-gallery/User_Pics/3.jpg">
							<p class="ig-name">@skiptotheru</p>
						</div>
						<div class="details">
							<div class="col-md-12 stats">
								<div class="col-md-4">
									<p>Posts Here</p>
									<p>5</p>
								</div>
								<div class="col-md-4">
									<p>Followers</p>
									<p>20.2M</p>
								</div>
								<div class="col-md-4">
									<p>Avg.Engagement</p>
									<p>14K</p>
								</div>
							</div>

							<div class="col-md-12 no-padding">
								<div class="col-md-12">
									<p style="color: #333;font-weight: 600">Interests</p>
								</div>
								<div class="col-md-7">
									<div class="tags">
										Cars
									</div>
									<div class="tags">
										Food
									</div>
									<div class="tags">
										Travel
									</div>
								</div>
								<div class="col-md-5 no-padding" style="">
									<p class="my-audiences"><a href="" ui-sref="audiences"><i class="fa fa-users"></i>My Audiences</a></p>
								</div>
							</div>
						</div>
					</div>
				</div> -->

				<!-- <div class="col-md-4 col-sm-12 col-xs-12 influencer">
					<div class="ig-wrapper">
						<div class="ig-img" style="background: url('../img/location-gallery/4.jpg') no-repeat; background-size: 100%">
							<img src="img/location-gallery/1.jpg">
						</div>
						<div class="ig-pp">
							<img src="img/location-gallery/User_Pics/4.jpg">
							<p class="ig-name">@Sittard-Geleen</p>
						</div>
						<div class="details">
							<div class="col-md-12 stats">
								<div class="col-md-4">
									<p>Posts Here</p>
									<p>8</p>
								</div>
								<div class="col-md-4">
									<p>Followers</p>
									<p>20M</p>
								</div>
								<div class="col-md-4">
									<p>Avg.Engagement</p>
									<p>14K</p>
								</div>
							</div>

							<div class="col-md-12 no-padding">
								<div class="col-md-12">
									<p style="color: #333;font-weight: 600">Interests</p>
								</div>
								<div class="col-md-7">
									<div class="tags">
										Cars
									</div>
									<div class="tags">
										Food
									</div>
									<div class="tags">
										Travel
									</div>
								</div>
								<div class="col-md-5 no-padding" style="">
									<p class="my-audiences"><a href="" ui-sref="audiences"><i class="fa fa-users"></i>My Audiences</a></p>
								</div>
							</div>
						</div>
					</div>
				</div> -->
			</div>

			<div id="tab2" class="col-md-12 no-padding influencer-content-wrapper" style="margin-top: 10px;min-height:266px;display:none;" >

			</div>

			<div id="tab3" class="col-md-12 no-padding influencer-content-wrapper" style="margin-top: 10px;min-height:266px;display:none;" >

			</div>

			<div class="col-md-12 no-padding" style="margin-top: 10px;text-align:center;">
				<p style="margin-top: 18px;margin-bottom: 20px;">
					<a href="" ng-click="influ_back()" class="location-arrow-left"><i class="fa fa-chevron-left"></i></a>
					<a href="" ng-click="influ_next()" class="location-arrow-left" style="margin-left: -4px;"><i class="fa fa-chevron-right"></i></a>
				</p>
			</div>

		</div>

		<!-- <div class="col-md-12 no-padding-right" style="margin-top: 20px" hidden>
			<div class="col-md-12 no-padding">
				<h4>Talking About</h4>
			</div>

			<div class="col-md-12 no-padding talking-content-wrapper" style="margin-top: 10px;">
				<div class="col-md-3 col-sm-12 col-xs-12 talk">
					<div class="talking">
						<div class="col-md-12">
							<h5>#Montecito</h5>
						</div>

						<div class="col-md-12 no-padding talking-content">
							<img src="img/pbit-assets-v4/1/1.jpg">
							<img src="img/pbit-assets-v4/1/2.jpg">
							<img src="img/pbit-assets-v4/1/3.jpg">
							<img src="img/pbit-assets-v4/1/4.jpg">
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-12 col-xs-12 talk">
					<div class="talking">
						<div class="col-md-12">
							<h5>#ClassicalWest</h5>
						</div>

						<div class="col-md-12 no-padding talking-content">
							<img src="img/pbit-assets-v4/2/1.jpg">
							<img src="img/pbit-assets-v4/2/2.jpg">
							<img src="img/pbit-assets-v4/2/3.jpg">
							<img src="img/pbit-assets-v4/2/4.jpg">
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-12 col-xs-12 talk">
					<div class="talking">
						<div class="col-md-12">
							<h5>#Mancostlin</h5>
						</div>

						<div class="col-md-12 no-padding talking-content">
							<img src="img/pbit-assets-v4/3/1.jpg">
							<img src="img/pbit-assets-v4/3/2.jpg">
							<img src="img/pbit-assets-v4/3/3.jpg">
							<img src="img/pbit-assets-v4/3/4.jpg">
						</div>
					</div>
				</div>

				<div class="col-md-3 col-sm-12 col-xs-12 talk">
					<div class="talking">
						<div class="col-md-12">
							<h5>#CoralCasino</h5>
						</div>

						<div class="col-md-12 no-padding talking-content">
							<img src="img/pbit-assets-v4/4/1.jpg">
							<img src="img/pbit-assets-v4/4/2.jpg">
							<img src="img/pbit-assets-v4/4/3.jpg">
							<img src="img/pbit-assets-v4/4/4.jpg">
						</div>
					</div>
				</div>

			</div>
		</div> -->
	</div>
</div>