<div class="col-md-12 location-result" location-result style="padding: 15px 55px;">
	<div class="col-md-12 no-padding" style="margin-top:15px;">
		<div class="col-md-2 col-sm-12 col-xs-12 no-padding" style="background:#ABABAB;min-height: 360px;border-radius: 6px;">
			<div class="date-wrapper" >
				<div class="up-arrow">
					<a href="" ng-click="customDatePicker( 0 )">
						<i class="glyphicon glyphicon-chevron-up"></i>
					</a>
				</div>
				<p ng-bind="datePicks.one" >01/12/16</p>
				<p ng-bind="datePicks.two" >01/23/16</p>
				<p ng-bind="datePicks.three" class="selected" >03/14/16</p>
				<p ng-bind="datePicks.four" >03/26/16</p>
				<p ng-bind="datePicks.five" >04/01/16</p>
				<div class="down-arrow">
					<a href="" ng-click="customDatePicker( 1 )">
						<i class="glyphicon glyphicon-chevron-down"></i>
					</a>
				</div>
			</div>	
		</div>

		<div class="col-md-6 col-sm-12 col-xs-12 no-padding custom-margin-top">
			<div class="col-md-12 no-padding custom-padding2" style="">
				<div class="col-md-12 circle-chart-wrapper" style="background:#FFF;padding:15px;box-shadow: 1px 2px 1px #BBB;">
					<div class="col-md-12"><span>Theme</span></div>

					<div class="col-md-12">
						<div id="graph" style="margin-left: -5px; margin-top: 0px;"></div>
					</div>
				</div>
			</div>

			<div class="col-md-12 no-padding  custom-padding2" style="margin-top:20px;">
				<div class="col-md-12  demography-wrapper" style="overflow: hidden;background:#FFF;padding:15px;box-shadow: 1px 2px 1px #BBB;">
					<div class="col-md-12">Demography</div>
					<div class="col-md-8" style="padding: 0;">
						<div id="bar-graph" style="margin-left: -50px;z-index: 10;"></div>
						<!-- <span style="color: #A9ADB0;">Age</span> -->
					</div>
					<div class="col-md-4" style="padding: 0;">
						<div id="pie-graph" style="margin-top: 13px; margin-left: -20px;"></div>
						<ul class="pie-graph-box" style="margin-top: -15px!important; margin-left: -30px!important;">
							<li><span class="first-pie"></span> Male</li>
							<li><span class="second-pie"></span> Female</li>
						</ul>
					</div>
					<!-- <div class="clear-both"></div> -->
					<!-- <hr style="margin-top: 0!important;" /> -->
					<div class="col-md-5">
						<div id="doughnut-graph" style="margin-left: -75px; width: 100%; margin-top: -80px;"></div>
					</div>
					<div class="col-md-7">
						<div class="col-md-8">
							<h4 style="color: #9E9FA4;">Theme</h4>
							<table class="table custom-table">
								<tbody>
									<tr ng-repeat="list in labels">
										<td><span class="legend-box" style="background: {{ list.color }};"></span> <span ng-bind="list.label" style="margin-left: 10px;"></span></td>
									</tr>
								</tbody>
							</table>
						</div>
						<div class="col-md-4">
							<h4 style="color: #9E9FA4;">Frequency</h4>
							<table class="table custom-table">
								<tbody>
									<tr ng-repeat="list in labels">
										<td><span ng-bind="list.value"></span></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-4 col-sm-12 col-xs-12 no-padding custom-margin-top" style="padding:15px;">
			<div class="right-wrapper">
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal1">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/1.jpg" class="img-avatar">
						<img src="img/location-gal/1.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal2">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/2.jpg" class="img-avatar">
						<img src="img/location-gal/2.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal3">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/3.jpg" class="img-avatar">
						<img src="img/location-gal/3.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal4">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/4.jpg" class="img-avatar">
						<img src="img/location-gal/4.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal5">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/5.jpg" class="img-avatar">
						<img src="img/location-gal/5.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal6">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/6.jpg" class="img-avatar">
						<img src="img/location-gal/6.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal6">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/7.jpg" class="img-avatar">
						<img src="img/location-gal/7.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal6">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/8.jpg" class="img-avatar">
						<img src="img/location-gal/8.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal6">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/9.jpg" class="img-avatar">
						<img src="img/location-gal/9.jpg">
					</a>
				</div>
				<div class="img-wrapper">
					<a href="" data-toggle="modal" data-target="#predict-modal6">
						<div class="overlay"></div>
						<img src="img/location-gal/User_Pics/10.jpg" class="img-avatar">
						<img src="img/location-gal/10.jpg">
					</a>
				</div>
			</div>
		</div>
	</div>
</div>