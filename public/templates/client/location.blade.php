<div class="col-md-12 location" style="padding: 15px 55px;" location-page>
	<div class="col-md-12 no-padding-left" style="margin-bottom:10px;">
		<span class="">SEARCH BY LOCATION</span>
	</div>
	<br >
	<div class="col-md-7 col-sm-12 col-xs-12 no-padding" >
		<!-- <div class="map-container" map-lazy-load="https://maps.google.com/maps/api/js" >
		  <ng-map center="{{ map.location }}" zoom="11" style="height: 340px;"></ng-map>
		</div> -->
		<div id="map" style="height: 340px;width: 98%"></div>
		<!-- <ui-gmap-google-map center='map.location'zoom='13'></ui-gmap-google-map> -->
	</div>
	<div class="col-md-5 col-sm-12 col-xs-12  no-padding">
		<div class="right-wrapper">
			<div class="col-md-12 no-padding">
				<div class="form-inline search-wrapper">
					<div class="form-wrapper" style="background:#009BF8;padding:6px;border-radius:4px;">
						<input type="text" vs-google-autocomplete name="place" ng-model="map.location" class="form-control search-location" placeholder="Search..." style="width:89%;border-radius:4px !important" vs-place="map_results.place" 
					         vs-place-id="map_results.components.placeId"
					         vs-street-number="map_results.components.streetNumber" 
					         vs-street="map_results.components.street"
					         vs-city="map_results.components.city"
					         vs-state="map_results.components.state"
					         vs-country-short="map_results.components.countryCode"
					         vs-country="map_results.components.country"
					         vs-district = "map_results.components.district" vs-autocomplete-validator>
						<button class="btn btn-default" ng-click="setLocation( map_results )" style="background:transparent;border:none;color:#fff;font-size:18px;">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</div>
			<div class="col-md-12 no-padding" style="height: 290px;overflow-y: auto;">
				<div class="panel panel-default" style="margin-top: 20px;" ng-repeat="list in map_data">
					<div class="panel-body">
						<div class="col-md-2">
							<img src="//maps.gstatic.com/tactile/pane/default_geocode-1x.png">
						</div>
						<div class="col-md-7">
							<span ng-bind="list.place_name"></span>
							<br />
							<span ng-bind="list.place_formatted"></span>
							<br />
							<span ng-bind="list.place_website"></span>
						</div>
						<div class="col-md-2">
							<button class="btn btn-lg btn-success save_{{ list.id }}" ng-click="saveLocation( list )">
								<span class="span_{{ list.id }}" style="transition: all 0.5s ease;">Submit</span>
								<span class="done_{{ list.id }}" hidden>Done <i class="glyphicon glyphicon-ok"></i></span>
								<img src="../img/loading_spinner.gif" id="image_loader_{{ list.id }}" style="width: auto;height: 30px; transition: all 0.5s ease;" hidden>
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12 no-padding">
		<table class="table table-responsive location-table table-striped" style="box-shadow: 1px 2px 1px #BBB;">
			<thead>
				<tr>
					<th><span class="icon-location-pin"></span> Location</th>
					<th><span class="icon-bar-chart"></span> Status</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="list in location_list" class="animated fadeInDown" id="list_{{list.location_id}}">
					<td><span ng-bind="list.location_name"></span></td>
					<td><i class="completed" ng-if="list.status == 'Completed'"></i> <i class="pending" ng-if="list.status == 'In Progress'"></i> <span ng-bind="list.status"></span></td>
					<td><a ui-sref="location-result-page" ng-if="list.status == 'Completed'">Show Result</a></td>
					<td><a href="javascript:void(0)" ng-click="deleteLocation($event, list, location_list._id)">Delete</a></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>