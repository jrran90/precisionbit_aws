<div class="col-md-12 audiences-v2" style="padding: 20px 40px;" >
	<span id="audiences" hidden>true</span>
	<div class="image-list" style="overflow: hidden;background: #fff;padding: 30px 15px;box-shadow: 1px 2px 1px #BBB;" >
		<div class="col-md-4">
			<div class="col-md-12 text-center">
				<label>Audience Affinity</label> <br>
				<div id="doughnut-graph" style="margin-left: -40px; width: 100%; margin-top: -80px;"></div>
			</div>
			<div class="col-md-12" style="position:relative;top:-50px;">
				<div class="col-md-12">
					<h4 style="color: #9E9FA4;">Theme</h4>
					<table class="table custom-table">
						<tbody>
							<tr ng-repeat="list in labels">
								<td><span class="legend-box" style="background: {{ list.color }};"></span> <span ng-bind="list.label" style="margin-left: 10px;"></span></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-8">
			<div class="table-responsive" style="box-shadow: 1px 1px 1px #DBDBDB;">
				<table class="table table-responsive table-striped audiences" style="margin-bottom: 0 !important;">
					<thead>
						<tr>
							<th style="border-radius: 3px 0 0 0;">Ranking</th>
							<th>Username</th>
							<th>Followers</th>
							<th>Posts</th>
							<th>Affinities</th>
							<th style="border-radius: 0 3px 0 0;">Other Interests</th>
						</tr>
					</thead>
					<tbody >
						<tr ng-repeat="profile in profileInfo">
							<td>
								<span ng-bind="$index + 1"></span> &nbsp;
								<img src="{{profile.img_url}}" alt="no image" class="audience-img">
							</td>
							<td>
								<span ng-bind="profile.username"></span>
							</td>
							<td>
								<span ng-bind="profile.followers"></span>
							</td>
							<td>
								<span ng-bind="profile.posts"></span>
							</td>
							<td>
								<span ng-bind="profile.affinities"></span>
							</td>
							<td>
								<span ng-bind="profile.other_int"></span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>