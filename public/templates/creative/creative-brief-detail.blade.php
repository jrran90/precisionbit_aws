<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
  <section style="font-size: 20px!important;">
      <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px; margin-left: 10px;">
      Creative Brief Details
      </h1>
  </section>
</div>
<div class="row">
  <div class="panel panel-default hovered-brief-detail">
    <table>
      <tbody>
        <tr>
          <th style="padding: 10px;">
            <h3 style="font-size: 14px;margin-left: 10px;font-weight: bold;margin-top: 5px;line-height: 1.5;">Participate in our Get to know you ReFUEL4 Brand ad Brief and earn US$10*</h3>
            <p style="margin: 0;margin-left: 10px;color: #BABABA;font-weight: normal;">
              <span class="fa fa-image"></span>
              <span>Image</span>
            </p>
            <p style="margin: 0;margin-left: 10px;color: #BABABA;font-weight: normal;">ReFUEL4 PTE LTD</p>
          </th>
          <td class="text-center">
            <p style="color: #cbcbcb;margin: 0;">No submission limit for this brief</p>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>