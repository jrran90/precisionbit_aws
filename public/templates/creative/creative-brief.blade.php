<div class="row" style="margin-top: -15px;background-color: #e7eaef;box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.05);border-bottom: 1px solid #d0d0d0;min-height: 54px;height: auto;">
    <section style="font-size: 20px!important;">
        <h1 class="pull-left wow slideInLeft header-title-section" data-wow-duration="0.5s" data-wow-delay="0.5s" style="font-size: 15px;margin: 10px 0 0 10px;">
        Find Briefs
        <br />
        <small>Discover and select Briefs you want to work on.</small>
        </h1>
    </section>
</div>
<div class="row" style="margin-top: 10px;">
  <div class="col-md-9">
    <div class="panel panel-default hovered-brief-detail" ng-repeat="i in number">
      <table>
        <tbody>
          <tr>
            <th style="padding: 10px;">
              <h3 style="font-size: 14px;margin-left: 10px;font-weight: bold;margin-top: 5px;line-height: 1.5;">Participate in our Get to know you ReFUEL4 Brand ad Brief and earn US$10*</h3>
              <p style="margin: 0;margin-left: 10px;color: #BABABA;font-weight: normal;">
                <span class="fa fa-image"></span>
                <span>Image</span>
              </p>
              <p style="margin: 0;margin-left: 10px;color: #BABABA;font-weight: normal;">ReFUEL4 PTE LTD</p>
            </th>
            <td class="text-center">
              <p style="color: #cbcbcb;margin: 0;">No submission limit for this brief</p>
            </td>
            <td class="text-center">
              <p style="font-weight: bold;font-size: 14px; margin: 0;">December 31, 2015</p>
              <p style="font-weight: bold;color: #cbcbcb;font-weight: 12px;margin: 0;">End date</p>
            </td>
            <td class="text-center" style="padding: 10px;">
              <a ng-click="viewBriefDetails()" href="javascript:void(0)" style="font-weight: bold;color: #00af7d;font-size: 12px;text-decoration: underline;">Details...</a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div class="text-center">
      <a href="javascript:void(0)" style="font-size: 30px;margin-right: 50px;color: #9EC3C1;"><span class="glyphicon glyphicon-menu-left"></span></a>
      <a href="javascript:void(0)" style="font-size: 30px;color: #29A9A2;"><span class="glyphicon glyphicon-menu-right"></span></a>
    </div>
  </div>
  <div class="col-md-3">
    <div class="panel panel-default">
      <div class="panel-header">
        <p style="margin: 0;margin-left: 10px;margin-top: 10px;font-weight: bold;font-size: 16px;">Sort</p>
      </div>
      <div class="panel-body" style="padding: 0;">
        <div class="checkbox">
          <div class="form-inline">
            <label for="new" style="color: #858585;">                                    
                <input type="radio" name="new">
                New 
            </label>   
            <label for="popular" style="color: #858585;">                                    
                <input type="radio" name="popular">
                Popular 
            </label> 
          </div>
        </div>
        <div class="checkbox">
          <div class="form-inline">
            <label for="budget" style="color: #858585;">                              
                <input type="radio" name="budget">
                Budget 
            </label> 
            <label for="limit" style="color: #858585;">                                    
                <input type="radio" name="limit">
                Limit 
            </label> 
          </div>
        </div>
      </div>
      <div class="panel-header">
        <p style="margin: 0;margin-left: 10px;margin-top: 10px;font-weight: bold;font-size: 16px;">Category</p>
      </div>
      <div class="panel-body" style="padding: 0;">
        <div class="form-group" style="padding: 10px">
          <select class="form-control">
            <option value="0">All</option>
            <option value="1">Agency</option>
            <option value="2">Automotive</option>
            <option value="3">Consumer Packaged Goods</option>
            <option value="4">Ecommerce</option>
            <option value="5">Education</option>
            <option value="6">Energy and Utilities</option>
            <option value="7">Entertainment and Media</option>
            <option value="8">Financial Services</option>
            <option value="9">Gaming</option>
            <option value="10">Government &amp; Politics</option>
            <option value="11">Organizations &amp; Associations</option>
            <option value="12">Professional Services</option>
            <option value="13">Retail</option>
            <option value="14">Technology</option>
            <option value="15">Telecom</option>
            <option value="16">Travel</option>
            <option value="17">TextPlus / Technology</option>
            <option value="18">Other</option>
            <option value="19">Action</option>
            <option value="20">Adventure</option>
            <option value="21">Arcade</option>
            <option value="22">Board</option>
            <option value="23">Card</option>
            <option value="24">Casino</option>
            <option value="25">Dice</option>
            <option value="26">Casual</option>
            <option value="27">Educational</option>
            <option value="28">Family</option>
            <option value="29">Music</option>
            <option value="30">Puzzle</option>
            <option value="31">Racing</option>
            <option value="32">Role Playing</option>
            <option value="33">Simulation</option>
            <option value="34">Sports</option>
            <option value="35">Strategy</option>
            <option value="36">Trivia</option>
            <option value="37">Word</option>
          </select>
        </div>
      </div>
      <div class="panel-header">
        <p style="margin: 0;margin-left: 10px;margin-top: 10px;font-weight: bold;font-size: 16px;">Budget</p>
      </div>
      <div class="panel-body" style="padding: 0;">
        <div class="form-group" style="padding: 10px;">
          <input type="text" id="budget" name="example_name" value="" />
        </div>
      </div>
      <div class="panel-body text-center">
        <button class="btn custom-btn-large"> Search </button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function( ) {
    $("#budget").ionRangeSlider({
      min: 0,
      max: 100,
      from: 0,
      to: 100,
      type: 'double',
      step: 1,
      postfix: "%",
      prettify: false,
      hasGrid: false
    });
  })
</script>