<div class="col-md-11 col-md-offset-1" user-database>


  	<div class="row">
  		<div class="white-space-20"></div>
  		<div class="col col-md-12" style="background: #fff;padding-top: 15px;">

			<!-- Single button -->
			<div class="btn-group" style="margin-bottom: 15px;">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    Filter by Data Source <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
			    <li><a style="cursor: pointer;" ng-click="source('Starhub followers')">Starhub Followers</a></li>
			    <li><a style="cursor: pointer;" ng-click="source('15 location')">15 locations</a></li>
			  </ul>
			</div>
			<!-- Single button -->
			<div class="btn-group" style="margin-bottom: 15px;">
			  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    Filter by Gender <span class="caret"></span>
			  </button>
			  <ul class="dropdown-menu">
			    <li><a style="cursor: pointer;" ng-click="gender('m')">Male</a></li>
			    <li><a style="cursor: pointer;" ng-click="gender('f')">Female</a></li>
			  </ul>
			</div>
			<!-- Single button -->
			<button class="btn btn-default pull-right" ng-click="showAll()" style="margin-bottom: 15px;">Show All</button>

			

			<table class="table table-condensed table-hovered table-striped">
				<thead>
					<th>#</th>
					<th>Username</th>
					<th>Data Source</th>
					<th>Posts</th>
					<th>Total Followers</th>
					<th>Total Following</th>
					<th>Age</th>
					<th>Gender</th>
					<th>Local</th>
					<th>Parent</th>
					<th>Public/Private Profile</th>
				</thead>
				<tbody>
					<tr ng-repeat="list in lists  | filter:search">
						<td>{{ $index + 1 }}</td>
						<td ng-bind="list.profile_name"></td>
						<td ng-bind="list.data_src"></td>
						<td ng-bind="list.total_posts"></td>
						<td ng-bind="list.total_followers"></td>
						<td ng-bind="list.total_following"></td>
						<td ng-bind="list.age"></td>
						<td ng-bind="list.gender"></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
				</tbody>
			</table>  

			<nav aria-label="Page navigation">
			  <ul class="pagination">
			    <li>
			      <a href="#" aria-label="Previous">
			        <span aria-hidden="true">&laquo;</span>
			      </a>
			    </li>
			    <li ng-repeat="page in pages track by $index" class="page_list" id="page-{{page}}"><a href="javascript:void(0)" ng-click="userInfo( page )" ng-bind="page"></a></li>
			    <li>
			      <a href="#" aria-label="Next">
			        <span aria-hidden="true">&raquo;</span>
			      </a>
			    </li>
			  </ul>
			</nav>
		</div>	
  	</div>
</div>