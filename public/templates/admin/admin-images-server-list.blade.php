<div class="col-md-6 col-md-offset-3" admin-server-image style="margin-top: 50px;">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3>Image List in Server Side Upload Api</h3>
    </div>
    <div class="panel-body">
      <ul class="list-group">
        <li class="list-group-item text-center">
          <input type="text" class="form-control" ng-model="keyword">
        </li>
        <li class="list-group-item" ng-repeat="list in images | filter: keyword">
          <span ng-bind="list.filename" class="pull-left"></span>
          <i class="glyphicon glyphicon-trash pull-right custom-list-image-delete" ng-click="remove(list, 'remove_single')"></i>
          <div class="clear-both"></div>
        </li>
      </ul>
    </div>
  </div>
</div>