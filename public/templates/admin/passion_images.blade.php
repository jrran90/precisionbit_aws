<div class="col-md-11 col-md-offset-1" passion-filter>

<div class="row" ng-if="no_images">
		<div class="white-space-20"></div>
			<div class="col-md-8 text-center">
				<h2>No User Data.</h2>
			</div>
	</div>

	<div class="row" ng-if="!no_images">
		<div class="white-space-20"></div>
		<div class="col-md-8">
			<h4 style="text-transform:capitalize">{{passion}} ({{passion_images.length}})</h4>
		  	<div class="gallery-images" style="padding-top: 20px;overflow: hidden;float: left;width: 100%;">
				<div class="row no-margin">
					<div class="no-padding col-md-4" ng-repeat="img in passion_images ">
						<div class="image-wrapper-filter">
							<h5 class="text-center">User : <b>{{img.user}}</b></h5>
							<p class="text-center"><i>Location</i> <button class="btn btn-primary btn-sm" ng-click="showLocationInput(img, $index)"><i class="glyphicon glyphicon-pencil"></i></button> </p>

							<input class="form-control interest-input" type="text" ng-blur="saveLocation($index, img)" ng-model="img.location" id="location_{{ $index }}">
							<span class="interest-data" ng-bind="img.location"></span>
							<p>&nbsp;</p>
							<img ng-src="{{ getImage(img) }}" err-src="https://thumbs.dreamstime.com/x/404-error-sign-16655756.jpg" class="img-responsive">
							<div class="white-space-10"></div>
							<div>
								<label>Interest Area : 
									<button class="btn btn-primary btn-sm" ng-click="showInterestInput(img, $index)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="interest-data" ng-bind="img.interest"></span>
								<input class="form-control interest-input" type="text" ng-blur="saveInterest($index, img)" ng-model="img.interest" id="interest_{{ $index }}">
							</div>
							<div>
								<label>Image Tags :</label> <br>
								<div class="tag" ng-repeat="tag in img.tags track by $index"><span ng-bind="tag"></span></div> 
							</div>
						</div>
					</div>

				</div>

				<div class="white-space-20"></div>
		  	</div>
		</div>


		
	</div>
  	<h4><a ui-sref="starhub-passion-points-location-rod" class="color-black">  Back </a></h4>
</div>