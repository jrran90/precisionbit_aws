<div class="col-md-9 col-md-offset-2">
	<br>
	<div class="sub-content-wrapper">
		<div class="col-md-12 nav-contents">
			<!-- Nav tabs -->
			<br>
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="list-tab exp-list active">
					<a href="javascript:void()" ng-click="openTab('list')" aria-controls="experiment-list" role="tab" data-toggle="tab">Experiment Lists</a>
				</li>
				<!-- <li role="presentation" class="list-tab exp-res">
					<a href="javascript:void()" ng-click="openTab('result')" aria-controls="experiment-result" role="tab" data-toggle="tab">Deliver Experiment Result</a>
				</li> -->
			</ul>

			<!-- Tab panes -->
			<div class="tab-content">
				<div role="tabpanel" class="tab-pane main-tab active" id="experiment-list">
					<br>
					<table class="table table-hover table-bordered table-striped" style="background:#fff">
						<thead>
							<th>Experiment ID</th>
							<th>Product Name</th>
							<th>Action</th>
						</thead>
						<tbody>
							<tr ng-repeat="list in clientObject.predict" id="{{list.predict_id}}">
								<td>
									<span ng-bind="list.predict_id"></span>
								</td>
								<td>
									<span ng-bind="list.product_name"></span>
								</td>
								<td class="text-center"> 
									<button class="btn btn-success btn-sm" ng-click="giveInput( list, $index )">Give Input</button>
									<button class="btn btn-primary btn-sm">Show Result</button>
									<button class="btn btn-danger btn-sm" ng-click="deleteExperiment( list )">Delete Experiment</button>
								</td>
							</tr>
						</tbody>
					</table>					
				</div>
				<div role="tabpanel" class="tab-pane main-tab" id="experiment-result">
					<br>
					<section class="predict-result-container">
						<div class="row custom-predict-row text-center">
							<p>We've sourced the best photos that wil engage your target audience</p>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="col-md-6">
									<section class="predict-image-container">
										<img src="http://placehold.it/350x150" ng-if="!defaultPic" class="img-responsive img-thumbnail">
										<img ng-src="http://52.77.17.243/upload_get_client/{{defaultPic}}" ng-if="defaultPic" class="img-responsive img-thumbnail">
									</section>
									<section class="predict-info-container">
										<section class="text-left">
											<div class="experiment-input">
												<label>Product Name:</label>
												<input type="text" class="form-control" value="{{selectedListObject.product_name}}" disabled>
											</div>
										</section>
										<section class="list-predict-info" style="margin-top: 10px;">
											<div class="experiment-input">
											</div>
										</section>
										<section class="list-predict-info">
											<div class="experiment-input">
												<label>Predict CTR:</label>
												<input type="text" class="form-control" ng-model="experimentAttrib.ctr">
											</div>	
										</section>
										<section class="list-predict-info">
											<div class="experiment-input">
												<label>Emotional Response:</label>
												<input type="text" class="form-control" ng-model="experimentAttrib.emotional_response">
											</div>	
										</section>
										<section class="list-predict-info">
											<div class="experiment-input">
												<label>Aesthetic Score:</label>
												<input type="text" class="form-control" ng-model="experimentAttrib.aesthetic_score">
											</div>	
										</section>
										<section class="list-predict-info">
											<div class="experiment-input">
												<label>Engaging Score:</label>
												<input type="text" class="form-control" ng-model="experimentAttrib.engaging_score">
											</div>
										</section>
										<section class="list-predict-info">
											<div class="experiment-input">
												<label>Image Tags:</label>
												<div class="input-group-wrapper" id="tag-input-content">
													<!-- Fetch tags for image update -->
													<div class="input-group" ng-repeat="tag in experimentAttrib.img_tags">
														<input type="text" class="form-control tag_input" value="{{tag}}">
														<span class="input-group-btn">
															<button class="btn btn-primary" type="button" ng-click="addNewTagInput()">+</button>
														</span>
													</div>
													<!-- End: Fetch tags for image update -->

													<!-- Default -->
													<div class="input-group">
														<input type="text" class="form-control tag_input" id="default_input">
														<span class="input-group-btn">
															<button class="btn btn-primary" type="button" ng-click="addNewTagInput()">+</button>
														</span>
													</div>
													<!-- End: Default -->	
													<div class="input-group"  ng-repeat="item in items">
														<input type="text" class="form-control tag_input">
														<span class="input-group-btn">
															<button class="btn btn-primary" type="button" ng-click="addNewTagInput()">+</button>
														</span>
													</div>													 
												</div>
											</div>	
										</section>
										<section class="list-predict-info">
											<div class="experiment-input">
												<button class="btn btn-success" ng-show="actionBtn" ng-click="saveExperimentAttrib('create')">Save Experiment</button>
												<button class="btn btn-info" ng-hide="actionBtn" ng-click="saveExperimentAttrib('update')">Update Experiment</button>
											</div>	
										</section>
									</section>
									<section class="chart-container" hidden>
										<canvas class="pull-left" id="myChart" width="150" height="150"></canvas>
										<h5>75% of your audience find this photo engaging</h5>
									</section>
								</div>
								<div class="col-md-6">
									<section class="image-result-show-container">
										<div class="panel panel-default custom-default-image-result-show-container">
											<div class="panel-heading text-center">
												<ul class="nav nav-tabs nav-justified">
													<li role="presentation">
														<a href="javascript:void()" ng-click="openSubTab('curated')" aria-controls="experiment-curated" role="tab" data-toggle="tab">Curated Images</a>
													</li>
													<li role="presentation" class="active">
														<a href="javascript:void()" ng-click="openSubTab('upload')" aria-controls="experiment-upload" role="tab" data-toggle="tab">Upload Images</a>
													</li>
												</ul>												
											</div>
											<div class="panel-body">
												<div class="tab-content">
													<div role="tabpanel" class="tab-pane sub-pane" id="experiment-curated">
														<div class="row row-divider">
															<div class="col-md-6" ng-repeat="image in clientImageObject" id="{{image._id}}">
																<div class="option">
																	<a href="javascript:void(0)" ng-click="deleteUploadImage( image )" class="text-danger"><i class="glyphicon glyphicon-trash"></i></a>
																</div>
																<img ng-src="http://52.77.17.243/upload_get_client/{{ image.filename }}" class="img-responsive" ng-click="showPicture(image)">
															</div>
														</div>
													</div>
													<div role="tabpanel" class="tab-pane sub-pane active" id="experiment-upload">
														<form enctype="multipart/form-data">
															<div class="form-group">
																<div id="kv-error-1" style="margin-top:10px;display:none"></div>
																<div id="kv-success-1" class="alert alert-success fade in" style="margin-top:10px;display:none"></div>
																<input id="exp-files" type="file" multiple class="file-loading" name="files" data-preview-file-type="text">
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
									</section>
								</div>
							</div>
						</div>
					</section>		
				</div>
			</div>



			<!-- Modal -->
			<!-- <div class="modal fade" id="modal-experiment-info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  	<div class="modal-dialog" role="document">
				    <div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							<h4 class="modal-title" id="myModalLabel" ng-bind="experimentData.headingtext">
							</h4>
						</div>
						<div class="modal-body">
							<div class="content-info">
								<p><strong>Predict ID</strong>: <span ng-bind="experimentData.predict_id"></span></p>
								<p><strong>Mediaplatform</strong>: <span ng-bind="experimentData.mediaplatform"></span></p>
								<p><strong>Geography</strong>: <span ng-bind="experimentData.geography"></span></p>
								<p><strong>Target Age</strong>: <span ng-bind="experimentData.age_min"></span> - <span ng-bind="experimentData.age_max"></span> years old</p>
								<p><strong>Target Gender</strong>: <span ng-bind="experimentData.targetgender"></span></p>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						</div>
				    </div>
			  	</div>
			</div>
 -->

		</div>
	</div>		
</div>