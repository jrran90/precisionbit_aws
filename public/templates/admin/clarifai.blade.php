<div class="row" clarifai>
	<div class="col-md-6 col-md-offset-4">
		<form ng-submit="sendClarifai()">
			<div class="form-group">
				<input type="text" ng-model="clarifai_data.name" class="form-control" required>
			</div>
			<div class="text-rigth">
				<button type="submit" class="btn btn-primary">Send</button>
			</div>
		</form>
	</div>

	<div class="col-md-6 col-md-offset-4">
		<table class="table table-stripped">
			<thead>
				<tr>
					<th>Image Tags</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="list in clarifai_list">
					<td ng-bind="list.class"></td>
				</tr>
			</tbody>
		</table>
	</div>
</div>