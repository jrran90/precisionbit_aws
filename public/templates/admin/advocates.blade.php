<div class="col-md-11 col-md-offset-1" advocate-lists>

	<div class="row">
		<div class="white-space-20"></div>
		<div class="col-md-4 no-padding">
			<div class="admin-white-wrapper">
		  		<h4 class="border-bottom" style="padding-left: 15px;">Advocates (<span ng-bind="userList.length"></span>)</h4>
		  		<ul class="nav">
		  			<li ng-repeat="list in userList"><a href="javascript:void(0)" ng-click="viewUser(list)"><span ng-bind="list"></span></a></li>
		  		</ul>
		  	</div>
		</div>

		<div class="col-md-3">
			<h4>Passion points</h4>
			<ul class="nav passion-filter-nav">
				<li ng-repeat="passion in all_passion_points"><a href="javascript:void(0)" ng-click="passionImages(passion.passion)" style="text-transform:capitalize">{{passion.passion}} <span class="pull-right">( <span ng-bind="passion.count"></span>) </span></a></li>
			</ul>
		</div>
		
		</div>
		
	</div>
  	<h4><a ui-sref="passion-points-tagging" class="color-black">  Back </a></h4>
</div>