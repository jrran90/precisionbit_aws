<div class="col-md-11 col-md-offset-1" passion-location>

	<div class="row">
		<div class="white-space-20"></div>
		<div class="col-md-4 no-padding">
			<div class="admin-white-wrapper">
		  		<h4 class="border-bottom" style="padding-left: 15px;">Locations </h4>

		  		<ul class="nav">
		  			<li><a href="" ng-click="location('ION Orchard','ion_orchard')">ION ORCHARD</a></li>
		  			<li><a href="" ng-click="location('East Coast Park', 'east_coast_park')">EAST COAST PARK</a></li>
		  			<li><a href="" ng-click="location('Bugis +', 'bugis_+')">BUGIS +</a></li>
		  			<li><a href="" ng-click="location('Bugis Street', 'bugis_street')">Bugis Street</a></li>
		  			<li><a href="" ng-click="location('Bugis Junction', 'bugis_junction')">Bugis Junction</a></li>
		  			<li><a href="" ng-click="location('JCube', 'jcube')">JCube</a></li>
		  			<li><a href="" ng-click="location('Clarke Quay', 'clarke_quay')">Clarke Quay</a></li>
		  			<li><a href="" ng-click="location('Haji Lane', 'haji_lane')">Haji Lane</a></li>
		  			<li><a href="" ng-click="location('Plaza Singapura', 'plaza_singapura')">Plaza Singapura</a></li>
		  			<li><a href="" ng-click="location('Raffles City', 'raffles_city')">Raffles City</a></li>
		  			<li><a href="" ng-click="location('Scape', 'scape')">Scape</a></li>
		  			<li><a href="" ng-click="location('The Cathay', 'the_cathay')">The Cathay</a></li>
		  			<li><a href="" ng-click="location('The Star Vista', 'the_star_vista')">The Star Vista</a></li>
		  			<li><a href="" ng-click="location('Zouk', 'zouk')">Zouk</a></li>
		  			<li><a href="" ng-click="location('Changi Airport', 'changi_airport')">Changi Airport</a></li>
		  		</ul>
		  	</div>
		</div>
		<div class="col-md-3" star-hub>
			<h4>Passion points</h4>
			<ul class="nav passion-filter-nav">
				<li ng-repeat="passion in all_passion_points"><a href="javascript:void(0)" ng-click="passionImages(passion.passion)" style="text-transform:capitalize">{{passion.passion}} <span class="pull-right">( <span ng-bind="passion.count"></span>) </span></a></li>
			</ul>
		</div>
	</div>
	
  	
</div>