<div class="col-md-11 col-md-offset-1 passion-points-tagging-users-tagging" passion-search>

	<div class="row">
		<div class="col-md-12">
			<h3>CSV Tag Search</h3>
		</div>

		<div class="col-md-6 ">
			<label>Tags : 
				<span class="tag" ng-repeat="tag in image_tags">{{tag}}
					<a href="" ng-click="deleteTag(tag)">
						<i class="glyphicon glyphicon-remove text-danger" style="position: relative;top: 2px;"></i>
					</a>
				</span>

				<!-- <span class="tag"><span ng-model="tag-filter">Tags</span> 
				<a href="">
				<i class="glyphicon glyphicon-remove text-danger" style="position: relative;top: 2px;"></i>
				</a>
				</span> -->
			</label>
		</div>

		<div class="col-md-12" style="margin-top: 20px;">
			<form ng-submit="getTagResults(tag)">
				<div class="form-inline">
					<label>Enter Tags</label>
					<input type="text" class="form-control enter-tag" ng-model="tag">
				</div>
			</form>
			<br>
			<div class="form-inline">
				<label>Affinity Filter</label>

				<select ng-options="affinityTags as affinityTags for affinityTags in affinityTags " 
				        ng-model="tags_filter"
				        class="form-control">
				</select>
				<button ng-click="filterResultssss()"class="btn btn-primary">Apply</button>

			</div>
			<br>
			<div class="form-inline">
				<label>Affinity to be applied</label>
				<!-- <input type="text" class="form-control"> -->
				<select ng-model="tags_toApply" class="form-control">
					<option value="Manmade beauty">Manmade beauty</option>
					<option value="Natural beauty">Natural beauty</option>
					<option value="Landmarks">Landmarks</option>
					<option value="Environment-s">Environment-s</option>
					<option value="Meal time">Meal time</option>
					<option value="Drinks and snack">Drinks and snack</option>
					<option value="Self-preparation">Self-preparation</option>
					<option value="Dessert">Dessert</option>
					<option value="Design-s">Design-s</option>
					<option value="Food-s">Food-s</option>
					<option value="Concerts">Concerts</option>
					<option value="Entertainment-s">Entertainment-s</option>
					<option value="Fitness">Fitness</option>
					<option value="Beach">Beach</option>
					<option value="Park life">Park life</option>
					<option value="Winter Sports">Winter Sports</option>
					<option value="Adventure">Adventure</option>
					<option value="Recreation-s">Recreation-s</option>
					<option value="Ootd">Ootd</option>
					<option value="Makeup look">Makeup look</option>
					<option value="Fashion & Beauty-s">Fashion & Beauty-s</option>
					<option value="Young family">Young family</option>
					<option value="Best friends">Best friends</option>
					<option value="Family">Family</option>
					<option value="Friends">Friends</option>
					<option value="Baby family">Baby family</option>
					<option value="Relationships-s">Relationships-s</option>
					<option value="Community Events">Community Events</option>
					<option value="Personal Events">Personal Events</option>
					<option value="Celebrations-s">Celebrations-s</option>
					<option value="Product/brand shots">Product/brand shots</option>
					<option value="Promotions">Promotions</option>
					<option value="Shopping">Shopping</option>
					<option value="Commerce-s">Commerce-s</option>
					<option value="Events">Events</option>
					<option value="Work">Work</option>
					<option value="Business-s">Business-s</option>
					<option value="Technology-s">Technology-s</option>
					<option value="Staycation">Staycation</option>
					<option value="Travel-s">Travel-s</option>
					<option value="Healthy Living-s">Healthy Living-s</option>

					<option value="Conf/Events">Conf/Events</option>
					<option value="Destinations">Destinations</option>
					<option value="Sports">Sports</option>
					<option value="My look">My look</option>
					<option value="Screenshots-s">Screenshots-s</option>
					<option value="Selfie">Selfie</option>
					<option value="My world today">My world today</option>
					<option value="Here I am">Here I am</option>
					<option value="With me">With me</option>
					<option value="Doing">Doing</option>
					<option value="My life-s">My life-s</option>
					<option value="Motivational">Motivational</option>
					<option value="Arts-s">Arts-s</option>
					<option value="Transportation-s">Transportation-s</option>
					<option value="Religion-s">Religion-s</option>
					<option value="Culture-s">Culture-s</option>
					<option value="Media-s">Media-s</option>
					<option value="Science-s">Science-s</option>
					<option value="Lifehacks-s">Lifehacks-s</option>
					<option value="Literature-s">Literature-s</option>
					<option value="Wedding Organisation-s">Wedding Organisation-s</option>
					<option value="Museum-s">Museum-s</option>
					<option value="Politics-s">Politics-s</option>
					<option value="Pets">Pets</option>
					<option value="Animals-s">Animals-s</option>
					<option value="Study-s">Study-s</option>
					<option value="Outdoor Hobbies-s">Outdoor Hobbies-s</option>
					<option value="Gifts-s">Gifts-s</option>
					<option value="Non Usable">Non Usable</option>
					<option value="Undefined">Undefined</option>
					<option value="Photography">Photography</option>
					<option value="Scene-s">Scene-s</option>
					<option value="Fun Filter">Fun Filter</option>
					<option value="Cute">Cute</option>
					<option value="Abstract">Cute</option>
				</select>
				<button class="btn btn-primary">Apply</button>
			</div>
			<br>
			<div class="form-inline">
				<label>Show < 2 Affinities</label>
				<input type="checkbox" ng-model="moreThanTwo"
           ng-true-value="true" ng-false-value="false" style="height: 20px;width: 20px;position: relative;top: 5px;" >
			</div>
		</div>

		<div class="col-md-12 text-right" style="margin-top: 20px">
			<button class="btn btn-primary" ng-click="selectAll()">Select All</button>
		</div>

		<div class="col-md-12 text-center" style="margin-bottom: 30px;">
			<button class="btn btn-default">Prev</button>
			<button class="btn btn-default">Next</button>
		</div>

		<div class="white-space-20"></div>

		<div class="col-md-12 no-padding">
			<div class="gallery-images" style="overflow: hidden;float: left;width: 100%;">
				<div class="row no-margin">
					<div class="no-padding col-md-4	" ng-repeat="list in image_results" ng-hide="moreThanTwo && list.primary_affinities.length > 2">
						<!-- <form ng-submit="saveDataPassion( $index, list )"> -->
						<form>
							<div class="user-project-image-wrapper">
								<h4 class="text-center">
									User : <span>{{list.username}}</span> 
									<input type="checkbox" class="pull-right checkbox-list" style="margin-top: 0;height: 16px;width: 16px;">
								</h4>
								<p class="text-center"><i>Location</i> <button class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-pencil" ng-click="showLocationInput($index)"></i></button></p>
								<input id="location_{{$index}}" type="text" class="form-control location-input">
								<br>
								<!-- <img ng-src="{{ getImage(list) }}" err-src="https://thumbs.dreamstime.com/x/404-error-sign-16655756.jpg" class="img-responsive"> -->
								<img ng-src="{{ getImage(list) }}" err-src="https://thumbs.dreamstime.com/x/404-error-sign-16655756.jpg" class="img-responsive">

								<div class="white-space-10"></div>
								<label>Interest Area :  <button class="btn btn-primary btn-sm" ng-click="showInterestInput($index)"><i class="glyphicon glyphicon-pencil"></i></button></label>
								<input id="interest_{{$index}}" type="text" class="form-control interest-input">

								<br>
								<div>
									<label>Image Tags :</label> <br>
									<!-- <div class="tag" ng-repeat="tags in list track by $index"><span ng-bind="tags.class"></span></div>  -->
									<div class="tag" ng-repeat="tags in list.tags track by $index"><span ng-bind="tags"></span></div>
								</div>
							</div>

						</form>
					</div>

					<div class="col-md-12 text-center" ng-if="!view_status">
						<h3>No Matches</h3>
					</div>	
				</div>

				<div class="white-space-20"></div>
		  	</div>
		</div>

		<div class="col-md-12 text-center" style="padding-top: 30px;">
			<button class="btn btn-default">Prev</button>
			<button class="btn btn-default">Next</button>
		</div>
		
		<div class="col-md-12" style="margin-top:20px;padding-bottom: 20px;">
			<div class="form-inline">
				<label>Affinity to be applied</label>
				<select ng-options="affinityTags as affinityTags for affinityTags in affinityTags " 
				        ng-model="tags_toApply"
				        class="form-control">
				</select>
				<button class="btn btn-primary">Apply</button>
			</div>
		</div>

	</div>
	
  	
</div>