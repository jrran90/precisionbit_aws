<div class="col-md-11 col-md-offset-1"  passion-specific-user>

	<div class="row" ng-if="!view_status">
		<div class="white-space-20"></div>
			<div class="col-md-8 text-center">
				<h2>No User Data.</h2>
			</div>
	</div>
	<div class="row" ng-if="view_status">
		<div class="white-space-20"></div>
		<div class="col-md-8">
			<div class="admin-white-wrapper" style="overflow: hidden;float: left;width: 100%;">
				<div class="row no-margin">
					<div class="col-md-3 no-padding">
						<img src="" class="img-circle" style="width: 130px;height: 130px;">
					</div>
					<div class="col-md-9 no-padding">
						<h2><a href="{{viewImage(userList)}}" target="_blank"><b>@<span ng-bind="userList.profile_name"></span></b></a></h2>

						<div class="col-md-4 no-padding">
							<label>Posts:</label> <br>
							<h4 ng-bind="userList.total_post"></h4>
						</div>

						<div class="col-md-4 no-padding">
							<label>Followers:</label> <br> 
							<h4 ng-bind="userList.total_followers"></h4>
						</div>

						<div class="col-md-4 no-padding">
							<label>Following:</label> <br> 
							<h4 ng-bind="userList.total_following"></h4>
						</div>
					</div>	
				</div>
				<div class="white-space-20"></div>
				<div class="row no-margin" style="background: #F5F5F5;border-radius: 4px;border: 1px solid #E1E1E1;">
					<div class="col-md-6 no-padding" style="border-right: 1px solid #E1E1E1;">
						<div id="pie-graph" style="text-align: center;margin-top: 20px;">
							
			          	</div>
			          	<p class="text-center" style="position: absolute;top: 90px;left: 110px;width: 130px;font-size: 16px;">Breakdown of passion points</p>
					</div>
					<div class="col-md-6 no-padding" style="border-left: 1px solid #E1E1E1;" >
						<h4 class="text-center">List of locations visited:</h4>
						<div class="list-wrapper" >
							<ul class="nav" style="overflow: auto;max-height: 200px">
								<li><a href="">Location 1</a></li>
								<li><a href="">Location 2</a></li>
								<li><a href="">Location 3</a></li>
								<li><a href="">Location 4</a></li>
								<li><a href="">Location 5</a></li>
								<li><a href="">Location 6</a></li>
								<li><a href="">Location 7</a></li>
							</ul>
						</div>
						
					</div>
				</div>
		  	</div>

		  	<div class="gallery-images" style="padding-top: 20px;overflow: hidden;float: left;width: 100%;">
				<div class="row no-margin">
					<div class="no-padding col-md-4	" ng-repeat="list in user_data track by $index" ng-if="list.length > 1" >
						<div class="image-wrapper">
							<p class="text-center"><i>Location</i></p>
							<img ng-src="{{ getImage(list) }}" err-src="https://thumbs.dreamstime.com/x/404-error-sign-16655756.jpg" class="img-responsive">
							<div class="white-space-10"></div>
							<div>
								<label>Passion point : 
									<button class="btn btn-primary btn-sm" ng-click="showPassionInput(list, $index)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="passion-data"> {{ getPassion(list) }} </span>
								<input class="form-control passion-input" type="text" ng-model="list.data" id="passion_{{ $index }}" ng-blur="savePassion($index, list)" auto-complete ui-items="passion_data">
							</div>
							<br>
							<div>
								<label>Interest Area : 
									<button class="btn btn-primary btn-sm" ng-click="showInterestInput(list, $index)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="interest-data">{{ getInterest(list) }}</span>
								<input class="form-control interest-input" type="text" ng-blur="saveInterest($index, list)" ng-model="list.data_1" id="interest_{{ $index }}">
							</div>
							<br>
							<div>
								<label>Image Tags :</label> <br>
								<div class="tag" ng-repeat="tags in list track by $index"><span ng-bind="tags.class"></span></div> 
							</div>
						</div>
					</div>

					<!-- <div class="col-md-4 no-padding">
						<div class="image-wrapper">
							<p class="text-center">Mall in ION ORCHARD</p>
							<img src="img/location-gallery/9.jpg" class="img-responsive">
							<div class="white-space-10"></div>
							<div>
								<label>Passion point : 
									<button class="btn btn-primary btn-sm" ng-click="showPassionInput(1)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="passion-data">Null</span>
								<input class="form-control passion-input" type="text" >
							</div>
							<br>
							<div>
								<label>Interest Area : 
									<button class="btn btn-primary btn-sm" ng-click="showInterestInput(1)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="interest-data">Null</span>
								<input class="form-control interest-input" type="text" >
							</div>
							<br>
							<div>
								<label>Image Tags :</label> <br>
								<div class="tag">Happy</div> 
								<div class="tag">Relax</div> 
								<div class="tag">Mall</div> 
							</div>
						</div>
					</div>

					<div class="col-md-4 no-padding">
						<div class="image-wrapper">
							<p class="text-center">Mall in ION ORCHARD</p>
							<img src="img/location-gallery/9.jpg" class="img-responsive">
							<div class="white-space-10"></div>
							<div>
								<label>Passion point : 
									<button class="btn btn-primary btn-sm" ng-click="showPassionInput(1)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="passion-data">Null</span>
								<input class="form-control passion-input" type="text" >
							</div>
							<br>
							<div>
								<label>Interest Area : 
									<button class="btn btn-primary btn-sm" ng-click="showInterestInput(1)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="interest-data">Null</span>
								<input class="form-control interest-input" type="text" >
							</div>
							<br>
							<div>
								<label>Image Tags :</label> <br>
								<div class="tag">Happy</div> 
								<div class="tag">Relax</div> 
								<div class="tag">Mall</div> 
							</div>
						</div>
					</div> -->
				</div>

				<div class="white-space-20"></div>
		  	</div>
		</div>


		
	</div>
  	<h4><a href="javascript:void(0)" ng-click="back()" class="color-black">  Back </a></h4>
</div>