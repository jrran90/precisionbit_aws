<div class="col-md-11 col-md-offset-1 passion-summary" passion-summary>
	<div class="row">
		<div class="col-md-12">
			<ul class="nav navbar-nav menu-summary">
				<li><a href="" ng-click="showSummary(1)" class="active">Overall</a></li>
				<li><a href="" ng-click="showSummary(2)">Affinities</a></li>
				<li><a href="" ng-click="showSummary(3)">CV Tags</a></li>
				<li><a href="" ng-click="showSummary(4)">Definitions</a></li>
			</ul>
		</div>
	</div>
	<div id="overall" class="row">
		<div class="white-space-20"></div>
		<div class="col-md-7">
			<table id="total_users" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th></th>
						<th>Users</th>
						<th>Percentage of total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Total</td>
						<td>0</td>
						<td>-</td>
					</tr>
					<tr>
						<td>Untagged</td>
						<td>0</td>
						<td>0</td>
					</tr>

				</tbody>
			</table>
		</div>

		<div class="col-md-7">
			<table id="total_images" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th></th>
						<th>Images</th>
						<th>Percentage of total</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Total</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>Untagged</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>2+ Affinities</td>
						<td>0</td>
						<td>0</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col-md-9">
			<table id="total_images" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Primary</th>
						<th>2nd Affinity</th>
						<th>Images</th>
						<th>Percentage of total</th>
						<th>Users</th>
						<th>Percentage of total Users</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Relationship</td>
						<td>-</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>Young Family</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>Best Friends(couple)</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>Environment</td>
						<td>-</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>Natural Beauty</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div id="affinities" class="row" hidden>
		<div class="white-space-20"></div>
		<div class="col-md-12">
			<table id="total_users" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Primary Affinity</th>
						<th>1</th>
						<th>2</th>
						<th>Percentage of users in this affinity (1) who also had this affinity (2)</th>
						<th>Percentage of user in affinity 1 that have more than one occurence of affinity 2</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Fashion and Beauty</td>
						<td>Make Up Look</td>
						<td>Selfie</td>
						<td>94</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>pets</td>
						<td>56</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>natural beauty</td>
						<td>33</td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td>all secondary affintiy ....</td>
						<td></td>
						<td></td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>

	<div id="cvtags" class="row" hidden>
		<div class="white-space-20"></div>
		<div class="col-md-12">
			<table id="total_users" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Tag stats by affinity</th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Fashion and Beauty</td>
						<td>Make Up Look</td>
						<td>Tag</td>
						<td>Woman</td>
						<td>young</td>
						<td>Pretty </td>
						<td>Skin</td>
						<td>for all tags</td>
					</tr>
					<tr>
						<td>Make Up Look</td>
						<td></td>
						<td>percentage Images tagged with this affinity have this tag</td>
						<td>75%</td>
						<td>72</td>
						<td>70</td>
						<td>68</td>
						<td></td>
					</tr>
					<tr>
						<td>Food</td>
						<td>Dessert</td>
						<td></td>
						<td>Sweet</td>
						<td>Cake</td>
						<td>Celebration</td>
						<td></td>
						<td></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td>75</td>
						<td>68</td>
						<td>55</td>
						<td></td>
						<td></td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>

	<div id="definition" class="row" hidden>
		<div class="white-space-20"></div>
		<div class="col-md-8">
			<table id="total_users" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th>Affinity Strength</th>
						<th>percentage of images with a specific secondary affinity</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td></td>
						<td></td>
						<td></td>
					</tr>

				</tbody>
			</table>
		</div>
	</div>


  	<h4><a href="javascript:void(0)" ng-click="back()" class="color-black">  Back </a></h4>
</div>
