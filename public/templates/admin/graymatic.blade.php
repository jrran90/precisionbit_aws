<div class="col-md-5 col-md-offset-4" graymatic style="margin-top: 20px;">
	<div class="form-group">
		<form ng-submit="postUrl()">
			<div ng-repeat="data in graymaticData track by $index">
				<input type="text" class="form-control pull-left" placeholder="Link..." ng-model="data.url" required style="width: 90%!important; margin-bottom: 5px;">
				<button type="button" class="btn btn-danger pull-right" ng-click="remove(data)" style="margin-left: 5px;" ng-if="$index > 0"><i class="glyphicon glyphicon-trash" ></i></button>
				<br />
			</div>
				<div style="clear: both"></div>
			<button type="button" class="btn btn-primary pull-left" ng-click="addMore()">Add more..</button>
			<button type="submit" class="btn btn-success pull-right">Submit</button>
		</form>
	</div>
</div>