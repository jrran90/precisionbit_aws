<div class="col-md-11 col-md-offset-1" passion-specific-user>

	<div class="row" ng-if="!view_status">
		<div class="white-space-20"></div>
			<div class="col-md-8 text-center">
				<h2>No User Data.</h2>
			</div>
	</div>
	<div class="row" ng-if="view_status">
		<div class="white-space-20"></div>
		<div class="col-md-12">
			<div class="admin-white-wrapper" style="overflow: hidden;float: left;width: 100%;" ng-if="userList">
				<div class="row no-margin">
					<div class="col-md-3 no-padding">
						<img src="" class="img-circle" style="width: 130px;height: 130px;">
					</div>
					<div class="col-md-9 no-padding">
						<h2><a href="{{ userList.profile_link }}" target="_blank"><b>@<span ng-bind="userList.profile_name"></span></b></a></h2>

						<div class="col-md-4 no-padding">
							<label>Posts:</label> <br>
							<h4 ng-if="userList.total_posts" ng-bind="userList.total_posts"></h4>
							<h4 ng-if="userList.total_post" ng-bind="userList.total_post"></h4>
						</div>

						<div class="col-md-4 no-padding">
							<label>Followers:</label> <br> 
							<h4 ng-bind="userList.total_followers"></h4>
						</div>

						<div class="col-md-4 no-padding">
							<label>Following:</label> <br> 
							<h4 ng-bind="userList.total_following"></h4>
						</div>
					</div>	
				</div>
				<div class="white-space-20"></div>
				<div class="row no-margin" style="background: #F5F5F5;border-radius: 4px;border: 1px solid #E1E1E1;">
					<div class="col-md-6 no-padding" style="border-right: 1px solid #E1E1E1;">
						<div id="pie-graph" style="text-align: center;margin-top: 20px;">
							
			          	</div>
			          	<p class="text-center" style="position: absolute;top: 90px;left: 110px;width: 130px;font-size: 16px;">Breakdown of passion points</p>
					</div>
					<div class="col-md-6 no-padding" style="border-left: 1px solid #E1E1E1;" >
						<h4 class="text-center">List of locations visited:</h4>
						<div class="list-wrapper" >
							<ul class="nav" style="overflow: auto;max-height: 200px">
								<li><a href="">Location 1</a></li>
								<li><a href="">Location 2</a></li>
								<li><a href="">Location 3</a></li>
								<li><a href="">Location 4</a></li>
								<li><a href="">Location 5</a></li>
								<li><a href="">Location 6</a></li>
								<li><a href="">Location 7</a></li>
							</ul>
						</div>
						
					</div>
				</div>
		  	</div>
		  	<div class="admin-white-wrapper" style="overflow: hidden;float: left;width: 100%;" ng-if="!userList">
				<div class="row no-margin">
				User: <h3 ng-bind="username"></h3>
				</div>
			</div>
		  	<div class="gallery-images" style="padding-top: 20px;overflow: hidden;float: left;width: 100%;">
				<div class="row no-margin">
					<div class="no-padding col-md-4	" ng-repeat="list in user_data track by $index" ng-if="list.length > 1" >
						<form ng-submit="saveDataPassion( $index, list )">
							<div class="image-wrapper">
								<p class="text-center"><i>Location</i></p>
								<img ng-src="{{ getImage(list) }}" err-src="https://thumbs.dreamstime.com/x/404-error-sign-16655756.jpg" class="img-responsive">
								<div class="white-space-10"></div>
								<div>
									<label style="font-size:12px;">Primary Affinity : 
									</label>
									<br>
									<div class="primary_passion_list primary_passion_list_{{$index}}">

										<span class="passion-data label label-info _delete_1" ng-if="list[0].primary_passion_point_1" style="margin-right: 4px;">{{ list[0].primary_passion_point_1 }}</span>

										<span class="passion-data label label-info _delete_2 " ng-if="list[0].primary_passion_point_2" style="margin-right: 4px;">{{ list[0].primary_passion_point_2 }}</span>

										<span class="passion-data label label-info _delete_3" ng-if="list[0].primary_passion_point_3" style="margin-right: 4px;">{{ list[0].primary_passion_point_3 }} </span>

										<span class="passion-data label label-info _delete_4" ng-if="list[0].primary_passion_point_4" style="margin-right: 4px;">{{ list[0].primary_passion_point_4 }}</span>

										<!-- <span class="passion-data label label-info" ng-if="pp[$index]"> {{ pp[$index] }} </span> -->
									</div>
									<!-- <input class="form-control passion-input" type="text" ng-model="list.primary_passion_point_a" id="passion_{{ $index }}A" ng-blur="savePassion($index, list)" auto-complete  ui-items="passion_data" style="display: block;"> -->
								</div>
								<br>
								<div>
									<label style="font-size:12px;">Secondary Affinity Box : 
									</label>
									<br>
									<!-- <input class="form-control passion-input" type="text" ng-model="list.secondary_passion_point_a" id="passion_{{ $index }}B" auto-complete ui-items="passion_data" style="display: block;"> -->
									
									<input class="form-control input_pp" list="secondary_passion" ng-model="list.secondary_passion_point_a">

									<datalist id="secondary_passion">
										<option value="Manmade beauty">Manmade beauty</option>
										<option value="Natural beauty">Natural beauty</option>
										<option value="Landmarks">Landmarks</option>
										<option value="Environment-s">Environment-s</option>
										<option value="Meal time">Meal time</option>
										<option value="Drinks and snack">Drinks and snack</option>
										<option value="Self-preparation">Self-preparation</option>
										<option value="Dessert">Dessert</option>
										<option value="Design-s">Design-s</option>
										<option value="Food-s">Food-s</option>
										<option value="Concerts">Concerts</option>
										<option value="Entertainment-s">Entertainment-s</option>
										<option value="Fitness">Fitness</option>
										<option value="Beach">Beach</option>
										<option value="Park life">Park life</option>
										<option value="Winter Sports">Winter Sports</option>
										<option value="Adventure">Adventure</option>
										<option value="Recreation-s">Recreation-s</option>
										<option value="Ootd">Ootd</option>
										<option value="Makeup look">Makeup look</option>
										<option value="Fashion & Beauty-s">Fashion & Beauty-s</option>
										<option value="Young family">Young family</option>
										<option value="Best friends">Best friends</option>
										<option value="Family">Family</option>
										<option value="Friends">Friends</option>
										<option value="Baby family">Baby family</option>
										<option value="Relationships-s">Relationships-s</option>
										<option value="Community Events">Community Events</option>
										<option value="Personal Events">Personal Events</option>
										<option value="Celebrations-s">Celebrations-s</option>
										<option value="Product/brand shots">Product/brand shots</option>
										<option value="Promotions">Promotions</option>
										<option value="Shopping">Shopping</option>
										<option value="Commerce-s">Commerce-s</option>
										<option value="Events">Events</option>
										<option value="Work">Work</option>
										<option value="Business-s">Business-s</option>
										<option value="Technology-s">Technology-s</option>
										<option value="Staycation">Staycation</option>
										<option value="Travel-s">Travel-s</option>
										<option value="Healthy Living-s">Healthy Living-s</option>

										<option value="Conf/Events">Conf/Events</option>
										<option value="Destinations">Destinations</option>
										<option value="Sports">Sports</option>
										<option value="My look">My look</option>
										<option value="Screenshots-s">Screenshots-s</option>
										<option value="Selfie">Selfie</option>
										<option value="My world today">My world today</option>
										<option value="Here I am">Here I am</option>
										<option value="With me">With me</option>
										<option value="Doing">Doing</option>
										<option value="My life-s">My life-s</option>
										<option value="Motivational">Motivational</option>
										<option value="Arts-s">Arts-s</option>
										<option value="Transportation-s">Transportation-s</option>
										<option value="Religion-s">Religion-s</option>
										<option value="Culture-s">Culture-s</option>
										<option value="Media-s">Media-s</option>
										<option value="Science-s">Science-s</option>
										<option value="Lifehacks-s">Lifehacks-s</option>
										<option value="Literature-s">Literature-s</option>
										<option value="Wedding Organisation-s">Wedding Organisation-s</option>
										<option value="Museum-s">Museum-s</option>
										<option value="Politics-s">Politics-s</option>
										<option value="Pets">Pets</option>
										<option value="Animals-s">Animals-s</option>
										<option value="Study-s">Study-s</option>
										<option value="Outdoor Hobbies-s">Outdoor Hobbies-s</option>
										<option value="Gifts-s">Gifts-s</option>
										<option value="Non Usable">Non Usable</option>
										<option value="Undefined">Undefined</option>
										<option value="Photography">Photography</option>
										<option value="Scene-s">Scene-s</option>
										<option value="Fun Filter">Fun Filter</option>
										<option value="Cute">Cute</option>
										<option value="Abstract">Cute</option>



										
									</datalist>									

									<br>
									<div class="sec_points sec_{{$index}}">
										<div class="label label-success section-inline-block _delete_1" ng-if="list[0].secondary_passion_point_1">
											<span class="passion-data  _delete"  style="margin-right:4px" >{{ list[0].secondary_passion_point_1 }}</span>

											<a href="" ng-click="deleteAffinity($index,list[0].url,list[0].secondary_passion_point_1, '1')" class="glyphicon glyphicon-remove _delete"></a>
										</div>
										
										<div class="label label-success section-inline-block _delete_2" ng-if="list[0].secondary_passion_point_2">
										<span class="passion-data _delete "  style="margin-right:4px" >{{ list[0].secondary_passion_point_2 }}</span>
										<a href="" ng-click="deleteAffinity($index,list[0].url,list[0].secondary_passion_point_2, '2')" class="glyphicon glyphicon-remove _delete"></a>
										</div>
										
										<div class="label label-success section-inline-block _delete_3" ng-if="list[0].secondary_passion_point_3">
										<span class="passion-data _delete"  style="margin-right:4px" >{{ list[0].secondary_passion_point_3 }}</span>
										<a href="" ng-click="deleteAffinity($index,list[0].url,list[0].secondary_passion_point_3, '3')" class="glyphicon glyphicon-remove _delete"></a>
										</div>
										
										<div class="label label-success section-inline-block _delete_4" ng-if="list[0].secondary_passion_point_4">
										<span class="passion-data  _delete"  style="margin-right:4px" >{{ list[0].secondary_passion_point_4 }}</span>
										<a href="" ng-click="deleteAffinity($index,list[0].url,list[0].secondary_passion_point_4, '4')" class="glyphicon glyphicon-remove _delete"></a>
										</div>
									</div>
								</div>
								<!-- <br>
								<div>
									<label>Interest Area : 
									</label>
									<br>
									<span class="interest-data text-info">{{ getInterest(list) }}</span>
									<input class="form-control interest-input" type="text" ng-blur="saveInterest($index, list)" ng-model="list.data_1" id="interest_{{ $index }}" style="display: block;">
								</div> -->
								<br>
								<div>
									<label>Image Tags :</label> <br>
									<div class="tag" ng-repeat="tags in list track by $index"><span ng-bind="tags.class"></span></div> 
								</div>
							</div>
						</form>
					</div>

					<!-- <div class="col-md-4 no-padding">
						<div class="image-wrapper">
							<p class="text-center">Mall in ION ORCHARD</p>
							<img src="img/location-gallery/9.jpg" class="img-responsive">
							<div class="white-space-10"></div>
							<div>
								<label>Passion point : 
									<button class="btn btn-primary btn-sm" ng-click="showPassionInput(1)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="passion-data">Null</span>
								<input class="form-control passion-input" type="text" >
							</div>
							<br>
							<div>
								<label>Interest Area : 
									<button class="btn btn-primary btn-sm" ng-click="showInterestInput(1)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="interest-data">Null</span>
								<input class="form-control interest-input" type="text" >
							</div>
							<br>
							<div>
								<label>Image Tags :</label> <br>
								<div class="tag">Happy</div> 
								<div class="tag">Relax</div> 
								<div class="tag">Mall</div> 
							</div>
						</div>
					</div>

					<div class="col-md-4 no-padding">
						<div class="image-wrapper">
							<p class="text-center">Mall in ION ORCHARD</p>
							<img src="img/location-gallery/9.jpg" class="img-responsive">
							<div class="white-space-10"></div>
							<div>
								<label>Passion point : 
									<button class="btn btn-primary btn-sm" ng-click="showPassionInput(1)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="passion-data">Null</span>
								<input class="form-control passion-input" type="text" >
							</div>
							<br>
							<div>
								<label>Interest Area : 
									<button class="btn btn-primary btn-sm" ng-click="showInterestInput(1)"><i class="glyphicon glyphicon-pencil"></i></button>
								</label>
								<br>
								<span class="interest-data">Null</span>
								<input class="form-control interest-input" type="text" >
							</div>
							<br>
							<div>
								<label>Image Tags :</label> <br>
								<div class="tag">Happy</div> 
								<div class="tag">Relax</div> 
								<div class="tag">Mall</div> 
							</div>
						</div>
					</div> -->
				</div>

				<div class="white-space-20"></div>
		  	</div>
		</div>


		
	</div>
  	<h4><a href="javascript:void(0)" ng-click="back()" class="color-black">  Back </a></h4>
</div>