<!-- 
<div class="col-md-9 col-md-offset-2">
  <form enctype="multipart/form-data">
    <div class="form-group">
      <div id="kv-error-1" style="margin-top:10px;display:none"></div>
      <div id="kv-success-1" class="alert alert-success fade in" style="margin-top:10px;display:none"></div>
      <input id="input-id" type="file" multiple class="file-loading" name="files" data-preview-file-type="text">
    </div>
  </form>
 -->

 <!-- Nav tabs -->
  <ul class="nav nav-tabs custom-nav-tabs" role="tablist">
    <li role="presentation" class="global-tab cg active"><a href="javacript:void(0)" aria-controls="clarifai-graymatic" role="tab" data-toggle="tab">Clarifai and Graymatic</a></li>
    <li role="presentation" class="global-tab imma" ng-click="immagaApi()"><a href="javascript:void(0)" aria-controls="immaga" role="tab" data-toggle="tab">Imagga</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content custom-tab-content">
    <div role="tabpanel" class="tab-pane active" id="clarify-graymatic">
      <form enctype="multipart/form-data">
        <div class="form-group">
          <div id="kv-error-1" style="margin-top:10px; display:none"></div>
          <div id="kv-success-1" class="alert alert-success fade in" style="margin-top:10px; display:none"></div>
          <input id="input-cg" type="file" multiple class="file-loading" name="files" data-preview-file-type="text">
        </div>
      </form>
    </div>
    <div role="tabpanel" class="tab-pane" id="imagga">
      <form enctype="multipart/form-data">
      <div class="form-group">
        <div id="kv-error-1-imm" style="margin-top:10px;display:none"></div>
        <div id="kv-success-1-imm" class="alert alert-success fade in" style="margin-top:10px;display:none"></div>
        <input id="input-imma" type="file" multiple class="file-loading" name="files" data-preview-file-type="text">
      </div>
    </form>
    </div>
  </div>


  <script type="text/javascript">
    $(function(){
      $('.cg').click(function(){
        // e.preventDefault();
        $('.global-tab').removeClass('active');
        $('.cg').addClass('active');
        $('#clarify-graymatic').show();
        $('#imagga').hide();
      });

      $('.imma').click(function(){
        // e.preventDefault();
        $('.global-tab').removeClass('active');
        $('.imma').addClass('active');
        $('#clarify-graymatic').hide();
        $('#imagga').show();
      });
    });
  </script>