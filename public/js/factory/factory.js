app
  .factory('serverUrl',[
      function factory(){
        return {
          url: window.location.origin + "/",
          header: {
            headers: {
              'Content-Type' : 'application/x-www-form-urlencoded'
              // 'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
            }
          }
        }
      }
  ])
  .factory('nodeUrl',[
      function factory(){
        return {
          // url: 'http://52.76.30.59:3000/',
          url: 'http://localhost:3000/',
          // url: 'http://52.77.17.243/',
          header: {
            headers: {
              'Content-Type' : 'application/x-www-form-urlencoded',
              'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
            }
          }
        }
      }
  ])
  .factory('insta',[
      function factory(){
        return {
          // url: 'http://52.76.30.59:3000/',
          url: 'https://www.instagram.com/',
          // url: 'http://52.77.17.243/',
          header: {
            headers: {
              'Content-Type' : 'application/x-www-form-urlencoded',
              'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE',
              'Access-Control-Allow-Origin': '*'
            }
          }
        }
      }
  ])
  .factory('accessInfo',[
      "$http",
      function factory( $http ){
        return {
          get: function () {
            return $http.get( window.location.origin + "/getuserdata");
          }
        }
      }
  ])
  // added by jhon
  .factory('GraymaticAPI', GraymaticAPI)
  .factory('ImaggaAPI', ImaggaAPI)
  .factory('Report', Report);


function GraymaticAPI($http) {
    return {
        get: function (image) {
            return $http.get('/api/graymatic', {params:{image: image}});
        }
    }
}

function ImaggaAPI($http) {
    return {
        show: function (image) {
          return $http.get('/api/imagga', {params:{image: image}});
        },
        store: function (image) {
          return $http({
            method:  'POST',
            url:     'api/imagga',
            // headers: {'Content-Type':'application/x-www-form-urlencoded'},
            data:    {image: image}
          });
        },
        destroy: function (image) {
          return $http.delete('api/imagga/'+image);
        }
    }
}

function Report($http) {
    return {
        show: function (type, filter) {
            return $http.get('/api/report', {params: {type: type, filter: filter || 'all'}});
        },
        tags: function (type, ad_id) {
            return $http.get('/api/report-tags', {params: {type: type, ad_id: ad_id}});
        }
    }
}
