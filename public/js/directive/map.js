var map = angular.module('map', [])

map.run(function(){
	console.log('running map module');
});

map.directive('mapData', [
	"$http",
	"serverUrl",
	function directive( $http, serverUrl ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('running mapData directive');
				var map;
				scope.searchFilter = {};

				scope.runMap = function runMap( ) {
					var address = "Singapore";

					var geocoder = new google.maps.Geocoder();

					var drawingManager = new google.maps.drawing.DrawingManager({
					    // drawingMode: google.maps.drawing.OverlayType.MARKER,
					    drawingControl: true,
					    drawingControlOptions: {
					      position: google.maps.ControlPosition.TOP_RIGHT,
					      drawingModes: [
					        google.maps.drawing.OverlayType.MARKER,
					        google.maps.drawing.OverlayType.CIRCLE,
					        google.maps.drawing.OverlayType.POLYGON,
					        // google.maps.drawing.OverlayType.POLYLINE,
					        // google.maps.drawing.OverlayType.RECTANGLE
					      ]
					    },
					    markerOptions: {animation: google.maps.Animation.DROP},
					    circleOptions: {
					      fillColor: '#BEC6CF',	
					      fillOpacity: .7,
					      strokeColor: '#FFF',
					      strokeWeight: 3,
					      clickable: true,
					      editable: true,
					      zIndex: 1
					    }
					  });

					geocoder.geocode({
				      'address': address
					   }, 
					   function(results, status) {
					      if(status == google.maps.GeocoderStatus.OK) {
					         // new google.maps.Marker({
					         //    position: results[0].geometry.location,
					         //    map: map
					         // });
					         map.setCenter(results[0].geometry.location);
					      }
					   });


					drawingManager.setMap(map);

					google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
					  if (event.type == google.maps.drawing.OverlayType.CIRCLE) {
					    var radius = event.overlay.getRadius();
					    console.log("lat: " + event.overlay.getCenter().lat());
					    console.log("lang: " + event.overlay.getCenter().lng());

					    var new_latlng = { lat: event.overlay.getCenter().lat(), lng: event.overlay.getCenter().lng() };

					    for(var x = 0; x < 5; x++ ){
					    	console.log("lat: " + event.overlay.getCenter().lat() + x + ", " + "lng: " + event.overlay.getCenter().lng() + x);
						    var marker = new google.maps.Marker({
							    position: { lat: event.overlay.getCenter().lat() + x / 1000, lng: event.overlay.getCenter().lng() + x / 1000 } ,
							    map: map,
							    title: 'Hello World!'
							  });
					    }

					    // console.log("event.overlay:" + event.overlay);
					    console.log('radius:' + radius);
					  }else if(event.type == google.maps.drawing.OverlayType.MARKER){
					  	var latlng = event.overlay.getPosition();	
						var latitude = latlng.lat();
						var longitude = latlng.lng();

						console.log("latitude: " + latitude + " " + "longitude: " + longitude);
						console.log(event);

						var infowindow = new google.maps.InfoWindow({
						    content: "latitude: " + latitude + " " + "longitude: " + longitude
						  });

						geocoder.geocode({
					      'address': address
						   }, 
						   function(results, status) {
						      if(status == google.maps.GeocoderStatus.OK) {
						         // new google.maps.Marker({
						         //    position: results[0].geometry.location,
						         //    map: map
						         // });
						         map.setCenter(results[0].geometry.location);
						      }
						   });


						drawingManager.setMap(map);

						google.maps.event.addListener(drawingManager, 'overlaycomplete', function(event) {
						  if (event.type == google.maps.drawing.OverlayType.CIRCLE) {
						    var radius = event.overlay.getRadius();
						    console.log("lat: " + event.overlay.getCenter().lat());
						    console.log("lang: " + event.overlay.getCenter().lng());
						    // console.log("event.overlay:" + event.overlay);
						    console.log('radius:' + radius);
						  }else if(event.type == google.maps.drawing.OverlayType.MARKER){
						  	var latlng = event.overlay.getPosition();	
							var latitude = latlng.lat();
							var longitude = latlng.lng();

							console.log("latitude: " + latitude + " " + "longitude: " + longitude);
							console.log(event);

							var infowindow = new google.maps.InfoWindow({
							    content: "latitude: " + latitude + " " + "longitude: " + longitude
							  });

							// infowindow.open(map);
							// marker.addListener('click', function() {
							//     infowindow.open(map, marker);
							//   });

						  }
						});
					}

					});
				};
				// function rad(x) {return x*Math.PI/180;}
				scope.getLocation = function getLocation( ) {
					$('#searchFilter').css({"width": "85%", "transition": "all 0.5s ease"});
					$('#load-location').show();
					setTimeout(function() {
						console.log( scope.searchFitler );
					}, 200);
					
					setTimeout(function() {
						$('#load-location').hide();
						$('#searchFilter').css({"width": "100%", "transition": "all 0.5s ease"});
					}, 3000);
				};

				scope.initializeControls = function initializeControls( map ) {
					var circleText = document.createElement('span');
					circleText.style.color = 'rgb(25,25,25)';
					circleText.style.fontFamily = 'Roboto,Arial,sans-serif';
					circleText.style.fontSize = '12px';
					circleText.style.padding = '10px';
					circleText.innerHTML = 'Circle';

					var polygonText = document.createElement('span');
					polygonText.style.color = 'rgb(25,25,25)';
					polygonText.style.fontFamily = 'Roboto,Arial,sans-serif';
					polygonText.style.fontSize = '12px';
					polygonText.style.padding = '10px';
					polygonText.innerHTML = 'polygon';

					var zoomInText = document.createElement('span');
					zoomInText.style.color = 'rgb(25,25,25)';
					zoomInText.style.fontFamily = 'Roboto,Arial,sans-serif';
					zoomInText.style.fontSize = '12px';
					zoomInText.style.padding = '10px';
					zoomInText.innerHTML = 'zoomIn';

					var zoomOutText = document.createElement('span');
					zoomOutText.style.color = 'rgb(25,25,25)';
					zoomOutText.style.fontFamily = 'Roboto,Arial,sans-serif';
					zoomOutText.style.fontSize = '12px';
					zoomOutText.style.padding = '10px';
					zoomOutText.innerHTML = 'zoomOut';

					var circleUI = document.createElement('div');
					circleUI.style.width = 'auto';
					circleUI.style.display = 'inline-block';
					circleUI.style.backgroundColor = '#fff';
					circleUI.style.border = '1px solid #DBDBDB';
					circleUI.style.cursor = 'pointer';
					circleUI.style.marginTop = '5px';
					circleUI.style.textAlign = 'center';
					circleUI.style.padding = '2px 0';
					circleUI.appendChild(circleText);

					var polygonUI = document.createElement('div');
					polygonUI.style.width = 'auto';
					polygonUI.style.display = 'inline-block';
					polygonUI.style.backgroundColor = '#fff';
					polygonUI.style.border = '1px solid #DBDBDB';
					polygonUI.style.cursor = 'pointer';
					polygonUI.style.marginTop = '5px';
					polygonUI.style.textAlign = 'center';
					polygonUI.style.padding = '2px 0';
					polygonUI.appendChild(polygonText);

					var zoomInUI = document.createElement('div');
					zoomInUI.style.width = 'auto';
					zoomInUI.style.display = 'inline-block';
					zoomInUI.style.backgroundColor = '#fff';
					zoomInUI.style.border = '1px solid #DBDBDB';
					zoomInUI.style.cursor = 'pointer';
					zoomInUI.style.marginTop = '5px';
					zoomInUI.style.textAlign = 'center';
					zoomInUI.style.padding = '2px 0';
					zoomInUI.appendChild(zoomInText);

					var zoomOutUI = document.createElement('div');
					zoomOutUI.style.width = 'auto';
					zoomOutUI.style.display = 'inline-block';
					zoomOutUI.style.backgroundColor = '#fff';
					zoomOutUI.style.border = '1px solid #DBDBDB';
					zoomOutUI.style.cursor = 'pointer';
					zoomOutUI.style.marginTop = '5px';
					zoomOutUI.style.textAlign = 'center';
					zoomOutUI.style.padding = '2px 0';
					zoomOutUI.appendChild(zoomOutText);

					var mainDiv = document.createElement('div');
					mainDiv.style.width = 'auto';
					mainDiv.style.marginLeft = '5px';
					mainDiv.appendChild(circleUI);
					mainDiv.appendChild(polygonUI);
					mainDiv.appendChild(zoomInUI);
					mainDiv.appendChild(zoomOutUI);


					mainDiv.index = 1;
					map.controls[google.maps.ControlPosition.TOP_LEFT].push(mainDiv);

					google.maps.event.addDomListener(circleUI, 'click', function() {
					  console.log("Circle");
					  var drawingManager = new google.maps.drawing.DrawingManager({
					    drawingMode: google.maps.drawing.OverlayType.CIRCLE,
					    drawingControl: false,
					    circleOptions: {
					      fillColor: '#BEC6CF',	
					      fillOpacity: .7,
					      strokeColor: '#FFF',
					      strokeWeight: 3,
					      clickable: true,
					      editable: true,
					      zIndex: 1
					    }
					  });
					  drawingManager.setMap(map);
					});

					google.maps.event.addDomListener(polygonUI, 'click', function() {
					  console.log("Polygon");
					});

					google.maps.event.addDomListener(zoomInUI, 'click', function() {
					  console.log("zoomIn");
					  map.setZoom(map.getZoom() + 1);
					});

					google.maps.event.addDomListener(zoomOutUI, 'click', function() {
					  console.log("zoomOut");
					  map.setZoom(map.getZoom() - 1);
					});
				};

				// scope.runMap( );
			}
		}
	}
]);

map.directive('locationData', [
	"$http",
	"serverUrl",
	function directive( $http, serverUrl ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('running mapData directive');
				scope.search_result = [
					'ION Orchard',
					'Orchard Park Suites',
					'A',
					'Q'
				];

				

				scope.allData = [
		          {
		            "Gender": "M",
		            "Age": "20-30",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          
		          {
		            "Gender": "M",
		            "Age": "10-20",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "M",
		            "Age": "40 above",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "M",
		            "Age": "10-20",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		        ];

		        scope.getAverage = function getAverage(){

		          var genderMale = 0;
		          var ageArr = [];
		          var ageCtr = [];

		          for( var x = 0; x < scope.allData.length; x++ ){
		            // console.log( scope.allData[x] );

		            if( scope.allData[x].Gender == "M" ){
		              genderMale++;
		            }

		            if( $.inArray( scope.allData[x].Age, ageArr ) == -1 ){
		              ageArr.push( scope.allData[x].Age );
		              ageCtr.push( 1 );
		            }else{
		              ageCtr[ ageArr.indexOf( scope.allData[x].Age ) ]++;  
		            }

		            if( x == (scope.allData.length-1) ){
		              var male = genderMale;
		              var female = scope.allData.length - genderMale;

		              setTimeout(function(){
		                scope.pieGraphAge( male, female );
		                scope.barGraph( ageArr, ageCtr);
		                scope.influBarGraph( ageArr, ageCtr);
		                scope.comparisonBarGraph( ageArr, ageCtr);
		                scope.lineChart( );
		                scope.timeGraph( );
		              },200);
		              
		            }

		          }

		        }

		        scope.pieGraphAge = function pieGraph( num1,num2 ) {
		          var pie = new d3pie("age-pie-graph", {
		  
		            "size": {
		              "canvasHeight": 60,
		              "canvasWidth": 60,
		              "pieOuterRadius": "100%",
		              "pieInnerRadius": "75%",
		            },
		            "data": {
		              "content": [
		                {
		                  // "value": (num2*10),
		                  "value": 48,
		                  "label": "Female",
		                  "color": "#2ca02c"
		                  
		                },
		                {
		                  // "value": (num1*10),
		                  "value": 21,
		                  "label": "Male",
		                  "color": "#CCD1D5"
		                },
		              ]
		            },
		            "labels": {
		              "outer": {
		                "pieDistance": 32,
		                "format": "none",
		              },
		              "inner": {
		                  "format": "none",
		                  "hideWhenLessThanPercentage": null
		              }
		            },
		            "effects": {
		              "pullOutSegmentOnClick": {
		                "effect": "none"
		              }
		            },
		            tooltips: {
		              enabled: true,
		              type: "placeholder",
		              string: "{label} : {value}"
		            }
		           

		            });

		         var pie2 = new d3pie("age-pie-graph2", {
		  
		            "size": {
		              "canvasHeight": 60,
		              "canvasWidth": 60,
		              "pieOuterRadius": "100%",
		              "pieInnerRadius": "75%",
		            },
		            "data": {
		              "content": [
		                {
		                  // "value": (num2*10),
		                  "value": 48,
		                  "label": "Female",
		                  "color": "#2ca02c"
		                  
		                },
		                {
		                  // "value": (num1*10),
		                  "value": 21,
		                  "label": "Male",
		                  "color": "#CCD1D5"
		                },
		              ]
		            },
		            "labels": {
		              "outer": {
		                "pieDistance": 32,
		                "format": "none",
		              },
		              "inner": {
		                  "format": "none",
		                  "hideWhenLessThanPercentage": null
		              }
		            },
		            "effects": {
		              "pullOutSegmentOnClick": {
		                "effect": "none"
		              }
		            },
		            tooltips: {
		              enabled: true,
		              type: "placeholder",
		              string: "{label} : {value}"
		            }
		           

		            });
		        };

		        scope.barGraph = function barGraph( arrX, arrY ) {

		            var trace1 = {
		              // x: arrX,
		              // y: arrY,
		              x: ["20-30","30-40","40 Above","10-20"] ,
		              y: [13,41,14,4],
		              marker:{
		                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
		                thicknessmode: 1
		              },
		              type: 'bar'
		            };


		            var data = [trace1];

		            var layout = { 
		                barmode: 'stack',
		                showlegend: false,
		                displayModeBar: false,
		                height: 215,
		                width: 210,
		                bargap: 0.05,
		                xaxis: { 
		                  showgrid: false,
		                  showticklabels: false,
		                  showline: false,
		                },
		                yaxis: {
		                  showgrid: false,
		                  showline: false,
		                  showticklabels: false,
		                  zeroline: false,
		                  grigwidth: 2
		                }
		            };

		            Plotly.newPlot('average-bar-graph', data, layout, {displayModeBar: false});
		            Plotly.newPlot('average-bar-graph2', data, layout, {displayModeBar: false});
		          };

		        scope.influBarGraph = function barGraph( arrX, arrY ) {

		            var trace1 = {
		              x: ['A','B','C',],
		              y: [6,2,2],
		              marker:{
		                color: ['#60BCD1', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
		                thicknessmode: 1
		              },
		              type: 'bar'
		            };


		            var data = [trace1];

		            var layout = { 
		                barmode: 'stack',
		                showlegend: false,
		                displayModeBar: false,
		                height: 215,
		                width: 210,
		                bargap: 0.05,
		                xaxis: { 
		                  showgrid: false,
		                  showticklabels: false,
		                  showline: false,
		                },
		                yaxis: {
		                  showgrid: false,
		                  showline: false,
		                  showticklabels: false,
		                  zeroline: false,
		                  grigwidth: 2
		                }
		            };

		            Plotly.newPlot('influ-bar-graph', data, layout, {displayModeBar: false});
		            Plotly.newPlot('influ-bar-graph2', data, layout, {displayModeBar: false});
		          };

		        scope.comparisonBarGraph = function barGraph( arrX, arrY ) {

		            var trace1 = {
		              x: ['M','T','W','TH','F','ST','S',],
		              y: [150,250,200,350,300,450,500],
		              marker:{
		                // color: ['#60BCD1', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
		                thicknessmode: 1
		              },
		              type: 'bar'
		            };

		            var trace2 = {
		              x: ['M','T','W','TH','F','ST','S',],
		              y: [250,150,300,250,400,350,600],
		              marker:{
		                // color: ['#60BCD1', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
		                thicknessmode: 1
		              },
		              type: 'bar'
		            };


		            var data = [trace1,trace2];

		            var layout = { 
		                barmode: 'group',
		                showlegend: true,
		                displayModeBar: false,
		                height: 440,
		                width: 700,
		                bargap: 1.5,
		                xaxis: { 
		                  showgrid: true,
		                  showticklabels: true,
		                  showline: true,
		                },
		                yaxis: {
		                  showgrid: false,
		                  showline: true,
		                  showticklabels: true,
		                  zeroline: false,
		                  grigwidth: 2
		                }
		            };

		            Plotly.newPlot('comp-bar-graph', data, layout, {displayModeBar: false});
		          };

		        scope.timeGraph = function(){
		        	var trace1 = {
		              x: ['0','2','4','6','8','10','12','14','16','18','20','22','24',],
		              y: [150,250,200,350,300,450,500],
		            };

		            var trace2 = {
		              x: ['0','2','4','6','8','10','12','14','16','18','20','22','24',],
		              y: [250,150,300,250,400,350,600],
		            };

		        	var data = [trace1,trace2];

					var layout = { 
		                showlegend: true,
		                displayModeBar: false,
		                height: 350,
		                width: 500,
		                bargap: 1.5,
		                xaxis: { 
		                  showgrid: true,
		                  showticklabels: true,
		                  showline: true,
		                },
		                yaxis: {
		                  showgrid: false,
		                  showline: false,
		                  showticklabels: false,
		                  zeroline: false,
		                  grigwidth: 2
		                }
		            };

					Plotly.newPlot('time-graph', data, layout, {displayModeBar: false});
		        }


		        scope.lineChart = function(){
		        	var trace1 = {
	        		  x: ['M', 'T', 'W', 'TH', 'F', 'ST', 'S',],
					  y: [80, 200, 150, 350, 250, 450, 500],
					  type: 'scatter'
					};

					var data = [trace1];


					var layout = {
					    showlegend: false,
		                displayModeBar: false,
		                autosize: false,
						width: 390,
						height: 300,
						margin: {
						l: 50,
						r: 50,
						b: 100,
						t: 100,
						pad: 4
						},
						yaxis:{
							dtick: 250,
						}
					};

					Plotly.newPlot('lineChart', data, layout);
		        }

		        

		        scope.get_summary = function(){
					$('.summary').css({bottom:'0px'});
				}
				
				scope.hide_summary = function(){
					$('.summary').css({bottom:'-410px'});
				}  

				scope.initMap = function initMap(){
					var address = "Singapore";
				
					var map = new google.maps.Map(document.getElementById('map'), { 
				       mapTypeId: google.maps.MapTypeId.ROADMAP,
				       zoom: 13,
				   	});

					var geocoder = new google.maps.Geocoder();

					geocoder.geocode({
				      'address': address
					   }, 
					   function(results, status) {
					      if(status == google.maps.GeocoderStatus.OK) {
					         new google.maps.Marker({
					            position: results[0].geometry.location,
					            map: map
					         });
					         map.setCenter(results[0].geometry.location);
					      }
					   });


				   	var map2 = new google.maps.Map(document.getElementById('map2'), { 
				       mapTypeId: google.maps.MapTypeId.ROADMAP,
				       zoom: 13,
				   	});

				   	var geocoder2 = new google.maps.Geocoder();

					geocoder2.geocode({
				      'address': address
					   }, 
					   function(results, status) {
					      if(status == google.maps.GeocoderStatus.OK) {
					         new google.maps.Marker({
					            position: results[0].geometry.location,
					            map: map2
					         });
					         map2.setCenter(results[0].geometry.location);
					      }
					   });
				}

				

				$('#searchFilter').change(function(){

					var data = $('#searchFilter').val();
					console.log(data);

					if( $('#searchFilter').val() != "" ){
						$('.result').show();
						console.log(data);
					}else{
						$('.result').hide();
					}
					
				});

				var ctr = 0;

				scope.toggleDrop = function(){

					if(ctr == 0){
						$( ".summary" ).css({position:'absolute', top: '-530px', transition: 'all 1s ease-in-out'});
						ctr = 1;
					}else{
						$( ".summary" ).css({position:'absolute', top: '250px' , transition: 'all 1s ease-in-out'});
						ctr = 0;
					}
					
				}

				scope.initMap();
		        scope.getAverage();
			}
		}
	}
]);