var graph = angular.module('graph', []);

graph.run(function(){
	console.log('graph module running');
});

graph
	.directive('graphPlotly', [
		"$http",
		"reportService",
    "Report",
		function directive( $http, reportService, Report ) {
			return {
				restrict: "A",
				scope: true,
				link: function link( scope, element, attributeSet ) {
					console.log('graph');
          scope.report_objects = [];
          scope.report_values_sub_category = [];
          scope.report_values = {};
          scope.report_obj = {};
          scope.tags = [];
          var acceptance = [];
          var anticipation = [];
          var interest = [];
          var joy = [];

				  Report
                .show( 'emotions ' )
                .then(function ( res ) {
                  console.log( res );
                  console.log( res.data.result.length );
                  scope.report_values = res;
                  for( var x = 0; x < res.data.result.length; x++ ) {
                    scope.report_values_sub_category.push( res.data.result[ x ].value[ 0 ] );
                    // if( res.data.result[ x ].value[ 0 ]  == "Acceptance" ) {
                    //   acceptance.push( res.data.result[ x ] );
                    // } else if( res.data.result[ x ].value[ 0 ]  == "Anticipation" ) {
                    //   anticipation.push( res.data.result[ x ]  );
                    // } else if( res.data.result[ x ].value[ 0 ] == "Interest") {
                    //   interest.push( res.data.result[ x ] );
                    // } else if( res.data.result[ x ].value[ 0 ] == "Joy") {
                    //   joy.push( res.data.result[ x ] );
                    // }
                  }

                  var new_value = _.uniq( scope.report_values_sub_category );
                  for( var a = 0; a < new_value.length; a++ ) {
                    for( var y = 0; y < scope.report_values.data.result.length; y++ ) {
                      if(new_value[ a ] == scope.report_values.data.result[ y ].value[ 0 ] ) {
                        var obj = {
                          value: new_value[ a ],
                          key: scope.report_values.data.result[ y ]
                        };
                        scope.report_objects.push( obj );
                      }
                      var tag = {
                        tags: scope.report_values.data.result[ y ].type,
                        ad_id: scope.report_values.data.result[ y ].ad_id
                      }
                      scope.tags.push( tag );
                    }
                  }



                  console.log( scope.report_objects );
                  console.log( scope.tags );
          });

          
				}
			}
		}
	]);
graph
  .directive('predictResultGraph', [
    "NgMap",
    "$window",
    function directive( NgMap , $window ) {
      return {
        restrict: "A",
        scope: true,
        link: function link( scope, element, attributeSet ) {
          scope.graph = {};
          scope.polar = {};

          scope.area = function area( ) {

              var data = [{
                values: [0.7, 0.3],
                labels: ['Game of Thrones actors', 'Game of Throne facts'],
                name: 'Themes',
                marker: { 
                  colors: ['#F34635', '#4BBEF7']
                },
                type: 'pie',
                // textinfo: 'none'
              }];

              var layout = {
                height: 450,
                width: 400,
                showlegend: false,
                legend: {
                  x: 0.3,
                  y: -0.3
                }
              };
              scope.data_1 = [];

              Plotly.newPlot('graph', data, layout, {displayModeBar: false});
              for( var x = 0; x < data[ 0 ].values.length; x++ ) {
                scope.data_1.push( { label: data[ 0 ].labels[ x ], value: (data[ 0 ].values[ x ] * 100).toFixed(0)+ "%", color: data[ 0 ].marker.colors[ x ] });
              }
          };

          scope.barGraph = function barGraph( ) {
            var trace1 = {
              x: [0, 1, 2, 3, 4],
              y: [10, 11, 21, 31, 4],
              name: 'Male',
              type: 'bar',
              marker: {
                color: '#F34635',
                thicknessmode: 10
              }
            };

            var trace2 = {
              x: [0, 1, 2, 3, 4],
              y: [12, 18, 29, 12, 18],
              name: 'Female',
              type: 'bar',
              marker: {
                color: '#4BBEF7',
                thicknessmode: 10
              }
            };

            var data = [trace1, trace2];
            var layout = { 
                title: 'Frequency',
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 400,
                width: 480,
                bargap: 0.1,
                bargroupgap: 0.8,
                xaxis: { 
                  title: 'Age',
                  titlefont: {
                    size: 16,
                    color: '#A9ADB0',
                  },
                  showgrid: false,
                  showticklabels: false,
                  showline: true,
                },
                yaxis: {
                  showgrid: false,
                  showline: true,
                  showticklabels: false
                }
            };

            Plotly.newPlot('bar-graph', data, layout, {displayModeBar: false});
          };
          scope.data_pie = [];
          scope.data_pie2 = [];
          scope.pie = function pie( ) {
            var pie = new d3pie("graph", {
    
              "size": {
                "canvasHeight": 400,
                "canvasWidth": 590,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Game of Thrones actors",
                    "value": 0.7,
                    "color": "#F34635"
                  },
                  {
                    "label": "Game of Throne facts",
                    "value": 0.3,
                    "color": "#4BBEF7"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32
                },
                "inner": {
                  "format": "value"
                },
                "mainLabel": {
                  "font": "Montserrat-Regular"
                },
                "percentage": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular",
                  "decimalPlaces": 0
                },
                "value": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular"
                },
                "lines": {
                  "enabled": true,
                  "color": "#cccccc"
                },
                "truncation": {
                  "enabled": true
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });

            console.log( pie.options.data.content );
            for( var x = 0; x < pie.options.data.content.length; x++ ) {
                scope.data_pie.push( { label: pie.options.data.content[ x ].label, value: pie.options.data.content[ x ].value, color: pie.options.data.content[ x ].color });
              }
            console.log( scope.data_pie );
          }

          scope.pie2 = function pie( ) {
            var pie2 = new d3pie("graph2", {
    
              "size": {
                "canvasHeight": 400,
                "canvasWidth": 590,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Delicious food",
                    "value": 0.33,
                    "color": "#F34635"
                  },
                  {
                    "label": "SG50 baby",
                    "value": 0.4,
                    "color": "#4BBEF7"
                  },
                  {
                    "label": "Singapore flyer",
                    "value": 0.33,
                    "color": "#51851A"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32
                },
                "inner": {
                  "format": "value"
                },
                "mainLabel": {
                  "font": "Montserrat-Regular"
                },
                "percentage": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular",
                  "decimalPlaces": 0
                },
                "value": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular"
                },
                "lines": {
                  "enabled": true,
                  "color": "#cccccc"
                },
                "truncation": {
                  "enabled": true
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });

            console.log( pie2.options.data.content );
            for( var x = 0; x < pie2.options.data.content.length; x++ ) {
                scope.data_pie2.push( { label: pie2.options.data.content[ x ].label, value: pie2.options.data.content[ x ].value, color: pie2.options.data.content[ x ].color });
              }
            console.log( scope.data_pie2 );
          }

          scope.pieGraph = function pieGraph( ) {
            var pie = new d3pie("pie-graph", {
    
              "size": {
                "canvasHeight": 300,
                "canvasWidth": 300,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Male",
                    "value": 65,
                    "color": "#F34635"
                  },
                  {
                    "label": "Female",
                    "value": 35,
                    "color": "#4BBEF7"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "percentage",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });
          };


          scope.barGraph( );
          scope.pieGraph( );
          scope.pie( );
          scope.pie2( );

          
        }
      }
    }
]);
graph.directive('locationResult', [
  "$http",
  "$window",
  function directive( $http , $window) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running locationResult directive' );

         scope.data_pie = [];
          scope.pie = function pie( ) {
            var pie = new d3pie("graph", {
    
              "size": {
                "canvasHeight": 400,
                "canvasWidth": 570,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Foodie",
                    "value": 0.4,
                    "color": "#F34635"
                  },
                  {
                    "label": "Time with others",
                    "value": 0.22,
                    "color": "#4BBEF7"
                  },
                  {
                    "label": "Retail therapy",
                    "value": 0.1,
                    "color": "#51851A"
                  },
                  {
                    "label": "Travel adventure",
                    "value": 0.05,
                    "color": "#7745FF"
                  },
                  {
                    "label": "Relaxation alone",
                    "value": 0.23,
                    "color": "#C560FF"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32
                },
                "inner": {
                  "format": "value"
                },
                "mainLabel": {
                  "font": "Montserrat-Regular",
                  "fontSize": 11
                },
                "percentage": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular",
                  "decimalPlaces": 0
                },
                "value": {
                  "color": "#e1e1e1",
                  "font": "Montserrat-Regular"
                },
                "lines": {
                  "enabled": true,
                  "color": "#cccccc"
                },
                "truncation": {
                  "enabled": true
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });

            console.log( pie.options.data.content );
            for( var x = 0; x < pie.options.data.content.length; x++ ) {
                scope.data_pie.push( { label: pie.options.data.content[ x ].label, value: pie.options.data.content[ x ].value, color: pie.options.data.content[ x ].color });
              }
            console.log( scope.data_pie );
          }
          scope.line = {};
          scope.lineGraph = function lineGraph( ) {
            console.log('line');
            scope.line.labels = ['', 'Engagement', 'Impressions', 'Likes'];
            scope.line.series = [];
            scope.line.data = [
              [0, 10, 5, 12, 20, 45, 30, 50, 70, 40]
            ];

            scope.line.options = {
              animation: false,
              scaleShowLabels : false,
              bezierCurve: false,
              scaleShowGridLines: false,
              scaleFontColor : "#000",
              pointDot : true,
              pointDotRadius: 0,
              datasetFill: false,
              scaleGridLineColor: 'white'
            };
          };

          scope.pieGraph = function pieGraph( ) {
            var pie = new d3pie("pie-graph", {
    
              "size": {
                "canvasHeight": 300,
                "canvasWidth": 200,
                "pieOuterRadius": "88%"
              },
              "data": {
                "content": [
                  {
                    "label": "Male",
                    "value": 68,
                    "color": "#F34635"
                  },
                  {
                    "label": "Female",
                    "value": 32,
                    "color": "#4BBEF7"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "percentage",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "linear",
                  "speed": 400,
                  "size": 8
                }
              }
              });
          };

          scope.barGraph = function barGraph( ) {
            var trace1 = {
              x: [0, 1, 2, 3, 4],
              y: [10, 25, 35, 25, 20],
              name: 'Male',
              type: 'bar',
              marker: {
                color: '#F34635',
                thicknessmode: 10
              }
            };

            var trace2 = {
              x: [0, 1, 2, 3, 4],
              y: [5, 15, 10, 15, 10],
              name: 'Female',
              type: 'bar',
              marker: {
                color: '#4BBEF7',
                thicknessmode: 10
              }
            };

            var data = [trace1, trace2];
            var layout = { 
                title: 'Frequency',
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 400,
                width: 480,
                bargap: 0.1,
                bargroupgap: 0.8,
                xaxis: { 
                  title: 'Age',
                  titlefont: {
                    size: 16,
                    color: '#A9ADB0',
                  },
                  showgrid: false,
                  showticklabels: false,
                  showline: true,
                },
                yaxis: {
                  showgrid: false,
                  showline: true,
                  showticklabels: false
                }
            };

            Plotly.newPlot('bar-graph', data, layout, {displayModeBar: false});
          };

          scope.circleGraph = function circleGraph( ) {
            var letters = '0123456789ABCDEF'.split('');
            var color = '#';
            var val = [
            {
              title: 'Healthy Carbs',
              subtitle: '',
              data: 200
            },
            {
              title: 'Yoga Poses',
              subtitle: '',
              data: 250,
            },
            {
              title: 'Outdoor Running',
              subtitle: '12000 people',
              data: 400,
            },
            {
              title: 'Cardio Kickboxing',
              'subtitle': '',
              'data': 210,
            },
            {
              title: 'Gym Workouts',
              subtitle: '',
              'data': 190
            }
            ];

            scope.circle = [];
            for( var x = 0; x < val.length; x++ )
            {
              val[ x ].data /= Math.pow(9, 3);
              var colors = randomColor({
                 luminosity: 'random',
                 hue: 'random'
              });
              scope.circle.push({ title: val[ x ].title, subtitle: val[ x ].subtitle, data: val[ x ].data + 0.4, color: colors });
            }

            console.log( scope.circle );
          };

          scope.datePicks = {};

          scope.dates = ["01/12/16","01/23/16","03/14/16","03/26/16","04/01/16","05/14/16","06/07/16","07/23/16"," - "];

          scope.customDatePicker = function customDatePicker( num ){
            var temp;

            if( num == 0 ){

              temp = scope.dates.pop();
              scope.dates.unshift( temp );

            }else if( num == 1 ){

              temp = scope.dates.shift();
              scope.dates.push( temp );

            }

            scope.datePicks.one = scope.dates[ 0 ];
            scope.datePicks.two = scope.dates[ 1 ];
            scope.datePicks.three = scope.dates[ 2 ];
            scope.datePicks.four = scope.dates[ 3 ];
            scope.datePicks.five = scope.dates[ 4 ]; 

            console.log(scope.dates);
          }

          scope.pie( );
          scope.lineGraph( );
          scope.pieGraph( );
          scope.barGraph( );
          scope.circleGraph( );
          scope.customDatePicker( 2 );


      }
    }
  }
]);

graph.directive('predictResultAverage', [
  "$http",
  "$window",
  function directive( $http , $window) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running predictResultAverage directive' );

        scope.allData = [
          {
            "Gender": "M",
            "Age": "20-30",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
          {
            "Gender": "F",
            "Age": "30-40",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
          {
            "Gender": "F",
            "Age": "30-40",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
          {
            "Gender": "F",
            "Age": "30-40",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
          
          {
            "Gender": "M",
            "Age": "10-20",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
          {
            "Gender": "F",
            "Age": "30-40",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
          {
            "Gender": "F",
            "Age": "30-40",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
          {
            "Gender": "F",
            "Age": "30-40",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
          {
            "Gender": "M",
            "Age": "40 above",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
          {
            "Gender": "M",
            "Age": "10-20",
            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
          },
        ];

        scope.getAverage = function getAverage(){

          var genderMale = 0;
          var ageArr = [];
          var ageCtr = [];

          for( var x = 0; x < scope.allData.length; x++ ){
            // console.log( scope.allData[x] );

            if( scope.allData[x].Gender == "M" ){
              genderMale++;
            }

            if( $.inArray( scope.allData[x].Age, ageArr ) == -1 ){
              ageArr.push( scope.allData[x].Age );
              ageCtr.push( 1 );
            }else{
              ageCtr[ ageArr.indexOf( scope.allData[x].Age ) ]++;  
            }

            if( x == (scope.allData.length-1) ){
              var male = genderMale;
              var female = scope.allData.length - genderMale;

              setTimeout(function(){
                scope.pieGraphAge( male, female );
                scope.barGraph( ageArr, ageCtr);
                scope.influBarGraph( ageArr, ageCtr);
              },200);
              
            }

          }

        }

        scope.pieGraphAge = function pieGraph( num1,num2 ) {
          var pie = new d3pie("age-pie-graph", {
  
            "size": {
              "canvasHeight": 100,
              "canvasWidth": 100,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 48,
                  "label": "Female",
                  "color": "#2ca02c"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 21,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

            });
        };

        scope.barGraph = function barGraph( arrX, arrY ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["20-30","30-40","40 Above","10-20"] ,
              y: [13,41,14,1],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 255,
                width: 250,
                bargap: 0.05,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('average-bar-graph', data, layout, {displayModeBar: false});
          };

          scope.influBarGraph = function barGraph( arrX, arrY ) {

            var trace1 = {
              x: ['A','B','C',],
              y: [6,2,2],
              marker:{
                color: ['#60BCD1', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 255,
                width: 250,
                bargap: 0.05,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('influ-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.getAverage();
        
        
      }
    }
  }
]);

graph
	.service('reportService', [
		function service( ) {
			var report = {};

			return {
				graphData: function graphData( obj_data, layout ) {
					if( obj_data && layout ) {
						return report = { data: obj_data, layout: layout };
					} else {
						return report;
					}
				},
				clearGraph: function graphData( ) {
					report = {};
					console.log( report );
				}
			}
		}
	]);

graph.directive('homeDataV2', [
  "$http",
  "$window",
  function directive( $http , $window) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running homeDataV2 directive' );

        scope.pieGraphAge = function pieGraph() {
          var pie = new d3pie("age-pie-graph", {
  
            "size": {
              "canvasHeight": 60,
              "canvasWidth": 60,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 60,
                  "label": "Female",
                  "color": "#2ca02c"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 40,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.pieGraphPersona = function pieGraphPersona() {
          var pie = new d3pie("persona-pie-graph", {
  
            "size": {
              "canvasHeight": 230,
              "canvasWidth": 230,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "0%",
            },
            "data": {
              "content": [
                
                {
                  "value": 40,
                  "label": "Travel Buff",
                  "color": "#4572A7"
                },
                {
                  "value": 20,
                  "label": "Luxury Shopper",
                  "color": "#AA4643"
                  
                },
                {
                  "value": 20,
                  "label": "Technophile",
                  "color": "#89A54E"
                },
                {
                  "value": 20,
                  "label": "Casual Gamers",
                  "color": "#71588F"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  // "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.barGraph = function barGraph( ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["20-30","30-40","40 Above","10-20"] ,
              y: [13,41,14,4],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 215,
                width: 210,
                bargap: 0.05,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('average-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.influBarGraph = function barGraph( ) {

            var trace1 = {
              x: ['A','B','C',],
              y: [6,2,2],
              marker:{
                color: ['#60BCD1', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 215,
                width: 210,
                bargap: 0.05,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('influ-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.right_top_nav = function (num) {
          
          $( '.right-top-nav li' ).removeClass('active');
          $( '.right-top-nav li .' + num ).addClass('active');

          var ctr = 0;

          if( num == 1 ){
            $( '.keytag-section' ).hide();
            $( '.gallery-section' ).hide();
            $( '.audience-section' ).hide();
            $( '.summary-tab' ).fadeIn();
            $( '.location-section' ).fadeIn();
          }else if( num == 2 ){
            $( '.location-section' ).hide();
            $( '.keytag-section' ).fadeIn();
            $( '.audience-section' ).hide();
            $( '.summary-tab' ).fadeIn();
            $( '.gallery-section' ).css({display:'inline'});
          }else if( num == 3 ){
            $( '.summary-tab' ).hide();
            $( '.keytag-section' ).hide();
            $( '.gallery-section' ).hide();
            $( '.audience-section' ).fadeIn();
          } 
        }

        var ctr_segment = 0;
          
        scope.segment = function segment(){

          $( '.chart-custom' ).css({display : 'inline-block'});
          $( '.gallery-custom' ).css({display : 'inline'});
          $( '.ul-chart' ).css({display : 'block'});
          $( '.right-seven-wrapper' ).css({display : 'none'});
          $( '.gallery-section' ).css({display : 'none'});

          if( ctr_segment == 0 ){
            scope.pieGraphAge( );
            scope.pieGraphPersona( );
            scope.barGraph( );
            scope.influBarGraph( );
            ctr_segment = 1;
          }
          
        }  

        scope.brand_explore = function brand_explore(){
          $( '.left-container' ).hide();
          $( '.left-menu2' ).show();
          console.log('sdfds');
          
        }  
        
        
      }
    }
  }
]);

graph.directive('travelersGraph', [
  "$http",
  "$window",
  "d3",
  function directive( $http , $window, d3) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running travelersGraph directive' );

        scope.pieGraphAge = function pieGraph() {
          var pie = new d3pie("age-pie-graph", {
  
            "size": {
              "canvasHeight": 60,
              "canvasWidth": 60,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 60,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 40,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.pieGraphPassport = function() {
          var pie = new d3pie("passport-pie-graph", {
  
            "size": {
              "canvasHeight": 64,
              "canvasWidth": 60,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "80%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 80,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 20,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.pieGraphPeople = function() {
          var pie = new d3pie("people-pie-graph", {
  
            "size": {
              "canvasHeight": 64,
              "canvasWidth": 60,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "80%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 30,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 70,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.barGraph = function barGraph( ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["Below 10","10-20","20-30","30-40","40 Above"] ,
              y: [35,45,30,20,15],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 225,
                width: 220,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('average-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.influBarGraph = function barGraph( ) {

            var trace1 = {
              x: ['A','B','C','D','E'],
              y: [13,15,10,5,2],
              marker:{
                color: [ 'rgba(204,204,204,1)','#60BCD1', 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 225,
                width: 220,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('influ-bar-graph', data, layout, {displayModeBar: false});
          };

          scope.pieGraphPersona = function pieGraphPersona() {
            var pie = new d3pie("persona-pie-graph", {
    
              "size": {
                "canvasHeight": 362,
                "canvasWidth": 350,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };

          scope.pieGraphAffinity = function pieGraphAffinity() {
            var pie = new d3pie("affinity-pie-graph", {
    
              "size": {
                "canvasHeight": 173,
                "canvasWidth": 185,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };



          scope.affinityGraph = function(){
              var trace1 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [150,250,200,350,300,450,500],
                line: {
                  color: '#60CFFF',
                }
              };

              var trace2 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [200,100,320,280,460,390,650],
                line: {
                  color: '#FFE8B3',
                }
              };

              var trace3 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [50,10,30,20,40,30,60],
                line: {
                  color: '#FFDE4F',
                }
              };

              var trace4 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [250,150,300,250,400,350,600],
                line: {
                  color: '#DA6349',
                }
              };

              var trace5 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [20,90,10,220,30,0,160],
                line: {
                  color: '#7B9AB6',
                }
              };

              var data = [trace1,trace2,trace3,trace4,trace5];

              var layout = { 
                        showlegend: false,
                        displayModeBar: false,
                        height: 450,
                        width: 650,
                        bargap: 1.5,
                        xaxis: { 
                          showgrid: true,
                          showticklabels: true,
                          showline: false,
                        },
                        yaxis: {
                          showgrid: true,
                          showline: false,
                          showticklabels: false,
                          zeroline: true,
                          grigwidth: 2
                        },
                    };

              Plotly.newPlot('affinity-graph', data, layout, {displayModeBar: false});
          }

          scope.trendingGraph = function(){
              var trace1 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [150,250,200,350,300,450,500],
                line: {
                  color: '#60CFFF',
                }
              };

              var trace2 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [200,100,320,280,460,390,650],
                line: {
                  color: '#FFE8B3',
                }
              };

              var trace3 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [50,10,30,20,40,30,60],
                line: {
                  color: '#FFDE4F',
                }
              };

              var trace4 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [250,150,300,250,400,350,600],
                line: {
                  color: '#DA6349',
                }
              };

              var trace5 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [20,90,10,220,30,0,160],
                line: {
                  color: '#7B9AB6',
                }
              };

              var data = [trace1,trace2,trace3,trace4,trace5];

              var layout = { 
                        showlegend: false,
                        displayModeBar: false,
                        height: 450,
                        width: 650,
                        bargap: 1.5,
                        xaxis: { 
                          showgrid: true,
                          showticklabels: true,
                          showline: false,
                        },
                        yaxis: {
                          showgrid: true,
                          showline: false,
                          showticklabels: false,
                          zeroline: true,
                          grigwidth: 2
                        },
                    };

              Plotly.newPlot('trending-graph', data, layout, {displayModeBar: false});
          }


          scope.initializeMap = function(){
            var address = "Singapore";

            map = new google.maps.Map(document.getElementById('map_canvas'), { 
                 mapTypeId: google.maps.MapTypeId.ROADMAP,
                 zoom: 13,
                 zoomControl: false,
              scaleControl: false,
              streetViewControl: false,
              rotateControl: false,
              fullscreenControl: false,
              disableDefaultUI: true
              });

            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({
                'address': address
               }, 
               function(results, status) {
                  if(status == google.maps.GeocoderStatus.OK) {
                     new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map
                     });
                     map.setCenter(results[0].geometry.location);
                  }
               });
          }

          scope.box_cloud = function(){
            var fill = d3.scale.category10();
            
            d3.layout.cloud().size([250, 140])
                .words([
                  "#whatcoloristhisdress", "#llamadrama", "#coffee", "Apple", "#computer", "#indianapolis", "#airplane"].map(function(d) {
                  return {text: d, size: 10 + Math.random() * 50};
                }))
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", first_box)
                .start();

            function first_box(words) {
              d3.select(".tags-wrapper").append("svg")
                  .attr("width", '100%')
                  .attr("height", '100%')
                .append("g")
                  .attr("transform", "translate(110,110)")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }
          }

          scope.box_cloud2 = function(){
            var fill = d3.scale.category10();
            
            d3.layout.cloud().size([250, 140])
                .words([
                  "#ChangiAriport", "#airplane", "#travel", "Goals", "#finally", "#holidays", "#summer"].map(function(d) {
                  return {text: d, size: 10 + Math.random() * 50};
                }))
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", first_box)
                .start();

            function first_box(words) {
              d3.select(".tags-wrapper2").append("svg")
                  .attr("width", '100%')
                  .attr("height", '100%')
                .append("g")
                  .attr("transform", "translate(110,110)")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }
          }

          scope.pieGraphAge( );
          scope.pieGraphPersona( );
          scope.pieGraphAffinity( );
          scope.barGraph( );
          scope.influBarGraph( );
          scope.initializeMap( );
          scope.box_cloud( );
          scope.affinityGraph( );
          scope.trendingGraph( );

          var box_cloud_ctr = 0;

          scope.next_tab = function(num){
            $( ".row_links ul li" ).removeClass( 'active' );
            $( ".row_links ul li:nth-child(" + num + ")" ).addClass( 'active' );
            $( ".row_links ul li .caret-up").hide();
            $( ".row_links ul li:nth-child(" + num + ") .caret-up" ).show();

            if( num == 1 ){
              $( '.location-section' ).hide();
              $( '.summary-section' ).show();
            }else if( num == 2 ){
              $( '.summary-section' ).hide();
              $( '.location-section' ).show();

              if( box_cloud_ctr == 0 ){

                scope.box_cloud2( );
                scope.pieGraphPassport( );
                scope.pieGraphPeople( );

                box_cloud_ctr = 1;
              }

              
            }
          }
      }
    }
  }
]);

graph.directive('contentPlanningGraph', [
  "$http",
  "$window",
  "d3",
  function directive( $http , $window, d3) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running contentPlanningGraph directive' );

        scope.pieGraphAge = function pieGraph() {
          var pie = new d3pie("age-pie-graph", {
  
            "size": {
              "canvasHeight": 60,
              "canvasWidth": 60,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 60,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 40,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.pieGraphPassport = function() {
          var pie = new d3pie("passport-pie-graph", {
  
            "size": {
              "canvasHeight": 64,
              "canvasWidth": 60,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "80%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 80,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 20,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.pieGraphPeople = function() {
          var pie = new d3pie("people-pie-graph", {
  
            "size": {
              "canvasHeight": 64,
              "canvasWidth": 60,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "80%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 30,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 70,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.barGraph = function barGraph( ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["Below 10","10-20","20-30","30-40","40 Above"] ,
              y: [35,45,30,20,15],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 225,
                width: 220,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('average-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.influBarGraph = function barGraph( ) {

            var trace1 = {
              x: ['A','B','C','D','E'],
              y: [13,15,10,5,2],
              marker:{
                color: [ 'rgba(204,204,204,1)','#60BCD1', 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 225,
                width: 220,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('influ-bar-graph', data, layout, {displayModeBar: false});
          };

          scope.pieGraphPersona = function pieGraphPersona() {
            var pie = new d3pie("persona-pie-graph", {
    
              "size": {
                "canvasHeight": 362,
                "canvasWidth": 350,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };

          scope.pieGraphAffinity = function pieGraphAffinity() {
            var pie = new d3pie("affinity-pie-graph", {
    
              "size": {
                "canvasHeight": 173,
                "canvasWidth": 185,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };



          scope.affinityGraph = function(){
              var trace1 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [150,250,200,350,300,450,500],
              };

              var trace2 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [200,100,320,280,460,390,650],
              };

              var trace3 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [50,10,30,20,40,30,60],
              };

              var trace4 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [250,150,300,250,400,350,600],
              };

              var data = [trace1,trace2,trace3,trace4];

              var layout = { 
                        showlegend: true,
                        displayModeBar: false,
                        height: 500,
                        width: 700,
                        bargap: 1.5,
                        xaxis: { 
                          showgrid: true,
                          showticklabels: true,
                          showline: true,
                        },
                        yaxis: {
                          showgrid: false,
                          showline: false,
                          showticklabels: false,
                          zeroline: false,
                          grigwidth: 2
                        }
                    };

              Plotly.newPlot('affinity-graph', data, layout, {displayModeBar: false});
          }

          scope.trendingGraph = function(){
              var trace1 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [150,250,200,350,300,450,500],
              };

              var trace2 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [200,100,320,280,460,390,650],
              };

              var trace3 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [50,10,30,20,40,30,60],
              };

              var trace4 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [250,150,300,250,400,350,600],
              };

              var data = [trace1,trace2,trace3,trace4];

              var layout = { 
                        showlegend: true,
                        displayModeBar: false,
                        height: 500,
                        width: 700,
                        bargap: 1.5,
                        xaxis: { 
                          showgrid: true,
                          showticklabels: true,
                          showline: true,
                        },
                        yaxis: {
                          showgrid: false,
                          showline: false,
                          showticklabels: false,
                          zeroline: false,
                          grigwidth: 2
                        }
                    };

              Plotly.newPlot('trending-graph', data, layout, {displayModeBar: false});
          }


          scope.initializeMap = function(){
            var address = "Singapore";

            map = new google.maps.Map(document.getElementById('map_canvas'), { 
                 mapTypeId: google.maps.MapTypeId.ROADMAP,
                 zoom: 13,
                 zoomControl: false,
              scaleControl: false,
              streetViewControl: false,
              rotateControl: false,
              fullscreenControl: false,
              disableDefaultUI: true
              });

            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({
                'address': address
               }, 
               function(results, status) {
                  if(status == google.maps.GeocoderStatus.OK) {
                     new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map
                     });
                     map.setCenter(results[0].geometry.location);
                  }
               });
          }

          scope.box_cloud = function(){
            var fill = d3.scale.category10();
            
            d3.layout.cloud().size([250, 140])
                .words([
                  "#whatcoloristhisdress", "#llamadrama", "#coffee", "Apple", "#computer", "#indianapolis", "#airplane"].map(function(d) {
                  return {text: d, size: 10 + Math.random() * 50};
                }))
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", first_box)
                .start();

            function first_box(words) {
              d3.select(".tags-wrapper").append("svg")
                  .attr("width", '100%')
                  .attr("height", '100%')
                .append("g")
                  .attr("transform", "translate(110,110)")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }
          }

          scope.box_cloud2 = function(){
            var fill = d3.scale.category10();
            
            d3.layout.cloud().size([250, 140])
                .words([
                  "#ChangiAriport", "#airplane", "#travel", "Goals", "#finally", "#holidays", "#summer"].map(function(d) {
                  return {text: d, size: 10 + Math.random() * 50};
                }))
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", first_box)
                .start();

            function first_box(words) {
              d3.select(".tags-wrapper2").append("svg")
                  .attr("width", '100%')
                  .attr("height", '100%')
                .append("g")
                  .attr("transform", "translate(110,110)")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }
          }

          scope.pieGraphAge( );
          scope.pieGraphPersona( );
          // scope.pieGraphAffinity( );
          scope.barGraph( );
          scope.influBarGraph( );
          // scope.initializeMap( );
          scope.affinityGraph( );
          // scope.trendingGraph( );

          var box_cloud_ctr = 0;

          scope.next_tab = function(num){
            $( ".row_links ul li" ).removeClass( 'active' );
            $( ".row_links ul li:nth-child(" + num + ")" ).addClass( 'active' );
            $( ".row_links ul li .caret-up").hide();
            $( ".row_links ul li:nth-child(" + num + ") .caret-up" ).show();

            if( num == 1 ){
              $( '.not-audience-section' ).show();
              $( '.location-section' ).hide();
              $( '.audience-section' ).hide();
              $( '.temporary' ).hide();
              $( '.analytics-section' ).show();
            }else if( num == 2 ){
              $( '.not-audience-section' ).show();
              $( '.analytics-section' ).hide();
              $( '.audience-section' ).hide();
              $( '.location-section' ).show();
              $( '.temporary' ).hide();
              if( box_cloud_ctr == 0 ){
                scope.box_cloud( );
                scope.box_cloud2( );

                box_cloud_ctr=1;
              } 
            }else if( num == 3 ){
              $( '.not-audience-section' ).hide();
              $( '.audience-section' ).show();
              $( '.temporary' ).hide();
            }
          }
      }
    }
  }
]);

graph.directive('brandsGraph', [
  "$http",
  "$window",
  "d3",
  function directive( $http , $window, d3) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running brandsGraph directive' );

        scope.pieGraphAge = function pieGraph() {
          var pie = new d3pie("age-pie-graph", {
  
            "size": {
              "canvasHeight": 120,
              "canvasWidth": 120,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 60,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 40,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };


        scope.barGraph = function barGraph( ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["Below 10","10-20","20-30","30-40","40 Above"] ,
              y: [35,45,30,20,15],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 275,
                width: 270,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('average-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.influBarGraph = function barGraph( ) {

            var trace1 = {
              x: ['A','B','C','D','E'],
              y: [13,15,10,5,2],
              marker:{
                color: [ 'rgba(204,204,204,1)','#60BCD1', 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 275,
                width: 270,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('influ-bar-graph', data, layout, {displayModeBar: false});
          };


        scope.pieGraphAge2 = function pieGraph() {
          var pie = new d3pie("age-pie-graph2", {
  
            "size": {
              "canvasHeight": 80,
              "canvasWidth": 80,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 60,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 40,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };


        scope.barGraph2 = function barGraph( ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["Below 10","10-20","20-30","30-40","40 Above"] ,
              y: [35,45,30,20,15],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 235,
                width: 230,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('average-bar-graph2', data, layout, {displayModeBar: false});
          };

        scope.influBarGraph2 = function barGraph( ) {

            var trace1 = {
              x: ['A','B','C','D','E'],
              y: [13,15,10,5,2],
              marker:{
                color: [ 'rgba(204,204,204,1)','#60BCD1', 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 235,
                width: 230,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('influ-bar-graph2', data, layout, {displayModeBar: false});
          };

        scope.pieGraphAge3 = function pieGraph() {
          var pie = new d3pie("age-pie-graph3", {
  
            "size": {
              "canvasHeight": 80,
              "canvasWidth": 80,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 60,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 40,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };


        scope.barGraph3 = function barGraph( ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["Below 10","10-20","20-30","30-40","40 Above"] ,
              y: [35,45,30,20,15],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 235,
                width: 230,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('average-bar-graph3', data, layout, {displayModeBar: false});
          };

        scope.influBarGraph3 = function barGraph( ) {

            var trace1 = {
              x: ['A','B','C','D','E'],
              y: [13,15,10,5,2],
              marker:{
                color: [ 'rgba(204,204,204,1)','#60BCD1', 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 235,
                width: 230,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('influ-bar-graph3', data, layout, {displayModeBar: false});
          };

        scope.pieGraphPersona = function pieGraphPersona() {
            var pie = new d3pie("affinity-wheel-graph", {
    
              "size": {
                "canvasHeight": 145,
                "canvasWidth": 145,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };

        scope.pieGraphKeyboard = function() {
            var pie = new d3pie("keyboard-wheel-graph", {
    
              "size": {
                "canvasHeight": 145,
                "canvasWidth": 145,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };

        scope.pieGraphObject = function() {
            var pie = new d3pie("object-wheel-graph", {
    
              "size": {
                "canvasHeight": 145,
                "canvasWidth": 145,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };

        scope.pieGraphLocation = function() {
            var pie = new d3pie("location-wheel-graph", {
    
              "size": {
                "canvasHeight": 145,
                "canvasWidth": 145,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };

        scope.pieGraphOne = function() {
            var pie = new d3pie("one-pie-graph", {
    
              "size": {
                "canvasHeight": 185,
                "canvasWidth": 145,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };

        scope.pieGraphTwo = function() {
            var pie = new d3pie("two-pie-graph", {
    
              "size": {
                "canvasHeight": 185,
                "canvasWidth": 145,
                "pieOuterRadius": "100%",
                "pieInnerRadius": "70%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 30,
                    "label": "Travel Buff",
                    "color": "#24C28D"
                  },
                  {
                    "value": 20,
                    "label": "Luxury Shopper",
                    "color": "#EACD5F"
                    
                  },
                  {
                    "value": 20,
                    "label": "Soccer Fans",
                    "color": "#6CD2D6"
                  },
                  {
                    "value": 20,
                    "label": "Casual Gamers",
                    "color": "#DA6349"
                  },
                  {
                    "value": 10,
                    "label": "Technophile",
                    "color": "#7B9AB6"
                  },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };


        scope.affinityGraph = function(){
              var trace1 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [150,250,200,350,300,450,500],
              };

              var trace2 = {
                x: ['0','2','4','6','8','10','12','14','16'],
                y: [200,100,320,280,460,390,650],
              };

              var data = [trace1,trace2];

              var layout = { 
                        showlegend: true,
                        displayModeBar: false,
                        height: 270,
                        width: 700,
                        bargap: 1.5,
                        xaxis: { 
                          showgrid: true,
                          showticklabels: true,
                          showline: true,
                        },
                        yaxis: {
                          showgrid: false,
                          showline: false,
                          showticklabels: false,
                          zeroline: false,
                          grigwidth: 2
                        }
                    };

              Plotly.newPlot('affinity-graph', data, layout, {displayModeBar: false});
          }



        scope.box_cloud = function(){
            var fill = d3.scale.category10();
            
            d3.layout.cloud().size([250, 110])
                .words([
                  "#whatcoloristhisdress", "#llamadrama", "#coffee", "Apple", "#computer", "#indianapolis", "#airplane"].map(function(d) {
                  return {text: d, size: 10 + Math.random() * 50};
                }))
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", first_box)
                .start();

            function first_box(words) {
              d3.select(".tags-wrapper").append("svg")
                  .attr("width", '100%')
                  .attr("height", '100%')
                .append("g")
                  .attr("transform", "translate(110,110)")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }
          }

        scope.box_cloud2 = function(){
            var fill = d3.scale.category10();
            
            d3.layout.cloud().size([250, 110])
                .words([
                  "#ChangiAriport", "#airplane", "#travel", "Goals", "#finally", "#holidays", "#summer"].map(function(d) {
                  return {text: d, size: 10 + Math.random() * 50};
                }))
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", first_box)
                .start();

            function first_box(words) {
              d3.select(".tags-wrapper2").append("svg")
                  .attr("width", '100%')
                  .attr("height", '100%')
                .append("g")
                  .attr("transform", "translate(110,110)")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }
          }

          scope.pieGraphAge( );
          scope.barGraph( );
          scope.influBarGraph( );

          scope.pieGraphAge2( );
          scope.barGraph2( );
          scope.influBarGraph2( );

          scope.pieGraphAge3( );
          scope.barGraph3( );
          scope.influBarGraph3( );

          scope.pieGraphPersona( );
          scope.pieGraphKeyboard( );
          scope.pieGraphObject( );
          scope.pieGraphLocation( );
          scope.pieGraphOne( );
          scope.pieGraphTwo( );
          
          scope.affinityGraph( );  
          scope.box_cloud( );
          scope.box_cloud2( );
      }
    }
  }
]);

graph.directive('socialGraph', [
  "$http",
  "$window",
  function directive( $http , $window) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running socialGraph directive' );

        scope.pieGraphAge = function pieGraph() {
          var pie = new d3pie("age-pie-graph", {
  
            "size": {
              "canvasHeight": 110,
              "canvasWidth": 110,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 60,
                  "label": "Female",
                  "color": "#3BAF9A",
                  "stroke": "none"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 40,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.barGraph = function barGraph( ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["20-30","30-40","40-50","50 Above","10-20"] ,
              y: [10,13,25,18,6],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 250,
                width: 270,
                bargap: 0.2,
                plot_bgcolor: '#293141',
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2,
                }
            };

            Plotly.newPlot('average-bar-graph', data, layout, {displayModeBar: false});
          }; 

        scope.pieGraphAge2 = function pieGraph() {
          var pie = new d3pie("analytic-age-pie-graph", {
  
            "size": {
              "canvasHeight": 80,
              "canvasWidth": 80,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 60,
                  "label": "Female",
                  "color": "#3BAF9A",
                  "stroke": "none"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 40,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.barGraph2 = function barGraph( ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["20-30","30-40","40-50","50 Above","10-20"] ,
              y: [10,13,25,18,6],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 240,
                width: 270,
                bargap: 0.2,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2,
                }
            };

            Plotly.newPlot('analytic-average-bar-graph', data, layout, {displayModeBar: false});
          };     

        scope.analyticsLineChart = function(){
          var trace1 = {
            x: ['M', 'T', 'W', 'TH', 'F','ST','S'],
            y: [1, 2, 4, 8, 16, 12, 14] , 
            margin: { t: 0 } ,
            type: 'scatter'
          };

          var data = [trace1];

          var layout = { 
                showlegend: false,
                displayModeBar: false,
                height: 245,
                width: 620,
                xaxis: { 
                  // showgrid: false,
                  showticklabels: true,
                  showline: false,
                  side: 'top'
                },
                yaxis: {
                  // showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  // grigwidth: 2,
                }
            };

          Plotly.newPlot('analytic-line-graph', data , layout, {displayModeBar: false});
        }   


        scope.initializeMap = function(){
            var address = "Singapore";

            map = new google.maps.Map(document.getElementById('analytics-map'), { 
                 mapTypeId: google.maps.MapTypeId.ROADMAP,
                 zoom: 13,
                 zoomControl: false,
              scaleControl: false,
              streetViewControl: false,
              rotateControl: false,
              fullscreenControl: false,
              disableDefaultUI: true
              });

            var geocoder = new google.maps.Geocoder();

            geocoder.geocode({
                'address': address
               }, 
               function(results, status) {
                  if(status == google.maps.GeocoderStatus.OK) {
                     new google.maps.Marker({
                        position: results[0].geometry.location,
                        map: map
                     });
                     map.setCenter(results[0].geometry.location);
                  }
               });
          }

        scope.socialClick = function socialClick(num){
          $( ".nav-social li" ).removeClass( 'active' );
          // $( ".nav-social li:nth-child("+ num +")" ).addClass( 'active' );

          if( num == 1 ){
            $("#main").hide();
            $("#family").show();
          }else if( num == 2 ){
            $("#microscope").hide();
            $("#people").hide();
            $("#analytics").show();
            $( ".nav-social li:nth-child("+ 1 +")" ).addClass( 'active' );
            scope.initializeMap();
          }else if( num == 3 ){
            
          }else if( num == 4 ){
            $("#microscope").hide();
            $("#analytics").hide();
            $("#people").show();
            $( ".nav-social li:nth-child("+ 3 +")" ).addClass( 'active' );
          }
        }

        scope.pieGraphAge();
        scope.barGraph();
        scope.pieGraphAge2();
        scope.barGraph2();
        scope.analyticsLineChart();
        
        
      }
    }
  }
]);

graph.directive('tempDirective', [
  "$http",
  "$window",
  function directive( $http , $window) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        

        
        
      }
    }
  }
]);

graph.directive('microGraph', [
  "$http",
  "$window",
  "d3",
  function directive( $http , $window, d3) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running travelersGraph directive' );

        scope.pieGraphAge = function pieGraph() {
          var pie = new d3pie("age-pie-graph", {
  
            "size": {
              "canvasHeight": 60,
              "canvasWidth": 60,
              "pieOuterRadius": "100%",
              "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  // "value": (num2*10),
                  "value": 60,
                  "label": "Female",
                  "color": "#18B08D"
                  
                },
                {
                  // "value": (num1*10),
                  "value": 40,
                  "label": "Male",
                  "color": "#CCD1D5"
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.barGraph = function barGraph( ) {

            var trace1 = {
              // x: arrX,
              // y: arrY,
              x: ["Below 10","10-20","20-30","30-40","40 Above"] ,
              y: [35,45,30,20,15],
              marker:{
                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 225,
                width: 220,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('average-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.influBarGraph = function barGraph( ) {

            var trace1 = {
              x: ['A','B','C','D','E'],
              y: [13,15,10,5,2],
              marker:{
                color: [ 'rgba(204,204,204,1)','#60BCD1', 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)'],
                thicknessmode: 1
              },
              type: 'bar'
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                showlegend: false,
                displayModeBar: false,
                height: 225,
                width: 220,
                bargap: 0.3,
                xaxis: { 
                  showgrid: false,
                  showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('influ-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.engageBarGraph = function barGraph( ) {

            var trace1 = {
              y: ['Fashion','Relationship','Celebrations','Business','Arts'],
              x: [22,9,4,5,2],
              // marker:{
                // color: [ 'rgba(204,204,204,1)','#60BCD1', 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)'],
              //   thicknessmode: 1
              // },
              type: 'bar',
              orientation: 'h',
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                // showlegend: false,
                displayModeBar: false,
                height: 400,
                width: 400,
                bargap: 0.7,
                
                xaxis: { 
                  showgrid: false,
                  // showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  // showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('engage-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.celebBarGraph = function barGraph( ) {

            var trace1 = {
              y: ['Fashion','Movie','Music','Video','Tv'],
              x: [4,32,13,2,2],
              // marker:{
                // color: [ 'rgba(204,204,204,1)','#60BCD1', 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)'],
              //   thicknessmode: 1
              // },
              type: 'bar',
              orientation: 'h',
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                // showlegend: false,
                displayModeBar: false,
                height: 400,
                width: 850,
                bargap: 0.7,
                
                xaxis: { 
                  showgrid: false,
                  // showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  // showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('celeb-bar-graph', data, layout, {displayModeBar: false});
          };

        scope.pieGraphPersona = function pieGraphPersona() {
            var pie = new d3pie("persona-pie-graph", {
    
              "size": {
                "canvasHeight": 185,
                "canvasWidth": 185,
                "pieOuterRadius": "100%",
              },
              "data": {
                "content": [
                  
                  {
                    "value": 84.9,
                    "label": "Micro Influencer",
                    "color": "#5E97F6"
                  },
                  {
                    "value": 15.1,
                    "label": "Advocate",
                    "color": "#DB4437"
                    
                  },
                  // {
                  //   "value": 20,
                  //   "label": "Soccer Fans",
                  //   "color": "#6CD2D6"
                  // },
                  // {
                  //   "value": 20,
                  //   "label": "Casual Gamers",
                  //   "color": "#DA6349"
                  // },
                  // {
                  //   "value": 10,
                  //   "label": "Technophile",
                  //   "color": "#7B9AB6"
                  // },
                ]
              },
              "labels": {
                "outer": {
                  "pieDistance": 32,
                  "format": "none",
                },
                "inner": {
                    "format": "none",
                    "hideWhenLessThanPercentage": null
                }
              },
              "effects": {
                "pullOutSegmentOnClick": {
                  "effect": "none"
                }
              },
              tooltips: {
                enabled: true,
                type: "placeholder",
                string: "{label} : {value}"
              }
             

            });
          };

        scope.pieGraphCulture = function pieGraph() {
          var pie = new d3pie("culture-pie-graph", {
  
            "size": {
              "canvasHeight": 250,
              "canvasWidth": 250,
              "pieOuterRadius": "100%",
              // "pieInnerRadius": "75%",
            },
            "data": {
              "content": [
                {
                  "value": 15,
                  "label": "America",
                  "color": "#3494BA"
                  
                },
                {
                  "value": 4,
                  "label": "China",
                  "color": "#75BDA7"
                },
                {
                  "value": 2,
                  "label": "Hong Kong",
                  "color": "#84ACB6"
                  
                },
                {
                  "value": 2,
                  "label": "Japan",
                  "color": "#1F5970"
                },
                {
                  "value": 8,
                  "label": "Korea",
                  "color": "#3B7C68"
                  
                },
                {
                  "value": 4,
                  "label": "Malaysia",
                  "color": "#466D76"
                },
                {
                  "value": 47,
                  "label": "Singapore",
                  "color": "#55ADCF"
                  
                },
                {
                  "value": 2,
                  "label": "Sweden",
                  "color": "#96CAD1"
                },
                {
                  "value": 16,
                  "label": "Taiwan",
                  "color": "#A3CFC0"
                  
                },
              ]
            },
            "labels": {
              "outer": {
                "pieDistance": 32,
                "format": "none",
              },
              "inner": {
                  "format": "none",
                  "hideWhenLessThanPercentage": null
              }
            },
            "effects": {
              "pullOutSegmentOnClick": {
                "effect": "none"
              }
            },
            tooltips: {
              enabled: true,
              type: "placeholder",
              string: "{label} : {value}"
            }
           

          });
        };

        scope.locationBarGraph = function barGraph( ) {

            var trace1 = {
              y: ['Bookstore','Café','Club','Office','Park','Restaurant'],
              x: [1,14,1,1,1,2],
              // marker:{
                // color: [ 'rgba(204,204,204,1)','#60BCD1', 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)' , 'rgba(204,204,204,1)'],
              //   thicknessmode: 1
              // },
              type: 'bar',
              orientation: 'h',
            };


            var data = [trace1];

            var layout = { 
                barmode: 'stack',
                // showlegend: false,
                displayModeBar: false,
                height: 400,
                width: 400,
                bargap: 0.7,
                
                xaxis: { 
                  showgrid: false,
                  // showticklabels: false,
                  showline: false,
                },
                yaxis: {
                  showgrid: false,
                  showline: false,
                  // showticklabels: false,
                  zeroline: false,
                  grigwidth: 2
                }
            };

            Plotly.newPlot('location-bar-graph', data, layout, {displayModeBar: false});
          };



          scope.pieGraphAge( );
          scope.barGraph( );
          scope.influBarGraph( );
          scope.pieGraphPersona( );

          scope.next_tab = function(num){
            $( ".row_links ul li" ).removeClass( 'active' );
            $( ".row_links ul li:nth-child(" + num + ")" ).addClass( 'active' );
            $( ".row_links ul li .caret-up").hide();
            $( ".row_links ul li:nth-child(" + num + ") .caret-up" ).show();

            if( num == 2 ){
              $( '#analytics-section' ).css({display:'none'});
              $( '#advocate-section' ).css({display:'none'});
              $( '#micro-section' ).show();
            }else if( num == 1 ){
              $( '#analytics-section' ).show();
              $( '#micro-section' ).css({display:'none'});
              $( '#advocate-section' ).css({display:'none'});
            }else if( num == 3 ){
              $( '#analytics-section' ).css({display:'none'});
              $( '#advocate-section' ).show();
              $( '#micro-section' ).css({display:'none'});
              scope.engageBarGraph();
              scope.celebBarGraph();
              scope.locationBarGraph();
              scope.pieGraphCulture( );
              
            }
          }
      }
    }
  }
]);

