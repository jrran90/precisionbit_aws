var upload = angular.module('upload', ['ngMaterial']);

upload.run(function() {
	console.log('upload running');
})
upload
	.factory('nodeUrl',[
	      function factory(){
	        return {
	          url: 'http://52.77.17.243/',
	          // url: 'http://localhost:8000/',
	          header: {
	            headers: {
	              'Content-Type' : 'application/x-www-form-urlencoded',
	              'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
	            }
	          }
	        }
	      }
	  ]);
upload
	.directive('uploadClient',[
	"$http",
	"nodeUrl",
	"serverUrl",
	"$mdDialog", 
	"$mdMedia",
	function directive( $http, nodeUrl, serverUrl, $mdDialog, $mdMedia ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('client upload');

				var imgData = {};
				var category = {};
				var dataCategories ={};
				scope.userImageUploads = {};
				scope.CategoryImages = {};
				scope.NumImageUploads = {};
				scope.preview = 'img/predict-gallery-v2/1.png';
				scope.has_img = false;
				scope.selectedImg = "";
				scope.is_preview = false;
				scope.IMGfilename = '';

				scope.getUserInfo = function getUserInfo () {
					$http.get( serverUrl.url + 'getuserdata' )
						.success( function success(response) {
							imgData._id = response[0]._id; 
							if ( response[0].client_images ) {
								scope.userImageUploads = response[0].client_images;
								console.log(scope.userImageUploads);
								scope.preview = 'http://52.77.17.243/get_client_personal_upload/' + response[0].client_images[0].filename;
								scope.has_img = true;

							} else {
               					 console.log('no image');
							}
							// scope.fetchPersonalImage( response[0]._id );
						} );
				}

				scope.backToCategory = function backToCategory() {

					if( $( '.view-type' ).text() == ' Filmstrip' ){
						$('.filmstrip').fadeIn();
						$('.gallery-category-images').hide();
					}else if( $( '.view-type' ).text() == ' List' ){
						$('.list').fadeIn();
						$('.gallery-category-images').hide();
					}else{
						$('.gallery').fadeIn();
						$('.gallery-category-images').hide();
					}

					
				}

				scope.getCategoryImages = function getCategoryImages (category) {
					console.log(category);
					$('.category-title').text(category);

					scope.CategoryImages = {};

					$http.get( serverUrl.url + 'getuserdata' )	
						.success( function success(response) {
							imgData._id = response[0]._id; 

							var categoryImages = [];

							if ( response[0].client_images ) {

								for( a in response[0].client_images ){
									if( response[0].client_images[a].category == category ){
										categoryImages.push( response[0].client_images[a] );
									}
								}

								scope.CategoryImages = categoryImages;
								scope.has_img = true;

							} else {
               					 console.log('no image');
							}
							console.log( scope.has_img );
						} );

						$('.filmstrip').hide();
						$('.list').hide();
						$('.gallery').hide();
						$('.gallery-category-images').fadeIn();
					

				}

				scope.downloadImg = function downloadImg() {
					var file = {};
					file.filename = scope.IMGfilename;
					file.filepath = scope.preview;
         			console.log(file);
					$http.post( serverUrl.url + 'downloadImage' , file )
						.success( function( response ) {
							console.log( response );
						} );
				}
				scope.getUserInfo( );

				scope.testApi = function testApi( ) {
					console.log('test')
					$http.get(nodeUrl.url)
					.success(function( response ){
						// console.log('res', response );
					})
					.error(function( error ){
						console.log( error );
					});
				}

				scope.showUploadInput = function showUploadInput () {
					$( '#imgupload-wrapper' ).fadeToggle();
					$( '.gallery-category-images' ).hide();
					$( '.gallery' ).fadeIn();

				}
				scope.previewImg = function previewImg (filename) {
					scope.preview = 'http://52.77.17.243/get_client_personal_upload/' + filename;
					scope.IMGfilename = filename;
				}
				scope.previewImgDemo = function previewImgDemo (filename) {
					scope.preview = 'img/predict-gallery-v2/' + filename;
				}
				scope.images = false;

				
				scope.uploadsImg = [];

				scope.uploadFile = function uploadFile() {
					console.log('initialize upload');

			      	var pro = $("#input-id").fileinput({  
				    	uploadUrl: nodeUrl.url + 'client_upload',// server upload action
				        uploadAsync: true,
				        uploadExtraData: function(previewId,index) {  // callback example
				            var out = {'image_question' : $('.kv-input:eq('+index+')').val() };
				            return out;
				        }
				    }).on('fileclear', function(event) {
					   $( '#imgupload-wrapper' ).fadeOut();
					}).on('filepreupload', function(event, data, previewId, index) {
				    	console.log(data);
				    	console.log( $('.group-name:visible').val()  );
				    	if( $('.group-name:visible').val() == "" ){
				    		sweetAlert("", "Please Input Group Name!", "error");
				    		return {
						           message: '',
						           data: {key1: 'Key 1', detail1: 'Detail 1'}
						       };
				    	}
				    }).on('fileuploaded', function(event, data, id, index) {
				    
				    	category.category_title = $('.group-name:visible').val();
				    	console.log(data);
				    	// clarifai api
				    	if( data.response == 'true'){
				    		sweetAlert("", "Image Duplicate! Image Not Uploaded.", "error");
				    	}else{
				    		console.log("IN");
				    		category.category_title = $('.group-name:visible').val();
					    
					    	imgData.category = category.category_title;
					    	imgData.filename = data.response.image;
					    	imgData.timeCreated = new Date();
					    	scope.uploadsImg.push( data.response.image );
					    	
					    	$http.post( serverUrl.url + 'userImageUploads' , imgData )
					    		.success( function success ( response ) {
					    			// console.log( response );
					    			
					    			scope.images = true;
		              				scope.has_img = false;
					    			scope.getUserInfo();
					    		} );

					    	$http.post( serverUrl.url + 'userCategoryUploads' , category )
					    		.success( function success ( response ) {
					    			// console.log( response );
					    		} );
				    	}	

				    }).on('filebatchuploadcomplete', function(){
				    	sweetAlert("", "Upload Successful!", "success");
				    	$( '#imgupload-wrapper' ).fadeOut();
				    	 console.log('File batch upload success');
				    }).on("filepredelete", function(jqXHR) {
				        var abort = true;
				        if (confirm("Are you sure you want to delete this image?")) {
				            abort = false;
				        }
				        return abort; // you can also send any data/object that you can receive on `filecustomerror` event
				    });

				}
				scope.deleteGroup = function deleteGroup ( ev, filename ) {
					console.log( filename );

					var postData = {
						'_id' 		: imgData._id,
						// 'filename'  : filename.filename
						'category' : filename.category
					};
					console.log(postData);
					 var confirm = $mdDialog.confirm()
				          .title('Would you like to delete this Group?')
				          .textContent('The images will not be recovered.')
				          .ariaLabel('Lucky day')
				          .targetEvent(ev)
				          .ok('Okay')
				          .cancel('Cancel');
					    $mdDialog.show(confirm).then(function() {					
					      	// scope.userImageUploads.splice(scope.userImageUploads.indexOf(filename),1);					
					      	$('.grp[data-category="'+filename.category+'"]').fadeOut();	
							$http.post( serverUrl.url + 'deleteUserGroup' , postData )
							.success( function success ( response ) {
								console.log( response );
								console.log('deleted');
							} );
				    }, function() {
				      console.log('cancel');
				    });

				}
				scope.deleteImg = function deleteImg ( ev, filename ) {
					console.log( filename );

					var postData = {
						'_id' 		: imgData._id,
						'filename'  : filename.filename
						// 'category' : filename.category
					};
					console.log(postData);
					 var confirm = $mdDialog.confirm()
				          .title('Would you like to delete this Image?')
				          .textContent('This image will not be recovered.')
				          .ariaLabel('Lucky day')
				          .targetEvent(ev)
				          .ok('Okay')
				          .cancel('Cancel');
					    $mdDialog.show(confirm).then(function() {					
					      	scope.userImageUploads.splice(scope.userImageUploads.indexOf(filename),1);	

					      	$('.grp[data-filename="'+filename.filename+'"]').fadeOut();	
							$http.post( serverUrl.url + 'deleteUserImage' , postData )
							.success( function success ( response ) {
								console.log( response );
								console.log('deleted');
							} );
				    }, function() {
				      console.log('cancel');
				    });

				}
				scope.uploadFile();
				scope.testApi();
			}
		}
	}
]);