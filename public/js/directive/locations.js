var locations = angular.module('locations', []);

locations.run(function( ) {
	console.log('locations module running');
});

locations.
	directive('locationPage', [
		"$http",
		"serverUrl",
		"$mdDialog",
		function directive( $http, serverUrl, $mdDialog ) {
			return {
				restrict: "A",
				scope: true,
				link: function link( scope, element, attributeSet ) {
					console.log(' locationPage directive running');

					scope.map = {};
					scope.map_results = {};
					scope.map_status = false;
					scope.load = true;
					scope.map_data = [];
					var counter_inc = 0;
					scope.location_list = {};
					// scope.map.location = 'KidZania, Singapore';

					scope.setLocation = function setLocation( map ) {
						console.log( map );
						var address = map.place.formatted_address;
						var map = new google.maps.Map(document.getElementById('map'), { 
				       mapTypeId: google.maps.MapTypeId.ROADMAP,
				       zoom: 16,
				   	});
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode({
					      'address': address
					   }, 
					   function(results, status) {
					   	console.log( status );
					      if(status == google.maps.GeocoderStatus.OK) {
					         new google.maps.Marker({
					            position: results[0].geometry.location,
					            map: map
					         });
					        map.setCenter(results[0].geometry.location);
					      }
					   });
						console.log( scope.map_results );
						scope.map_data.push({id: counter_inc++, place_name: scope.map_results.place.name, place_formatted: scope.map_results.place.formatted_address, place_website: scope.map_results.place.website });
						scope.map_status = true; 

					};


					scope.defaultMap = function defaultMap( ) {
						var address = "Singapore";
						var map = new google.maps.Map(document.getElementById('map'), { 
				       mapTypeId: google.maps.MapTypeId.ROADMAP,
				       zoom: 15,
				   	});
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode({
					      'address': address
					   }, 
					   function(results, status) {
					      if(status == google.maps.GeocoderStatus.OK) {
					         new google.maps.Marker({
					            position: results[0].geometry.location,
					            map: map
					         });
					         map.setCenter(results[0].geometry.location);
					      }
					   });
					};

					// save location
					scope.saveLocation = function saveLocation( response ) {

						console.log( response );
						$('.save_' + response.id ).attr('disabled', true);
						$('.span_' + response.id ).hide();
						$('#image_loader_' + response.id ).fadeIn();
						$http.post( serverUrl.url + 'location/create', { location_name: response.place_name })
						.success(function( result ){
							$('#image_loader_' + response.id ).hide();
							$('.done_' + response.id).fadeIn();
							scope.getLocations( );
						})
						.error(function( err ){
							alert('Please check your connection');
							$('.save_' + response.id ).attr('disabled', false);
							$('#image_loader_' + response.id ).hide();
							$('.span_' + response.id ).fadeIn();
						});
					};

					scope.getLocations = function getLocations( ) {
						$http.get( serverUrl.url + 'location/get')
						.success(function( response ) {
							console.log( response );
							scope.location_list = response;
							// scope.location_list._id = response[0]._id;
						});
					};

					scope.deleteLocation = function deleteLocation( evt, response, id ) {
						console.log( response );
						 var confirm = $mdDialog.confirm()
                  .title('Are you sure you want to delete this location data?')
                  .textContent('This data will permanently delete.')
                  .ariaLabel('Lucky day')
                  .targetEvent(evt)
                  .ok('Delete')
                  .cancel('Cancel');
                $mdDialog.show(confirm).then(function() {
									$http.post( serverUrl.url + 'location/delete', { _id: response._id })
									.success(function( result ) {
									 	$('#list_' + response.location_id ).addClass('fadeOutLeft').fadeOut(600);
									})
									.error(function( err ){
										console.log( err );
										alert('Please check your connection.');
									});
            }, function() {
              console.log('cancel');
            });
					};
					scope.defaultMap( );
					scope.getLocations( );
				}
			}
		}
]);