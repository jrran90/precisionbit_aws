var page = angular.module('page', []);

page.directive('pageLoad',[
  "pageService",
  function directive( pageService ) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log('page load running');
        $('.custom-wapper').addClass('remove-height');
        scope.menu = {};

        scope.menu.isOpen = false;

        // scope.$watch(function(){
        //   return pageService.pageStatus( );
        // }, function( newValue, oldValue ){
        //   console.log( newValue );
        // });

        scope.menuOpen = function menuOpen( ) {
          $('.navigation-top-menu').slideToggle(500);
        }

        setTimeout(function() {
          $('.loader-wrapper').css({"transform":"scale(0.2"});
          $('.loader-wrapper').animate({"margin-top" : "-200%"});
          $('.custom-wapper').removeClass('remove-height');
          setTimeout(function() {
            $('.logo-container img').css({"transform":"scale(1)"});
          }, 500);
        }, 500);
      }
    }
  }
]);
page.directive('home', [
  "d3",
  function directive( d3 ) {
    return {
      restrict: "A",
      scope: true,
      link: function link( scope, element, attributeSet ) {
        console.log( 'running home directive' );
        

        var ctr = 0;

        scope.initialize = function(){
          if( ctr == 0){
            console.log("inisdanfidsahfusdah");
            var fill = d3.scale.category10();
            
            d3.layout.cloud().size([250, 150])
                .words([
                  "#whatcoloristhisdress", "#llamadrama", "#coffee", "Apple", "#computer", "#indianapolis", "#airplane"].map(function(d) {
                  return {text: d, size: 10 + Math.random() * 50};
                }))
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", first_box)
                .start();

            function first_box(words) {
              d3.select(".first-word-cloud").append("svg")
                  .attr("width", 250)
                  .attr("height", 150)
                .append("g")
                  .attr("transform", "translate(110,110)")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }


            d3.layout.cloud().size([250, 150])
                .words([
                  "#Trees","#Outdoors", "#Indoors", "#Women", "#Men", "#Jeans"].map(function(d) {
                  return {text: d, size: 10 + Math.random() * 50};
                }))
                .rotate(function() { return ~~(Math.random() * 2) * 90; })
                .font("Impact")
                .fontSize(function(d) { return d.size; })
                .on("end", second_box)
                .start();

            function second_box(words) {
              d3.select(".second-word-cloud").append("svg")
                  .attr("width", 250)
                  .attr("height", 150)
                .append("g")
                  .attr("transform", "translate(110,110)")
                .selectAll("text")
                  .data(words)
                .enter().append("text")
                  .style("font-size", function(d) { return d.size + "px"; })
                  .style("font-family", "Impact")
                  .style("fill", function(d, i) { return fill(i); })
                  .attr("text-anchor", "middle")
                  .attr("transform", function(d) {
                    return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")";
                  })
                  .text(function(d) { return d.text; });
            }

            ctr = 1;
          }
          
        }

        

      }
    }
  }
]);
page.service('pageService', [
    function service( ) {
      var page_status = "";

      return {
        pageStatus: function pageStatus( data ) {
          console.log( data );
          if( data ) {
            page_status = data;
          } else {
            return page;
          }
        }
      }
    }
]);