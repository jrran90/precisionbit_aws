var predict = angular.module('predict', [ 'angular-owl-carousel' , 'simplePagination'] );



predict.run(function(){
	console.log('predict module running...');
});
predict.factory('serverUrl',[
    function factory(){
      return {
        url : window.location.origin + "/",
        header: {
          headers: {
            'Content-Type' : 'application/x-www-form-urlencoded'
          }
        }
      }
    }
]);
predict.directive('predictView', [
	"$http",
	"$state",
	"serverUrl",
	"predictService",
	function directive( $http, $state, serverUrl, predictService ) {

		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('predict view directive');
				$('.treeview').removeClass('active');
				$('.predict-menu').addClass('active');
				// $scope.defaultPic = "";

				scope.predictObject = {};

				// Set Default to Select Tags
				scope.predictObject.product_category = 'Acquisition';
				scope.predictObject.gender = null;
				scope.predictObject.objective = 'Clicks to Website';

				scope.defaultPic = "samplepage_background.jpg";
				$( '.predict-submenu-list' ).slideDown();
				// scope.predictObject = {};
				scope.is_demo_img = false;

				scope.close = function close (id) { // Close the alert msg box
					$( '#predict-' + id ).fadeOut(  );
				}

				// var gender_list = [];
				// scope.toggleBoxGender = function toggleBox ( data ) {
				// 	var idx = gender_list.indexOf( data );
				// 	if (idx > -1) gender_list.splice(idx, 1);
    //     			else gender_list.push(data);
				// 	scope.predictObject.targetgender = gender_list;
				// };
				// var library_list = [];
				// scope.toggleBoxLib = function toggleBoxLib ( data ) {
				// 	var idx = library_list.indexOf( data );
				// 	if (idx > -1) library_list.splice(idx, 1);
    //     			else library_list.push(data);
				// 	scope.predictObject.recommendedlibraries = library_list;
				// };
				// scope.showInput = false;
				// scope.selectEvent = function selectEvent() {
				// 	if ( scope.predictObject.event == 'Others' ) {
				// 		scope.showInput = true;						
				// 	}else {
				// 		scope.showInput = false;	
				// 	}
				// }
				// Submit Predict Queries
				scope.createPredict = function createPredict() {
					$('header').animatescroll();
					var width = 0;
					if ( scope.showInput ) {
						scope.predictObject.event = scope.predictObject.otherEvent;
						$( 'option[value="Others"]' ).attr( 'selected' , true );
					};

					if ( scope.predictObject.product_name == null || 
						// scope.predictObject.product_category == null ||
						scope.predictObject.country == null || 
						scope.predictObject.gender == null || 
						// scope.predictObject.objective == null || 
						scope.predictObject.hashtags == null || 
						scope.predictObject.mediaplatform == null
					) {
						sweetAlert("Oops...", "Dont leave the fields empty.", "error");
					}else {
						if ( scope.is_demo_img  == false ) {	
					       $http.post( serverUrl.url + 'createPrediction', scope.predictObject )
								.success(function success(response ){
									console.log(response);
					      })	
						};		
						$('.content-manager').fadeOut(500, function(){
							$('.loading-predict-container').fadeIn(500);
							scope.close( 'msg001' );
							scope.close( 'msg002' );
							$( '#predict-msg003' ).show();
							setTimeout(function() {
								$( '#predict-progress' ).css( 'width' , '100%' );
							}, 10);
						})			
					}
					console.log( scope.predictObject );			
				}

				scope.socialPlatform = function socialPlatform( platform ) {
					scope.predictObject.mediaplatform = platform;
					console.log(platform);
					$( '.icon-media' ).removeClass( 'active-img-predict' );
					if( platform == 'facebook' )
					{
						$( '#sm-fb img' ).addClass( 'active-img-predict' );
					} else if( platform == 'instagram' ){
						$( '#sm-insta img' ).addClass( 'active-img-predict' );
					} else if( platform == 'linkedin' ) {
						$( '#sm-in img' ).addClass( 'active-img-predict' );
					}
				}


				scope.useraction = function useraction ( option ) { // Sets default user action for Demo or Skip
					scope.close( 'msg001' ) // Removes the alert msg box
					if ( option == 'demo' ) {
						predictService.predictStatus("true");
						$( '#sm-fb' ).addClass( 'active' );
						scope.predictObject.product_category = 'Recontract'; // Will be filled up the heading textbox
						scope.predictObject.product_name = 'Mobile Plan'; // Selected platform  
						scope.predictObject.country = 'Australia'; // Selected Target Gender  
						scope.predictObject.gender = 'Both'; // Selected Geography  
						scope.predictObject.objective = 'Increase Sales'; // Selected Geography  

						scope.predictObject.mediaplatform = 'facebook'; // Selected Target object  
						scope.predictObject.hashtags = '#Health, #Summer, #Food';  

						scope.predictObject.demo = true;
						scope.is_demo_img = true;
						console.log( scope.predictObject );

					}else if( option == 'skip' ) {
						scope.is_demo_img = false;
						scope.predictObject.demo = false;
						scope.predictObject = {}; // Remove all defaults
					}
				}

				scope.demoImages = function demoImages () {

					predictService.set( "imageData" , null );					
					$state.go('predict-result');
					// scope.is_demo_img = true;
					// scope.defaultPic = "samplepage_background.jpg";
					// $('.loading-predict-container').fadeOut(500,function(){
					// 	$('.predict-result-container').fadeIn(500);
					// 	scope.doughNut();
					// });
				}
			}
		}
	}
]);

predict.directive('predictResult', [
	"$http",
	"$state",
	"serverUrl",
	"predictService",
  "$mdDialog",
  "$mdMedia",
	function directive( $http, $state, serverUrl, predictService, $mdDialog, $mdMedia ) {

		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				scope.predictResult = { };
				scope.defaultImage = "photo1.png";
				scope.clientImageObject = {};

				scope.url = serverUrl.url;			

				scope.themeData = [ 
					{
						id 				: 0,
						// theme 			: 'Technology',
						themeTitle 		: 'Outdoor Yoga Poses',
						engaging_score	: '70%',
						galleryImages 	: [
							'../img/pbit-assets-v4/Yoga/1.jpg',
							'../img/pbit-assets-v4/Yoga/2.jpg',
							'../img/pbit-assets-v4/Yoga/3.jpg',
							'../img/pbit-assets-v4/Yoga/4.jpg',
							'../img/pbit-assets-v4/Yoga/5.png',
							'../img/pbit-assets-v4/Yoga/6.png'
						],
						elemObject		: 'Human', 
						elemScene		: 'Landscape', 
						elemEmotion		: 'Amazement', 
						elemComposition	: 'Viewpoint', 
						elemLighting	: 'Natural' 
					},
					{ 
						id 				: 1,
						// theme 			: 'Sports',
						themeTitle 		: 'Pleated Meals',
						engaging_score	: '30%',
						galleryImages 	: [
							'../img/pbit-assets-v4/Salad/1.JPG',
							'../img/pbit-assets-v4/Salad/2.png',
							'../img/pbit-assets-v4/Salad/3.jpg',
							'../img/pbit-assets-v4/Salad/4.jpg',
							'../img/pbit-assets-v4/Salad/5.jpg',
							'../img/pbit-assets-v4/Salad/6.jpeg'
						],
						elemObject		: 'Food', 
						elemScene		: 'Portrait', 
						elemEmotion		: 'Interest', 
						elemComposition	: 'Viewpoint', 
						elemLighting	: 'Artificial Lighting' 
					},
					{ 
						id 				: 2,
						// theme 			: 'Musical',
						themeTitle 		: 'Weekend Relaxation',
						engaging_score	: '25%',
						galleryImages 	: [
							'../img/pbit-assets-v4/Summer/1.jpg',
							'../img/pbit-assets-v4/Summer/2.jpg',
							'../img/pbit-assets-v4/Summer/3.jpg',
							'../img/pbit-assets-v4/Summer/4.jpeg',
							'../img/pbit-assets-v4/Summer/5.jpeg',
							'../img/pbit-assets-v4/Summer/6.jpg'
						],
						elemObject		: 'Human', 
						elemScene		: 'Landscape', 
						elemEmotion		: 'Serenity', 
						elemComposition	: 'Viewpoint', 
						elemLighting	: 'Natural Lighting' 
					}
				];
				scope.galleryImageData  = scope.themeData[0]; // Default Theme Preview
				scope.imgPreview 		= scope.themeData[0].galleryImages[0]; //Default Theme Preview Image
				scope.galleryID 		= 0;
				scope.thumbnailStatus 	= {};


				scope.thumbnailStatus.status = false;

				scope.previewGalleryImg = function previewGalleryImg( filepath , data, theme_id ) {
		          scope.imgPreview = filepath;
		          if ( data == true ) {
		            scope.thumbnailStatus.status = true;
		            scope.thumbnailStatus.filepath = filepath;
		            scope.themePreview( theme_id, true );
		          }else {
		            scope.thumbnailStatus.status = false;           
		          }
		        };
				
				scope.themePreview 		= function themePreview( id, status ) {
					scope.galleryID = id;
					scope.galleryImageData  = scope.themeData[id]; // Default Theme Preview
					
					if (status == false) {
						scope.thumbnailStatus.status == false;
						scope.imgPreview 		= scope.themeData[id].galleryImages[0];				
					} else if ( scope.thumbnailStatus.status == true ) {
						scope.imgPreview 		= scope.thumbnailStatus.filepath;
					} else {
						scope.imgPreview 		= scope.themeData[id].galleryImages[0]; //Default Theme Preview Image
					}

					console.log( scope.imgPreview, status, scope.thumbnailStatus.status);
					$( '.list-wrapper' ).removeClass( 'active' );							
					$( '#' + id ).addClass( 'active' );							
				};


		        scope.downloadPreview = function downloadPreview( ev ) {
		          // var useFullScreen = ($mdMedia('sm') || $mdMedia('xs'))  && scope.customFullscreen;
		          	console.log(scope.galleryID);
		          	
		          	var template = 'templates/client/download-preview.blade.php';

		          	if( scope.galleryID == 0 ){
		          		template = 'templates/client/download-preview.blade.php';
		          	}else if ( scope.galleryID == 1 ){
		          		template = 'templates/client/download-preview2.blade.php';
		          	}else if( scope.galleryID == 2 ){
		          		template = 'templates/client/download-preview3.blade.php';
		          	}

		            $mdDialog.show({
		              // controller: imageCtrl,
		              templateUrl: template,
		              parent: angular.element(document.body),
		              targetEvent: ev,
		              clickOutsideToClose:true,
		              fullscreen: $mdMedia('lg')
		            })
		            .then(function(answer) {
		              // $scope.status = 'You said the information was "' + answer + '".';
		            }, function() {
		              // $scope.status = 'You cancelled the dialog.';
		            });
		        };
		        // scope.downloadPreview( event );
				scope.newChart = function newChart (data) {
					var less_value = 100 - parseFloat( data.engaging_score );
					console.log( less_value );
					var data = [
				    {
				        value: data.engaging_score,
				        color:"#2EAEB3",
				        highlight: "#2EAEB3",
				    },
				    {
				        value: less_value,
				        color: "#CED2D6",
				    },
					]
					var ctx = document.getElementById("myChart").getContext("2d");
					var myDoughnutChart = new Chart(ctx).Doughnut(data);
				};

				scope.getData = function getData ( ) {
					scope.predictResult = predictService.imageData( );	
					console.log( scope.predictResult );
					// scope.newChart( 
					// 	( (scope.predictResult) ? scope.predictResult.client_upload_data[0] : {
					// 		"engaging_score": 75
					// 	} )
					// );					
				};

				scope.getData( );

				scope.imgObject = {};
				scope.showPicture = function showPicture ( image ) {					
					if ( !scope.predictResult ) {					
						scope.defaultImage = image;						
					}else {
						// scope.newChart(image.client_upload_data[0]);
						scope.defaultPic = image.filename;	
						scope.imgObject  = image;
						console.log( image );
						
						scope.predictResult = image;
					}
				}
				
				scope.downloadBrief = function downloadBrief (num) {
					console.log(num);
					$http.get( serverUrl.url + 'downloadBrief/' + num )
						.success( function success (response) {
							console.log( 'downloaded' );
						} );
				};

				scope.fetchImages = function fetchImages( category ) {
		    	$http.get( serverUrl.url + 'getCategoryImages/' + category )
		    		.success( function success( response ) {
		    			scope.clientImageObject = response;
		    		} );
		    }

				scope.fetchImages( 'client_upload' );
			}
		}
	}
]);
predict
	.directive('briefPredict', [
		"$http",
		"$state",
		"serverUrl",
		"predictService",
    "$mdDialog", 
    "$mdMedia",
		function directive( $http, $state, serverUrl, predictService, $mdDialog, $mdMedia ) {
			return {
				restrict: "A",
				scope: true,
				link: function link( scope, element, attributeSet ) {
					console.log( 'running briefPredict directive' );

					scope.predictObject = {};
					scope.progressValue = 0;
					scope.imageObjectData = {};
					scope.clientObjectData = {};

					scope.predictObject.product_category = null;
					scope.predictObject.gender = null;
					scope.predictObject.objective = null;
					scope.resultObject = {};
					scope.clientObject = {};
					scope.predictObject = {};
	      			scope.predictObject.mediaplatform = null;	

					console.log( scope.predictObject );
      		var is_show = false;

      		scope.close = function close (id) { // Close the alert msg box
						$( '#predict-' + id ).fadeOut(  );
					};

					scope.setGlobalUserData = function setGlobalUserData() {
						$http.get( serverUrl.url + 'getuserdata' )
							.success( function success( response ) {
								console.log( response );
								scope.getClientData( response[0].company );
							} );
					}

					// get data from server
					scope.getClientData = function getClientData( id ) {
						$http.get( serverUrl.url + 'getClientExperiments/' + id )
							.success( function success( response ) {
								// $scope.clientObject.predict = response.predict;
								scope.clientObject.predict = response;
								
								for( var x = 0; x < response.length; x++ ) {
									for( y in response[x].hashtags ) {
										console.log( response[x].hashtags[y] )
										response[x].hashtags[y].hashColor = scope.getRandomColor();
									}
								}
								
								console.log( response )
								scope.client_id = response._id;
							} );
					}
					
					scope.getRandomColor = function() {
					    return '#'+(Math.random()*0xFFFFFF<<0).toString(16);
					}				

					// delete experiment
					scope.deleteExperiment = function deleteExperiment (evt, arr) {
	      		console.log( arr );
             var confirm = $mdDialog.confirm()
                  .title('Are you sure you want to delete this experiment?')
                  .textContent('This experiment will permanently delete.')
                  .ariaLabel('Lucky day')
                  .targetEvent(evt)
                  .ok('Okay')
                  .cancel('Cancel');
                $mdDialog.show(confirm).then(function() {
                  $http.post( serverUrl.url + 'deleteExperiment' , arr )
                 .success( function success (response) {
                   $( '#' + arr.predict_id ).fadeOut();
                   console.log( response )
                 });
            }, function() {
              console.log('cancel');
            });
	      	}

	      	// Submit Predict Queries
	      	scope.createExperiment = function createExperiment () {
	      		console.log( scope.hashtags.length );
	      		scope.hashList = scope.hashtags;

	      		for( var x = 0; x < scope.hashList.length; x++ ) {
	      			if ( scope.hashList[x].hash == null ) {
	      				scope.hashList.splice( scope.hashList.indexOf( scope.hashList[x], 1 ) );
	      				console.log( 'empty value ');
	      			} else {
	      				console.log( scope.hashList[x].hash );
	      			}
	      		}
	      		scope.predictObject.hashtags = scope.hashList;

						if ( scope.predictObject.product_name == null || 
							 scope.predictObject.campaign_name == null
						) {
							sweetAlert("Oops...", "Dont leave the fields empty.", "error");
						}else {					
					       $http.post( serverUrl.url + 'createPrediction', scope.predictObject )
								.success(function success(response ){
									console.log(response);
									scope.setGlobalUserData();
									setTimeout(function() {
										$('#' + response.predict_id).animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});
									}, 500);
					      
					      });
						}

	      	};
					// End: submit	

					// choose platform
	      	scope.selectedMedia = function selectedMedia ( media ) {
	      		scope.predictObject.mediaplatform = media;
	      		$( '.btn-social-exp' ).removeClass( 'active' );	
	      		$( '#' + media ).addClass( 'active' );
	      	};

	      	scope.img_tag_objects = [];
					scope.showresult = function showresult ( arr ) {				
						$http.get( serverUrl.url + 'getSingleImage/' + arr.filename )
							.success( function success( response ) {
								var image = response.shift( );
								predictService.set( "imageData" , image );
								$state.go( 'predict-results-template' );
							} );
					};

					scope.countryList = {};

					scope.getCountry = function getCountry( ) {
						$http.get('https://restcountries.eu/rest/v1/all')
						.success(function( response ){
							scope.countryList = response;
							console.log( response );
						});
					}

					scope.setGlobalUserData();
					scope.getCountry( );
				}
			}
		}

]);

predict.directive('predictShow', [
	"$http",
	"serverUrl",
	"Pagination",
	function directive( $http, serverUrl, Pagination ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {

				scope.user_list = {};
				scope.current_user = {};
				scope.user_list_counter = 1;
				scope.user_list_counter_end = 0;
				scope.user_list_num = '1 - 12';

				scope.temp_counter = 0;


				scope.user = function user( ) {
					$http.get( serverUrl.url + "get/loaded_js/" + scope.user_list_counter )
					.success(function( response ) {
						console.log( response );
						scope.user_list = response;

						$http.get( serverUrl.url + 'export/csv/affinities/' + response[0].username )
						.success(function(result){
							console.log( result );
							scope.current_user = result[0];
							$('.user_' + scope.user_list[0].profile_id ).addClass('table-location-result-active');
								setTimeout(function() {
									$('.global-profile-img').removeClass('active-profile-img');
									$('#img-user_' + scope.current_user.profile_id).addClass('active-profile-img');
								}, 500);
						});

						$http.get( serverUrl.url + 'export/csv/json/' + response[0].username )
						.success(function(result){
							console.log( result );
							scope.user_list = response;
							scope.current_user = response[0];
							scope.current_user.gallery = result;
							scope.pagination = Pagination.getNew(6);
							scope.pagination.numPages = Math.ceil(scope.current_user.gallery.length/scope.pagination.perPage);

						})
					});
				};

				scope.viewUser = function viewUser( response ) {
					// scope.current_user.posts = 0;
					// scope.current_user.followers = 0;
					// scope.current_user.following = 0;
					console.log( response );
					$('.global-active' ).removeClass('table-location-result-active');
					$('.global-profile-img').hide();
					$http.get( serverUrl.url + 'export/csv/affinities/' + response.username )
						.success(function(result){

						console.log( result );
						// scope.current_user = result[0];
						
						$('.user_' + response.profile_id ).addClass('table-location-result-active');
						$('.global-profile-img').show();
						$('.global-profile-img').removeClass('active-profile-img');

						setTimeout(function() {
							$('#img-user_' + scope.current_user.profile_id).addClass('active-profile-img');
						}, 500);
						

						$http.get( serverUrl.url + 'export/csv/json/' + response.username )
							.success(function( result2 ){
								scope.current_user = result[0];
								scope.current_user.gallery = result2;
								scope.galleryChange();
								scope.pagination = Pagination.getNew(6);
								scope.pagination.numPages = Math.ceil(scope.current_user.gallery.length/scope.pagination.perPage);
							});


					});

					
				}

				scope.user( );

				var addend1;
				var addend2;

				scope.users_next = function next() {
				  scope.user_list_counter++;
				  scope.temp_counter += 12;
			      $http.get( serverUrl.url + "get/loaded_js/" + scope.user_list_counter )
					.success(function( response ) {
						if( response.length != 0 ){

							if( _.keys(response).length < 12  ){
								addend1 = ( 12 * scope.user_list_counter ) - 11;
								addend2 = (12 * (scope.user_list_counter-1) ) + _.keys(response).length;

								scope.user_list_num = addend1 + ' - ' + addend2;

								scope.user_list_counter_end += _.keys(response).length;
							}else{
								addend1 = ( 12 * scope.user_list_counter ) - 11;
								addend2 = 12 * scope.user_list_counter;

								scope.user_list_num = addend1 + ' - ' + addend2;
							}

							$http.get( serverUrl.url + 'export/csv/json/' + response[scope.temp_counter].username )
								.success(function(result){
								scope.user_list = response;
								// scope.current_user = response[0];
								scope.current_user = response[scope.temp_counter];
								scope.current_user.gallery = result;
								scope.galleryChange();
								scope.pagination = Pagination.getNew(6);
								scope.pagination.numPages = Math.ceil(scope.current_user.gallery.length/scope.pagination.perPage);
								setTimeout(function() {
									$('.user_' + scope.current_user.profile_id ).addClass('table-location-result-active');
								}, 500);
								
							});
						}
						
					});
			    }

			    scope.users_back = function next() {
				  scope.user_list_counter--;
				  scope.temp_counter -= 12;

			      $http.get( serverUrl.url + "get/loaded_js/" + scope.user_list_counter )
					.success(function( response ) {

						if( response.length != 0 ){

							if( scope.user_list_counter_end == 0 ){
								addend1 -= 12;
							  	addend2 -= 12;

							  	scope.user_list_num = addend1 + ' - ' + addend2;

							}else{	
								addend1 -= 12;
							  	addend2 -= scope.user_list_counter_end ;

							  	scope.user_list_num = addend1 + ' - ' + addend2;
							  	scope.user_list_counter_end = 0;
							}
						  	

							$http.get( serverUrl.url + 'export/csv/json/' + response[scope.temp_counter].username )
								.success(function(result){
								scope.user_list = response;
								// scope.current_user = response[0];
								scope.current_user = response[scope.temp_counter];
								scope.current_user.gallery = result;
								scope.galleryChange();
								scope.pagination = Pagination.getNew(6);
								scope.pagination.numPages = Math.ceil(scope.current_user.gallery.length/scope.pagination.perPage);
								setTimeout(function() {
									$('.user_' + scope.current_user.profile_id ).addClass('table-location-result-active');
								}, 500);
								
							});
							
						}
						
					});
			    }
 
			    scope.galleryChange = function galleryChange(){
			    	$( ".repeat-image-gallery" ).hide();
			    }


			    scope.allData = [
		          {
		            "Gender": "M",
		            "Age": "20-30",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          
		          {
		            "Gender": "M",
		            "Age": "10-20",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "F",
		            "Age": "30-40",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "M",
		            "Age": "40 above",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		          {
		            "Gender": "M",
		            "Age": "10-20",
		            "Affinities": ["Foodies","Travel Buffs","Fashionistas"]
		          },
		        ];

		        scope.getAverage = function getAverage(){

		          var genderMale = 0;
		          var ageArr = [];
		          var ageCtr = [];

		          for( var x = 0; x < scope.allData.length; x++ ){
		            // console.log( scope.allData[x] );

		            if( scope.allData[x].Gender == "M" ){
		              genderMale++;
		            }

		            if( $.inArray( scope.allData[x].Age, ageArr ) == -1 ){
		              ageArr.push( scope.allData[x].Age );
		              ageCtr.push( 1 );
		            }else{
		              ageCtr[ ageArr.indexOf( scope.allData[x].Age ) ]++;  
		            }

		            if( x == (scope.allData.length-1) ){
		              var male = genderMale;
		              var female = scope.allData.length - genderMale;

		              setTimeout(function(){
		                scope.pieGraphAge( male, female );
		                scope.barGraph( ageArr, ageCtr);
		                scope.influBarGraph( ageArr, ageCtr);
		              },200);
		              
		            }

		          }

		        }

		        scope.pieGraphAge = function pieGraph( num1,num2 ) {
		          var pie = new d3pie("age-pie-graph", {
		  
		            "size": {
		              "canvasHeight": 100,
		              "canvasWidth": 100,
		              "pieOuterRadius": "100%",
		              "pieInnerRadius": "75%",
		            },
		            "data": {
		              "content": [
		                {
		                  // "value": (num2*10),
		                  "value": 48,
		                  "label": "Female",
		                  "color": "#2ca02c"
		                  
		                },
		                {
		                  // "value": (num1*10),
		                  "value": 21,
		                  "label": "Male",
		                  "color": "#CCD1D5"
		                },
		              ]
		            },
		            "labels": {
		              "outer": {
		                "pieDistance": 32,
		                "format": "none",
		              },
		              "inner": {
		                  "format": "none",
		                  "hideWhenLessThanPercentage": null
		              }
		            },
		            "effects": {
		              "pullOutSegmentOnClick": {
		                "effect": "none"
		              }
		            },
		            tooltips: {
		              enabled: true,
		              type: "placeholder",
		              string: "{label} : {value}"
		            }
		           

		            });
		        };

		        scope.barGraph = function barGraph( arrX, arrY ) {

		            var trace1 = {
		              // x: arrX,
		              // y: arrY,
		              x: ["20-30","30-40","40 Above","10-20"] ,
		              y: [13,41,14,1],
		              marker:{
		                color: ['rgba(204,204,204,1)', 'rgba(222,45,38,0.8)', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
		                thicknessmode: 1
		              },
		              type: 'bar'
		            };


		            var data = [trace1];

		            var layout = { 
		                barmode: 'stack',
		                showlegend: false,
		                displayModeBar: false,
		                height: 255,
		                width: 250,
		                bargap: 0.05,
		                xaxis: { 
		                  showgrid: false,
		                  showticklabels: false,
		                  showline: false,
		                },
		                yaxis: {
		                  showgrid: false,
		                  showline: false,
		                  showticklabels: false,
		                  zeroline: false,
		                  grigwidth: 2
		                }
		            };

		            Plotly.newPlot('average-bar-graph', data, layout, {displayModeBar: false});
		          };

		          scope.influBarGraph = function barGraph( arrX, arrY ) {

		            var trace1 = {
		              x: ['A','B','C',],
		              y: [6,2,2],
		              marker:{
		                color: ['#60BCD1', 'rgba(204,204,204,1)', 'rgba(204,204,204,1)'],
		                thicknessmode: 1
		              },
		              type: 'bar'
		            };


		            var data = [trace1];

		            var layout = { 
		                barmode: 'stack',
		                showlegend: false,
		                displayModeBar: false,
		                height: 255,
		                width: 250,
		                bargap: 0.05,
		                xaxis: { 
		                  showgrid: false,
		                  showticklabels: false,
		                  showline: false,
		                },
		                yaxis: {
		                  showgrid: false,
		                  showline: false,
		                  showticklabels: false,
		                  zeroline: false,
		                  grigwidth: 2
		                }
		            };

		            Plotly.newPlot('influ-bar-graph', data, layout, {displayModeBar: false});
		          };

		        scope.getAverage();


			    

			}
		}
	}
]);

