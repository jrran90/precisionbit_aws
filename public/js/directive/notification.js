var notification = angular.module('notification', []);

notification.run( function( ) {
	console.log( 'running notification module' );
});

notification
	.factory( 'serverUrl',  [
		function( ) {
			return {
				url: window.location.origin + "/",
				header: {
					headers: {
            'Content-Type' : 'application/x-www-form-urlencoded',
            'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, DELETE'
          }
				}
			}
		}
]);

notification
	.directive( 'notificationInformation', [
		"$http",
		"serverUrl",
		function directive( $http, serverUrl ) {
			return {
				restrict: "A",
				scope: true,
				compile: function compile( element, attributeSet ) {

					return {
						pre: function pre( scope, element, attributeSet ) {
							console.log( 'running notificationInformation directive' );
						},
						post: function post( scope, element, attributeSet ) {

						}
					}
				}
			}
		}
]);
