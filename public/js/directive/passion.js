var passion = angular.module('passion', []);

passion.run(function( ){
	console.log('passion module running');
});

passion.directive('passionLocation', [
	"$http",
	"serverUrl",
	"$state",
	"$stateParams",
	function directive( $http, serverUrl, $state, $stateParams ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('passion directive running');

				scope.location = function location( data, folder ) {
					console.log( data );
					$state.go('passion-points-location-location', { location: data, folder: folder }).then(function( response ) {
					  console.log( response );
					});
				};
			}
		}
	}
]);

passion.directive('passionSpecificLocation', [
	"$http",
	"serverUrl",
	"$state",
	"$stateParams",
	function directive( $http, serverUrl, $state, $stateParams ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('passion directive running');
				console.log( $stateParams.folder );
				scope.userList = {};
				scope.location_name = $stateParams.location;
				scope.getListUserByLocation = function getListUserByLocation( ) {
					$http.post( serverUrl.url + 'user/location/', { type: 'location', location: $stateParams.location })
					.success(function( response ) {
						console.log( response );
						scope.userList = response.data;
					});
				};

				scope.viewUser = function  viewUser( data ) {
					$state.go('passion-points-user', { location: $stateParams.location, user: data.profile_name, folder: $stateParams.folder  }).then(function( response ) {
					  console.log( response );
					});
				};
				scope.getListUserByLocation( );
			}
		}
	}
]);

passion.directive('passionSpecificUser', [
	"$http",
	"serverUrl",
	"$state",
	"$stateParams",
	"insta",
	"$compile",
	"$localStorage",
	function directive( $http, serverUrl, $state, $stateParams, insta ,$compile,$localStorage) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('passion directive running');
				console.log( $stateParams );
				scope.pp = [];
				scope.userList = {};
				scope.user_data = {};
				scope.all_data = [];
				scope.view_status = true;

				scope.passion_data = [];
				scope.interest_data = [];
				scope.username = $stateParams.user;


				scope.viewUser = function  viewUser( ) {
					$http.post( serverUrl.url + 'user/location/info/', { type: 'profile_name', location: $stateParams.user })
					.success(function( response ) {
						console.log( response );
						if( response != "false" ) {
							scope.userList = response.data[0];
							scope.user_data = response.csv_content;
							console.log(response.csv_content);
							scope.view_status = true;
							scope.pieGraph( response.csv_content, response.csv_count );

						} else {
							scope.view_status = false;
						}
					});
				};

				scope.getImage = function( list ) {
					return list[0].url;
				};

				scope.primary_points = [
					/*0*/  'Environment',
					/*1*/  'Food',
					/*2*/  'Entertainment',
					/*3*/  'Recreation',
					/*4*/  'Fashion & Beauty',
					/*5*/  'Relationships',
					/*6*/  'Celebrations',
					/*7*/  'Commerce',
					/*8*/  'Business',
					/*9*/  'Technology',
					/*10*/ 'Travel',
					/*11*/ 'Technology',
					/*12*/ 'Not useable',
					/*13*/ 'My life',
					/*14*/ 'Healthy Living',
					/*15*/ 'Design',
					/*16*/ 'Photography',
					/*17*/ 'Arts',
					/*18*/ 'Transportation',
					/*19*/ 'Religion',
					/*20*/ 'Animals',
					/*21*/ 'Outdoor Hobbies',
					/*22*/ 'Non Usable',
					/*23*/ 'Undefined',
					/*24*/ 'Scene',
					/*25*/ 'Culture',
					/*26*/ 'Media',
					/*27*/ 'Science',
					/*28*/ 'Lifehacks',
					/*29*/ 'Literature',
					/*30*/ 'Media',
					/*31*/ 'Wedding Organisation',
					/*32*/ 'Museum',
					/*33*/ 'Politics',
					/*34*/ 'Animals',
					/*35*/ 'Outdoor Hobbies',
					/*36*/ 'Gifts',
					/*37*/ 'Undefined',
					/*38*/ 'Fun Filter',
					/*39*/ 'Cute',
					/*40*/ 'Abstract',

				];


				// Submit Passion Data
				scope.saveDataPassion = function saveDataPassion( index , data ) {
					if ( data.secondary_passion_point_a == 'Manmade beauty' || data.secondary_passion_point_a == 'Natural beauty' || data.secondary_passion_point_a == 'Landmarks' || data.secondary_passion_point_a == 'Environment-s' ) {
						scope.pp[index] = scope.primary_points[0];
					} else if ( data.secondary_passion_point_a == 'Meal time' || data.secondary_passion_point_a == 'Drinks and snack' || data.secondary_passion_point_a == 'Self-preparation' || data.secondary_passion_point_a == 'Dessert' || data.secondary_passion_point_a == 'Food-s' ) {
						scope.pp[index] = scope.primary_points[1];
					} else if ( data.secondary_passion_point_a == 'Concerts' || data.secondary_passion_point_a == 'Entertainment-s' || data.secondary_passion_point_a == 'Destinations'|| data.secondary_passion_point_a == 'Sports'|| data.secondary_passion_point_a == 'Events' ) {
						scope.pp[index] = scope.primary_points[2];
					} else if ( data.secondary_passion_point_a == 'Fitness' || data.secondary_passion_point_a == 'Beach' || data.secondary_passion_point_a == 'Park life' || data.secondary_passion_point_a == 'Winter Sports' || data.secondary_passion_point_a == 'Adventure'  || data.secondary_passion_point_a == 'Recreation-s' ) {
						scope.pp[index] = scope.primary_points[3];
					} else if ( data.secondary_passion_point_a == 'Ootd' || data.secondary_passion_point_a == 'Makeup look' || data.secondary_passion_point_a == 'Fashion & Beauty-s'  || data.secondary_passion_point_a == 'My look' ) {
						scope.pp[index] = scope.primary_points[4];
					} else if ( data.secondary_passion_point_a == 'Young family' || data.secondary_passion_point_a == 'Best friends' || data.secondary_passion_point_a == 'Family' || data.secondary_passion_point_a == 'Friends' || data.secondary_passion_point_a == 'Baby family'  || data.secondary_passion_point_a == 'Relationships-s' ) {
						scope.pp[index] = scope.primary_points[5];
					} else if ( data.secondary_passion_point_a == 'Community Events' || data.secondary_passion_point_a == 'Personal Events' || data.secondary_passion_point_a == 'Celebrations-s' ) {
						scope.pp[index] = scope.primary_points[6];
					} else if ( data.secondary_passion_point_a == 'Product/brand shots' || data.secondary_passion_point_a == 'Promotions' || data.secondary_passion_point_a == 'Shopping' || data.secondary_passion_point_a == 'Commerce-s' ) {
						scope.pp[index] = scope.primary_points[7];
					} else if ( data.secondary_passion_point_a == 'Events' || data.secondary_passion_point_a == 'Work' || data.secondary_passion_point_a == 'Business-s' || data.secondary_passion_point_a == 'Study-s' || data.secondary_passion_point_a == 'Conf/Events' ) {
						scope.pp[index] = scope.primary_points[8];
					} else if ( data.secondary_passion_point_a == 'Screenshots-s' || data.secondary_passion_point_a == 'Technology-s') {
						scope.pp[index] = scope.primary_points[9];
					} else if ( data.secondary_passion_point_a == 'Staycation' || data.secondary_passion_point_a == 'Travel-s') {
						scope.pp[index] = scope.primary_points[10];
					} else if ( data.secondary_passion_point_a == 'Selfie' || data.secondary_passion_point_a == 'My world today' || data.secondary_passion_point_a == 'Here I am' || data.secondary_passion_point_a == 'With me' || data.secondary_passion_point_a == 'Doing' || data.secondary_passion_point_a == 'My life-s') {
						scope.pp[index] = scope.primary_points[13];
					} else if ( data.secondary_passion_point_a == 'Healthy Living-s') {
						scope.pp[index] = scope.primary_points[14];
					} else if ( data.secondary_passion_point_a == 'Design-s') {
						scope.pp[index] = scope.primary_points[15];
					} else if ( data.secondary_passion_point_a == 'Photography-s') {
						scope.pp[index] = scope.primary_points[16];
					} else if ( data.secondary_passion_point_a == 'Arts-s' || data.secondary_passion_point_a == 'Motivational' ) {
						scope.pp[index] = scope.primary_points[17];
					} else if ( data.secondary_passion_point_a == 'Transportation-s') {
						scope.pp[index] = scope.primary_points[18];
					} else if ( data.secondary_passion_point_a == 'Religion-s') {
						scope.pp[index] = scope.primary_points[19];
					} else if ( data.secondary_passion_point_a == 'Non Usable') {
						scope.pp[index] = scope.primary_points[20];
					} else if ( data.secondary_passion_point_a == 'Scene-s') {
						scope.pp[index] = scope.primary_points[24];
					}else if ( data.secondary_passion_point_a == 'Culture-s') {
						scope.pp[index] = scope.primary_points[25];
					}else if ( data.secondary_passion_point_a == 'Media-s') {
						scope.pp[index] = scope.primary_points[30];
					} else if ( data.secondary_passion_point_a == 'Science-s') {
						scope.pp[index] = scope.primary_points[27];
					} else if ( data.secondary_passion_point_a == 'Lifehacks-s') {
						scope.pp[index] = scope.primary_points[28];
					} else if ( data.secondary_passion_point_a == 'Literature-s') {
						scope.pp[index] = scope.primary_points[29];
					} else if ( data.secondary_passion_point_a == 'Wedding Organisation-s') {
						scope.pp[index] = scope.primary_points[31];
					} else if ( data.secondary_passion_point_a == 'Museum-s') {
						scope.pp[index] = scope.primary_points[32];
					} else if ( data.secondary_passion_point_a == 'Politics-s') {
						scope.pp[index] = scope.primary_points[33];
					} else if ( data.secondary_passion_point_a == 'Animals-s' || data.secondary_passion_point_a == 'Pets') {
						scope.pp[index] = scope.primary_points[34];
					} else if ( data.secondary_passion_point_a == 'Outdoor Hobbies-s') {
						scope.pp[index] = scope.primary_points[35];
					} else if ( data.secondary_passion_point_a == 'Gifts-s') {
						scope.pp[index] = scope.primary_points[36];
					} else if ( data.secondary_passion_point_a == 'Undefined') {
						scope.pp[index] = scope.primary_points[37];
					} else if ( data.secondary_passion_point_a == 'Fun Filter') {
						scope.pp[index] = scope.primary_points[38];
					} else if ( data.secondary_passion_point_a == 'Cute') {
						scope.pp[index] = scope.primary_points[39];
					} else if ( data.secondary_passion_point_a == 'Abstract') {
						scope.pp[index] = scope.primary_points[40];
					} else if( data.secondary_passion_point_a == '' || data.secondary_passion_point_a == null ) {
						scope.pp[index] = 'Blank';
						data.secondary_passion_point_a = 'Blank';
					} else {
						scope.pp[index] = 'Blank';
					}


					// FOR PRIMARY PASSION IF ALREADY EXISTS
					var pp_length = $(".primary_passion_list_"+index+" span").filter(function() {
					    return $(this).text() == scope.pp[index];
					}).length;

					var pp_length_length = $(".primary_passion_list_"+index+" span").length + 1;
					console.log(pp_length_length);

					if ( pp_length < 1 ) {
						$( '.primary_passion_list_' + index ).append( '<span class="passion-data label label-info _delete_'+pp_length_length+'" style="margin-right:4px;display:inline-block">'+scope.pp[index]+'</span>' );
					}

					// FOR SECONDARY PASSION IF ALREADY EXISTS
					var sp_length = $(".sec_"+index+" span").filter(function() {
					    return $(this).text() == data.secondary_passion_point_a;
					}).length;

					console.log(sp_length);

					// Check if under same Primary!

					// jQuery.inArray( data.secondary_passion_point_a ,  );
					var current_sec_points = $(".sec_"+index+" span").map(function(i, el) {
					    var is_true = 0;

					    if ( data.secondary_passion_point_a == 'Manmade beauty' ) {
							if ( $(el).text() == 'Natural beauty' || $(el).text() == 'Landmarks' || $(el).text() == 'Environment-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Natural beauty' ) {
							if ( $(el).text() == 'Manmade beauty' || $(el).text() == 'Landmarks' || $(el).text() == 'Environment-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Landmarks' ) {
							if ( $(el).text() == 'Manmade beauty' || $(el).text() == 'Natural beauty' || $(el).text() == 'Environment-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Environment-s' ) {
							if ( $(el).text() == 'Manmade beauty' || $(el).text() == 'Natural beauty' || $(el).text() == 'Landmarks' ) {
								is_true = 1;
							}
					    }

					    if( data.secondary_passion_point_a == 'Meal time' ) {
							if ( $(el).text() == 'Drinks and snack' || $(el).text() == 'Self-preparation' || $(el).text() == 'Dessert' || $(el).text() == 'Food-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Drinks and snack' ) {
							if ( $(el).text() == 'Meal time' || $(el).text() == 'Self-preparation' || $(el).text() == 'Dessert' || $(el).text() == 'Food-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Self-preparation' ) {
							if ( $(el).text() == 'Drinks and snack' || $(el).text() == 'Meal time' || $(el).text() == 'Dessert' || $(el).text() == 'Food-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Dessert' ) {
							if ( $(el).text() == 'Drinks and snack' || $(el).text() == 'Self-preparation' || $(el).text() == 'Meal time' || $(el).text() == 'Food-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Food-s' ) {
							if ( $(el).text() == 'Drinks and snack' || $(el).text() == 'Self-preparation' || $(el).text() == 'Dessert' || $(el).text() == 'Meal time' ) {
								is_true = 1;
							}
					    }

					    if( data.secondary_passion_point_a == 'Concerts' ) {
							if ( $(el).text() == 'Entertainment-s' ||  $(el).text() == 'Destinations' || $(el).text() == 'Sports' || $(el).text() == 'Events' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Entertainment-s' ) {
							if ( $(el).text() == 'Concerts' ||  $(el).text() == 'Destinations' || $(el).text() == 'Sports' || $(el).text() == 'Events' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Destinations' ) {
							if ( $(el).text() == 'Concerts' ||  $(el).text() == 'Entertainment-s' || $(el).text() == 'Sports' || $(el).text() == 'Events' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Sports' ) {
							if ( $(el).text() == 'Concerts' ||  $(el).text() == 'Destinations' || $(el).text() == 'Entertainment-s' || $(el).text() == 'Events' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Events' ) {
							if ( $(el).text() == 'Concerts' ||  $(el).text() == 'Destinations' || $(el).text() == 'Sports' || $(el).text() == 'Entertainment-s' ) {
								is_true = 1;
							}
					    }

					    if( data.secondary_passion_point_a == 'Fitness' ) {
							if ( $(el).text() == 'Beach' || $(el).text() == 'Park life' || $(el).text() == 'Winter Sports'  || $(el).text() == 'Adventure'  || $(el).text() == 'Recreation-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Beach' ) {
							if ( $(el).text() == 'Fitness' || $(el).text() == 'Park life' || $(el).text() == 'Winter Sports'  || $(el).text() == 'Adventure'  || $(el).text() == 'Recreation-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Park life' ) {
							if ( $(el).text() == 'Beach' || $(el).text() == 'Fitness' || $(el).text() == 'Winter Sports'  || $(el).text() == 'Adventure'  || $(el).text() == 'Recreation-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Winter Sports' ) {
							if ( $(el).text() == 'Beach' || $(el).text() == 'Park life' || $(el).text() == 'Fitness'  || $(el).text() == 'Adventure'  || $(el).text() == 'Recreation-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Adventure' ) {
							if ( $(el).text() == 'Beach' || $(el).text() == 'Park life' || $(el).text() == 'Winter Sports'  || $(el).text() == 'Fitness'  || $(el).text() == 'Recreation-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Recreation-s' ) {
							if ( $(el).text() == 'Beach' || $(el).text() == 'Park life' || $(el).text() == 'Winter Sports'  || $(el).text() == 'Fitness'  || $(el).text() == 'Adventure' ) {
								is_true = 1;
							}
					    }

					    if( data.secondary_passion_point_a == 'Ootd' ) {
							if ( $(el).text() == 'Makeup look' || $(el).text() == 'Fashion & Beauty-s' || $(el).text() == 'My look' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Makeup look' ) {
							if ( $(el).text() == 'Ootd' || $(el).text() == 'Fashion & Beauty-s' || $(el).text() == 'My look' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Fashion & Beauty-s' ) {
							if ( $(el).text() == 'Makeup look' || $(el).text() == 'Ootd' || $(el).text() == 'My look' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'My look' ) {
							if ( $(el).text() == 'Makeup look' || $(el).text() == 'Fashion & Beauty-s' || $(el).text() == 'Ootd' ) {
								is_true = 1;
							}
					    }


					    if( data.secondary_passion_point_a == 'Young family' ) {
							if ( $(el).text() == 'Best friends' || $(el).text() == 'Family' || $(el).text() == 'Friends'  || $(el).text() == 'Baby family'  || $(el).text() == 'Relationships-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Best friends' ) {
							if ( $(el).text() == 'Young family' || $(el).text() == 'Family' || $(el).text() == 'Friends'  || $(el).text() == 'Baby family'  || $(el).text() == 'Relationships-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Family' ) {
							if ( $(el).text() == 'Best friends' || $(el).text() == 'Young family' || $(el).text() == 'Friends'  || $(el).text() == 'Baby family'  || $(el).text() == 'Relationships-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Friends' ) {
							if ( $(el).text() == 'Best friends' || $(el).text() == 'Family' || $(el).text() == 'Young family'  || $(el).text() == 'Baby family'  || $(el).text() == 'Relationships-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Baby family' ) {
							if ( $(el).text() == 'Best friends' || $(el).text() == 'Family' || $(el).text() == 'Friends'  || $(el).text() == 'Young family'  || $(el).text() == 'Relationships-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Relationships-s' ) {
							if ( $(el).text() == 'Best friends' || $(el).text() == 'Family' || $(el).text() == 'Friends'  || $(el).text() == 'Baby family'  || $(el).text() == 'Young family' ) {
								is_true = 1;
							}
					    }

					    if( data.secondary_passion_point_a == 'Community Events' ) {
							if ( $(el).text() == 'Personal Events' || $(el).text() == 'Celebrations-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Personal Events' ) {
							if ( $(el).text() == 'Community Events' || $(el).text() == 'Celebrations-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Celebrations-s' ) {
							if ( $(el).text() == 'Personal Events' || $(el).text() == 'Community Events' ) {
								is_true = 1;
							}
					    }

					    if( data.secondary_passion_point_a == 'Product/brand shots' ) {
							if ( $(el).text() == 'Promotions' || $(el).text() == 'Shopping' || $(el).text() == 'Commerce-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Promotions' ) {
							if ( $(el).text() == 'Product/brand shots' || $(el).text() == 'Shopping' || $(el).text() == 'Commerce-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Shopping' ) {
							if ( $(el).text() == 'Promotions' || $(el).text() == 'Product/brand shots' || $(el).text() == 'Commerce-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Commerce-s' ) {
							if ( $(el).text() == 'Promotions' || $(el).text() == 'Shopping' || $(el).text() == 'Product/brand shots' ) {
								is_true = 1;
							}
					    }

					    if( data.secondary_passion_point_a == 'Events' ) {
							if ( $(el).text() == 'Work' || $(el).text() == 'Conf/Events' || $(el).text() == 'Business-s' || $(el).text() == 'Study' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Work' ) {
							if ( $(el).text() == 'Events' || $(el).text() == 'Conf/Events' || $(el).text() == 'Business-s' || $(el).text() == 'Study' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Conf/Events' ) {
							if ( $(el).text() == 'Work' || $(el).text() == 'Events' || $(el).text() == 'Business-s' || $(el).text() == 'Study' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Business-s' ) {
							if ( $(el).text() == 'Work' || $(el).text() == 'Conf/Events' || $(el).text() == 'Events' || $(el).text() == 'Study' ) {
								is_true = 1;
							}
					    }  if( data.secondary_passion_point_a == 'Study' ) {
							if ( $(el).text() == 'Work' || $(el).text() == 'Conf/Events' || $(el).text() == 'Business-s' || $(el).text() == 'Events' ) {
								is_true = 1;
							}
					    }


					    if( data.secondary_passion_point_a == 'Selfie' ) {
							if ( $(el).text() == 'My world today' || $(el).text() == 'Here I am' || $(el).text() == 'With me'  || $(el).text() == 'Doing'  || $(el).text() == 'My life' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'My world today' ) {
							if ( $(el).text() == 'Selfie' || $(el).text() == 'Here I am' || $(el).text() == 'With me'  || $(el).text() == 'Doing'  || $(el).text() == 'My life' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Here I am' ) {
							if ( $(el).text() == 'My world today' || $(el).text() == 'Selfie' || $(el).text() == 'With me'  || $(el).text() == 'Doing'  || $(el).text() == 'My life' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'With me' ) {
							if ( $(el).text() == 'My world today' || $(el).text() == 'Here I am' || $(el).text() == 'Selfie'  || $(el).text() == 'Doing'  || $(el).text() == 'My life' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Doing' ) {
							if ( $(el).text() == 'My world today' || $(el).text() == 'Here I am' || $(el).text() == 'With me'  || $(el).text() == 'Selfie'  || $(el).text() == 'My life' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'My life' ) {
							if ( $(el).text() == 'My world today' || $(el).text() == 'Here I am' || $(el).text() == 'With me'  || $(el).text() == 'Doing'  || $(el).text() == 'Selfie' ) {
								is_true = 1;
							}
					    }


					    if( data.secondary_passion_point_a == 'Technology-s' ) {
							if ( $(el).text() == 'Screenshots' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Screenshots' ) {
							if ( $(el).text() == 'Technology-s' ) {
								is_true = 1;
							}
					    }


					    if( data.secondary_passion_point_a == 'Healthy Living-s' ) {

					    }

					    if( data.secondary_passion_point_a == 'Photography-s' ) {

					    }


					    if( data.secondary_passion_point_a == 'Arts-s' ) {
							if ( $(el).text() == 'Motivational' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Motivational' ) {
							if ( $(el).text() == 'Arts-s' ) {
								is_true = 1;
							}
					    }

					    if( data.secondary_passion_point_a == 'Transportation-s' ) {

					    }

					    if( data.secondary_passion_point_a == 'Religion-s' ) {

					    }

					    if( data.secondary_passion_point_a == 'Animals-s' ) {
							if ( $(el).text() == 'Pets' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Pets' ) {
							if ( $(el).text() == 'Animals-s' ) {
								is_true = 1;
							}
					    }

					    if( data.secondary_passion_point_a == 'Outdoor Hobbies-s' ) {

					    }

					    if( data.secondary_passion_point_a == 'Gifts-s' ) {

					    }

					    if( data.secondary_passion_point_a == 'Non Usable' ) {

					    }

					    if( data.secondary_passion_point_a == 'Undefined' ) {

					    }


					    if( data.secondary_passion_point_a == 'Scene-s' ) {
							if ( $(el).text() == 'Fun Filter' || $(el).text() == 'Cute' || $(el).text() == 'Abstract' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Fun Filter' ) {
							if ( $(el).text() == 'Scene-s' || $(el).text() == 'Cute' || $(el).text() == 'Abstract' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Cute' ) {
							if ( $(el).text() == 'Fun Filter' || $(el).text() == 'Scene-s' || $(el).text() == 'Abstract' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Abstract' ) {
							if ( $(el).text() == 'Fun Filter' || $(el).text() == 'Cute' || $(el).text() == 'Scene-s' ) {
								is_true = 1;
							}
					    }


					    if( data.secondary_passion_point_a == 'Staycation' ) {
							if ( $(el).text() == 'Travel-s' ) {
								is_true = 1;
							}
					    } if( data.secondary_passion_point_a == 'Travel-s' ) {
							if ( $(el).text() == 'Staycation' ) {
								is_true = 1;
							}
					    }
						return is_true;
					}).get();

					console.log( current_sec_points );

					var cur_sp = 1;
					if ( (current_sec_points.length < 1) ) {
						cur_sp = 0;
					} else {
						cur_sp = $(current_sec_points).get(0);
					}

					var cur_sp_length = $(".sec_"+index+" span").length + 1;

					data.url = data[0].url;


					var data_a = data.secondary_passion_point_a;

					console.log(data_a);


					localStorage.selected_url = data.url;
					localStorage.selected_data_a = data_a;

					if ( sp_length < 1 && cur_sp != 1 ) {
						// $( '.sec_' + index ).append( '<span class="passion-data label label-success" style="margin-right:4px;display:inline-block">'+data.secondary_passion_point_a+'</span>' );
						$( '.sec_' + index ).append( $compile('<div class="label label-success section-inline-block _delete_'+cur_sp_length+'" style="margin-right:4px;display:inline-block"><span class="passion-data  _delete"  style="margin-right:4px" >'+data.secondary_passion_point_a+'</span><a href="javascript:void(0)" ng-click="deleteCustom('+index+','+cur_sp_length+')" class="glyphicon glyphicon-remove _delete"></a></div>')(scope) );

					}
					$( '.input_pp' ).val(null);

					// get the Primary Passion Point
					var prim_points = $(".primary_passion_list_"+index+" span").map(function(i, el) {
					    return $(el).text();
					}).get();

					// get the Secondary Passion Point
					var sec_points = $(".sec_"+index+" span").map(function(i, el) {
					    return $(el).text();
					}).get();

					var passion_points = {
						primary_affinity : prim_points,
						secondary_affinity : sec_points,
					}
					//

					$http.post( serverUrl.url + 'user/location/update/', { type: 'passion', user: $stateParams.user, url: data[0].url, data: passion_points, folder: $stateParams.folder })
					.success(function(response){
						console.log( response );
					})
					.error(function(err){
						console.log(err);
					});

				}

				scope.deleteCustom = function deleteCustom(index, field ){
					console.log(index);
					console.log(field);
					field = parseInt(field);
					var url = localStorage.selected_url;
					var value = localStorage.selected_data_a;
					console.log(url);
					console.log(value);
					$http.post( serverUrl.url + 'user/delete/passion/', { user: $stateParams.user, url: url, data: value, field : field })
					.success(function(response){
						console.log( response );
						$( '.primary_passion_list_' + index  + ' ._delete_' + field).remove();
						$( '.sec_'+ index +' ._delete_' + field).remove();
					})

					.error(function(err){
						console.log(err);
					});
				}


				scope.deleteAffinity = function(index, url, value, field ){
					// console.log(index);
					// console.log(field);
					// console.log(url);
					// console.log(value);

					$http.post( serverUrl.url + 'user/delete/passion/', { user: $stateParams.user, url: url, data: value, field : field })
					.success(function(response){
						console.log( response );
						$( '.primary_passion_list_' + index  + ' ._delete_' + field).remove();
						$( '.sec_'+ index +' ._delete_' + field).remove();
					})

					.error(function(err){
						console.log(err);
					});
				}

				$('.side-bar-menu').removeClass('active');
				$('.passion-points-li').addClass('active');

				scope.showPassionInput = function(data, index){
					// console.log(index);

					// var data = $(".image-wrapper:nth-child("+index+") .passion-data").text();
					$("#passion_" + index ).toggle();
					// $(".image-wrapper:nth-child("+index+") .passion-input").val(data);
					// $(".image-wrapper:nth-child("+index+") .passion-input").toggle();
				}

				scope.showInterestInput = function(data, index){
					// console.log(index);

					// var data = $(".image-wrapper:nth-child("+index+") .interest-data").text();
					$("#interest_" + index ).toggle();
					// $(".image-wrapper:nth-child("+index+") .interest-input").val(data);
					// $(".image-wrapper:nth-child("+index+") .interest-input").toggle();
				}

				scope.savePassion = function savePassion( index, data ) {
					// console.log( data )
					var passion_points = {
						primary_passion_point_a		: data.primary_passion_point_a,
						secondary_passion_point_a	: data.secondary_passion_point_a,
						primary_passion_point_b		: data.primary_passion_point_b,
						secondary_passion_point_b	: data.secondary_passion_point_b,
					}

					// $http.post( serverUrl.url + 'user/location/update/', { type: 'passion', user: $stateParams.user, url: data[0].url, data: passion_points, folder: $stateParams.folder })
					// .success(function(response){
					// 	scope.viewUser( );
					// 	$("#passion_" + index ).toggle();
					// 	console.log( response );
					// })
					// .error(function(err){
					// 	console.log(err);
					// });
				};

				scope.saveInterest = function saveInterest( index, data ) {
					console.log( data );
					$http.post( serverUrl.url + 'user/location/update/', { type: 'interest', user: $stateParams.user, url: data[0].url, data: data.data_1, folder: $stateParams.folder })
					.success(function(response){
						scope.viewUser( );
						console.log( response );
						// $("#interest_" + index ).toggle();
					})
					.error(function(err){
						console.log(err);
					});
				};

				scope.getPassion = function getPassion( data ) {
					// console.log( data );
					// return data[0];
				};

				scope.getInterest = function getInterest( data ) {
					// console.log( data[0].interest );
					return data[0].interest;
				};
				var pie;



				scope.pieGraph = function pieGraph( data, count ) {
				  if( pie ){
				  	pie.destroy();
				  }

					var passion = [];
					var passion_ctr = [];
					var interest = [];
					var interest_ctr = [];

					var data_content = [];

					for(var x = 2; x <= count; x++) {
						// console.log(data[x]);
						// if( x != 1 ){
							for( var y = 0; y < data[x].length; y++) {

								if( data[x][y].passion != "" ){
									if( $.inArray(data[x][y].passion , passion) == -1 ){
										passion.push(data[x][y].passion);
										passion_ctr.push(1);
									}else{
										var index = $.inArray(data[x][y].passion , passion);
										passion_ctr[index]++;
									}

								}

								if( x == count && y == ( data[x].length-1 ) ){


									scope.passion_data = passion;
									scope.passion_data.push(
										"arts",
										"architecture",
										"museums",
										"art styles",
										"business",
										"transportation",
										"entertainment",
										"celebrations",
										"food",
										"fashion",
										"beauty",
										"healthy living",
										"recreation",
										"lifehacks",
										"gifts",
										"relationships",
										"outdoor hobbies",
										"urbanism",
										"travel",
										"pets",
										"paranormal",
										"weddings organisation",
										"shopping",
										"driving",
										"sex",
										"culture",
										"religion",
										"politics",
										"science",
										"tech",
										"people studying",
										"media"
									);
									scope.interest_data = interest;

									console.log(passion);
									console.log(passion_ctr);
									console.log(scope.passion_data);
									console.log(scope.interest_data);


									for( var i = 0; i < passion.length; i++){
										data_content.push(
											{
							                  "value": passion_ctr[i],
							                  "label": passion[i]

							                }
										);
									}



									pie = new d3pie("pie-graph", {

							            "size": {
							              "canvasHeight": 200,
							              "canvasWidth": 200,
							              "pieOuterRadius": "100%",
							              "pieInnerRadius": "75%",
							            },
							            "data": {
							              "content": data_content
							            },
							            "labels": {
							              "outer": {
							                "pieDistance": 32,
							                "format": "none",
							              },
							              "inner": {
							                  "format": "none",
							                  "hideWhenLessThanPercentage": null
							              }
							            },
							            "effects": {
							              "pullOutSegmentOnClick": {
							                "effect": "none"
							              }
							            },
							            tooltips: {
							              enabled: true,
							              type: "placeholder",
							              string: "{label} : {value}"
							            }


							          });
								}
							}
						// }

					}

		        };


		        scope.viewImage = function viewImage(data) {

		   //      	$.ajax({
					//   type: "GET",
					//   dataType: "jsonp",
					//   cache: false,
					//   url: "https://api.instagram.com/v1/tags/" + data.profile_name + "/media/recent?client_id=8ee1d8c4cb76439cacec733859929c56",
					//   success: function(response) {
					//     var length = response.data != 'undefined' ? response.data.length : 0;
					//     var limit = photoCount != null && photoCount < length ? photoCount : length;
					//     if(limit > 0) {
					//       for(var i = 0; i < limit; i++) {
					//         // $('<img>', {
					//         //   src: response.data[i].images.standard_resolution.url
					//         // }).appendTo($("#photos"));
					//         console.log(response.data[i]);
					//       }
					//     }
					//   }
					// });
		        }

		        scope.back = function back( ) {
		        	$state.go('starhub-passion-points-user', { location: $stateParams.location, folder: $stateParams.folder }).then(function( response ) {
					  console.log( response );
					});
		        }
				scope.viewUser( );
	        	// scope.pieGraph();
			}
		}
	}
]);

passion.directive('passionUsers', [
	"$http",
	"serverUrl",
	"$state",
	"$stateParams",
	function directive( $http, serverUrl, $state, $stateParams ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('passion directive running');

				scope.userList = [];

				scope.getListUser = function getListUser( ) {
					$http.get( serverUrl.url + 'user/locals/singapore', { })
					.success(function( response ) {
						console.log( response );
						scope.userList = response;
					});
				};

				scope.viewUser = function  viewUser( data ) {
					$state.go('passion-points-user', {user: data.user.profile_name}).then(function( response ) {
					  console.log( response );
					});
				};

				scope.getListUser( );
			}
		}
	}
]);

passion.directive('passionSearch', [
	"$http",
	"serverUrl",
	"$state",
	"$stateParams",
	function directive( $http, serverUrl, $state, $stateParams ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('passionSearch directive running');

				scope.image_tags = [];
				scope.image_results = [];
				scope.view_status = false;

				scope.affinityTags = [
					/*0*/  'Environment',
					/*1*/  'Food',
					/*2*/  'Entertainment',
					/*3*/  'Recreation',
					/*4*/  'Fashion & Beauty',
					/*5*/  'Relationships',
					/*6*/  'Celebrations',
					/*7*/  'Commerce',
					/*8*/  'Business',
					/*9*/  'Technology',
					/*10*/ 'Travel',
					/*11*/ 'Technology',
					/*12*/ 'Not useable',
					/*13*/ 'My life',
					/*14*/ 'Healthy Living',
					/*15*/ 'Design',
					/*16*/ 'Photography',
					/*17*/ 'Arts',
					/*18*/ 'Transportation',
					/*19*/ 'Religion',
					/*20*/ 'Animals',
					/*21*/ 'Outdoor Hobbies',
					/*22*/ 'Non Usable',
					/*23*/ 'Undefined',
					/*24*/ 'Scene',
					/*25*/ 'Culture',
					/*26*/ 'Media',
					/*27*/ 'Science',
					/*28*/ 'Lifehacks',
					/*29*/ 'Literature',
					/*30*/ 'Media',
					/*31*/ 'Wedding Organisation',
					/*32*/ 'Museum',
					/*33*/ 'Politics',
					/*34*/ 'Animals',
					/*35*/ 'Outdoor Hobbies',
					/*36*/ 'Gifts',
					/*37*/ 'Undefined',
					/*38*/ 'Fun Filter',
					/*39*/ 'Cute',
					/*40*/ 'Abstract',

				];

				scope.moreThanTwo = false;
				scope.tags_filter;

				scope.getTagResults = function  getTagResults(tag) {
					scope.image_results = [];
					if(tag && $.inArray(tag,scope.image_tags) == -1){

							scope.image_tags.push(tag);
							$( ".enter-tag" ).val("");
							

						if( scope.tags_filter ){
							console.log(scope.tags_filter);
							$http.post( serverUrl.url + 'user/search/image', { tags : scope.image_tags})
							.success(function( response ) {
								console.log( response );
								if( response != "false" ) {
									scope.image_results = response;
									scope.view_status = true;
								} else {
									scope.view_status = false;
								}
							});
						}else{

							$http.post( serverUrl.url + 'user/search/image', { tags : scope.image_tags})
							.success(function( response ) {
								console.log( response );
								if( response != "false" ) {
									scope.image_results = response;
									scope.view_status = true;
								} else {
									scope.view_status = false;
								}
							});
						}
						
					}

				};

				scope.filterResultssss = function(){
					console.log("in")
					scope.image_results = [];
					$http.post( serverUrl.url + 'user/search/image', { tags : scope.image_tags})
							.success(function( response ) {
								console.log( response );
								if( response != "false" ) {
									scope.image_results = response;
									scope.view_status = true;
								} else {
									scope.view_status = false;
								}
							});
				}

				scope.deleteTag = function  deleteTag( tag ) {
					scope.image_results = [];
					scope.image_tags = $.grep(scope.image_tags, function(value) {
										  	return value != tag;
									   });
					console.log(scope.image_tags);

					if( scope.image_tags.length == 0 ){
						scope.image_results = [];
						scope.view_status = false;
					}else{
						if( scope.tags_filter ){
							console.log(scope.tags_filter);
							$http.post( serverUrl.url + 'user/search/image', { tags : scope.image_tags})
							.success(function( response ) {
								console.log( response );
								if( response != "false" ) {
									scope.image_results = response;
									scope.view_status = true;
								} else {
									scope.view_status = false;
								}
							});
						}else{

							$http.post( serverUrl.url + 'user/search/image', { tags : scope.image_tags})
							.success(function( response ) {
								console.log( response );
								if( response != "false" ) {
									scope.image_results = response;
									scope.view_status = true;
								} else {
									scope.view_status = false;
								}
							});
						}
					}
				};

				scope.filterAffinityTagResults = function(tag){
					console.log(tag);
				}

				scope.showInterestInput = function(index){
					console.log(index);
					$("#interest_" + index ).toggle();
				}

				scope.showLocationInput = function(index){
					console.log(index);
					$("#location_" + index ).toggle();
				}

				var checked_ctr = 0;

				scope.selectAll = function(){
					if( checked_ctr == 0 ){
						$('.checkbox-list').attr('Checked','Checked'); 
						checked_ctr = 1;
					}else{
						$('.checkbox-list').removeAttr('Checked');
						checked_ctr = 0;
					}
				    
				}

				scope.getImage = function( list ) {
					return list.url;
				};

			}
		}
	}
]);

passion.directive('passionSummary', [
	"$http",
	"serverUrl",
	"$state",
	"$stateParams",
	function directive( $http, serverUrl, $state, $stateParams ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log('passion summary directive running');

				// formula : (Total / Untagged )*10
				scope.showSummary = function(index){
					console.log(index);
					$(".menu-summary li a").removeClass('active');
					$(".menu-summary li:nth-child("+(index)+") a").addClass('active');

					if(index == 1){
						$("#affinities").hide();
						$("#cvtags").hide();
						$("#definition").hide();
						$("#overall").show();
					}else if(index == 2){
						$("#affinities").show();
						$("#cvtags").hide();
						$("#definition").hide();
						$("#overall").hide();
					}else if(index == 3){
						$("#affinities").hide();
						$("#cvtags").show();
						$("#definition").hide();
						$("#overall").hide();
					}else if(index == 4){
						$("#affinities").hide();
						$("#cvtags").hide();
						$("#definition").show();
						$("#overall").hide();
					}
				}

			}
		}
	}
]);

passion.directive('starHub', [
	"$http",
	"serverUrl",
	"$state",
	"$localStorage",
	function directive( $http, serverUrl, $state, $localStorage ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {

				scope.userList = {};

				scope.passion_points = [
					"arts",
					"architecture",
					"museums",
					"art styles",
					"business",
					"transportation",
					"entertainment",
					"celebrations",
					"food",
					"fashion",
					"beauty",
					"healthy living",
					"recreation",
					"lifehacks",
					"gifts",
					"relationships",
					"outdoor hobbies",
					"urbanism",
					"travel",
					"pets",
					"paranormal",
					"weddings organisation",
					"shopping",
					"driving",
					"sex",
					"culture",
					"religion",
					"politics",
					"science",
					"tech",
					"people studying",
					"media"
				];

				scope.all_passion_points = [];
				var passion_point;

				scope.getCountPassion = function getCountPassion( ) {

					angular.forEach(scope.passion_points, function( data ){
						$http.post( serverUrl.url + 'user/get/count/', { users: $localStorage.users, data: data })
						.success(function(response){
							scope.all_passion_points.push({ passion: data, count: response });
						});
					});

					console.log( scope.all_passion_points );
				}

				scope.user = function user( ) {
					$http.get(serverUrl.url + 'user/get/filename/')
					.success(function(response){
						scope.userList = response;
						$localStorage.users = response;
						setTimeout(function() {
							scope.getCountPassion();
						}, 500);
					});
				};

				scope.viewUser = function  viewUser( data ) {
					$state.go('passion-points-user', { location: 'rod', user: data, folder: 'rod'  }).then(function( response ) {
					  console.log( response );
					});
				};

				scope.passionImages = function ( data ) {
					$localStorage.users = scope.userList;
					$state.go('passion_images', { passion: data }).then(function( response ) {
					  console.log( response );
					});
				};

				scope.user();
			}
		}
	}
]);


passion.directive('starHubPassionPoints', [
	"$http",
	"serverUrl",
	"$state",
	"$stateParams",
	function directive( $http, serverUrl, $state, $stateParams ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				console.log( $stateParams.passion);
			}
		}
	}
])

passion.directive('passionFilter', [
	"$http",
	"serverUrl",
	"$state",
	"$stateParams",
	"$localStorage",
	function directive( $http, serverUrl, $state, $stateParams, $localStorage ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {

				scope.passion_images = [];

				console.log($localStorage.users);

				scope.passion = $stateParams.passion;

				scope.getImages = function() {
					scope.passion_images = [];
					$http.post( serverUrl.url + 'user/get/passion/' , { passion : $stateParams.passion, users: $localStorage.users } )
					.success(function( response ) {
						scope.passion_images = [];
						console.log( response );
						if(response) {
							angular.forEach(response, function( data ) {
								$http.post( serverUrl.url + 'user/get/tags', { folder: 'rod', user: data.user, permalink: data.permalink })
								.success(function(response){
									console.log(response);
									scope.passion_images.push({ _id: data._id, interest: data.interest, interest_area: data.interest_area, passion: data.passion, permalink: data.permalink, user: data.user, tags: response.tags });
								});
							});
						}

						// scope.passion_images = response;
						scope.passion_images_length = scope.passion_images.length;
						console.log(scope.passion_images);
						if( response == "" ){
							scope.no_images = true;
						}else{
							scope.no_images = false;
						}
					});
				}

				scope.getImage = function getImage( data ) {
					return data.permalink;
				}

				scope.showLocationInput = function(data, index){
					$("#location_" + index ).toggle();
				}

				scope.showInterestInput = function(data, index){
					$("#interest_" + index ).toggle();
				}

				scope.saveInterest = function saveInterest( index, data ) {
					scope.passion_images = [];
					data.type = 'interest';
					$http.post( serverUrl.url + 'user/get/interest/update', data )
					.success(function( response ) {
						$("#interest_" + index ).toggle();
						console.log( response );
						scope.getImages();
					});

				}

				scope.getTags = function getTags( data ) {
					// console.log(data.permalink);


				}

				scope.saveLocation = function saveLocation( index, data ) {
					$http.post( serverUrl.url + 'user/get/location/update',  { data: data, location: data.location, url: data.permalink , user: data.user })
					.success(function( response ) {
						$("#location_" + index ).toggle();
						console.log( response )
					});
				}

				scope.getImages();
			}
		}
	}
])

passion.directive('advocateLists', [
	"$http",
	"serverUrl",
	"$state",
	"$localStorage",
	function directive( $http, serverUrl, $state,$localStorage ) {
		return {
			restrict: "A",
			scope: true,
			link: function link( scope, element, attributeSet ) {
				scope.userList = ['cahskb', 'veeywn', 'theamirrulraizo', 'c_serie', 'suffianhakim', 'tingting_589', 'ya.dama', 'alicemuiyun', 'dicksonhung123', 'mastaroshie'];
				scope.all_passion_points = [];
				scope.passion_points = [
					"arts",
					"architecture",
					"museums",
					"art styles",
					"business",
					"transportation",
					"entertainment",
					"celebrations",
					"food",
					"fashion",
					"beauty",
					"healthy living",
					"recreation",
					"lifehacks",
					"gifts",
					"relationships",
					"outdoor hobbies",
					"urbanism",
					"travel",
					"pets",
					"paranormal",
					"weddings organisation",
					"shopping",
					"driving",
					"sex",
					"culture",
					"religion",
					"politics",
					"science",
					"tech",
					"people studying",
					"media"
				];

				scope.getCountPassion = function getCountPassion( ) {

					angular.forEach(scope.passion_points, function( data ){
						$http.post( serverUrl.url + 'user/get/count/', { users: scope.userList, data: data })
						.success(function(response){
							scope.all_passion_points.push({ passion: data, count: response });
						});
					});

					console.log( scope.all_passion_points );
				};

				scope.viewUser = function  viewUser( data ) {
					$state.go('passion-points-user', { location: 'rod', user: data, folder: 'rod'  }).then(function( response ) {
					  console.log( response );
					});
				};

				scope.passionImages = function ( data ) {
					$localStorage.users = scope.userList;
					$state.go('passion_images', { passion: data }).then(function( response ) {
					  console.log( response );
					});
				};

				scope.getCountPassion();
			}
		}
	}
]);
passion.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
});

passion.directive('autoComplete', function($timeout) {
    return function(scope, iElement, iAttrs) {
            iElement.autocomplete({
                source: scope[iAttrs.uiItems],
                select: function() {
                    $timeout(function() {
                      iElement.trigger('input');
                    }, 0);
                }
            });
    };
});
