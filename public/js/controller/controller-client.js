app
  .controller('dashboardController',[
    "$http",
    "$scope",
    "serverUrl",
    "$localStorage",
    "$rootScope",
    "$state",
    "createService",
    "netWork",
    function controller( $http, $scope, serverUrl, $localStorage, $rootScope, $state, createService, netWork ) {
      // $('.treeview').removeClass('active');
      // $('.treeview').removeClass('active-idea');
      // $('.dashboard-menu').addClass('active');
      $scope.create = {};
      netWork.loadNetwork('home');
      console.log('run dashboardController');
        $scope.showGraph = function( ) {
            var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
            var lineChartData = {
              labels : ["January","February","March","April","May","June","July"],
              datasets : [
                {
                  label: "Ads A",
                  fillColor : "rgba(95,157,214,0)",
                  strokeColor : "rgba(95,157,214,1)",
                  pointColor : "rgba(95,157,214,1)",
                  pointStrokeColor : "#fff",
                  pointHighlightFill : "#fff",
                  pointHighlightStroke : "rgba(95,157,214,1)",
                  showTooltip: false,
                  data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                },
                {
                  label: "Ads B",
                  fillColor : "rgba(1,188,140,0)",
                  strokeColor : "rgba(1,188,140,1)",
                  pointColor : "rgba(1,188,140,1)",
                  pointStrokeColor : "#fff",
                  pointHighlightFill : "#fff",
                  pointHighlightStroke : "rgba(1,188,140,1)",
                  showTooltip: false,
                  data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                },
                {
                  label: "Ads C",
                  fillColor : "rgba(239,111,108,0)",
                  strokeColor : "rgba(239,111,108,1)",
                  pointColor : "rgba(239,111,108,1)",
                  pointStrokeColor : "#fff",
                  pointHighlightFill : "#fff",
                  pointHighlightStroke : "rgba(239,111,108,1)",
                  data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
                }
              ]

            }

          window.onload = function(){
            var context_dashboard = document.getElementById("dashboard-report").getContext("2d");
            legend(document.getElementById("legend-dashboard"), lineChartData);
            window.myLine = new Chart(context_dashboard).Line(lineChartData, {
              responsive: true
            });
          }

        }

        $scope.checkCampaign = function checkCampaign( ) {
          $http.get( serverUrl.url + 'campaigns/count/' + $localStorage.user.uniqid )
          .success(function( response ){
            console.log( response );
            // $scope.campaignStatus = response;
            if( response > 0 ) {
              $('.true-campaign').fadeIn(100);
              $('.false-campaign').fadeOut(100);
            } else {
              $('.true-campaign').fadeOut(100);
              $('.false-campaign').fadeIn(100);
            }
          });
        }

        $scope.regFB = function regFB( ) {
          swal({   
            title: "AdHero!",   
            text: "This is just a test prompt, will be moving to laravel template",   
            imageUrl: "/assets/img/ads-icons/facebook.svg",
            confirmButtonText: "Start!",
            closeOnConfirm: false,   
            closeOnCancel: false,
            function(){
              console.log('ok')
              // console.log(isConfirm);
              // if (isConfirm) {
                $rootScope.$broadcast('hide');
                $rootScope.$broadcast('show-create-campaign');
              // }  
            }
          });
        }

        $scope.createCampaign = function createCampaign( ) {
          createService.createCampaignName( $scope.create.campaign_name);
          console.log( $scope.create.campaign_name );
          $state.go('create-brief');
        }

        // $scope.checkCampaign();
        // $scope.showGraph();

        $scope.$on( "show-dashboard" , 
          function onReceive(){
            // $scope.checkCampaign();
            $scope.showGraph();
        });       
    }
])
  .controller('profileController',[
    "$http",
    "$scope",
    "serverUrl",
    "netWork",
    function controller( $http, $scope, serverUrl, netWork ) {
       netWork.loadNetwork('profile');
    }
])
  .controller('listController',[
    "$http",
    "$scope",
    "serverUrl",
    function controller( $http, $scope, serverUrl ) {
      
    }
])
  .controller('revenueController',[
    "$http",
    "$scope",
    "serverUrl",
    function controller( $http, $scope, serverUrl ) {
      
    }
])
  .controller('contactController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl,  $rootScope, netWork ) {
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.contact-us').addClass('active');
       netWork.loadNetwork('contact');
    }
])
  .controller('createCampaignController',[
    "$http",
    "$scope",
    "serverUrl",
    "$localStorage",
    "$rootScope",
    "createService",
    "netWork",
    "$state",
    function controller( $http, $scope, serverUrl, $localStorage, $rootScope, createService, netWork, $state ) {
      $scope.campaign = {};
      $scope.temp = {}
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.create-menu').addClass('active');
      netWork.loadNetwork('create-brief');
      $scope.campaign.uniqid = $localStorage.user.uniqid;
      $scope.campaign.uid = $localStorage.user.uid;
      $scope.campaign.campaign_name = createService.createCampaignName();
      console.log( $scope.campaign );

      // jrran90
      $scope.createCampaign = function createCampaign( ) {
        $('.bs-wizard-step-2').removeClass('active');
          $('.bs-wizard-step-2').addClass('complete');
          $('.bs-wizard-step-3').addClass('active');
          $('.campaign-form-fillup').fadeOut(500, function( ) {
            $('.campaign-intro-select').fadeOut(500);
            $('.campaign-form').fadeOut(500);
            $('.campaign-pay-select').fadeIn(500);
            $('.payment-method-campaign').fadeIn(500);
          });
      }

      $scope.choose = function choose( response ) {
        var step_2 = false;
        console.log( response );
        if( response == "facebook" ) {
          $('.bs-wizard-step-2').addClass('active');
          $('.campaign-platform').fadeOut(500, function( ) {
            $('.campaign-form').fadeIn(500);
            $('.campaign-intro-select').fadeOut(500);
            $('.campaign-form-fillup').fadeIn(500);
          });
          step_2 = true;
        } else if( response == "instagram" ) {
          $('.bs-wizard-step-2').addClass('active');
          $('.campaign-platform').fadeOut(500, function( ) {
            $('.campaign-form').fadeIn(500);
            $('.campaign-intro-select').fadeOut(500);
            $('.campaign-form-fillup').fadeIn(500);
          });
          step_2 = true;
        } else if( response == "step-1") {
           $('.bs-wizard-step-2').removeClass('active');
            $('.campaign-form').fadeOut(500, function( ) {
              $('.campaign-platform').fadeIn(500);
              $('.campaign-intro-select').fadeIn(500);
              $('.campaign-form-fillup').fadeOut(500);
            });
        }
      }

      $scope.payNow = function payNow( ) {
        $scope.campaign.status = "paid";
        // $('.bs-wizard-step-2').removeClass('complete');
        // $('.bs-wizard-step-3').removeClass('active');
        // $('.campaign-pay-select').fadeOut(500);
        // $('.campaign-intro-select').fadeIn(500);
        // $('.campaign-platform').fadeIn(500);
        // $('.payment-method-campaign').fadeOut(500);
        // $rootScope.$broadcast('hide');
        // $rootScope.$broadcast('show-payment');
        $http.post(serverUrl.url + 'createBriefCampaign', $scope.campaign )
        .success(function success(response ){
          console.log(response);
          if(response == "success") {
            $scope.campaign = {};
            $('.create-campaign-loading').fadeOut(500);
                swal({   
                title: "Good Job!",  
                text: "Please see your email.",
                type: "success",
                timer: 2000,   
                showConfirmButton: false
            });
            $state.go('preview-ad');
          } else {
            sweetAlert("Oops...", "Please check your connection and try again!", "error");
            $('.create-campaign-loading').fadeIn(500);
          }
        })
        .error(function(m) {
          console.log(m)
        });
      }
      $scope.showAds = function showAds( ) {
        $('.bs-wizard-step-2').removeClass('complete');
        $('.bs-wizard-step-3').removeClass('active');
        $('.campaign-pay-select').fadeOut(500);
        $('.campaign-intro-select').fadeIn(500);
        $('.campaign-platform').fadeIn(500);
        $('.payment-method-campaign').fadeOut(500);
        $rootScope.$broadcast('hide');
        $rootScope.$broadcast('show-preview-ad');
      }
      
      $scope.saveAds = function saveAds( ) {
        $scope.campaign.status = "pending";
        $scope.campaign.target_age = $scope.temp.minimum_age + "-" + $scope.temp.maximum_age;
        $('.create-campaign-loading').fadeIn(500);
        console.log($scope.campaign);
        
        $http.post(serverUrl.url + 'createBriefCampaign', $scope.campaign )
        .success(function success(response ){
          console.log(response);
          if(response == "success") {
            $scope.campaign = {};
            $('.create-campaign-loading').fadeOut(500);
                swal({   
                title: "Good Job!",  
                text: "Please see your email.",
                type: "success",
                timer: 2000,   
                showConfirmButton: false
            });
            $state.go('preview-ad');
          } else {
            sweetAlert("Oops...", "Please check your connection and try again!", "error");
            $('.create-campaign-loading').fadeIn(500);
          }
        })
        .error(function(m) {
          console.log(m)
        });
      }

      $scope.getGender = function getGender( response, state ) {
        $scope.campaign.target_audience = response;
        $('.btn').removeClass('active-target');
        $("#state_" + state ).addClass('active-target');
        console.log( response, state );
        // $(".btn-group > .btn").addClass("active-targe").siblings().removeClass("active");
      }

      $scope.getPaypal = function getPaypal( ) {

      }
    }
])


  // jrran90
  .controller('paymentController',[
    "$http",
    "$scope",
    "serverUrl",
    "$localStorage",
    "$state",
    "netWork",
    function controller( $http, $scope, serverUrl, $localStorage, $state, netWork, $stateParams ) {
      netWork.loadNetwork('payment');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.payment-menu').addClass('active');

      $scope.editDataCampaign = {};


      // jrran90
      $scope.doPayment = function () {
        console.log($stateParams)
        // $('#paypalDeposit').modal('show');
      }

      $scope.payNow = function payNow( ) {
        console.log( 'pay' );
      }

      $scope.getCreateList = function getCreateList( ) {
        $http.get( serverUrl.url + 'campaigns/' + $localStorage.user.uniqid )
        .success(function( response ){
          console.log(response);
          $scope.campaignList = response.data;
        });
      }

      $scope.removeCampaign = function removeCampaign( list ) {
        swal({ 
          title: "Are you sure?",   
          text: "You will not be able to recover this campaign once remove.",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Yes, delete this campaign please!",   
          cancelButtonText: "No, and do nothing!",   
          closeOnConfirm: true,   
          closeOnCancel: true }, 
          function(isConfirm){   
            if (isConfirm) {
              $http.get( serverUrl.url + 'campaigns/deleteCampaign/' + list.id )
              .success(function( response ){
                console.log(response);
                if( response == "success" ) {
                  $scope.campaignList.splice($scope.campaignList.indexOf(list), 1);
                  swal("Deleted!", "Campaign was successfully deleted.", "success");
                } else {
                  swal({   title: "Error!",  
                    text: "An error occured while deleting the campaign, Please try again.",   
                    timer: 2000,   
                    showConfirmButton: true 
                  });
                }
              });   
                 
            } });
      }

      $scope.editCampaign = function editCampaign( response ) {
        $scope.editDataCampaign = response;
        console.log( response );
        $('.payment-overview').fadeOut(100, function( ){
          $('.show-update-campaign').fadeIn(100);
        })
      }

      $scope.updateCampaign = function updateCampaign( ) {
        $('.update-campaign-loading').fadeIn(500);
        console.log($scope.editDataCampaign);
        $http.post( serverUrl.url + 'campaigns/update/', $scope.editDataCampaign )
        .success(function( response ){
          console.log(response);
          if( response == "success" ) {
             swal({
              title: "Campaign updated!",
              timer: 2000,
              type: "success",
              showConfirmButton: true 
            });
             $scope.getCreateList();
             $('.update-campaign-loading').fadeOut(500);
             $('.show-update-campaign').fadeOut(100, function( ){
                $('.payment-overview').fadeIn(100);
              })
          } else {
            // swal({   
            //   title: "Error!",
            //   type: "error",
            //   text: "An error occured while updating the campaign, Please try again.",   
            //   timer: 2000,   
            //   showConfirmButton: true 
            // });
            $state.go('500-error');
            $('.update-campaign-loading').fadeOut(500);
          }
        });
      }

      $scope.cancelUpdate = function cancelUpdate( ) {
        $('.show-update-campaign').fadeOut(500, function( ) {
          $('.payment-overview').fadeIn(500);
          $('.custom-box-solid').fadeIn(500);
        });
      }
     // $scope.$on( "show-payment" , 
        // function onReceive(){
          $scope.getCreateList();
      // });    
    }
])
  .controller('adsResultController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl, $rootScope, netWork ) {
      netWork.loadNetwork('campaign-report');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.ads-result').addClass('active');

      $scope.showGraph = function showGraph( ) {
        console.log('run');
       var randomScalingFactor = function(){ return Math.round(Math.random()*100)};
          var lineChartData = {
            labels : ["January","February","March","April","May","June","July"],
            datasets : [
              {
                label: "Ads A",
                fillColor : "rgba(95,157,214,0)",
                strokeColor : "rgba(95,157,214,1)",
                pointColor : "rgba(95,157,214,1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(95,157,214,1)",
                showTooltip: false,
                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
              },
              {
                label: "Ads B",
                fillColor : "rgba(1,188,140,0)",
                strokeColor : "rgba(1,188,140,1)",
                pointColor : "rgba(1,188,140,1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(1,188,140,1)",
                showTooltip: false,
                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
              },
              {
                label: "Ads C",
                fillColor : "rgba(239,111,108,0)",
                strokeColor : "rgba(239,111,108,1)",
                pointColor : "rgba(239,111,108,1)",
                pointStrokeColor : "#fff",
                pointHighlightFill : "#fff",
                pointHighlightStroke : "rgba(239,111,108,1)",
                data : [randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor(),randomScalingFactor()]
              }
            ]

          }

        // window.onload = function(){
          var report = document.getElementById("show-campaign-result").getContext("2d");
          legend(document.getElementById("legend-indi"), lineChartData);
          window.myLine = new Chart(report).Line(lineChartData, {
            responsive: true
          });
        // }
      }

      $scope.viewDetails = function viewDetails( ) {
        $rootScope.$broadcast('hide');
        $rootScope.$broadcast('show-campaign-details');
      }
      // $scope.showGraph();

      // $scope.$on( "show-ads-result" , 
          // function onReceive(){
            // $scope.showGraph();
            console.log('show-ads-result');
        // });    
    }
])


// jrran90
.controller('previewAdController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "$localStorage",
    "$state",
    "netWork",
    "campaignHolder",
    function controller( $http, $scope, serverUrl, $rootScope, $localStorage, $state, netWork, campaignHolder ) {
      netWork.loadNetwork('preview-ad');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.preview-ad-menu').addClass('active');

      $scope.getCampaign = function getCampaign( ) {
        $http.get( serverUrl.url + 'campaigns/' + $localStorage.user.uniqid )
        .success(function( response ){
          console.log(response);
          $scope.campaignList = response.data;
        });
      }

      $scope.doPayment = function doPayment( id ) {
        $scope.id_campaign = id;
        console.log( $scope.id_campaign );
         $('#paypalDeposit').modal('show');
      }
      $scope.deposit = function deposit( id ) {
        console.log( id );
        $http.post( serverUrl.url + 'campaignsUpdate', { id: id, status: "in-progress" } )
        .success(function( response ){
          console.log( response );
          if( response == 'success'){
            swal({   
                title: "Thank You!",  
                text: "Please check your email.",
                type: "success",
                timer: 2000,   
                showConfirmButton: false
            });
            $scope.getCampaign();
            $('#paypalDeposit').modal('hide');
          } else {

          }
        });
      }

      $scope.togglePreview = function togglePreview() {
          $( '#ads-preview' ).modal('show');
      }

      $scope.goChoose = function goChoose( response ){
        campaignHolder.campaignName( response );
        $state.go('preview-ad-details');
      }
      $scope.campaignName = campaignHolder.campaignName( );
      $scope.launch = function launch( ) {
        console.log('launch');
        $http.get(serverUrl.url + 'sendLaunchCampaignEmail')
        .success(function( response ){
          swal({   
            title: "Thank You!",  
            text: "Please check your email for details.",
            type: "success",
            timer: 2000,   
            showConfirmButton: false
            });

          $state.go('home');
        });
      }

      console.log("show-campaign-details");
      $scope.getCampaign(); 
    }
])
.controller('settingsController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl, $rootScope, netWork ) {
      netWork.loadNetwork('settings');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.settings').addClass('active');
    }
])
.controller('ideasController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl, $rootScope, netWork ) {
      netWork.loadNetwork('ideas');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.ideas-menu').addClass('active-idea');
      $scope.sample_ads = [
        {
          id: 1,
          link_page: 'https://facebook.com/HiredInc',
          link_title: 'Hey, Toronto Engineers, Programmers & Data Scientists',
          description: 'Sign Up For HIRED & See What Companies Will Pay You (Equity + Salary) & Then Hear Their Job Offer.',
          link: 'The Tech Talent Marketplace For The Top 5%',
          img: '4.jpg',
          name: 'Sample Ad 1',
          industries: 'B2B, B2C, Education, Entertainment, Marketing, Services, Startup, Technology',
          objectives: 'Content Promotion, Lead Generation',
          attributes: 'Has Humans, Is Illustration, Is Photo, Is Video, Split Test',
          placement: 'Newsfeed Desktop',
          language: 'English',

        },
        {
          id: 2,
          link_page: 'https://facebook.com/KaplanUniversity',
          link_title: 'Changing Careers? Learn how to develop and implement a successful career change plan.',
          description: 'Career change doesn’t just happen overnight. It requires much consideration, research and a good game plan.',
          link: 'Five Tips to Help You Through Your Career Change Journey',
          img: '1.jpg',
          name: 'Sample Ad 1',
          industries: 'B2B, B2C, Education, Entertainment, Marketing, Startup',
          objectives: 'Content Promotion, Lead Generation',
          attributes: 'Is Illustration, Is Photo, Is Video, Split Test',
          placement: 'Newsfeed Desktop',
          language: 'English',
        },
        {
          id: 3,
          link_page: 'https://facebook.com/answerslifestyle',
          link_title: 'Camping just got a whole lot easier.',
          description: 'Make your camping experience much more effortless with these 10 genius camping hacks and wonder how on earth you didn’t think of them sooner.',
          link: '10 Camping Tricks That Will Make Your Head Spin',
          img: '3.jpg',
          name: 'Sample Ad 1',
          industries: 'B2B, B2C, Education, Entertainment, Marketing, Services, Startup, Technology',
          objectives: 'Content Promotion',
          attributes: 'Is Photo, Is Video, Split Test',
          placement: 'Has Humans, Is Illustration, Is Photo, Is Video, Split Test',
          language: 'English',
        },
        {
          id: 4,
          link_page: 'https://facebook.com/answerslifestyle',
          link_title: 'Camping just got a whole lot easier.',
          description: 'Make your camping experience much more effortless with these 10 genius camping hacks and wonder how on earth you didn’t think of them sooner.',
          link: '10 Camping Tricks That Will Make Your Head Spin',
          img: '2.jpg',
          name: 'Sample Ad 1',
          industries: 'B2B, B2C, Education, Entertainment, Marketing, Services, Startup, Technology',
          objectives: 'Content Promotion',
          attributes: 'Is Photo, Is Video, Split Test',
          placement: 'Has Humans, Is Illustration, Is Photo, Is Video, Split Test',
          language: 'English',
        },
        {
          id: 5,
          link_page: 'https://facebook.com/answerslifestyle',
          link_title: 'Camping just got a whole lot easier.',
          description: 'Make your camping experience much more effortless with these 10 genius camping hacks and wonder how on earth you didn’t think of them sooner.',
          link: '10 Camping Tricks That Will Make Your Head Spin',
          img: '2.jpg',
          name: 'Sample Ad 1',
          industries: 'B2B, B2C, Education, Entertainment, Marketing, Services, Startup, Technology',
          objectives: 'Content Promotion',
          attributes: 'Is Photo, Is Video, Split Test',
          placement: 'Has Humans, Is Illustration, Is Photo, Is Video, Split Test',
          language: 'English',
        },
        {
          id: 6,
          link_page: 'https://facebook.com/answerslifestyle',
          link_title: 'Camping just got a whole lot easier.',
          description: 'Make your camping experience much more effortless with these 10 genius camping hacks and wonder how on earth you didn’t think of them sooner.',
          link: '10 Camping Tricks That Will Make Your Head Spin',
          img: '2.jpg',
          name: 'Sample Ad 1',
          industries: 'B2B, B2C, Education, Entertainment, Marketing, Services, Startup, Technology',
          objectives: 'Content Promotion',
          attributes: 'Is Photo, Is Video, Split Test',
          placement: 'Has Humans, Is Illustration, Is Photo, Is Video, Split Test',
          language: 'English',
        }
        ];

      console.log( $scope.sample_ads );
      $scope.showAdDetail = function showAdDetail( response ) {
        console.log( 'enter: ad_' + response );
        // $('#ad_' + response).removeClass('adBtnDetails');
        // $('#ad_' + response ).addClass('showBtn');
      }
      $scope.hideAdDetails = function hideAdDetails( response ){
        console.log( 'leave: ad_' + response );
        // $('#ad_' + response).addClass('adBtnDetails');
        // $('#ad_' + response ).removeClass('showBtn');
      }
      $scope.getViewAd = function getViewAd( response ){
        $scope.details_campaign = response;
        $('#adsView').modal('show')
      }
    }
])
.controller('creativeBriefController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl, $rootScope, netWork ) {
      netWork.loadNetwork('creative-brief');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.briefs-menu').addClass('active');

      $scope.number = [
      {
        id: 1
      },
      {
        id: 1
      },{
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      },
      {
        id: 1
      }];
    }
])
.controller('revenuesController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    function controller( $http, $scope, serverUrl, $rootScope, netWork ) {
      netWork.loadNetwork('revenues');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.revenues-menu').addClass('active');
      $scope.CurrentDate = new Date();
    }
])
.controller('predictController',[
    "$http",
    "$scope",
    "serverUrl",
    "$rootScope",
    "netWork",
    "$mdDialog",
    "$mdMedia",
    function controller( $http, $scope, serverUrl, $rootScope, netWork, $mdDialog, $mdMedia ) {
      netWork.loadNetwork('predict');
      $('.treeview').removeClass('active');
      $('.treeview').removeClass('active-idea');
      $('.predict').addClass('active');
      var cost_result = [];
      var ctr_result = [];
      var like_result = [];
      var comments_result = [];
      var shares_result = [];
      var next_status = 0;
      $scope.cost = false;
      $scope.ctr = false;
      $scope.likes = false;
      $scope.comments = false;
      $scope.shares = false;
      $scope.tooltip = {
        showTooltip : false,
        tipDirection : ''
      };

      $scope.on = false;
      $scope.data = {
        group1 : 'Default',
        group2 : 'Default',
        group3 : 'Default',
        group4 : 'Default',
      };


      $scope.industry_category = {
        insurance: true,
        financial: false,
        travel: false,
        consumer: false
      };

      $scope.category_status = false;

      $scope.getCategory = function  getCategory ( response ) {
        $scope.category_status = true;
        $('.panel-industry-category').fadeIn(500);
        $('.icon-container-questionnaire').removeClass('category-industry-active');
        $('.' + response + '-container').addClass('category-industry-active');
        switch( response ) {
          case 'insurance':
            $scope.industry_category.insurance = true;
            $scope.industry_category.financial = false;
            $scope.industry_category.travel = false;
            $scope.industry_category.consumer = false;
          break;
          case 'financial':
            $scope.industry_category.insurance = false;
            $scope.industry_category.financial = true;
            $scope.industry_category.travel = false;
            $scope.industry_category.consumer = false;
          break;
          case 'travel':
            $scope.industry_category.insurance = false;
            $scope.industry_category.financial = false;
            $scope.industry_category.travel = true;
            $scope.industry_category.consumer = false;
          break;
          case 'consumer':
            $scope.industry_category.insurance = false;
            $scope.industry_category.financial = false;
            $scope.industry_category.travel = false;
            $scope.industry_category.consumer = true;
          break;
        }
      }
// effects
      $scope.showSubMenu = function showSubMenu () {
        if( $scope.on == false ) {
          $('.explore-sub-menu').slideToggle(500);
          $('.trigger-arrow-glyp').addClass('explore-rotate-arrow');
          $scope.on = true;
        } else if( $scope.on == true ) {
          $('.explore-sub-menu').slideToggle(500);
          $('.trigger-arrow-glyp').removeClass('explore-rotate-arrow');
          $scope.on = false;
        }
      }

      $scope.hidePedictMenu = function  hidePedictMenu () {
        $('.sidebar-reports-container').addClass('remove-side-bar-reports-container');
        $('.right-content-container').removeClass('col-md-8');
        $('.right-content-container').addClass('col-md-12');
        $('.show-side-bar-menu').fadeIn(500);
      }

      $scope.showSideBarMenu = function showSideBarMenu () {
        $('.sidebar-reports-container').removeClass('remove-side-bar-reports-container');
        $('.right-content-container').addClass('col-md-8');
        $('.right-content-container').removeClass('col-md-12');
        $('.show-side-bar-menu').fadeOut(500);
      }

      $scope.showElements = function showElements ( response ) {
        console.log( response );
        $('.static-element-indicator').fadeOut(500, function( ) {
          $('#element-' + response).fadeIn(500);
        });
      }

      $scope.showTabsUl = function  showTabsUl ( id ) {
        $('.structure-components').collapse('hide');
        $('#' + id ).collapse('show');
      }
// end of effect
      $scope.gotStep_2 = function gotStep_2( ) {
        $('#collapseOne').collapse('hide');
        $('.step-1').fadeOut(500);
        $('.step-2').fadeIn(500);
        $('#collapseTwo').collapse('show');
      }

      $scope.gotStep_3 = function gotStep_3( ) {
        console.log( $scope.data );
        $('#collapseTwo').collapse('hide');
        $('.step-2').fadeOut(500);
        $('.step-3').fadeIn(500);
        $('#collapseThree').collapse('show');
      }

      $scope.gotStep_4 = function gotStep_4( ) {
        $('#collapseThree').collapse('hide');
        $('.step-3').fadeOut(500);
        $('.step-4').fadeIn(500);
        $('#collapseFour').collapse()
      }

      $scope.showPredict = function showPredict( ) {
        $('.prediction-container-main').fadeOut();
        $('#collapseFour').collapse('hide');
        $('.step-4').fadeOut(500);
        $('.show-predict-loading').fadeIn(500);
        $('.predict-loading-data').fadeIn(500);
        $('#predict_state_1').css({ "background-color" : "#28A6A0", "color" : "white", "transition" : "1.5s" });
        cost_result.push({ mark: "5", color: "#28A6A0", type: "Cost of Conversion" });
        $scope.allResult = cost_result;

        console.log( $scope.allResult);

        setTimeout(function() {
          $('.steps-predict').fadeIn(500);
          $('.predict-loading-data').fadeOut(500);
          $('.prediction-container-main').fadeIn(500);
          $scope.cost = true;
        }, 3000);
      }

      $scope.getPredictState = function getPredictState( response, state ) {
        console.log( response );
        console.log( state );
        var rand = Math.random() * (100 - 0) + 0;
        // console.log( rand );
        // var color = getRandomColor();
        
        
        // $('.marker-indicator').css( { "margin-left" : rand + "%", "color" : color });
        // console.log( color );
        $('.default-predic-status').css({ "background-color" : "#F4F4F4", "color" : "black", "transition" : "1.5s" });
        // $('#' + state ).addClass('active-predict-selection');
        if( response == "cost_conversion" ) {
          $('#' + state ).css({ "background-color" : "#28A6A0", "color" : "white", "transition" : "1.5s" });
          cost_result.push({ mark: rand, color: "#28A6A0", type: "Cost of Conversion" });
          $scope.allResult = cost_result;
          $scope.cost = true;
          $scope.ctr = false;
          $scope.likes = false;
          $scope.comments = false;
          $scope.shares = false;
        } else if( response == "ctr" ) {
          $('#' + state ).css({ "background-color" : "#3892E3", "color" : "white", "transition" : "1.5s" });
          $scope.allResult = {};
          ctr_result.push({ mark: rand, color: "#3892E3", type: "CTR" });
          $scope.allResult = ctr_result;
          $scope.cost = false;
          $scope.ctr = true;
          $scope.likes = false;
          $scope.comments = false;
          $scope.shares = false;
        } else if( response == "likes" ) {
          $('#' + state ).css({ "background-color" : "#515763", "color" : "white", "transition" : "1.5s" });
          $scope.allResult = {};
          like_result.push({ mark: rand, color: "#515763", type: "Likes" });
          $scope.allResult = like_result;
          $scope.cost = false;
          $scope.ctr = false;
          $scope.likes = true;
          $scope.comments = false;
          $scope.shares = false;
        } else if( response == "comments" ) {
          $('#' + state ).css({ "background-color" : "#715391", "color" : "white", "transition" : "1.5s" });
          $scope.allResult = {};
          comments_result.push({ mark: rand, color: "#715391", type: "Comments" });
          $scope.allResult = comments_result;
          $scope.cost = false;
          $scope.ctr = false;
          $scope.likes = false;
          $scope.comments = true;
          $scope.shares = false;
        } else if( response == "shares" ) {
          $('#' + state ).css({ "background-color" : "#A3D04C", "color" : "white", "transition" : "1.5s" });
          $scope.allResult = {};
          shares_result.push({ mark: rand, color: "#A3D04C", type: "Shares" });
          $scope.allResult = shares_result;
          $scope.cost = false;
          $scope.ctr = false;
          $scope.likes = false;
          $scope.comments = false;
          $scope.shares = true;
        }
      }

      function getRandomColor() {
          var letters = '0123456789ABCDEF'.split('');
          var color = '#';
          for (var i = 0; i < 6; i++ ) {
              color += letters[Math.floor(Math.random() * 16)];
          }
          return color;
      }

      $scope.showBar = function showBar( ) {
        
      }

      $scope.costPreview = function costPreview( response ) {
        console.log( response );
        if( response == "step_1" ) {
            $('#cpcPreview').modal('show');
            $('#cpcPreview').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("CpcchartContainer", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "Ave. Cost per click",
                     showInLegend: false,
                     dataPoints: [
                     { label: "Ads in 1st Person", y: 18 },
                     { label: "Ads in 2nd Person", y: 29 },
                     { label: "Ads in 3rd Person", y: 40 },                                    
                     { label: "Total Ads", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        } else if( response == "step_2" ) {
            $('#cpcPreview2').modal('show');
            $('#cpcPreview2').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("CpcchartContainer2", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: true,
                     dataPoints: [
                     { label: "1 - 5 words", y: 18 },
                     { label: "6 - 10 words", y: 29 },
                     { label: "More than 10 words", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        } else if( response == "step_3" ) {
            $('#cpcPreview3').modal('show');
            $('#cpcPreview3').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("CpcchartContainer3", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "request, command, call to action", y: 18 },
                     { label: "Questions", y: 29 },
                     { label: "Humoroues Statements", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        } else if( response == "step_4" ) {
            $('#cpcPreview4').modal('show');
            $('#cpcPreview4').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("CpcchartContainer4", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "Realistic Image", y: 18 },
                     { label: "Illustrated Image", y: 29 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        }
      }

      $scope.ctrPreview = function ctrPreview( response ) {
        console.log( response );
        if( response == "step_1" ) {
            $('#ctrPreview').modal('show');
            $('#ctrPreview').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("ctrchartContainer", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. CTR"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "Ave. CTR",
                     showInLegend: false,
                     dataPoints: [
                     { label: "Ads in 1st Person", y: 18 },
                     { label: "Ads in 2nd Person", y: 29 },
                     { label: "Ads in 3rd Person", y: 40 },                                    
                     { label: "Total Ads", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                    suffix: "%" 
                  }    
                });

              chart.render();
            });
        } else if( response == "step_2" ) {
            $('#ctrPreview2').modal('show');
            $('#ctrPreview2').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("ctrchartContainer2", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: true,
                     dataPoints: [
                     { label: "1 - 5 words", y: 18 },
                     { label: "6 - 10 words", y: 29 },
                     { label: "More than 10 words", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "%",
                    suffix: "%" 
                  }    
                });

              chart.render();
            });
        } else if( response == "step_3" ) {
          $('#ctrPreview3').modal('show');
            $('#ctrPreview3').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("cdtrchartContainer3", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "request, command, call to action", y: 18 },
                     { label: "Questions", y: 29 },
                     { label: "Humoroues Statements", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
          } else if( response == "step_4" ) {
            $('#ctrPreview4').modal('show');
            $('#ctrPreview4').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("ctrchartContainer4", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "Realistic Image", y: 18 },
                     { label: "Illustrated Image", y: 29 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                    suffix: "%" 
                  }    
                });

              chart.render();
            });
        }
      }

      $scope.likePreview = function likePreview( response ) {
        console.log( response );
        if( response == "step_1" ) {
            $('#likesPreview').modal('show');
            $('#likesPreview').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("likeschartContainer", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Likes"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "Ave. Likes",
                     showInLegend: false,
                     dataPoints: [
                     { label: "Ads in 1st Person", y: 18 },
                     { label: "Ads in 2nd Person", y: 29 },
                     { label: "Ads in 3rd Person", y: 40 },                                    
                     { label: "Total Ads", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                    // suffix: "%" 
                  }    
                });

              chart.render();
            });
        } else if( response == "step_2" ) {
            $('#likesPreview2').modal('show');
            $('#likesPreview2').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("likeschartContainer2", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: true,
                     dataPoints: [
                     { label: "1 - 5 words", y: 18 },
                     { label: "6 - 10 words", y: 29 },
                     { label: "More than 10 words", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        } else if( response == "step_3" ) {
            $('#likesPreview3').modal('show');
            $('#likesPreview3').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("likeschartContainer3", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "request, command, call to action", y: 18 },
                     { label: "Questions", y: 29 },
                     { label: "Humoroues Statements", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        } else if( response == "step_4" ) {
            $('#likesPreview4').modal('show');
            $('#likesPreview4').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("likeschartContainer4", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Cost per click"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "Realistic Image", y: 18 },
                     { label: "Illustrated Image", y: 29 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        }
      }
      $scope.sharePreview = function sharePreview( response ) {
        console.log( response );
        if( response == "step_1" ) {
            $('#sharePreview').modal('show');
            $('#sharePreview').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("sharechartContainer", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Shares"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "Ave. Shares",
                     showInLegend: false,
                     dataPoints: [
                     { label: "Ads in 1st Person", y: 18 },
                     { label: "Ads in 2nd Person", y: 29 },
                     { label: "Ads in 3rd Person", y: 40 },                                    
                     { label: "Total Ads", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                    // suffix: "%" 
                  }    
                });

              chart.render();
            });
        }
      }
      $scope.commentPreview = function commentPreview( response ){
        console.log( response );

        if( response == "step_1" ) {
            $('#commentsPreview').modal('show');
            $('#commentsPreview').on('shown.bs.modal', function (event) {
              // CHART NI XA PARA SA COMMENTS 
                var chart = new CanvasJS.Chart("commentschartContainer",
                {
                  animationEnabled: true,
                  title:{
                    text: "Ave. Comments"
                  },
                  data: [
                  {
                    type: "column", //change type to bar, line, area, pie, etc
                    dataPoints: [
                      { label: "Ads in 1st Person", y: 85 },
                      { label: "Ads in 2nd Person", y: 29 },
                      { label: "Ads in 3rd Person",  y: 40 },                                    
                      { label: "Total Ads",  y: 34 }
                    ]
                  }
                  ]
                  });

                chart.render();
            });
        } else if( response == "step_2" ) {
            $('#commentsPreview2').modal('show');
            $('#commentsPreview2').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("commentsContainer2", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Comments"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: true,
                     dataPoints: [
                     { label: "1 - 5 words", y: 18 },
                     { label: "6 - 10 words", y: 29 },
                     { label: "More than 10 words", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        } else if( response == "step_3" ) {
            $('#commentsreview3').modal('show');
            $('#commentsreview3').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("commentschartContainer3", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Comments"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "request, command, call to action", y: 18 },
                     { label: "Questions", y: 29 },
                     { label: "Humoroues Statements", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        } else if( response == "step_4" ) {
            $('#commentsreview4').modal('show');
            $('#commentsreview4').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("commentschartContainer4", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Comments"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "Realistic Image", y: 18 },
                     { label: "Illustrated Image", y: 29 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        }
      }
      $scope.sharePreview = function sharePreview( response ) {
        console.log( response );
        if( response == "step_1" ) {
            $('#sharePreview').modal('show');
            $('#sharePreview').on('shown.bs.modal', function (event) {
              // CHART NI XA PARA SA COMMENTS 
                var chart = new CanvasJS.Chart("sharechartContainer",
                {
                  animationEnabled: true,
                  title:{
                    text: "Ave. Shares"
                  },
                  data: [
                  {
                    type: "column", //change type to bar, line, area, pie, etc
                    dataPoints: [
                      { label: "Ads in 1st Person", y: 85 },
                      { label: "Ads in 2nd Person", y: 29 },
                      { label: "Ads in 3rd Person",  y: 40 },                                    
                      { label: "Total Ads",  y: 34 }
                    ]
                  }
                  ]
                  });

                chart.render();
            });
        } else if( response == "step_2" ) {
            $('#sharePreview2').modal('show');
            $('#sharePreview2').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("sharechartContainer2", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Shares"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: true,
                     dataPoints: [
                     { label: "1 - 5 words", y: 18 },
                     { label: "6 - 10 words", y: 29 },
                     { label: "More than 10 words", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        } else if( response == "step_3" ) {
            $('#sharePreview3').modal('show');
            $('#sharePreview3').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("sharechartContainer3", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Shares"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "request, command, call to action", y: 18 },
                     { label: "Questions", y: 29 },
                     { label: "Humoroues Statements", y: 40 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        } else if( response == "step_4" ) {
            $('#sharePreview4').modal('show');
            $('#sharePreview4').on('shown.bs.modal', function (event) {
                // CHART NI XA PARA SA cpc
                var chart = new CanvasJS.Chart("sharechartContainer4", {

                    theme: "theme2",
                          
                    title:{
                      text: "Ave. Shares"              
                    },

                    data: [  //array of dataSeries     
                    { //dataSeries - first quarter                  
                     type: "column",
                     name: "No. of Headline words",
                     showInLegend: false,
                     dataPoints: [
                     { label: "Realistic Image", y: 18 },
                     { label: "Illustrated Image", y: 29 },                                    
                     { label: "Total", y: 34 }
                     ]
                   },
                  ],
               /** Set axisY properties here*/
                  axisY:{
                    // prefix: "$",
                   /* suffix: "K" */
                  }    
                });

              chart.render();
            });
        }
      }
      $scope.showPreview = function showPreview( ) {
        $('#suggestionsPreview').modal('show');
        $('#suggestionsPreview').on('shown.bs.modal', function (event) {

            // CHART NI XA PARA SA COMMENTS 
            /*var chart = new CanvasJS.Chart("chartContainer",
            {
              animationEnabled: true,
              title:{
                text: "Ave. Comments"
              },
              data: [
              {
                type: "column", //change type to bar, line, area, pie, etc
                dataPoints: [
                  { label: "Ads in 1st Person", y: 85 },
                  { label: "Ads in 2nd Person", y: 29 },
                  { label: "Ads in 3rd Person",  y: 40 },                                    
                  { label: "Total Ads",  y: 34 }
                ]
              }
              ]
              });

            chart.render();*/

            // CHART NI XA PARA SA cpc
            //   var chart = new CanvasJS.Chart("chartContainer", {

            //       theme: "theme2",
                        
            //       title:{
            //         text: "Ave. Cost per click"              
            //       },

            //       data: [  //array of dataSeries     
            //       { //dataSeries - first quarter                  
            //        type: "column",
            //        name: "Ave. Cost per click",
            //        showInLegend: false,
            //        dataPoints: [
            //        { label: "Ads in 1st Person", y: 18 },
            //        { label: "Ads in 2nd Person", y: 29 },
            //        { label: "Ads in 3rd Person", y: 40 },                                    
            //        { label: "Total Ads", y: 34 }
            //        ]
            //      },
            //     ],
            //  /** Set axisY properties here*/
            //     axisY:{
            //       prefix: "$",
            //      /* suffix: "K" */
            //     }    
            //   });

            // chart.render();

            // CHART NI XA PARA SA CTR
            //   var chart = new CanvasJS.Chart("chartContainer", {

            //       theme: "theme2",
                        
            //       title:{
            //         text: "Ave. CTR"              
            //       },

            //       data: [  //array of dataSeries     
            //       { //dataSeries - first quarter                  
            //        type: "column",
            //        name: "Ave. CTR",
            //        showInLegend: false,
            //        dataPoints: [
            //        { label: "Ads in 1st Person", y: 1.3 },
            //        { label: "Ads in 2nd Person", y: 0.54 },
            //        { label: "Ads in 3rd Person", y: 0.06 },                                    
            //        { label: "Total Ads", y: 0.70 }
            //        ]
            //      },
            //     ],
            //  /** Set axisY properties here*/
            //     axisY:{
            //       // prefix: "$",
            //       suffix: "%",
            //     }    
            //   });

            // chart.render();

            // CHART NI XA PARA SA CTR
              var chart = new CanvasJS.Chart("chartContainer", {

                  theme: "theme2",
                        
                  title:{
                    text: "Ave. Likes"              
                  },

                  data: [  //array of dataSeries     
                  { //dataSeries - first quarter                  
                   type: "column",
                   name: "Ave. CTR",
                   showInLegend: false,
                   dataPoints: [
                   { label: "Ads in 1st Person", y: 121 },
                   { label: "Ads in 2nd Person", y: 253 },
                   { label: "Ads in 3rd Person", y: 65 },                                    
                   { label: "Total Ads", y: 146 }
                   ]
                 },
                ],
             /** Set axisY properties here*/
                axisY:{
                  // prefix: "$",
                  // suffix: "%",
                }    
              });

            chart.render();


          // var areaChartData = {
          //       labels: ["with incoporated into images", "w/o incoporated into images"],
          //       datasets: [{
          //           label: "Electronics",
          //           fillColor: "rgba(254, 201, 12, 1)",
          //           strokeColor: "rgba(254, 201, 12, 1)",
          //           pointColor: "rgba(254, 201, 12, 1)",
          //           pointStrokeColor: "#c1c7d1",
          //           pointHighlightFill: "#fff",
          //           pointHighlightStroke: "rgba(254, 201, 12, 1)",
          //           data: [65,30]
          //         }]
          //     };
          //     Chart.types.Bar.extend({
          //         name: "BarAlt",
          //         draw: function(){
          //             this.options.barValueSpacing = this.chart.width / 5;
          //             Chart.types.Bar.prototype.draw.apply(this, arguments);
          //         }
          //     });

          //     var ctx = $("#barChart").get(0).getContext("2d");
          //       var myLineChart = new Chart(ctx).BarAlt(areaChartData, {
          //           scaleShowGridLines: false,
          //           scaleShowHorizontalLines: false,
          //           scaleShowVerticalLines: false,
          //           scaleLabel:  "<%=value%>",
          //           responsive: true
          //       });
              // var barChartCanvas = $("#barChart").get(0).getContext("2d");
              // var barChart = new Chart(barChartCanvas);
              // var barChartData = areaChartData;
              // barChartData.datasets[1].fillColor = "#00a65a";
              // barChartData.datasets[1].strokeColor = "#00a65a";
              // barChartData.datasets[1].pointColor = "#00a65a";
              // var barChartOptions = {
              //   //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
              //   scaleBeginAtZero: true,
              //   //Boolean - Whether grid lines are shown across the chart
              //   scaleShowGridLines: false,
              //   //String - Colour of the grid lines
              //   scaleGridLineColor: "rgba(0,0,0,.05)",
              //   //Number - Width of the grid lines
              //   scaleGridLineWidth: 0,
              //   //Boolean - Whether to show horizontal lines (except X axis)
              //   scaleShowHorizontalLines: false,
              //   //Boolean - Whether to show vertical lines (except Y axis)
              //   scaleShowVerticalLines: false,
              //   //Boolean - If there is a stroke on each bar
              //   barShowStroke: true,
              //   //Number - Pixel width of the bar stroke
              //   barStrokeWidth: 10,
              //   //Number - Spacing between each of the X value sets
              //   barValueSpacing: 20,
              //   //Number - Spacing between data sets within X values
              //   barDatasetSpacing: 20,
              //   //String - A legend template
              //   legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
              //   //Boolean - whether to make the chart responsive
              //   responsive: true,
              //   maintainAspectRatio: true
              // };

              // barChartOptions.datasetFill = false;
              // barChart.Bar(barChartData, barChartOptions);
        });
        // $('#suggestionsPreview').modal('show');
      }
      $scope.SingleImage = function SingleImage( ) {
        $('.singleImageSelection').fadeIn(500);
        $('.singleImageQuestion_1').fadeIn(700);
        $('.singleImageQuestion_2').fadeIn(900);
        $('.singleImageQuestion_3').fadeIn(1100);
        $('.singleImageQuestion_4').fadeIn(1300);
        $('.singleImageQuestion_5').fadeIn(1500);
      }

      $scope.Realistic = function Realistic( ) {
        $('.singleImageQuestion_2_featured').slideToggle(700);
        $('.singleImageQuestion_2_people').slideToggle(900);
        $('.singleImageQuestion_2_ethnic').slideToggle(1100);
      }

      $scope.object = function object( ) {
        $('.singleImageQuestion_2_objects').slideToggle(700);
        $('.singleImageQuestion_2_pictures').slideToggle(900);
      }

      $scope.CarouselImage = function CarouselImage( ) {
        $('.singleImageSelection').fadeOut(700);
        $('.carouselQuestion_1').fadeIn(900);
      }

      $scope.letsDoit = function letsDoit( ) {
        $('.predict-status-information-container').fadeOut(500);
        // $('.suggestion-marker-1').fadeIn(500).css("margin-left","10%");
        $('.suggestion_1').fadeIn(700);
        // $('.next-indicator').fadeIn(500);
        // $('.to-top-indicator').fadeOut(200);
        $scope.allResult = cost_result;
      }

      // $scope.nextFunction = function nextFunction( ) {
      //   console.log( next_status);
      //   if(next_status == 0) {
      //     $('.suggestion_2').fadeIn( 700 );
      //     $('.suggestion-marker-2').fadeIn(500).css("margin-left","30%");
      //     next_status = 1;
      //   } else if( next_status == 1) {
      //     $('.suggestion_3').fadeIn( 700 );
      //     $('.suggestion-marker-3').fadeIn(500).css("margin-left","60%");
      //     next_status = 2;
      //   } else if( next_status == 2) {
      //     $('.suggestion_4').fadeIn( 700 );
      //     $('.suggestion-marker-4').fadeIn(500).css("margin-left","90%");
      //     $('.next-indicator-buttons').fadeOut(500, function( ) {
      //       $('.to-top-indicator').fadeIn(200);
      //     })
      //   }
      // }

      $scope.nextSuggestion = function nextSuggestion( show_section_id , current_section_id , suggestion_details_id, slide_marker, percent ) {
        
        console.log( slide_marker, percent );

        if(show_section_id == "final-suggestion") 
        {
          $('.next-indicator-buttons').fadeOut(500);
          $('.email-add-button').fadeIn(500);
          $( '.' + slide_marker ).fadeIn(500).css("margin-left", percent);
        } else {
          $( '#' + show_section_id ).collapse();
          $( '#' + current_section_id ).collapse( 'hide' );
          $( '#' + suggestion_details_id ).fadeIn();
          $( '#suggestion-details-1 label' ).fadeIn();
          $( '#' + suggestion_details_id + ' label' ).fadeIn();
          $( '.' + slide_marker ).fadeIn(500).css("margin-left", percent);
        }
      }
      $scope.previewSuggestion = function previewSuggestion(  show_section_id ) {      
        $( '.collapsed-section' ).collapse( 'hide' );
        $( '#' + show_section_id ).collapse( 'show' );
      }
      var alert;
      $scope.showConfirm = function() {
        swal("Done!", "This suggestion will be send to your email address.", "success");
        $('.next-indicator-buttons').fadeIn(500);
        $('.email-add-button').fadeOut(500);
        $('#suggestion_4_collapse').collapse('hide');
      };

      // explore charts
      $scope.showExploreGraphsStrucuture = function  showExploreGraphsStrucuture ( result, id ) {
        var rand_1 = Math.round(Math.random()*100);
        var rand_2 = Math.round(Math.random()*100);
        var rand_3 = Math.round(Math.random()*100);
        var rand_4 = Math.round(Math.random()*100);
        var rand_5 = Math.round(Math.random()*100);
        var rand_6 = Math.round(Math.random()*100);

        var color = getRandomColor();

        $('.structure-components').collapse('hide');
        $('#' + id ).collapse('show');

        function getRandomColor() {
          var letters = '0123456789ABCDEF'.split('');
          var color = '#';
          for (var i = 0; i < 6; i++ ) {
              color += letters[Math.floor(Math.random() * 16)];
          }
          return color;
        }


        var chart = new CanvasJS.Chart("explore_engagement_stucture", {
            animationEnabled: true,
            dataPointWidth: 10,
            axisX:{
             title: "Length of a Post ( in characters )",
             titleFontColor: "#28A6A0",
             titleFontSize: 12,
             tickLength: 0,
             tickThickness: 0,
             gridThickness: 0
            },
            axisY:{
             title: result,
             titleFontColor: "#28A6A0",
             titleFontSize: 10,
             tickLength: 0,
             tickThickness: 0,
             gridThickness: 0
            },
            data: [  //array of dataSeries     
            { //dataSeries - first quarter                  
             color: color,
             type: "column",
             showInLegend: false,
             dataPoints: [
             { label: rand_1, y: rand_1 },
             { label: rand_2, y: rand_2 },
             { label: rand_3, y: rand_3 },
             { label: rand_4, y: rand_4 },
             { label: rand_5, y: rand_5 },
             { label: rand_6, y: rand_6 }
             ]
           },
          ],
         /** Set axisY properties here*/
            axisY:{
              // prefix: "k",
              suffix: "k",
              tickLength: 2
            }    
          });

        chart.render();
      }
      $scope.showExploreGraphsTone = function  showExploreGraphsTone ( result, id ) {
        var rand_1 = Math.round(Math.random()*100);
        var rand_2 = Math.round(Math.random()*100);
        var rand_3 = Math.round(Math.random()*100);
        var rand_4 = Math.round(Math.random()*100);
        var rand_5 = Math.round(Math.random()*100);
        var rand_6 = Math.round(Math.random()*100);

        var color = getRandomColor();

        $('.structure-components').collapse('hide');
        $('#' + id ).collapse('show');

        function getRandomColor() {
          var letters = '0123456789ABCDEF'.split('');
          var color = '#';
          for (var i = 0; i < 6; i++ ) {
              color += letters[Math.floor(Math.random() * 16)];
          }
          return color;
        }


        var chart = new CanvasJS.Chart("explore_engagement_tone", {
            animationEnabled: true,
            dataPointWidth: 10,
            axisX:{
             title: "Post contains Question",
             titleFontColor: "#28A6A0",
             titleFontSize: 12,
             tickLength: 0,
             lineThickness: 0,
             gridThickness: 0,
            },
            axisY:{
             title: result,
             titleFontColor: "#28A6A0",
             titleFontSize: 10,
             tickLength: 0,
             gridThickness: 0,
             lineThickness: 0,
            },
            data: [  //array of dataSeries     
            { //dataSeries - first quarter                  
             color: color,
             type: "column",
             showInLegend: false,
             dataPoints: [
             { label: "Yes", y: rand_1 },
             { label: "No", y: rand_2 },
             ]
           },
          ],
         /** Set axisY properties here*/
            axisY:{
              // prefix: "k",
              suffix: "k",
              tickLength: 2
            }    
          });

        chart.render();
      }
      $scope.showExploreGraphsIntent = function  showExploreGraphsIntent ( result, id ) {
        var rand_1 = Math.round(Math.random()*100);
        var rand_2 = Math.round(Math.random()*100);
        var rand_3 = Math.round(Math.random()*100);
        var rand_4 = Math.round(Math.random()*100);
        var rand_5 = Math.round(Math.random()*100);
        var rand_6 = Math.round(Math.random()*100);

        var color = getRandomColor();

        $('.structure-components').collapse('hide');
        $('#' + id ).collapse('show');

        function getRandomColor() {
          var letters = '0123456789ABCDEF'.split('');
          var color = '#';
          for (var i = 0; i < 6; i++ ) {
              color += letters[Math.floor(Math.random() * 16)];
          }
          return color;
        }


        var chart = new CanvasJS.Chart("explore_engagement_intent", {
            animationEnabled: true,
            dataPointWidth: 10,
            axisX:{
             title: "Post refers to a Brand Product",
             titleFontColor: "#28A6A0",
             titleFontSize: 12,
             tickLength: 0,
             lineThickness: 0,
             gridThickness: 0,
            },
            axisY:{
             title: result,
             titleFontColor: "#28A6A0",
             titleFontSize: 10,
             tickLength: 0,
             gridThickness: 0,
             lineThickness: 0,
            },
            data: [  //array of dataSeries     
            { //dataSeries - first quarter                  
             color: color,
             type: "column",
             showInLegend: false,
             dataPoints: [
             { label: "Yes", y: rand_1 },
             { label: "No", y: rand_2 },
             ]
           },
          ],
         /** Set axisY properties here*/
            axisY:{
              // prefix: "k",
              suffix: "k",
              tickLength: 2
            }    
          });

        chart.render();
      }
      $scope.showExploreGraphsContext = function  showExploreGraphsContext ( result, id ) {
        var rand_1 = Math.round(Math.random()*100);
        var rand_2 = Math.round(Math.random()*100);
        var rand_3 = Math.round(Math.random()*100);
        var rand_4 = Math.round(Math.random()*100);
        var rand_5 = Math.round(Math.random()*100);
        var rand_6 = Math.round(Math.random()*100);

        var color = getRandomColor();

        $('.structure-components').collapse('hide');
        $('#' + id ).collapse('show');

        function getRandomColor() {
          var letters = '0123456789ABCDEF'.split('');
          var color = '#';
          for (var i = 0; i < 6; i++ ) {
              color += letters[Math.floor(Math.random() * 16)];
          }
          return color;
        }


        var chart = new CanvasJS.Chart("explore_engagement_context", {
            animationEnabled: true,
            dataPointWidth: 10,
            axisX:{
             title: "Post refers to a Mobile/Tablet App",
             titleFontColor: "#28A6A0",
             titleFontSize: 12,
             tickLength: 0,
             lineThickness: 0,
             gridThickness: 0,
            },
            axisY:{
             title: result,
             titleFontColor: "#28A6A0",
             titleFontSize: 10,
             tickLength: 0,
             gridThickness: 0,
             lineThickness: 0,
            },
            data: [  //array of dataSeries     
            { //dataSeries - first quarter                  
             color: color,
             type: "column",
             showInLegend: false,
             dataPoints: [
             { label: "Yes", y: rand_1 },
             { label: "No", y: rand_2 },
             ]
           },
          ],
         /** Set axisY properties here*/
            axisY:{
              // prefix: "k",
              suffix: "k",
              tickLength: 2
            }    
          });

        chart.render();
      }


      $scope.category = function category ( response ) {
        
        if( response == "image-category" ) 
        {
          $('.text-category').fadeOut(500);
          $('.image-category').fadeIn(500);
        } else if( response == "text-category" ) {
          $('.text-category').fadeIn(500);
          $('.image-category').fadeOut(500);
        }
      }



      $scope.predictResult = function predictResult () {
        $('.predict-btn-trigger').fadeOut(500, function(){
          $('.predict-progress').fadeIn(1000,function(){
            $('.qustionnaires').fadeOut(10, function(){
              $('.prediction-container').fadeIn();
              $('body').animatescroll({scrollSpeed:2000,easing:'easeInOutQuint'});
            });
          });
        });
      }

      $scope.imageFocusGraph = function imageFocusGraph( ) {

        var chart = new CanvasJS.Chart("imageFocusBarGraph", {
            animationEnabled: true,
            dataPointWidth: 10,
            axisX:{
             // title: "Length of a Post ( in characters )",
             titleFontColor: "#28A6A0",
             titleFontSize: 12,
             tickLength: 0,
             tickThickness: 0,
             gridThickness: 0
            },
            axisY:{
             title: '',
             titleFontColor: "#28A6A0",
             titleFontSize: 10,
             tickLength: 0,
             tickThickness: 0,
             gridThickness: 0
            },
            data: [  //array of dataSeries     
            { //dataSeries - first quarter                  
             color: "#28A6A0",
             type: "column",
             showInLegend: false,
             dataPoints: [
             { label: "Human", y: 80 },
             { label: "Object", y: 10 },
             { label: "Landscape", y: 5 },
             ]
           },
          ],
         /** Set axisY properties here*/
            axisY:{
              // prefix: "k",
              suffix: "k",
              tickLength: 2
            }    
          });

        chart.render();
      }

      $scope.imageFocusImageGraph = function imageFocusImageGraph( ) {

        var chart = new CanvasJS.Chart("imageFocusImageBarGraph", {
            animationEnabled: true,
            dataPointWidth: 10,
            axisX:{
             // title: "Length of a Post ( in characters )",
             titleFontColor: "#28A6A0",
             titleFontSize: 12,
             tickLength: 0,
             tickThickness: 0,
             gridThickness: 0
            },
            axisY:{
             title: '',
             titleFontColor: "#28A6A0",
             titleFontSize: 10,
             tickLength: 0,
             tickThickness: 0,
             gridThickness: 0
            },
            data: [  //array of dataSeries     
            { //dataSeries - first quarter                  
             color: "#28A6A0",
             type: "column",
             showInLegend: false,
             dataPoints: [
             { label: "Human", y: 80 },
             { label: "Object", y: 10 },
             { label: "Landscape", y: 5 },
             ]
           },
          ],
         /** Set axisY properties here*/
            axisY:{
              // prefix: "k",
              suffix: "k",
              tickLength: 2
            }    
          });

        chart.render();
      }
      // $scope.showExploreGraphsStrucuture('engagement', '');
      // $scope.showBar();
    }
]);