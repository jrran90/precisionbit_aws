app
	.controller('homeController', [
		"$http",
		"$scope",
		"nodeUrl",
		"Upload",
		"GraymaticAPI",
		"ImaggaAPI",
		"serverUrl",
		function controller( $http, $scope, nodeUrl, Upload, GraymaticAPI, ImaggaAPI, serverUrl ){
			console.log('homeController');
			$('.side-bar-menu').removeClass('active');
			$('.home-li').addClass('active');
			$scope.data = {};
			$scope.testApi = function testApi( ) {


				console.log('test')
				$http.get(nodeUrl.url)
				.success(function( response ){
					console.log('res', response );
				})
				.error(function( error ){
					console.log( error );
				});
			}


			$scope.postLink = function postLink( ) {

				console.log( $scope.data.imageLink );
				$http.post( nodeUrl.url + '/image_text', $scope.data )
				.success(function( response){
					console.log( response );
				})
				.error(function( response ){
					console.log( response );
				});
			}

			$scope.submit = function() {
				console.log( $scope.file );
		      // if ($scope.form.file.$valid && $scope.file) {
		        $scope.upload($scope.file);
		      // }
		    };

			$scope.upload = function (file) {
		        Upload.upload({
		            url: nodeUrl.url + 'image_pic_api',
		            data: {file: file }
		        }).then(function (resp) {
		            console.log(resp);
		        });
		    };

		    $scope.imageInit = function imageInit( ) {

		    	$("#input-cg").fileinput({  
			      uploadUrl: nodeUrl.url + 'image_pic_api',// server upload action
			        uploadAsync: true,
			        uploadExtraData: function(previewId, index) {  // callback example

			            var out = {'image_question' : $('.kv-input:eq('+index+')').val() };

			            return out;

			        }
			    }).on('filepreupload', function(event, data, previewId, index) {
			      $('#kv-success-1').html('<h4>Upload Status</h4><ul></ul>').hide();
			    }).on('fileuploaded', function(event, data, id, index) {
			    	// clarifai api
			    	console.log('clarifai',data);
            if( data.response == "true" ) {
              sweetAlert("", "Image already uploaded!", "error");
            } else if( data.response != "true" ) {
			        // --------------------------
			        // Graymatic API
			        // --------------------------
			        GraymaticAPI
			        	.get(data.response.cloud_image)
				        .then(function(res) {
				        	console.log('graymatic', res.data);

					        var fname = data.files[index].name,
					            out = '<li>' + 'Uploaded file # ' + (index + 1) + ' - '  +  
					                fname + ' successfully.' + '</li>';
					        $('#kv-success-1 ul').append(out);
					        $('#kv-success-1').fadeIn('slow');

					        // new object
					        var newData = {
					        	clarifai: data.response,
					        	graymatic: res.data.results,
					        	category: 'api_upload',
					        	image: data.response.image
					        };

					        console.log( 'newData',newData );

					        $http.post( serverUrl.url + 'upload-experiments', newData)
					        .success(function(response){
					        	console.log(response);
					        })
					        .error(function(error) {
					        	// body...
					        	console.log(error);
					        })
					        // end of new object

				        },function(err) {
				         	alert('Error!');
				         	console.log(err.data)
				        });
            }

			    }).on("filepredelete", function(jqXHR) {
			        var abort = true;
			        if (confirm("Are you sure you want to delete this image?")) {
			            abort = false;
			        }
			        return abort; // you can also send any data/object that you can receive on `filecustomerror` event
			    });

		    }

        // for Imagga API
        $scope.immagaApi = function() {
          $("#input-imma").fileinput({  
            uploadUrl: nodeUrl.url + 'immaga_api',// server upload action
              uploadAsync: true,
              uploadExtraData: function(previewId,index) {  // callback example

                  var out = {'image_question' : $('.kv-input:eq('+index+')').val() };

                  return out;

              }
          }).on('filepreupload', function(event, data, previewId, index) {
            $('#kv-success-1-imm').html('<h4>Upload Status</h4><ul></ul>').hide();
          }).on('fileuploaded', function(event, data, id, index) {
            console.log(data)

            if( data.response == "true" ) {
              sweetAlert("", "Image already uploaded!", "error");
            } else if( data.response != "true" ) {
	            var image = data.response.image,
	                  out = '<li>' + 'Uploaded file # ' + (index + 1) + ' - '  +  
	                        image + ' successfully.' + '</li>';
            
	        	  ImaggaAPI.store(data.response.cloud_image)
	        		.then(function (res) {
	    	    		console.log(res.data)
			            $('#kv-success-1-imm ul').append(out);
			            $('#kv-success-1-imm').fadeIn('slow');
                  // $('html,body').animate({scrollTop: dest}, 1000,'swing');
	        		});
            }



          }).on("filepredelete", function(jqXHR) {
              var abort = true;
              if (confirm("Are you sure you want to delete this image?")) {
                  abort = false;
              }
              return abort; // you can also send any data/object that you can receive on `filecustomerror` event
          });
        }

		    $scope.imageInit();
				$scope.testApi();
		}
	])
	.controller('clientListController', [
		"$http",
		"$scope",
		"nodeUrl",
		"serverUrl",
		function controller( $http, $scope, nodeUrl, serverUrl ){
			console.log('clientListController');
			$('.side-bar-menu').removeClass('active');
			$('.client-li').addClass('active');
			$scope.clientObject = {};
			$scope.experimentData = {};
			// Display all clients in Client Tab
			$scope.clientLists = function clientLists() {
				$http.get( serverUrl.url + 'getClientLists' )
					.success( function success( response ) {
						// console.log( response );
					} );
			}
			// Get Client Data 
			$scope.getClientData = function getClientData( id ) {
				$http.get( serverUrl.url + 'getClientExperiments/' + id )
					.success( function success( response ) {
						console.log( response );
						// $scope.clientObject.predict = response.predict;
						$scope.clientObject.predict = response;
						$scope.client_id = response._id;
					} );
			}
			// Client Experiment list in Modal
			// $scope.experimentPreview = function experimentPreview( arr ) {
			// 	$scope.experimentData = arr;
			// }

			// $scope.defaultPic = "samplepage_background.jpg";

			$scope.clientLists();
			$scope.getClientData( 'Telstra' );
		}
	])
	.controller('clientController', [
		"$http",
		"$scope",
		"nodeUrl",
		"serverUrl",
		"Upload",
		function controller( $http, $scope, nodeUrl, serverUrl, Upload ){
			console.log('clientController');
			$('.side-bar-menu').removeClass('active');
			$('.client-li').addClass('active');

		    $scope.imageInit = function imageInit( ) {
		    	$("#exp-files").fileinput({  
			      uploadUrl: nodeUrl.url + 'image_pic_client',// server upload action
			        uploadAsync: true,
			        uploadExtraData: function(previewId,index) {  // callback example

			            var out = {'image_question' : $('.kv-input:eq('+index+')').val() };			            
			            return out;

			        }
			    }).on('filepreupload', function(event, data, previewId, index) {
			      $('#kv-success-1').html('<h4>Upload Status</h4><ul></ul>').hide();
			    }).on('fileuploaded', function(event, data, id, index) {
			    	// console.log( data.response );
			        var fname = data.files[index].name,
			            out = '<li>' + 'Uploaded file # ' + (index + 1) + ' - '  +  
			                fname + ' successfully.' + '</li>';
			        $('#kv-success-1 ul').append(out);
			        $('#kv-success-1').fadeIn('slow');

			        var filedata = data.response;
			        filedata.category = "client_upload";
			        $scope.imageUpload( filedata );

			    }).on("filepredelete", function(jqXHR) {
			        var abort = true;
			        if (confirm("Are you sure you want to delete this image?")) {
			            abort = false;
			        }
			        return abort; // you can also send any data/object that you can receive on `filecustomerror` event
			    });
		    }
		    $scope.experimentAttrib = {};
		    $scope.actionBtn = true;
		    $scope.haveSelectedImage = false;
			$scope.showPicture = function showPicture (arr) {
				$scope.defaultPic = arr.filename;
				$scope.haveSelectedImage = true;
				$scope.img_id = arr.image_id;
				console.log( arr.img_tags )
				if ( arr.img_tags == undefined ) {
					$scope.experimentAttrib.img_tags = false;
				};
				if ( arr.client_upload_data != undefined ) {					
					$scope.experimentAttrib.img_tags = arr.img_tags;
					$scope.experimentAttrib.emotional_response = arr.client_upload_data[0].emotional_response;
					$scope.experimentAttrib.aesthetic_score = arr.client_upload_data[0].aesthetic_score;
					$scope.experimentAttrib.engaging_score = arr.client_upload_data[0].engaging_score;
					$scope.experimentAttrib.ctr = arr.client_upload_data[0].ctr;
		    		$scope.actionBtn = false;

				}else {
					$scope.experimentAttrib.emotional_response = null;
					$scope.experimentAttrib.aesthetic_score = null;
					$scope.experimentAttrib.engaging_score = null;
					$scope.experimentAttrib.ctr = null;
		    		$scope.actionBtn = true;
				}
			}

		    // Upload Image Functions
		    $scope.clientImageObject = {};

		    $scope.fetchImages = function fetchImages( category ) {
		    	$http.get( serverUrl.url + 'getCategoryImages/' + category )
		    		.success( function success( response ) {
		    			$scope.clientImageObject = response;
		    		} );
		    }		   
		    $scope.imageUpload = function imageUpload( arr ) {
		    	$http.post( serverUrl.url + 'upload-experiments' , arr )
		    		.success( function success( response ) {
		    			console.log( response );
		    		} );
		    }
		    $scope.saveExperimentAttrib = function saveExperimentAttrib( action ) {
		    	$scope.experimentAttrib.filename =  $scope.defaultPic;
		    	$scope.experimentAttrib.img_id = $scope.img_id;

		    	var img_tags = $('.tag_input').map( function() { 		    		
		    		return $(this).val();	
		    	} ).get();
		    	
		    	$scope.experimentAttrib.img_tags = [];
		    	for( x in img_tags ){
		    		if ( img_tags[x].length !== 0 ) {
		    			$scope.experimentAttrib.img_tags[x] = img_tags[x];
		    		}
		    	}
		    	if ( $scope.defaultPic ) {		    		
			    	if (action == 'create'  ) {
			    		$scope.experimentAttrib.img_tags;
			    		$( '#default_input' ).val(null);
			    		$scope.items = [];
			    		console.log( $scope.experimentAttrib )
			    		$http.post( serverUrl.url + 'addimageexperimentattribute' , $scope.experimentAttrib )
				    		.success( function success( response ) {
				    			sweetAlert("", "Successfully added information on image!", "success");
				    			console.log( 'create Image data' );
				    			console.log( response );
				    			$scope.fetchImages( 'client_upload' );
				    			setTimeout(function() {$scope.actionBtn = false;}, 10);
				    		} );
			    	}else if( action == 'update'  ) {
			    		$scope.items = [];
			    		$scope.experimentAttrib.img_tags;
			    		$( '#default_input' ).val(null);
			    		console.log( $scope.experimentAttrib )
			    		$http.post( serverUrl.url + 'updateimageexperimentattribute' , $scope.experimentAttrib )
				    		.success( function success( response ) {
				    			console.log( response );
				    			console.log( 'update image data' );
				    			sweetAlert("", "Successfully updated.", "success");
				    		} );	
			    	}
			    	if( $scope.giveInputAccess == true && $scope.haveSelectedImage == true ) {		    		
			    		$scope.selectedListObject.filename =  $scope.experimentAttrib.filename;
			    		console.log( $scope.selectedListObject )
				    	console.log( 'add image file to user experiment' );
			    		$http.post( serverUrl.url + 'addclientexperimentimage' , $scope.selectedListObject )
				    		.success( function success( response ) {
				    			console.log( response );
				    		} );	
			    	}
		    	}else {
		    		sweetAlert("", "Upload or choose an image before saving.", "error");
		    	}
		    }


		    $scope.fetchImages( 'client_upload' ); //Display alls uploaded images with category of "client_upload"
		    $scope.imageInit();

		    $scope.selectedListObject = {};
		    $scope.giveInput = function giveInput( arr , index ) {
		    	console.log( arr )
		    	$scope.openTab( 'result' );
		    	$( '.list-tab' ).removeClass( 'active' );
		    	$( '.list-tab.exp-res' ).addClass( 'active' );
		    	$scope.selectedListObject = arr;
		    	$scope.selectedListObject.date_created = arr.date_created;
		    	console.log( arr.date_created );
		    	$scope.giveInputAccess = true;
		    	if ( arr.filename ) {
		    		$scope.defaultPic = arr.filename;
		    		// image inputs if available
		    		$http.get( serverUrl.url + 'getSingleImage/' + arr.filename )
		    			.success( function success( response ) {
		    				console.log( response[0].client_upload_data[0] );
		    				$scope.experimentAttrib.emotional_response = response[0].client_upload_data[0].emotional_response;
							$scope.experimentAttrib.aesthetic_score = response[0].client_upload_data[0].aesthetic_score;
							$scope.experimentAttrib.engaging_score = response[0].client_upload_data[0].engaging_score;
		    			} );

		    	}else {
		    		$scope.defaultPic = null;
					$scope.experimentAttrib.emotional_response = null;
					$scope.experimentAttrib.aesthetic_score = null;
					$scope.experimentAttrib.engaging_score = null;
					$scope.experimentAttrib.img_tags = false;
		    	}

		    	$scope.selectedListObject.client_id = arr.client_id;
		    	$scope.selectedListObject.index = index;
		    }
			$scope.openTab = function openTab( id ) {
				$( '.tab-pane.main-tab' ).removeClass( 'active' );
				$( '#experiment-' + id ).addClass( 'active' );
			}		
			$scope.openSubTab = function openSubTab( id ) {
				$( '.tab-pane.sub-pane' ).removeClass( 'active' );
				$( '#experiment-' + id ).addClass( 'active' );
				$scope.fetchImages( 'client_upload' );
			}
			$scope.items = [];
	      	$scope.addNewTagInput = function addNewTagInput() {
				$scope.items.push({ 
		            sample: "foo"
		          });
	      	}
	      	$scope.deleteExperiment = function deleteExperiment (arr) {
	      		console.log( arr )
	      		if (confirm( 'Are you sure you want to delete this experiment?' )) {
	      			$http.post( serverUrl.url + 'deleteExperiment' , arr )
	      				.success( function success (response) {
	      					$( '#' + arr.predict_id ).fadeOut();
	      					console.log( response )
	      				})
	      		}
	      	}
	      	$scope.deleteUploadImage = function deleteUploadImage( arr ) {
	      		console.log( arr );
	      		if (confirm( 'Are you sure you want to delete this image?' )) {
	      			$http.post( serverUrl.url + 'deleteImageUpload' , arr )
	      				.success( function success (response) {
	      					// console.log( response )
	      					$( '#' + arr._id ).fadeOut();
	      					$scope.defaultPic = false;
	      				})
	      		}	      		
	      	}


		}
	])
	.controller('imageCtrl', [
		"$http",
		"$scope",
		"nodeUrl",
		"serverUrl",
		function controller( $http, $scope, nodeUrl, serverUrl ){

			$('.side-bar-menu').removeClass('active');
			$('.image-result-li').addClass('active');

	      // {image: "2.jpg", tags: "man", score: "0.97"},
	      // {image: "3.jpg", tags: "child", score: "0.96"},
	    // ];
	    $scope.newArray = [];
	    $scope.newObj = [];

			$scope.clientImageObject = [];
	    $scope.next_page = "";
	    $scope.server = window.location.origin + "/";
	    // console.log($scope.getArray);


	    $scope.getArray = function getArray( ) {
	      // return [{a: 1, b:2, c:3}, {a:3, b:4, c:5}];
	      $http.get( serverUrl.url + 'api/admin/report/clarifai')
	      .success(function(response){
	        console.log( response );

	        for(var x = 0; x < response.result.length; x++) {
	          for(var y = 0; y < response.result[ x ].scores.length; y++){
	            $scope.newArray.push({ a: response.result[ x ].image, b: response.result[ x ].tags[ y ], c: response.result[ x ].scores[ y ] });
	          }
	        }

	        console.log($scope.newArray);
	        return $scope.newArray;
	      });
	    }


	    // $scope.getArray();

	    $scope.download = function download( ) {
	      $http.get( serverUrl.url + 'api/admin/report/clarifai')
	      .success(function(response){
	        console.log(response);
	      });
	    }

	    $scope.fetchImages = function fetchImages( category ) {
	    	$http.get( serverUrl.url + 'api/uploads ')
	    		.success( function success( response ) {
	          console.log(response);
	          $scope.next_page = response.next_page_url;
	          for(var x = 0; x < response.data.length; x++) {
	            $scope.clientImageObject.push(response.data[x]);
	          }
	    		} );
	    }

	    $scope.next = function next(data) {
	      $('.loading-images').fadeIn(100);
	      $('.load-images-btn').addClass('loader-state-btn');
	      $('.load-images-btn').attr('disabled', true);
	      $http.get( data )
	      .success(function(response){
	        console.log(response);
	        $scope.next_page = response.next_page_url;
	         for(var x = 0; x < response.data.length; x++) {
	            $scope.clientImageObject.push(response.data[x]);
	          }
	         console.log($scope.clientImageObject);
	          $('.loading-images').fadeOut(100);
	          $('.load-images-btn').removeClass('loader-state-btn');
	          $('.load-images-btn').attr('disabled', false);
	          // console.log($scope.clientImageObject.length, response.total);
	          if($scope.clientImageObject.length == response.total) {
	            $('.load-images-btn').fadeOut();
	          }
	         
	      });
	    }

	    $scope.deleteImage = function deleteImage(id) {
	    	console.log(id);
	    	swal({   
	    		title: "Are you sure?",
	    		text: "You will not be able to recover this image file!",   
	    		type: "warning",   
	    		showCancelButton: true,   
	    		confirmButtonColor: "#DD6B55",   
	    		confirmButtonText: "Yes, delete it!",   
	    		closeOnConfirm: true }, 
	    		function(){

	    			$http.get( serverUrl.url + 'api/graymatic-clarifai/' + id._id.$id)
	    			.success(function(response) {
	    				swal("Deleted!", "Your image file has been deleted.", "success");
	    				console.log(response);
	            $http.get( nodeUrl.url + 'delete_image_admin/' + id.filename )
	            .success(function(response) {
	              $scope.clientImageObject.splice($scope.clientImageObject.indexOf(id), 1);
	              console.log(response);
	            })
	    			})
	    		});
	    	// body...
	    }

	   

			$scope.fetchImages('api_upload');
		}

	])
	.controller('passionController', [
		"$http",
		"$scope",
		"nodeUrl",
		"serverUrl",
		function controller( $http, $scope, nodeUrl, serverUrl ){

			$('.side-bar-menu').removeClass('active');
			$('.passion-points-li').addClass('active');

			$scope.showPassionInput = function(index){
				console.log(index);
				
				var data = $(".image-wrapper:nth-child("+index+") .passion-data").text();
				$(".image-wrapper:nth-child("+index+") .passion-data").toggle();
				$(".image-wrapper:nth-child("+index+") .passion-input").val(data);
				$(".image-wrapper:nth-child("+index+") .passion-input").toggle();
			}

			$scope.showInterestInput = function(index){
				console.log(index);
				
				var data = $(".image-wrapper:nth-child("+index+") .interest-data").text();
				$(".image-wrapper:nth-child("+index+") .interest-data").toggle();
				$(".image-wrapper:nth-child("+index+") .interest-input").val(data);
				$(".image-wrapper:nth-child("+index+") .interest-input").toggle();
			}


			// $scope.pieGraph = function pieGraph() {
	  //         var pie = new d3pie("pie-graph", {
	  
	  //           "size": {
	  //             "canvasHeight": 200,
	  //             "canvasWidth": 200,
	  //             "pieOuterRadius": "100%",
	  //             "pieInnerRadius": "75%",
	  //           },
	  //           "data": {
	  //             "content": [
	  //               {
	  //                 // "value": (num2*10),
	  //                 "value": 60,
	  //                 "label": "PP 1",
	  //                 "color": "#18B08D"
	                  
	  //               },
	  //               {
	  //                 // "value": (num1*10),
	  //                 "value": 40,
	  //                 "label": "PP 2",
	  //                 "color": "#CCD1D5"
	  //               },
	  //             ]
	  //           },
	  //           "labels": {
	  //             "outer": {
	  //               "pieDistance": 32,
	  //               "format": "none",
	  //             },
	  //             "inner": {
	  //                 "format": "none",
	  //                 "hideWhenLessThanPercentage": null
	  //             }
	  //           },
	  //           "effects": {
	  //             "pullOutSegmentOnClick": {
	  //               "effect": "none"
	  //             }
	  //           },
	  //           tooltips: {
	  //             enabled: true,
	  //             type: "placeholder",
	  //             string: "{label} : {value}"
	  //           }
	           

	  //         });
	  //       };

	        
	        // $scope.pieGraph();

		}
	])

	.controller('starhubController', [
		"$http",
		"$scope",
		"nodeUrl",
		"serverUrl",
		function controller( $http, $scope, nodeUrl, serverUrl ){
			$('.side-bar-menu').removeClass('active');
			$('.client-li').addClass('active');
		}
	]);