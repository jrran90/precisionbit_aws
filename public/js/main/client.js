var app = angular.module('client', ['ui.router','ngStorage', 'countrySelect', 'ngAnimate', 'angular-loading-bar', 'ordinal', 'ngMaterial', 'graph', 'upload', 'predict','angular.filter', 'page', 'user', 'brief', 'notification', 'image', 'report', 'd3', 'ngMap', 'chart.js', 'influencers_info', 'locations', 'vsGoogleAutocomplete', 'map'])

app.config(function( $stateProvider, $urlRouterProvider, $locationProvider, $compileProvider ){

  $compileProvider.debugInfoEnabled(false);

	$stateProvider
    .state('home', {
      url: '/',
      views: {
        'main': {
          templateUrl: 'templates/client/dashboard-v6/home.blade.php',
          controller: 'dashboardController'
        },
    }
  })
  .state('content-planning', {
    url: '/content-planning',
    views: {
        'main': {
          templateUrl: 'templates/client/dashboard-v6/content-planning.blade.php',
          controller: 'ContentPlanningCtrl'
        }
    }
  }) 
  .state('affinity-segmentation', {
    url: '/affinity-segmentation',
    views: {
        'main': {
          templateUrl: 'templates/client/dashboard-v6/affinity-segmentation.blade.php',
          controller: 'AffinityCtrl'
        }
    }
  }) 
  .state('brands', {
    url: '/brands',
    views: {
        'main': {
          templateUrl: 'templates/client/dashboard-v6/brands.blade.php',
          controller: 'BrandsCtrl'
        }
    }
  }) 

  .state('social_listening', {
    url: '/social_listening',
    views: {
        'main': {
          templateUrl: 'templates/client/dashboard-v6/social_listening.blade.php',
          controller: 'SocialCtrl'
        }
    }
  }) 

  .state('clarifai_output', {
    url: '/clarifai_output',
    views: {
        'main': {
          templateUrl: 'templates/client/dashboard-v6/display_output.blade.php',
          controller: 'clarifaiOutputCtrl'
        }
    }
  }) 

  .state('micro-influencer', {
    url: '/micro-influencer',
    views: {
        'main': {
          templateUrl: 'templates/client/dashboard-v6/micro-influencer.blade.php',
          controller: 'AffinityCtrl'
        }
    }
  }) 

  

  
  .state('summary', {
    url: '/summary',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'ReportSummaryCtrl'
        },
        'main': {
          templateUrl: 'templates/client/new/three.blade.php',
          controller: 'ReportSummaryCtrl'
        }
    }
  })  
  .state('report-telstra', {
    url: '/report-telstra',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'reportController'
        },
        'main': {
          templateUrl: 'templates/client/new/four.blade.php',
          controller: 'reportController'
        }
    }
  })
  .state('report-comp', {
    url: '/report-comp',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'reportController'
        },
        'main': {
          templateUrl: 'templates/client/new/five.blade.php',
          controller: 'reportController'
        }
    }
  })
  .state('predict', {
    url: '/predict',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/predict-ui-v4/predict.blade.php',
          controller: 'predictResultController'
        }
    }
  })
  .state('gallery', {
    url: '/gallery',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/new/gallery.blade.php',
          controller: 'galleryController'
        }
    }
  })
  .state('predict-result', {
    url: '/predict-result',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/predict-ui-v4/predict-results.blade.php',
          'controller': 'predictResultController'
        }
    }    
  })
  .state('predict-results-template', {
    url: '/predict-results-template',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/predict-ui-v4/predict-results-template.blade.php',
          'controller': 'predictResultController'
        }
    }    
  })
  .state('predict-results-template2', {
    url: '/predict-results-template2',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/predict-ui-v4/predict-results-template2.blade.php',
          'controller': 'predictResultController'
        }
    }    
  })
  .state('location', {
    url: '/location',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/dashboard-v6/location.blade.php',
          // templateUrl: 'templates/client/location.blade.php',
          'controller': 'locationController'
        }
    }    
  })
  .state('location-result', {
    url: '/location-result',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          // templateUrl: 'templates/client/location-result.blade.php',
          templateUrl: 'templates/client/dashboard-v6/location-result.blade.php',
          'controller': 'locationController'
        }
    }    
  })
  .state('explist', {
    url: '/explist',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/new/explist.blade.php'
        }
    }
  })
  .state('influencers', {
    url: '/influencers',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/new/influencers.blade.php',
          controller: 'influencersCtrl'
        }
    }
  })
  .state('audiences', {
    url: '/audiences',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php'
        },
        'main': {
          templateUrl: 'templates/client/audiences.blade.php',
          controller: 'influencersCtrl'
        }
    }
  })
  .state('influencer-info', {
    url: '/influencer-info/:id',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: ''
        },
        'main': {
          templateUrl: 'templates/client/influencer-info.blade.php',
          controller: 'influencersCtrl'
        }
    }
  })
  .state('upload', {
    url: '/upload',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'UploadCtrl'
        },
        'main': {
          templateUrl: 'templates/client/new/upload.blade.php',
          controller: 'UploadCtrl'
        }
    }
  })
  .state('saved-results', {
    url: '/saved-results',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'predictController'
        },
        'main': {
          templateUrl: 'templates/client/new/saved-results.blade.php',
          controller: 'predictController'
      }
    }
  })
  .state('location-result-page', {
    url: '/location-result-page',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'predictController'
        },
        'main': {
          templateUrl: 'templates/client/new/location-result-neuro.blade.php',
          controller: 'predictController'
      }
    }
  })
  .state('bulb', {
    url: '/bulb',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
        },
        'main': {
          templateUrl: 'templates/client/dashboard-v6/bulb.blade.php',
      }
    }
  })
  .state('contact-us', {
    url: '/contact-us',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'predictController'
        },
        'main': {
          templateUrl: 'templates/client/new/contact-us.blade.php',
          controller: 'predictController'
      }
    }
  })

  .state('faq', {
    url: '/faq',
    views: {
        'side-menu': {
          templateUrl: 'templates/client/side-menu.blade.php',
          controller: 'predictController'
        },
        'main': {
          templateUrl: 'templates/client/new/faq.blade.php',
          controller: 'predictController'
      }
    }
  });
  // .state('predict-result', {
  //   url: '/predict-result',
  //   templateUrl: 'templates/client/new/predict-data-result.blade.php',
  // });


  $urlRouterProvider.otherwise('/');
});