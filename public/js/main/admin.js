var app = angular.module('admin', ['ui.router','ngStorage', 'countrySelect', 'ngFileUpload', 'angular-loading-bar', 'ngMaterial', 'ngSanitize', 'ngCsv','d3', 'passion'])


app.config(function( $stateProvider, $urlRouterProvider, $locationProvider ){

    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/admin/dashboard.blade.php',
      controller: 'homeController'
    })
    .state('image-result', {
      url: '/image-result',
      templateUrl: 'templates/admin/image-result.blade.php',
      controller: 'imageCtrl'
    })
    .state('client', {
      url: '/client',
      templateUrl: 'templates/admin/client.blade.php'
    })
    .state('settings', {
      url: '/settings',
      templateUrl: 'templates/admin/settings.blade.php'
    })
    .state('client-telstra', {
      url: '/client-telstra',
      templateUrl: 'templates/admin/clients/client-telstra.blade.php',
      controller: 'clientController'
    })
    .state('image-result-immaga', {
      url: '/image-result-immaga',
      templateUrl: 'templates/admin/image-result-2.blade.php',
    })
    .state('admin-server-images', {
      url: '/admin-server-images',
      templateUrl: 'templates/admin/admin-images-server-list.blade.php',
    })
    .state('admin-server-images-cloudinary', {
      url: '/admin-server-images-cloudinary',
      templateUrl: 'templates/admin/cloudinary.blade.php',
    })
    .state('table-clarifai', {
      url: '/table-clarifai',
      templateUrl: 'templates/admin/table-clarifai.blade.php',
    })
    .state('clarifai', {
      url: '/clarifai',
      templateUrl: 'templates/admin/clarifai.blade.php',
    })
    .state('passion-points-tagging', {
      url: '/passion-points-tagging',
      templateUrl: 'templates/admin/passion-points-tagging.blade.php',
    })
    .state('passion-points-tagging-users', {
      url: '/passion-points-tagging-users',
      templateUrl: 'templates/admin/passion-points-tagging-users.blade.php',
    })
    .state('passion-points-tagging-users-tagging', {
      url: '/passion-points-tagging-users-tagging',
      templateUrl: 'templates/admin/passion-points-tagging-users-tagging.blade.php',
    })
    .state('passion-points-location-location', {
      url: '/passion-points-tagging/location/:location/:folder',
      templateUrl: 'templates/admin/passion-points-location.blade.php',
    })
    // .state('passion-points-user', {
    //   url: '/passion-points-tagging/location/user/:location/:user/:folder',
    //   templateUrl: 'templates/admin/passion-points-user.blade.php',
    // })

    .state('passion-points-user', {
      url: '/passion-points-tagging-users/:user',
      templateUrl: 'templates/admin/passion-points-user.blade.php',
    })

    .state('passion-points-summary', {
      url: '/passion-points-summary',
      templateUrl: 'templates/admin/passion-points-summary.blade.php',
    })

    .state('starhub-passion-points-tagging', {
      url: '/starhub-passion-points-tagging',
      templateUrl: 'templates/admin/starhub-passion-points-tagging.blade.php',
      controller: 'starhubController'
    })
    .state('starhub-passion-points-location-rod', {
      url: '/starhub-passion-points-tagging/rod',
      templateUrl: 'templates/admin/starhub-passion-points-rod.blade.php',
      controller: 'starhubController'
    })
    .state('starhub-passion-points-user', {
      url: '/starhub-passion-points-tagging/location/user/:location/:user/:folder',
      templateUrl: 'templates/admin/starhub-passion-points-user.blade.php',
      controller: 'starhubController'
    })
    .state('passion_images', {
      url: '/passion_images/:passion',
      templateUrl: 'templates/admin/passion_images.blade.php',
      controller: 'starhubController'
    })
    .state('advocates', {
      url: '/advocates',
      templateUrl: 'templates/admin/advocates.blade.php',
    })
    .state('graymatic', {
      url: '/graymatic',
      templateUrl: 'templates/admin/graymatic.blade.php',
    })
    .state('500-error', {
      url: '/500-error',
      templateUrl: 'templates/error/500-error.blade.php'
    })
    .state('user-database', {
      url: '/user-database/',
      templateUrl: 'templates/admin/user-database.blade.php',
    });
    // .state('upload', {
    //   url: '/upload',
    //   controller: 'uploadCtrl',
    //   templateUrl: 'templates/upload.blade.php',
    // });
    $urlRouterProvider.otherwise('/');
    // $locationProvider.html5Mode(true);
});
