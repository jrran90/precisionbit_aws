var app = angular.module('admin', ['ui.router','ngStorage', 'countrySelect', 'ngFileUpload', 'angular-loading-bar'])

app.config(function( $stateProvider, $urlRouterProvider, $locationProvider ){

    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'templates/admin/dashboard.blade.php',
      controller: 'dashboardController'
    })
    .state('500-error', {
      url: '/500-error',
      templateUrl: 'templates/error/500-error.blade.php',
    })
    .state('upload', {
      url: '/upload',
      controller: 'uploadCtrl',
      templateUrl: 'templates/upload.blade.php',
    });
    $urlRouterProvider.otherwise('/upload');
    // $locationProvider.html5Mode(true);
});

app.controller('userInfo', function( $scope, $http, $localStorage, serverUrl ){
  $scope.getUserInfo = function getUserInfo( ) {
    $http.get( serverUrl.url + 'sessionUserInfo')
    .success(function( response ){
      console.log( response );
      $localStorage.user = response;
      $scope.user = response;
    });
  }

  $scope.logout = function logout( ) {
    $localStorage.$reset();
  }

  $scope.getUserInfo();
});