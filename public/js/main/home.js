var app = angular.module('home', [ ]);

app.controller('emailController',[
  "$scope", 
  "$http",
  "serverUrl", 
  "nodeUrl",
  function( $scope, $http, serverUrl, nodeUrl ){
  console.log( 'running home' );
  $scope.mail = {};


  if(localStorage.emailTrigger == "true") {
    $(window).load(function(){
        $('.form-content').animatescroll();

        setTimeout(function() {
          localStorage.removeItem("emailTrigger");
        }, 500);
     });
  };

  $scope.sendEmail = function sendEmail( ) {
    console.log( $scope.mail );
    var btnElement = $( '#btn-submit' );

    btnElement.addClass( 'btn-disable' ).attr( 'disabled' );
    btnElement.find( 'strong' ).text( 'Sending...' );
    $http.post( serverUrl.url + 'sendEmail', $scope.mail ) 
      .success( function success ( response ) {
        console.log( response );
        if( response == 1)
        {
          sweetAlert("", "Your request was successfully sent!", "success");
          $scope.mail = {};
        } else {
          sweetAlert("Ooops!", "An error occured while sending your message!", "error");
        }
        btnElement.removeClass( 'btn-disable' );
        btnElement.find( 'strong' ).text( 'Submit' );
      } )
      .error( function( err ) {
        console.log( err );
        sweetAlert("", "Please provide a verified email address", "error");
        btnElement.removeClass( 'btn-disable' );
        btnElement.find( 'strong' ).text( 'Submit' );
      });
  };
}]);