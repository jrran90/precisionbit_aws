<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnCampaignNameCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaign', function ( $table ) {
            $table->string('campaign_name')->after( 'uniqid' );
            $table->string('target_age')->after( 'target_audience' );
            $table->string('target_language')->after( 'target_age' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaign', function( $table ) {
           $table->dropColumn('campaign_name');
        });
    }
}