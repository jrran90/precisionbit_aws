<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdSetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_set', function (Blueprint $table) {
            $table->increments('id');
            $table->string('campaign_id');
            $table->string('creative_uid');
            $table->string('client_uid');
            $table->string('ad_set_name');
            $table->integer('result_id');
            $table->integer('amount_spent');
            $table->string('cost');
            $table->string('reach');
            $table->string('relevance');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads_set');
    }
}
