<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDummyReportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * Fields are not really necessary to be put up here since
         * we're not using RDBMS
         */
        Schema::create('dummy_reports', function (Blueprint $table) {

            $table->increments('id');
            $table->float('unique_ctr');
            $table->unique('ad_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dummy_reports');
    }
}
