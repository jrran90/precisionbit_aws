<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$credentials = [
    		'firstname'	=> 'Precision',
    		'lastname'	=> 'Bit',
    		'email'		=> 'account@youradhero.com',
    		'password'	=> bcrypt('adheroamoy'),
    		'has_role'	=> 1
    	];

        \DB::collection('users')->insert($credentials);
    }
}
